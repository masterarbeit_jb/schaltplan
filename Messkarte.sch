<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="7.2.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.05" unitdist="inch" unit="inch" style="dots" multiple="1" display="yes" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tPadExt" color="7" fill="1" visible="no" active="no"/>
<layer number="54" name="bPadExt" color="1" fill="1" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="90" name="Modules" color="7" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="100" name="text" color="7" fill="1" visible="yes" active="yes"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="yes" active="yes"/>
<layer number="102" name="Mittellin" color="7" fill="1" visible="no" active="yes"/>
<layer number="104" name="Name" color="7" fill="1" visible="no" active="yes"/>
<layer number="106" name="segment" color="15" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="no" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="231" name="Eagle3D_PG1" color="14" fill="1" visible="yes" active="yes"/>
<layer number="232" name="Eagle3D_PG2" color="14" fill="2" visible="yes" active="yes"/>
<layer number="233" name="Eagle3D_PG3" color="14" fill="4" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="OrgLBR" color="7" fill="1" visible="no" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="rc-master-smd">
<packages>
<package name="L0402">
<description>&lt;B&gt;SMD INDUCTOR&lt;/B&gt; - 0402</description>
<wire x1="-0.245" y1="0.174" x2="0.245" y2="0.174" width="0.1016" layer="51"/>
<wire x1="0.245" y1="-0.174" x2="-0.245" y2="-0.174" width="0.1016" layer="51"/>
<wire x1="-1.1555" y1="0.483" x2="1.1555" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.1555" y1="0.483" x2="1.1555" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.1555" y1="-0.483" x2="-1.1555" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.1555" y1="-0.483" x2="-1.1555" y2="0.483" width="0.0508" layer="39"/>
<wire x1="-1.016" y1="0.508" x2="1.016" y2="0.508" width="0.2032" layer="21"/>
<wire x1="1.016" y1="0.508" x2="1.016" y2="-0.508" width="0.2032" layer="21"/>
<wire x1="1.016" y1="-0.508" x2="-1.016" y2="-0.508" width="0.2032" layer="21"/>
<wire x1="-1.016" y1="-0.508" x2="-1.016" y2="0.508" width="0.2032" layer="21"/>
<smd name="1" x="-0.508" y="0" dx="0.5" dy="0.5" layer="1"/>
<smd name="2" x="0.508" y="0" dx="0.5" dy="0.5" layer="1"/>
<text x="-1.016" y="0.762" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-1.016" y="-1.397" size="0.6096" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.5" y1="-0.25" x2="-0.254" y2="0.25" layer="51"/>
<rectangle x1="0.2588" y1="-0.25" x2="0.5" y2="0.25" layer="51"/>
<rectangle x1="-0.1" y1="-0.2" x2="0.1" y2="0.2" layer="35"/>
</package>
<package name="L0603">
<description>&lt;B&gt;SMD INDUCTOR&lt;/B&gt; - 0603</description>
<wire x1="-0.432" y1="-0.306" x2="0.432" y2="-0.306" width="0.1016" layer="51"/>
<wire x1="0.432" y1="0.306" x2="-0.432" y2="0.306" width="0.1016" layer="51"/>
<wire x1="-1.473" y1="0.6655" x2="1.473" y2="0.6655" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.6655" x2="1.473" y2="-0.6655" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.6655" x2="-1.473" y2="-0.6655" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.6655" x2="-1.473" y2="0.6655" width="0.0508" layer="39"/>
<wire x1="-1.397" y1="0.635" x2="1.397" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.397" y1="0.635" x2="1.397" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="1.397" y1="-0.635" x2="-1.397" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.397" y1="-0.635" x2="-1.397" y2="0.635" width="0.2032" layer="21"/>
<smd name="1" x="-0.762" y="0" dx="0.8" dy="0.8" layer="1"/>
<smd name="2" x="0.762" y="0" dx="0.8" dy="0.8" layer="1"/>
<text x="-1.524" y="1.016" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-1.524" y="-1.524" size="0.6096" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="0.4318" y1="-0.4" x2="0.8" y2="0.4" layer="51"/>
<rectangle x1="-0.8" y1="-0.4" x2="-0.4318" y2="0.4" layer="51"/>
<rectangle x1="-0.1999" y1="-0.25" x2="0.1999" y2="0.25" layer="35"/>
</package>
<package name="L0805">
<description>&lt;B&gt;SMD INDUCTOR&lt;/B&gt; - 0805</description>
<wire x1="-0.51" y1="0.535" x2="0.51" y2="0.535" width="0.1016" layer="51"/>
<wire x1="-0.51" y1="-0.535" x2="0.51" y2="-0.535" width="0.1016" layer="51"/>
<wire x1="-1.8143" y1="0.8243" x2="1.8143" y2="0.8243" width="0.0508" layer="39"/>
<wire x1="1.8143" y1="0.8243" x2="1.8143" y2="-0.8243" width="0.0508" layer="39"/>
<wire x1="1.8143" y1="-0.8243" x2="-1.8143" y2="-0.8243" width="0.0508" layer="39"/>
<wire x1="-1.8143" y1="-0.8243" x2="-1.8143" y2="0.8243" width="0.0508" layer="39"/>
<wire x1="-1.8542" y1="0.889" x2="1.8542" y2="0.889" width="0.2032" layer="21"/>
<wire x1="1.8542" y1="0.889" x2="1.8542" y2="-0.889" width="0.2032" layer="21"/>
<wire x1="1.8542" y1="-0.889" x2="-1.8542" y2="-0.889" width="0.2032" layer="21"/>
<wire x1="-1.8542" y1="-0.889" x2="-1.8542" y2="0.889" width="0.2032" layer="21"/>
<smd name="1" x="-1.016" y="0" dx="1.2" dy="1.3" layer="1"/>
<smd name="2" x="1.016" y="0" dx="1.2" dy="1.3" layer="1"/>
<text x="-1.778" y="1.27" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-1.778" y="-1.778" size="0.6096" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.65" x2="1" y2="0.65" layer="51"/>
<rectangle x1="-1" y1="-0.65" x2="-0.4168" y2="0.65" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
</package>
<package name="L1210">
<description>&lt;B&gt;SMD INDUCTOR&lt;/B&gt; - 1210</description>
<wire x1="-2.6317" y1="1.483" x2="2.6318" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.6318" y1="1.483" x2="2.6318" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.6318" y1="-1.483" x2="-2.6317" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.6317" y1="-1.483" x2="-2.6317" y2="1.483" width="0.0508" layer="39"/>
<wire x1="1.0525" y1="-1.1128" x2="-1.0652" y2="-1.1128" width="0.1016" layer="51"/>
<wire x1="1.0525" y1="1.1128" x2="-1.0652" y2="1.1128" width="0.1016" layer="51"/>
<wire x1="-2.413" y1="1.524" x2="2.413" y2="1.524" width="0.2032" layer="21"/>
<wire x1="2.413" y1="1.524" x2="2.413" y2="-1.524" width="0.2032" layer="21"/>
<wire x1="2.413" y1="-1.524" x2="-2.413" y2="-1.524" width="0.2032" layer="21"/>
<wire x1="-2.413" y1="-1.524" x2="-2.413" y2="1.524" width="0.2032" layer="21"/>
<smd name="2" x="1.524" y="0" dx="1.2" dy="2.5" layer="1"/>
<smd name="1" x="-1.524" y="0" dx="1.2" dy="2.5" layer="1"/>
<text x="-2.286" y="1.778" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-2.286" y="-2.286" size="0.6096" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.3" y1="-0.8999" x2="0.3" y2="0.8999" layer="35"/>
<rectangle x1="-1.6" y1="-1.2" x2="-0.9" y2="1.2" layer="51"/>
<rectangle x1="0.9001" y1="-1.2" x2="1.6" y2="1.2" layer="51" rot="R180"/>
</package>
<package name="PM0603">
<description>&lt;B&gt;SMD INDUCTOR&lt;/B&gt;&lt;p&gt; J.W. Miller, PM0603 Series</description>
<wire x1="-0.432" y1="-0.306" x2="0.432" y2="-0.306" width="0.1016" layer="51"/>
<wire x1="0.432" y1="0.306" x2="-0.432" y2="0.306" width="0.1016" layer="51"/>
<wire x1="-1.473" y1="0.6655" x2="1.473" y2="0.6655" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.6655" x2="1.473" y2="-0.6655" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.6655" x2="-1.473" y2="-0.6655" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.6655" x2="-1.473" y2="0.6655" width="0.0508" layer="39"/>
<wire x1="-1.397" y1="0.635" x2="1.397" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.397" y1="0.635" x2="1.397" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="1.397" y1="-0.635" x2="-1.397" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.397" y1="-0.635" x2="-1.397" y2="0.635" width="0.2032" layer="21"/>
<smd name="1" x="-0.762" y="0" dx="0.8" dy="0.8" layer="1"/>
<smd name="2" x="0.762" y="0" dx="0.8" dy="0.8" layer="1"/>
<text x="-1.524" y="1.016" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-1.524" y="-1.524" size="0.6096" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="0.4318" y1="-0.4" x2="0.8" y2="0.4" layer="51"/>
<rectangle x1="-0.8" y1="-0.4" x2="-0.4318" y2="0.4" layer="51"/>
<rectangle x1="-0.1999" y1="-0.25" x2="0.1999" y2="0.25" layer="35"/>
</package>
<package name="PM0805">
<description>&lt;B&gt;SMD INDUCTOR&lt;/B&gt;&lt;p&gt; J.W. Miller, PM0805 Series</description>
<wire x1="-0.51" y1="0.535" x2="0.51" y2="0.535" width="0.1016" layer="51"/>
<wire x1="-0.51" y1="-0.535" x2="0.51" y2="-0.535" width="0.1016" layer="51"/>
<wire x1="-1.8143" y1="0.8243" x2="1.8143" y2="0.8243" width="0.0508" layer="39"/>
<wire x1="1.8143" y1="0.8243" x2="1.8143" y2="-0.8243" width="0.0508" layer="39"/>
<wire x1="1.8143" y1="-0.8243" x2="-1.8143" y2="-0.8243" width="0.0508" layer="39"/>
<wire x1="-1.8143" y1="-0.8243" x2="-1.8143" y2="0.8243" width="0.0508" layer="39"/>
<wire x1="-1.8542" y1="0.889" x2="1.8542" y2="0.889" width="0.2032" layer="21"/>
<wire x1="1.8542" y1="0.889" x2="1.8542" y2="-0.889" width="0.2032" layer="21"/>
<wire x1="1.8542" y1="-0.889" x2="-1.8542" y2="-0.889" width="0.2032" layer="21"/>
<wire x1="-1.8542" y1="-0.889" x2="-1.8542" y2="0.889" width="0.2032" layer="21"/>
<smd name="1" x="-1.016" y="0" dx="1.2" dy="1.3" layer="1"/>
<smd name="2" x="1.016" y="0" dx="1.2" dy="1.3" layer="1"/>
<text x="-1.778" y="1.27" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-1.778" y="-1.778" size="0.6096" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.65" x2="1" y2="0.65" layer="51"/>
<rectangle x1="-1" y1="-0.65" x2="-0.4168" y2="0.65" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
</package>
<package name="PM1008">
<description>&lt;B&gt;SMD INDUCTOR&lt;/B&gt;&lt;p&gt; J.W. Miller, PM1008 Series</description>
<wire x1="-0.345" y1="1.124" x2="0.345" y2="1.124" width="0.2032" layer="21"/>
<wire x1="0.345" y1="-1.124" x2="-0.345" y2="-1.124" width="0.2032" layer="21"/>
<wire x1="-1.1555" y1="0.483" x2="1.1555" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.1555" y1="0.483" x2="1.1555" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.1555" y1="-0.483" x2="-1.1555" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.1555" y1="-0.483" x2="-1.1555" y2="0.483" width="0.0508" layer="39"/>
<wire x1="-0.745" y1="1.124" x2="-0.255" y2="1.124" width="0.2032" layer="51"/>
<wire x1="0.255" y1="1.124" x2="0.745" y2="1.124" width="0.2032" layer="51"/>
<wire x1="-0.255" y1="-1.124" x2="-0.745" y2="-1.124" width="0.2032" layer="51"/>
<wire x1="0.745" y1="-1.124" x2="0.255" y2="-1.124" width="0.2032" layer="51"/>
<wire x1="-2.05" y1="1.55" x2="2.05" y2="1.55" width="0.2032" layer="21"/>
<wire x1="2.05" y1="1.55" x2="2.05" y2="-1.55" width="0.2032" layer="21"/>
<wire x1="2.05" y1="-1.55" x2="-2.05" y2="-1.55" width="0.2032" layer="21"/>
<wire x1="-2.05" y1="-1.55" x2="-2.05" y2="1.55" width="0.2032" layer="21"/>
<smd name="1" x="-1.143" y="0" dx="1.2" dy="2.54" layer="1"/>
<smd name="2" x="1.143" y="0" dx="1.2" dy="2.54" layer="1"/>
<text x="-2.032" y="1.778" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-2.032" y="-2.286" size="0.6096" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.5" y1="-1.2" x2="-0.7" y2="1.2" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<rectangle x1="0.7" y1="-1.2" x2="1.5" y2="1.2" layer="51"/>
</package>
<package name="PM1210">
<description>&lt;B&gt;SMD INDUCTOR&lt;/B&gt;&lt;p&gt; J.W. Miller,  PM1210 Series</description>
<wire x1="-1.75" y1="-1.25" x2="1.75" y2="-1.25" width="0.2032" layer="21"/>
<wire x1="1.75" y1="-1.25" x2="1.75" y2="1.25" width="0.2032" layer="51"/>
<wire x1="1.75" y1="1.25" x2="-1.75" y2="1.25" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="1.25" x2="-1.75" y2="-1.25" width="0.2032" layer="51"/>
<smd name="1" x="-1.524" y="0" dx="2" dy="1.7" layer="1"/>
<smd name="2" x="1.524" y="0" dx="2" dy="1.7" layer="1"/>
<text x="-1.778" y="1.524" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-1.778" y="-2.032" size="0.6096" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.0638" y1="-0.635" x2="-1.7463" y2="0.635" layer="51"/>
<rectangle x1="1.7464" y1="-0.635" x2="2.0638" y2="0.635" layer="51"/>
</package>
<package name="PM1812">
<description>&lt;B&gt;SMD INDUCTOR&lt;/B&gt;&lt;p&gt;J.W. Miller,  PM1812 Series</description>
<wire x1="-2.25" y1="-1.75" x2="2.25" y2="-1.75" width="0.2032" layer="21"/>
<wire x1="2.25" y1="-1.75" x2="2.25" y2="1.75" width="0.2032" layer="51"/>
<wire x1="2.25" y1="1.75" x2="-2.25" y2="1.75" width="0.2032" layer="21"/>
<wire x1="-2.25" y1="1.75" x2="-2.25" y2="-1.75" width="0.2032" layer="51"/>
<smd name="1" x="-2.032" y="0" dx="2" dy="1.7" layer="1"/>
<smd name="2" x="2.032" y="0" dx="2" dy="1.7" layer="1"/>
<text x="-2.286" y="2.032" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-2.286" y="-2.667" size="0.6096" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.54" y1="-0.635" x2="-2.2225" y2="0.635" layer="51"/>
<rectangle x1="2.2226" y1="-0.635" x2="2.54" y2="0.635" layer="51"/>
</package>
<package name="L1206">
<description>&lt;B&gt;SMD INDUCTOR&lt;/B&gt; - 1206 (3216)</description>
<wire x1="1.0525" y1="-0.7128" x2="-1.0652" y2="-0.7128" width="0.1016" layer="51"/>
<wire x1="1.0525" y1="0.7128" x2="-1.0652" y2="0.7128" width="0.1016" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.4731" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.4731" y1="0.983" x2="2.4731" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.4731" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-2.4892" y1="1.143" x2="2.4892" y2="1.143" width="0.2032" layer="21"/>
<wire x1="2.4892" y1="1.143" x2="2.4892" y2="-1.143" width="0.2032" layer="21"/>
<wire x1="2.4892" y1="-1.143" x2="-2.4892" y2="-1.143" width="0.2032" layer="21"/>
<wire x1="-2.4892" y1="-1.143" x2="-2.4892" y2="1.143" width="0.2032" layer="21"/>
<smd name="2" x="1.524" y="0" dx="1.3" dy="1.6" layer="1"/>
<smd name="1" x="-1.524" y="0" dx="1.3" dy="1.6" layer="1"/>
<text x="-2.286" y="1.524" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-2.286" y="-2.032" size="0.6096" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.6" y1="-0.8" x2="-0.9" y2="0.8" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
<rectangle x1="0.9001" y1="-0.8" x2="1.6" y2="0.8" layer="51" rot="R180"/>
</package>
<package name="L5650M">
<description>&lt;b&gt;INDUCTOR&lt;/b&gt;&lt;p&gt;
molded</description>
<wire x1="-3.973" y1="2.983" x2="3.973" y2="2.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-2.983" x2="-3.973" y2="-2.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-2.983" x2="-3.973" y2="2.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="2.983" x2="3.973" y2="-2.983" width="0.0508" layer="39"/>
<wire x1="-2.108" y1="-2.591" x2="2.083" y2="-2.591" width="0.1524" layer="51"/>
<wire x1="2.083" y1="2.591" x2="-2.108" y2="2.591" width="0.1524" layer="51"/>
<wire x1="2.184" y1="2.032" x2="2.184" y2="-2.032" width="0.1524" layer="51"/>
<wire x1="-2.21" y1="2.032" x2="-2.21" y2="-2.032" width="0.1524" layer="51"/>
<wire x1="-2.108" y1="2.591" x2="-2.108" y2="-2.591" width="0.1524" layer="51"/>
<wire x1="2.083" y1="2.591" x2="2.083" y2="-2.591" width="0.1524" layer="51"/>
<smd name="1" x="-2.3" y="0" dx="1.8" dy="4" layer="1"/>
<smd name="2" x="2.3" y="0" dx="1.8" dy="4" layer="1"/>
<text x="-1.905" y="3.175" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-4.445" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
</packages>
<symbols>
<symbol name="L-">
<wire x1="-2.54" y1="0.3175" x2="-1.27" y2="0.3175" width="0.254" layer="94" curve="-180"/>
<wire x1="-1.27" y1="0.3175" x2="0" y2="0.3175" width="0.254" layer="94" curve="-180"/>
<wire x1="0" y1="0.3175" x2="1.27" y2="0.3175" width="0.254" layer="94" curve="-180"/>
<wire x1="1.27" y1="0.3175" x2="2.54" y2="0.3175" width="0.254" layer="94" curve="-180"/>
<wire x1="-2.54" y1="0.3175" x2="-2.54" y2="0" width="0.254" layer="94"/>
<wire x1="2.54" y1="0.3175" x2="2.54" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="1.905" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="1" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="L_" prefix="L">
<description>INDUCTOR</description>
<gates>
<gate name="G$1" symbol="L-" x="0" y="0"/>
</gates>
<devices>
<device name="0402" package="L0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="L0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="L0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1210" package="L1210">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PM0603" package="PM0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PM0805" package="PM0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PM1008" package="PM1008">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PM1210" package="PM1210">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PM1812" package="PM1812">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="L1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="" package="L5650M">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1">
<packages>
</packages>
<symbols>
<symbol name="AGND">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<wire x1="-1.0922" y1="-0.508" x2="1.0922" y2="-0.508" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="AGND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="-3V3">
<gates>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="AGND" prefix="AGND">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="VR1" symbol="AGND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="rcl">
<packages>
<package name="R0402">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="R0603">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.432" y1="-0.356" x2="0.432" y2="-0.356" width="0.1524" layer="51"/>
<wire x1="0.432" y1="0.356" x2="-0.432" y2="0.356" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.85" y="0" dx="1" dy="1.1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1" dy="1.1" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4318" y1="-0.4318" x2="0.8382" y2="0.4318" layer="51"/>
<rectangle x1="-0.8382" y1="-0.4318" x2="-0.4318" y2="0.4318" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="R0805">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
</package>
<package name="R0805W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt; wave soldering&lt;p&gt;</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.0525" y="0" dx="1.5" dy="1" layer="1"/>
<smd name="2" x="1.0525" y="0" dx="1.5" dy="1" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
</package>
<package name="R1005">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="R1206">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="0.9525" y1="-0.8128" x2="-0.9652" y2="-0.8128" width="0.1524" layer="51"/>
<wire x1="0.9525" y1="0.8128" x2="-0.9652" y2="0.8128" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="2" x="1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<smd name="1" x="-1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6891" y1="-0.8763" x2="-0.9525" y2="0.8763" layer="51"/>
<rectangle x1="0.9525" y1="-0.8763" x2="1.6891" y2="0.8763" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R1206W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-0.913" y1="0.8" x2="0.888" y2="0.8" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-0.8" x2="0.888" y2="-0.8" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.499" y="0" dx="1.8" dy="1.2" layer="1"/>
<smd name="2" x="1.499" y="0" dx="1.8" dy="1.2" layer="1"/>
<text x="-1.905" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-0.8763" x2="-0.9009" y2="0.8738" layer="51"/>
<rectangle x1="0.889" y1="-0.8763" x2="1.6391" y2="0.8738" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R1210">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-0.8999" x2="0.3" y2="0.8999" layer="35"/>
</package>
<package name="R1210W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.499" y="0" dx="1.8" dy="1.8" layer="1"/>
<smd name="2" x="1.499" y="0" dx="1.8" dy="1.8" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-0.8001" x2="0.3" y2="0.8001" layer="35"/>
</package>
<package name="R2010">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<smd name="2" x="2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<text x="-3.175" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.175" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
</package>
<package name="R2010W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-2.311" y="0" dx="2" dy="1.8" layer="1"/>
<smd name="2" x="2.311" y="0" dx="2" dy="1.8" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
</package>
<package name="R2012">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1001" y1="-0.5999" x2="0.1001" y2="0.5999" layer="35"/>
</package>
<package name="R2012W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.94" y="0" dx="1.5" dy="1" layer="1"/>
<smd name="2" x="0.94" y="0" dx="1.5" dy="1" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1001" y1="-0.5999" x2="0.1001" y2="0.5999" layer="35"/>
</package>
<package name="R2512">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<smd name="1" x="-2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<smd name="2" x="2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R2512W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<smd name="1" x="-2.896" y="0" dx="2" dy="2.1" layer="1"/>
<smd name="2" x="2.896" y="0" dx="2" dy="2.1" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R3216">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.913" y1="0.8" x2="0.888" y2="0.8" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-0.8" x2="0.888" y2="-0.8" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.905" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-0.8763" x2="-0.9009" y2="0.8738" layer="51"/>
<rectangle x1="0.889" y1="-0.8763" x2="1.6391" y2="0.8738" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R3216W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-0.913" y1="0.8" x2="0.888" y2="0.8" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-0.8" x2="0.888" y2="-0.8" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.499" y="0" dx="1.8" dy="1.2" layer="1"/>
<smd name="2" x="1.499" y="0" dx="1.8" dy="1.2" layer="1"/>
<text x="-1.905" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-0.8763" x2="-0.9009" y2="0.8738" layer="51"/>
<rectangle x1="0.889" y1="-0.8763" x2="1.6391" y2="0.8738" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R3225">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-1" x2="0.3" y2="1" layer="35"/>
</package>
<package name="R3225W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.499" y="0" dx="1.8" dy="1.8" layer="1"/>
<smd name="2" x="1.499" y="0" dx="1.8" dy="1.8" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-1" x2="0.3" y2="1" layer="35"/>
</package>
<package name="R5025">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<smd name="2" x="2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<text x="-3.175" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.175" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R5025W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-2.311" y="0" dx="2" dy="1.8" layer="1"/>
<smd name="2" x="2.311" y="0" dx="2" dy="1.8" layer="1"/>
<text x="-3.175" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.175" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R6332">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
Source: http://download.siliconexpert.com/pdfs/2005/02/24/Semi_Ap/2/VSH/Resistor/dcrcwfre.pdf</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<smd name="1" x="-3.1" y="0" dx="1" dy="3.2" layer="1"/>
<smd name="2" x="3.1" y="0" dx="1" dy="3.2" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R6332W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt; wave soldering&lt;p&gt;
Source: http://download.siliconexpert.com/pdfs/2005/02/24/Semi_Ap/2/VSH/Resistor/dcrcwfre.pdf</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<smd name="1" x="-3.196" y="0" dx="1.2" dy="3.2" layer="1"/>
<smd name="2" x="3.196" y="0" dx="1.2" dy="3.2" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="M0805">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.10 W</description>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="0.7112" y1="0.635" x2="-0.7112" y2="0.635" width="0.1524" layer="51"/>
<wire x1="0.7112" y1="-0.635" x2="-0.7112" y2="-0.635" width="0.1524" layer="51"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.6" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.6" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.0414" y1="-0.7112" x2="-0.6858" y2="0.7112" layer="51"/>
<rectangle x1="0.6858" y1="-0.7112" x2="1.0414" y2="0.7112" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5999" x2="0.1999" y2="0.5999" layer="35"/>
</package>
<package name="M1206">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.25 W</description>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="1.143" y1="0.8382" x2="-1.143" y2="0.8382" width="0.1524" layer="51"/>
<wire x1="1.143" y1="-0.8382" x2="-1.143" y2="-0.8382" width="0.1524" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.9144" x2="-1.1176" y2="0.9144" layer="51"/>
<rectangle x1="1.1176" y1="-0.9144" x2="1.7018" y2="0.9144" layer="51"/>
<rectangle x1="-0.3" y1="-0.8001" x2="0.3" y2="0.8001" layer="35"/>
</package>
<package name="M1406">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.12 W</description>
<wire x1="-2.973" y1="0.983" x2="2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-0.983" x2="-2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-0.983" x2="-2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="0.983" x2="2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.3208" y1="0.762" x2="-1.3208" y2="0.762" width="0.1524" layer="51"/>
<wire x1="1.3208" y1="-0.762" x2="-1.3208" y2="-0.762" width="0.1524" layer="51"/>
<smd name="1" x="-1.7" y="0" dx="1.4" dy="1.8" layer="1"/>
<smd name="2" x="1.7" y="0" dx="1.4" dy="1.8" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.8542" y1="-0.8382" x2="-1.2954" y2="0.8382" layer="51"/>
<rectangle x1="1.2954" y1="-0.8382" x2="1.8542" y2="0.8382" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="M2012">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.10 W</description>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="0.7112" y1="0.635" x2="-0.7112" y2="0.635" width="0.1524" layer="51"/>
<wire x1="0.7112" y1="-0.635" x2="-0.7112" y2="-0.635" width="0.1524" layer="51"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.6" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.6" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.0414" y1="-0.7112" x2="-0.6858" y2="0.7112" layer="51"/>
<rectangle x1="0.6858" y1="-0.7112" x2="1.0414" y2="0.7112" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5999" x2="0.1999" y2="0.5999" layer="35"/>
</package>
<package name="M2309">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.25 W</description>
<wire x1="-4.473" y1="1.483" x2="4.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="4.473" y1="-1.483" x2="-4.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-4.473" y1="-1.483" x2="-4.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="4.473" y1="1.483" x2="4.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.413" y1="1.1684" x2="-2.4384" y2="1.1684" width="0.1524" layer="51"/>
<wire x1="2.413" y1="-1.1684" x2="-2.413" y2="-1.1684" width="0.1524" layer="51"/>
<smd name="1" x="-2.85" y="0" dx="1.5" dy="2.6" layer="1"/>
<smd name="2" x="2.85" y="0" dx="1.5" dy="2.6" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.048" y1="-1.2446" x2="-2.3876" y2="1.2446" layer="51"/>
<rectangle x1="2.3876" y1="-1.2446" x2="3.048" y2="1.2446" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="M3216">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.25 W</description>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="1.143" y1="0.8382" x2="-1.143" y2="0.8382" width="0.1524" layer="51"/>
<wire x1="1.143" y1="-0.8382" x2="-1.143" y2="-0.8382" width="0.1524" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.9144" x2="-1.1176" y2="0.9144" layer="51"/>
<rectangle x1="1.1176" y1="-0.9144" x2="1.7018" y2="0.9144" layer="51"/>
<rectangle x1="-0.3" y1="-0.8001" x2="0.3" y2="0.8001" layer="35"/>
</package>
<package name="M3516">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.12 W</description>
<wire x1="-2.973" y1="0.983" x2="2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-0.983" x2="-2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-0.983" x2="-2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="0.983" x2="2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.3208" y1="0.762" x2="-1.3208" y2="0.762" width="0.1524" layer="51"/>
<wire x1="1.3208" y1="-0.762" x2="-1.3208" y2="-0.762" width="0.1524" layer="51"/>
<smd name="1" x="-1.7" y="0" dx="1.4" dy="1.8" layer="1"/>
<smd name="2" x="1.7" y="0" dx="1.4" dy="1.8" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.8542" y1="-0.8382" x2="-1.2954" y2="0.8382" layer="51"/>
<rectangle x1="1.2954" y1="-0.8382" x2="1.8542" y2="0.8382" layer="51"/>
<rectangle x1="-0.4001" y1="-0.7" x2="0.4001" y2="0.7" layer="35"/>
</package>
<package name="M5923">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.25 W</description>
<wire x1="-4.473" y1="1.483" x2="4.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="4.473" y1="-1.483" x2="-4.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-4.473" y1="-1.483" x2="-4.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="4.473" y1="1.483" x2="4.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.413" y1="1.1684" x2="-2.4384" y2="1.1684" width="0.1524" layer="51"/>
<wire x1="2.413" y1="-1.1684" x2="-2.413" y2="-1.1684" width="0.1524" layer="51"/>
<smd name="1" x="-2.85" y="0" dx="1.5" dy="2.6" layer="1"/>
<smd name="2" x="2.85" y="0" dx="1.5" dy="2.6" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.048" y1="-1.2446" x2="-2.3876" y2="1.2446" layer="51"/>
<rectangle x1="2.3876" y1="-1.2446" x2="3.048" y2="1.2446" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="0204/5">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0204, grid 5 mm</description>
<wire x1="2.54" y1="0" x2="2.032" y2="0" width="0.508" layer="51"/>
<wire x1="-2.54" y1="0" x2="-2.032" y2="0" width="0.508" layer="51"/>
<wire x1="-1.778" y1="0.635" x2="-1.524" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.778" y1="-0.635" x2="-1.524" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="1.524" y1="-0.889" x2="1.778" y2="-0.635" width="0.1524" layer="21" curve="90"/>
<wire x1="1.524" y1="0.889" x2="1.778" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.778" y1="-0.635" x2="-1.778" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-1.524" y1="0.889" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="0.762" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="-0.889" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="-0.762" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="0.762" x2="1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="0.762" x2="-1.143" y2="0.762" width="0.1524" layer="21"/>
<wire x1="1.143" y1="-0.762" x2="1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="-0.762" x2="-1.143" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="1.524" y1="0.889" x2="1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.524" y1="-0.889" x2="1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-0.635" x2="1.778" y2="0.635" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.0066" y="1.1684" size="0.9906" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.1336" y="-2.3114" size="0.9906" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.032" y1="-0.254" x2="-1.778" y2="0.254" layer="51"/>
<rectangle x1="1.778" y1="-0.254" x2="2.032" y2="0.254" layer="51"/>
</package>
<package name="0204/7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0204, grid 7.5 mm</description>
<wire x1="3.81" y1="0" x2="2.921" y2="0" width="0.508" layer="51"/>
<wire x1="-3.81" y1="0" x2="-2.921" y2="0" width="0.508" layer="51"/>
<wire x1="-2.54" y1="0.762" x2="-2.286" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.54" y1="-0.762" x2="-2.286" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="-0.762" width="0.1524" layer="21" curve="90"/>
<wire x1="2.286" y1="1.016" x2="2.54" y2="0.762" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.54" y1="-0.762" x2="-2.54" y2="0.762" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="1.016" x2="-1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-1.778" y1="0.889" x2="-1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="-1.016" x2="-1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-1.778" y1="-0.889" x2="-1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0.889" x2="1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0.889" x2="-1.778" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-0.889" x2="1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-0.889" x2="-1.778" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="2.286" y1="1.016" x2="1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.286" y1="-1.016" x2="1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-0.762" x2="2.54" y2="0.762" width="0.1524" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.54" y="1.2954" size="0.9906" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.6256" y="-0.4826" size="0.9906" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="2.54" y1="-0.254" x2="2.921" y2="0.254" layer="21"/>
<rectangle x1="-2.921" y1="-0.254" x2="-2.54" y2="0.254" layer="21"/>
</package>
<package name="0204V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0204, grid 2.5 mm</description>
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.508" layer="51"/>
<wire x1="-0.127" y1="0" x2="0.127" y2="0" width="0.508" layer="21"/>
<circle x="-1.27" y="0" radius="0.889" width="0.1524" layer="51"/>
<circle x="-1.27" y="0" radius="0.635" width="0.0508" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.1336" y="1.1684" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.1336" y="-2.3114" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="0207/10">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 10 mm</description>
<wire x1="5.08" y1="0" x2="4.064" y2="0" width="0.6096" layer="51"/>
<wire x1="-5.08" y1="0" x2="-4.064" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="21"/>
<pad name="1" x="-5.08" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.048" y="1.524" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.2606" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
</package>
<package name="0207/12">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 12 mm</description>
<wire x1="6.35" y1="0" x2="5.334" y2="0" width="0.6096" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.334" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="4.445" y1="0" x2="4.064" y2="0" width="0.6096" layer="21"/>
<wire x1="-4.445" y1="0" x2="-4.064" y2="0" width="0.6096" layer="21"/>
<pad name="1" x="-6.35" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.175" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-0.6858" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
<rectangle x1="4.445" y1="-0.3048" x2="5.3086" y2="0.3048" layer="21"/>
<rectangle x1="-5.3086" y1="-0.3048" x2="-4.445" y2="0.3048" layer="21"/>
</package>
<package name="0207/15">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 15mm</description>
<wire x1="7.62" y1="0" x2="6.604" y2="0" width="0.6096" layer="51"/>
<wire x1="-7.62" y1="0" x2="-6.604" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="5.715" y1="0" x2="4.064" y2="0" width="0.6096" layer="21"/>
<wire x1="-5.715" y1="0" x2="-4.064" y2="0" width="0.6096" layer="21"/>
<pad name="1" x="-7.62" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.175" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-0.6858" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
<rectangle x1="5.715" y1="-0.3048" x2="6.5786" y2="0.3048" layer="21"/>
<rectangle x1="-6.5786" y1="-0.3048" x2="-5.715" y2="0.3048" layer="21"/>
</package>
<package name="0207/2V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 2.5 mm</description>
<wire x1="-1.27" y1="0" x2="-0.381" y2="0" width="0.6096" layer="51"/>
<wire x1="-0.254" y1="0" x2="0.254" y2="0" width="0.6096" layer="21"/>
<wire x1="0.381" y1="0" x2="1.27" y2="0" width="0.6096" layer="51"/>
<circle x="-1.27" y="0" radius="1.27" width="0.1524" layer="21"/>
<circle x="-1.27" y="0" radius="1.016" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-0.0508" y="1.016" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.0508" y="-2.2352" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="0207/5V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 5 mm</description>
<wire x1="-2.54" y1="0" x2="-0.889" y2="0" width="0.6096" layer="51"/>
<wire x1="-0.762" y1="0" x2="0.762" y2="0" width="0.6096" layer="21"/>
<wire x1="0.889" y1="0" x2="2.54" y2="0" width="0.6096" layer="51"/>
<circle x="-2.54" y="0" radius="1.27" width="0.1016" layer="21"/>
<circle x="-2.54" y="0" radius="1.016" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-1.143" y="0.889" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.143" y="-2.159" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="0207/7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 7.5 mm</description>
<wire x1="-3.81" y1="0" x2="-3.429" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="51"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="51"/>
<wire x1="3.429" y1="0" x2="3.81" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.54" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-0.5588" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-3.429" y1="-0.3048" x2="-3.175" y2="0.3048" layer="51"/>
<rectangle x1="3.175" y1="-0.3048" x2="3.429" y2="0.3048" layer="51"/>
</package>
<package name="0309/10">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0309, grid 10mm</description>
<wire x1="-4.699" y1="0" x2="-5.08" y2="0" width="0.6096" layer="51"/>
<wire x1="-4.318" y1="1.27" x2="-4.064" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.064" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="-1.524" x2="4.318" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="1.524" x2="4.318" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.318" y2="1.27" width="0.1524" layer="51"/>
<wire x1="-4.064" y1="1.524" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="1.397" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-4.064" y1="-1.524" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="-1.397" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="-3.302" y2="1.397" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="-3.302" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="4.064" y1="1.524" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="4.064" y1="-1.524" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="4.318" y1="-1.27" x2="4.318" y2="1.27" width="0.1524" layer="51"/>
<wire x1="5.08" y1="0" x2="4.699" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-5.08" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="0.8128" shape="octagon"/>
<text x="-4.191" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.6858" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-4.6228" y1="-0.3048" x2="-4.318" y2="0.3048" layer="51"/>
<rectangle x1="4.318" y1="-0.3048" x2="4.6228" y2="0.3048" layer="51"/>
</package>
<package name="0309/12">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0309, grid 12.5 mm</description>
<wire x1="6.35" y1="0" x2="5.08" y2="0" width="0.6096" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.08" y2="0" width="0.6096" layer="51"/>
<wire x1="-4.318" y1="1.27" x2="-4.064" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.064" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="-1.524" x2="4.318" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="1.524" x2="4.318" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.318" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-4.064" y1="1.524" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="1.397" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-4.064" y1="-1.524" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="-1.397" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="-3.302" y2="1.397" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="-3.302" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="4.064" y1="1.524" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="4.064" y1="-1.524" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="4.318" y1="-1.27" x2="4.318" y2="1.27" width="0.1524" layer="21"/>
<pad name="1" x="-6.35" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="0.8128" shape="octagon"/>
<text x="-4.191" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.6858" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="4.318" y1="-0.3048" x2="5.1816" y2="0.3048" layer="21"/>
<rectangle x1="-5.1816" y1="-0.3048" x2="-4.318" y2="0.3048" layer="21"/>
</package>
<package name="0309V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0309, grid 2.5 mm</description>
<wire x1="1.27" y1="0" x2="0.635" y2="0" width="0.6096" layer="51"/>
<wire x1="-0.635" y1="0" x2="-1.27" y2="0" width="0.6096" layer="51"/>
<circle x="-1.27" y="0" radius="1.524" width="0.1524" layer="21"/>
<circle x="-1.27" y="0" radius="0.762" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="0.254" y="1.016" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0.254" y="-2.2098" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="0.254" y1="-0.3048" x2="0.5588" y2="0.3048" layer="51"/>
<rectangle x1="-0.635" y1="-0.3048" x2="-0.3302" y2="0.3048" layer="51"/>
<rectangle x1="-0.3302" y1="-0.3048" x2="0.254" y2="0.3048" layer="21"/>
</package>
<package name="0411/12">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0411, grid 12.5 mm</description>
<wire x1="6.35" y1="0" x2="5.461" y2="0" width="0.762" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.461" y2="0" width="0.762" layer="51"/>
<wire x1="5.08" y1="-1.651" x2="5.08" y2="1.651" width="0.1524" layer="21"/>
<wire x1="4.699" y1="2.032" x2="5.08" y2="1.651" width="0.1524" layer="21" curve="-90"/>
<wire x1="-5.08" y1="-1.651" x2="-4.699" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="4.699" y1="-2.032" x2="5.08" y2="-1.651" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="1.651" x2="-4.699" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="2.032" x2="4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="3.937" y1="1.905" x2="4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-2.032" x2="4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="3.937" y1="-1.905" x2="4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="1.905" x2="-4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="1.905" x2="3.937" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="-1.905" x2="-4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="-1.905" x2="3.937" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.651" x2="-5.08" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="2.032" x2="-4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="-2.032" x2="-4.064" y2="-2.032" width="0.1524" layer="21"/>
<pad name="1" x="-6.35" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="0.9144" shape="octagon"/>
<text x="-5.08" y="2.413" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.5814" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-5.3594" y1="-0.381" x2="-5.08" y2="0.381" layer="21"/>
<rectangle x1="5.08" y1="-0.381" x2="5.3594" y2="0.381" layer="21"/>
</package>
<package name="0411/15">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0411, grid 15 mm</description>
<wire x1="5.08" y1="-1.651" x2="5.08" y2="1.651" width="0.1524" layer="21"/>
<wire x1="4.699" y1="2.032" x2="5.08" y2="1.651" width="0.1524" layer="21" curve="-90"/>
<wire x1="-5.08" y1="-1.651" x2="-4.699" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="4.699" y1="-2.032" x2="5.08" y2="-1.651" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="1.651" x2="-4.699" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="2.032" x2="4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="3.937" y1="1.905" x2="4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-2.032" x2="4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="3.937" y1="-1.905" x2="4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="1.905" x2="-4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="1.905" x2="3.937" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="-1.905" x2="-4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="-1.905" x2="3.937" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.651" x2="-5.08" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="2.032" x2="-4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="-2.032" x2="-4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="0" x2="-6.35" y2="0" width="0.762" layer="51"/>
<wire x1="6.35" y1="0" x2="7.62" y2="0" width="0.762" layer="51"/>
<pad name="1" x="-7.62" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="0.9144" shape="octagon"/>
<text x="-5.08" y="2.413" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.5814" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="5.08" y1="-0.381" x2="6.477" y2="0.381" layer="21"/>
<rectangle x1="-6.477" y1="-0.381" x2="-5.08" y2="0.381" layer="21"/>
</package>
<package name="0411V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0411, grid 3.81 mm</description>
<wire x1="1.27" y1="0" x2="0.3048" y2="0" width="0.762" layer="51"/>
<wire x1="-1.5748" y1="0" x2="-2.54" y2="0" width="0.762" layer="51"/>
<circle x="-2.54" y="0" radius="2.032" width="0.1524" layer="21"/>
<circle x="-2.54" y="0" radius="1.016" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.9144" shape="octagon"/>
<text x="-0.508" y="1.143" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.5334" y="-2.413" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.4732" y1="-0.381" x2="0.2032" y2="0.381" layer="21"/>
</package>
<package name="0414/15">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0414, grid 15 mm</description>
<wire x1="7.62" y1="0" x2="6.604" y2="0" width="0.8128" layer="51"/>
<wire x1="-7.62" y1="0" x2="-6.604" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.096" y1="1.905" x2="-5.842" y2="2.159" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.096" y1="-1.905" x2="-5.842" y2="-2.159" width="0.1524" layer="21" curve="90"/>
<wire x1="5.842" y1="-2.159" x2="6.096" y2="-1.905" width="0.1524" layer="21" curve="90"/>
<wire x1="5.842" y1="2.159" x2="6.096" y2="1.905" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.096" y1="-1.905" x2="-6.096" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-5.842" y1="2.159" x2="-4.953" y2="2.159" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="2.032" x2="-4.953" y2="2.159" width="0.1524" layer="21"/>
<wire x1="-5.842" y1="-2.159" x2="-4.953" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="-2.032" x2="-4.953" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="4.826" y1="2.032" x2="4.953" y2="2.159" width="0.1524" layer="21"/>
<wire x1="4.826" y1="2.032" x2="-4.826" y2="2.032" width="0.1524" layer="21"/>
<wire x1="4.826" y1="-2.032" x2="4.953" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="4.826" y1="-2.032" x2="-4.826" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="5.842" y1="2.159" x2="4.953" y2="2.159" width="0.1524" layer="21"/>
<wire x1="5.842" y1="-2.159" x2="4.953" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-1.905" x2="6.096" y2="1.905" width="0.1524" layer="21"/>
<pad name="1" x="-7.62" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="1.016" shape="octagon"/>
<text x="-6.096" y="2.5654" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.318" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="6.096" y1="-0.4064" x2="6.5024" y2="0.4064" layer="21"/>
<rectangle x1="-6.5024" y1="-0.4064" x2="-6.096" y2="0.4064" layer="21"/>
</package>
<package name="0414V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0414, grid 5 mm</description>
<wire x1="2.54" y1="0" x2="1.397" y2="0" width="0.8128" layer="51"/>
<wire x1="-2.54" y1="0" x2="-1.397" y2="0" width="0.8128" layer="51"/>
<circle x="-2.54" y="0" radius="2.159" width="0.1524" layer="21"/>
<circle x="-2.54" y="0" radius="1.143" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="1.016" shape="octagon"/>
<text x="-0.381" y="1.1684" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.381" y="-2.3622" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.2954" y1="-0.4064" x2="1.2954" y2="0.4064" layer="21"/>
</package>
<package name="0617/17">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0617, grid 17.5 mm</description>
<wire x1="-8.89" y1="0" x2="-8.636" y2="0" width="0.8128" layer="51"/>
<wire x1="-7.874" y1="3.048" x2="-6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="2.794" x2="-6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-7.874" y1="-3.048" x2="-6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="-2.794" x2="-6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="2.794" x2="6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="2.794" x2="-6.731" y2="2.794" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.794" x2="6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.794" x2="-6.731" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="7.874" y1="3.048" x2="6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="7.874" y1="-3.048" x2="6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-8.255" y1="-2.667" x2="-8.255" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-8.255" y1="1.016" x2="-8.255" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="-8.255" y1="1.016" x2="-8.255" y2="2.667" width="0.1524" layer="21"/>
<wire x1="8.255" y1="-2.667" x2="8.255" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="8.255" y1="1.016" x2="8.255" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="8.255" y1="1.016" x2="8.255" y2="2.667" width="0.1524" layer="21"/>
<wire x1="8.636" y1="0" x2="8.89" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.255" y1="2.667" x2="-7.874" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="7.874" y1="3.048" x2="8.255" y2="2.667" width="0.1524" layer="21" curve="-90"/>
<wire x1="-8.255" y1="-2.667" x2="-7.874" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="7.874" y1="-3.048" x2="8.255" y2="-2.667" width="0.1524" layer="21" curve="90"/>
<pad name="1" x="-8.89" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="8.89" y="0" drill="1.016" shape="octagon"/>
<text x="-8.128" y="3.4544" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.096" y="-0.7112" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-8.5344" y1="-0.4064" x2="-8.2296" y2="0.4064" layer="51"/>
<rectangle x1="8.2296" y1="-0.4064" x2="8.5344" y2="0.4064" layer="51"/>
</package>
<package name="0617/22">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0617, grid 22.5 mm</description>
<wire x1="-10.287" y1="0" x2="-11.43" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.255" y1="-2.667" x2="-8.255" y2="2.667" width="0.1524" layer="21"/>
<wire x1="-7.874" y1="3.048" x2="-6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="2.794" x2="-6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-7.874" y1="-3.048" x2="-6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="-2.794" x2="-6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="2.794" x2="6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="2.794" x2="-6.731" y2="2.794" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.794" x2="6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.794" x2="-6.731" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="7.874" y1="3.048" x2="6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="7.874" y1="-3.048" x2="6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="8.255" y1="-2.667" x2="8.255" y2="2.667" width="0.1524" layer="21"/>
<wire x1="11.43" y1="0" x2="10.287" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.255" y1="2.667" x2="-7.874" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="-8.255" y1="-2.667" x2="-7.874" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="7.874" y1="3.048" x2="8.255" y2="2.667" width="0.1524" layer="21" curve="-90"/>
<wire x1="7.874" y1="-3.048" x2="8.255" y2="-2.667" width="0.1524" layer="21" curve="90"/>
<pad name="1" x="-11.43" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.43" y="0" drill="1.016" shape="octagon"/>
<text x="-8.255" y="3.4544" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.477" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-10.1854" y1="-0.4064" x2="-8.255" y2="0.4064" layer="21"/>
<rectangle x1="8.255" y1="-0.4064" x2="10.1854" y2="0.4064" layer="21"/>
</package>
<package name="0617V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0617, grid 5 mm</description>
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.8128" layer="51"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.8128" layer="51"/>
<circle x="-2.54" y="0" radius="3.048" width="0.1524" layer="21"/>
<circle x="-2.54" y="0" radius="1.143" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="1.016" shape="octagon"/>
<text x="0.635" y="1.4224" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0.635" y="-2.6162" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.3208" y1="-0.4064" x2="1.3208" y2="0.4064" layer="21"/>
</package>
<package name="0922/22">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0922, grid 22.5 mm</description>
<wire x1="11.43" y1="0" x2="10.795" y2="0" width="0.8128" layer="51"/>
<wire x1="-11.43" y1="0" x2="-10.795" y2="0" width="0.8128" layer="51"/>
<wire x1="-10.16" y1="-4.191" x2="-10.16" y2="4.191" width="0.1524" layer="21"/>
<wire x1="-9.779" y1="4.572" x2="-8.89" y2="4.572" width="0.1524" layer="21"/>
<wire x1="-8.636" y1="4.318" x2="-8.89" y2="4.572" width="0.1524" layer="21"/>
<wire x1="-9.779" y1="-4.572" x2="-8.89" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="-8.636" y1="-4.318" x2="-8.89" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="8.636" y1="4.318" x2="8.89" y2="4.572" width="0.1524" layer="21"/>
<wire x1="8.636" y1="4.318" x2="-8.636" y2="4.318" width="0.1524" layer="21"/>
<wire x1="8.636" y1="-4.318" x2="8.89" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="8.636" y1="-4.318" x2="-8.636" y2="-4.318" width="0.1524" layer="21"/>
<wire x1="9.779" y1="4.572" x2="8.89" y2="4.572" width="0.1524" layer="21"/>
<wire x1="9.779" y1="-4.572" x2="8.89" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="10.16" y1="-4.191" x2="10.16" y2="4.191" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="-4.191" x2="-9.779" y2="-4.572" width="0.1524" layer="21" curve="90"/>
<wire x1="-10.16" y1="4.191" x2="-9.779" y2="4.572" width="0.1524" layer="21" curve="-90"/>
<wire x1="9.779" y1="-4.572" x2="10.16" y2="-4.191" width="0.1524" layer="21" curve="90"/>
<wire x1="9.779" y1="4.572" x2="10.16" y2="4.191" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-11.43" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.43" y="0" drill="1.016" shape="octagon"/>
<text x="-10.16" y="5.1054" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.477" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-10.7188" y1="-0.4064" x2="-10.16" y2="0.4064" layer="51"/>
<rectangle x1="10.16" y1="-0.4064" x2="10.3124" y2="0.4064" layer="21"/>
<rectangle x1="-10.3124" y1="-0.4064" x2="-10.16" y2="0.4064" layer="21"/>
<rectangle x1="10.16" y1="-0.4064" x2="10.7188" y2="0.4064" layer="51"/>
</package>
<package name="P0613V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0613, grid 5 mm</description>
<wire x1="2.54" y1="0" x2="1.397" y2="0" width="0.8128" layer="51"/>
<wire x1="-2.54" y1="0" x2="-1.397" y2="0" width="0.8128" layer="51"/>
<circle x="-2.54" y="0" radius="2.286" width="0.1524" layer="21"/>
<circle x="-2.54" y="0" radius="1.143" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="1.016" shape="octagon"/>
<text x="-0.254" y="1.143" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.254" y="-2.413" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.2954" y1="-0.4064" x2="1.3208" y2="0.4064" layer="21"/>
</package>
<package name="P0613/15">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0613, grid 15 mm</description>
<wire x1="7.62" y1="0" x2="6.985" y2="0" width="0.8128" layer="51"/>
<wire x1="-7.62" y1="0" x2="-6.985" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.477" y1="2.032" x2="-6.223" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.477" y1="-2.032" x2="-6.223" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="6.223" y1="-2.286" x2="6.477" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="6.223" y1="2.286" x2="6.477" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.223" y1="2.286" x2="-5.334" y2="2.286" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="2.159" x2="-5.334" y2="2.286" width="0.1524" layer="21"/>
<wire x1="-6.223" y1="-2.286" x2="-5.334" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="-2.159" x2="-5.334" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="5.207" y1="2.159" x2="5.334" y2="2.286" width="0.1524" layer="21"/>
<wire x1="5.207" y1="2.159" x2="-5.207" y2="2.159" width="0.1524" layer="21"/>
<wire x1="5.207" y1="-2.159" x2="5.334" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="5.207" y1="-2.159" x2="-5.207" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="6.223" y1="2.286" x2="5.334" y2="2.286" width="0.1524" layer="21"/>
<wire x1="6.223" y1="-2.286" x2="5.334" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="6.477" y1="-0.635" x2="6.477" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="6.477" y1="-0.635" x2="6.477" y2="0.635" width="0.1524" layer="51"/>
<wire x1="6.477" y1="2.032" x2="6.477" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-6.477" y1="-2.032" x2="-6.477" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-6.477" y1="0.635" x2="-6.477" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-6.477" y1="0.635" x2="-6.477" y2="2.032" width="0.1524" layer="21"/>
<pad name="1" x="-7.62" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="1.016" shape="octagon"/>
<text x="-6.477" y="2.6924" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.318" y="-0.7112" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-7.0358" y1="-0.4064" x2="-6.477" y2="0.4064" layer="51"/>
<rectangle x1="6.477" y1="-0.4064" x2="7.0358" y2="0.4064" layer="51"/>
</package>
<package name="P0817/22">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0817, grid 22.5 mm</description>
<wire x1="-10.414" y1="0" x2="-11.43" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.509" y1="-3.429" x2="-8.509" y2="3.429" width="0.1524" layer="21"/>
<wire x1="-8.128" y1="3.81" x2="-7.239" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="3.556" x2="-7.239" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-8.128" y1="-3.81" x2="-7.239" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="-3.556" x2="-7.239" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="6.985" y1="3.556" x2="7.239" y2="3.81" width="0.1524" layer="21"/>
<wire x1="6.985" y1="3.556" x2="-6.985" y2="3.556" width="0.1524" layer="21"/>
<wire x1="6.985" y1="-3.556" x2="7.239" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="6.985" y1="-3.556" x2="-6.985" y2="-3.556" width="0.1524" layer="21"/>
<wire x1="8.128" y1="3.81" x2="7.239" y2="3.81" width="0.1524" layer="21"/>
<wire x1="8.128" y1="-3.81" x2="7.239" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-3.429" x2="8.509" y2="3.429" width="0.1524" layer="21"/>
<wire x1="11.43" y1="0" x2="10.414" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.509" y1="3.429" x2="-8.128" y2="3.81" width="0.1524" layer="21" curve="-90"/>
<wire x1="-8.509" y1="-3.429" x2="-8.128" y2="-3.81" width="0.1524" layer="21" curve="90"/>
<wire x1="8.128" y1="3.81" x2="8.509" y2="3.429" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.128" y1="-3.81" x2="8.509" y2="-3.429" width="0.1524" layer="21" curve="90"/>
<pad name="1" x="-11.43" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.43" y="0" drill="1.016" shape="octagon"/>
<text x="-8.382" y="4.2164" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.223" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="6.604" y="-2.2606" size="1.27" layer="51" ratio="10" rot="R90">0817</text>
<rectangle x1="8.509" y1="-0.4064" x2="10.3124" y2="0.4064" layer="21"/>
<rectangle x1="-10.3124" y1="-0.4064" x2="-8.509" y2="0.4064" layer="21"/>
</package>
<package name="P0817V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0817, grid 6.35 mm</description>
<wire x1="-3.81" y1="0" x2="-5.08" y2="0" width="0.8128" layer="51"/>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.8128" layer="51"/>
<circle x="-5.08" y="0" radius="3.81" width="0.1524" layer="21"/>
<circle x="-5.08" y="0" radius="1.27" width="0.1524" layer="51"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="1.016" shape="octagon"/>
<text x="-1.016" y="1.27" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.016" y="-2.54" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-6.858" y="2.032" size="1.016" layer="21" ratio="12">0817</text>
<rectangle x1="-3.81" y1="-0.4064" x2="0" y2="0.4064" layer="21"/>
</package>
<package name="V234/12">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type V234, grid 12.5 mm</description>
<wire x1="-4.953" y1="1.524" x2="-4.699" y2="1.778" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="1.778" x2="4.953" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="-1.778" x2="4.953" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-4.953" y1="-1.524" x2="-4.699" y2="-1.778" width="0.1524" layer="21" curve="90"/>
<wire x1="-4.699" y1="1.778" x2="4.699" y2="1.778" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="1.524" x2="-4.953" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-1.778" x2="-4.699" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="4.953" y1="1.524" x2="4.953" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="6.35" y1="0" x2="5.461" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.461" y2="0" width="0.8128" layer="51"/>
<pad name="1" x="-6.35" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="1.016" shape="octagon"/>
<text x="-4.953" y="2.159" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.81" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="4.953" y1="-0.4064" x2="5.4102" y2="0.4064" layer="21"/>
<rectangle x1="-5.4102" y1="-0.4064" x2="-4.953" y2="0.4064" layer="21"/>
</package>
<package name="V235/17">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type V235, grid 17.78 mm</description>
<wire x1="-6.731" y1="2.921" x2="6.731" y2="2.921" width="0.1524" layer="21"/>
<wire x1="-7.112" y1="2.54" x2="-7.112" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.921" x2="-6.731" y2="-2.921" width="0.1524" layer="21"/>
<wire x1="7.112" y1="2.54" x2="7.112" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="8.89" y1="0" x2="7.874" y2="0" width="1.016" layer="51"/>
<wire x1="-7.874" y1="0" x2="-8.89" y2="0" width="1.016" layer="51"/>
<wire x1="-7.112" y1="-2.54" x2="-6.731" y2="-2.921" width="0.1524" layer="21" curve="90"/>
<wire x1="6.731" y1="2.921" x2="7.112" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.731" y1="-2.921" x2="7.112" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-7.112" y1="2.54" x2="-6.731" y2="2.921" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-8.89" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="8.89" y="0" drill="1.1938" shape="octagon"/>
<text x="-6.858" y="3.302" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.842" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="7.112" y1="-0.508" x2="7.747" y2="0.508" layer="21"/>
<rectangle x1="-7.747" y1="-0.508" x2="-7.112" y2="0.508" layer="21"/>
</package>
<package name="V526-0">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type V526-0, grid 2.5 mm</description>
<wire x1="-2.54" y1="1.016" x2="-2.286" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.286" y1="1.27" x2="2.54" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.286" y1="-1.27" x2="2.54" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.54" y1="-1.016" x2="-2.286" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="2.286" y1="1.27" x2="-2.286" y2="1.27" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.016" x2="2.54" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="-1.27" x2="2.286" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.016" x2="-2.54" y2="-1.016" width="0.1524" layer="21"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.413" y="1.651" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.413" y="-2.794" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0102R">
<description>&lt;b&gt;CECC Size RC2211&lt;/b&gt; Reflow Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-1" y1="-0.5" x2="1" y2="-0.5" width="0.2032" layer="51"/>
<wire x1="1" y1="-0.5" x2="1" y2="0.5" width="0.2032" layer="51"/>
<wire x1="1" y1="0.5" x2="-1" y2="0.5" width="0.2032" layer="51"/>
<wire x1="-1" y1="0.5" x2="-1" y2="-0.5" width="0.2032" layer="51"/>
<smd name="1" x="-0.9" y="0" dx="0.5" dy="1.3" layer="1"/>
<smd name="2" x="0.9" y="0" dx="0.5" dy="1.3" layer="1"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0102W">
<description>&lt;b&gt;CECC Size RC2211&lt;/b&gt; Wave Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-1" y1="-0.5" x2="1" y2="-0.5" width="0.2032" layer="51"/>
<wire x1="1" y1="-0.5" x2="1" y2="0.5" width="0.2032" layer="51"/>
<wire x1="1" y1="0.5" x2="-1" y2="0.5" width="0.2032" layer="51"/>
<wire x1="-1" y1="0.5" x2="-1" y2="-0.5" width="0.2032" layer="51"/>
<smd name="1" x="-0.95" y="0" dx="0.6" dy="1.3" layer="1"/>
<smd name="2" x="0.95" y="0" dx="0.6" dy="1.3" layer="1"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0204R">
<description>&lt;b&gt;CECC Size RC3715&lt;/b&gt; Reflow Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-1.7" y1="-0.6" x2="1.7" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="1.7" y1="-0.6" x2="1.7" y2="0.6" width="0.2032" layer="51"/>
<wire x1="1.7" y1="0.6" x2="-1.7" y2="0.6" width="0.2032" layer="51"/>
<wire x1="-1.7" y1="0.6" x2="-1.7" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="0.938" y1="0.6" x2="-0.938" y2="0.6" width="0.2032" layer="21"/>
<wire x1="-0.938" y1="-0.6" x2="0.938" y2="-0.6" width="0.2032" layer="21"/>
<smd name="1" x="-1.5" y="0" dx="0.8" dy="1.6" layer="1"/>
<smd name="2" x="1.5" y="0" dx="0.8" dy="1.6" layer="1"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0204W">
<description>&lt;b&gt;CECC Size RC3715&lt;/b&gt; Wave Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-1.7" y1="-0.6" x2="1.7" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="1.7" y1="-0.6" x2="1.7" y2="0.6" width="0.2032" layer="51"/>
<wire x1="1.7" y1="0.6" x2="-1.7" y2="0.6" width="0.2032" layer="51"/>
<wire x1="-1.7" y1="0.6" x2="-1.7" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="0.684" y1="0.6" x2="-0.684" y2="0.6" width="0.2032" layer="21"/>
<wire x1="-0.684" y1="-0.6" x2="0.684" y2="-0.6" width="0.2032" layer="21"/>
<smd name="1" x="-1.5" y="0" dx="1.2" dy="1.6" layer="1"/>
<smd name="2" x="1.5" y="0" dx="1.2" dy="1.6" layer="1"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0207R">
<description>&lt;b&gt;CECC Size RC6123&lt;/b&gt; Reflow Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-2.8" y1="-1" x2="2.8" y2="-1" width="0.2032" layer="51"/>
<wire x1="2.8" y1="-1" x2="2.8" y2="1" width="0.2032" layer="51"/>
<wire x1="2.8" y1="1" x2="-2.8" y2="1" width="0.2032" layer="51"/>
<wire x1="-2.8" y1="1" x2="-2.8" y2="-1" width="0.2032" layer="51"/>
<wire x1="1.2125" y1="1" x2="-1.2125" y2="1" width="0.2032" layer="21"/>
<wire x1="-1.2125" y1="-1" x2="1.2125" y2="-1" width="0.2032" layer="21"/>
<smd name="1" x="-2.25" y="0" dx="1.6" dy="2.5" layer="1"/>
<smd name="2" x="2.25" y="0" dx="1.6" dy="2.5" layer="1"/>
<text x="-2.2225" y="1.5875" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.2225" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0207W">
<description>&lt;b&gt;CECC Size RC6123&lt;/b&gt; Wave Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-2.8" y1="-1" x2="2.8" y2="-1" width="0.2032" layer="51"/>
<wire x1="2.8" y1="-1" x2="2.8" y2="1" width="0.2032" layer="51"/>
<wire x1="2.8" y1="1" x2="-2.8" y2="1" width="0.2032" layer="51"/>
<wire x1="-2.8" y1="1" x2="-2.8" y2="-1" width="0.2032" layer="51"/>
<wire x1="1.149" y1="1" x2="-1.149" y2="1" width="0.2032" layer="21"/>
<wire x1="-1.149" y1="-1" x2="1.149" y2="-1" width="0.2032" layer="21"/>
<smd name="1" x="-2.6" y="0" dx="2.4" dy="2.5" layer="1"/>
<smd name="2" x="2.6" y="0" dx="2.4" dy="2.5" layer="1"/>
<text x="-2.54" y="1.5875" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="0922V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0922, grid 7.5 mm</description>
<wire x1="2.54" y1="0" x2="1.397" y2="0" width="0.8128" layer="51"/>
<wire x1="-5.08" y1="0" x2="-3.81" y2="0" width="0.8128" layer="51"/>
<circle x="-5.08" y="0" radius="4.572" width="0.1524" layer="21"/>
<circle x="-5.08" y="0" radius="1.905" width="0.1524" layer="21"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="1.016" shape="octagon"/>
<text x="-0.508" y="1.6764" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.508" y="-2.9972" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-6.858" y="2.54" size="1.016" layer="21" ratio="12">0922</text>
<rectangle x1="-3.81" y1="-0.4064" x2="1.3208" y2="0.4064" layer="21"/>
</package>
<package name="RDH/15">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type RDH, grid 15 mm</description>
<wire x1="-7.62" y1="0" x2="-6.858" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.096" y1="3.048" x2="-5.207" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="2.794" x2="-5.207" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-6.096" y1="-3.048" x2="-5.207" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="-2.794" x2="-5.207" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="4.953" y1="2.794" x2="5.207" y2="3.048" width="0.1524" layer="21"/>
<wire x1="4.953" y1="2.794" x2="-4.953" y2="2.794" width="0.1524" layer="21"/>
<wire x1="4.953" y1="-2.794" x2="5.207" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="4.953" y1="-2.794" x2="-4.953" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="6.096" y1="3.048" x2="5.207" y2="3.048" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-3.048" x2="5.207" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-6.477" y1="-2.667" x2="-6.477" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-6.477" y1="1.016" x2="-6.477" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="-6.477" y1="1.016" x2="-6.477" y2="2.667" width="0.1524" layer="21"/>
<wire x1="6.477" y1="-2.667" x2="6.477" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="6.477" y1="1.016" x2="6.477" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="6.477" y1="1.016" x2="6.477" y2="2.667" width="0.1524" layer="21"/>
<wire x1="6.858" y1="0" x2="7.62" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.477" y1="2.667" x2="-6.096" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.096" y1="3.048" x2="6.477" y2="2.667" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.477" y1="-2.667" x2="-6.096" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="6.096" y1="-3.048" x2="6.477" y2="-2.667" width="0.1524" layer="21" curve="90"/>
<pad name="1" x="-7.62" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="1.016" shape="octagon"/>
<text x="-6.35" y="3.4544" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.318" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="4.572" y="-1.7272" size="1.27" layer="51" ratio="10" rot="R90">RDH</text>
<rectangle x1="-6.7564" y1="-0.4064" x2="-6.4516" y2="0.4064" layer="51"/>
<rectangle x1="6.4516" y1="-0.4064" x2="6.7564" y2="0.4064" layer="51"/>
</package>
<package name="MINI_MELF-0102AX">
<description>&lt;b&gt;Mini MELF 0102 Axial&lt;/b&gt;</description>
<circle x="0" y="0" radius="0.6" width="0" layer="51"/>
<circle x="0" y="0" radius="0.6" width="0" layer="52"/>
<smd name="1" x="0" y="0" dx="1.9" dy="1.9" layer="1" roundness="100"/>
<smd name="2" x="0" y="0" dx="1.9" dy="1.9" layer="16" roundness="100"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
<hole x="0" y="0" drill="1.3"/>
</package>
<package name="R0201">
<description>&lt;b&gt;RESISTOR&lt;/b&gt; chip&lt;p&gt;
Source: http://www.vishay.com/docs/20008/dcrcw.pdf</description>
<smd name="1" x="-0.255" y="0" dx="0.28" dy="0.43" layer="1"/>
<smd name="2" x="0.255" y="0" dx="0.28" dy="0.43" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.3" y1="-0.15" x2="-0.15" y2="0.15" layer="51"/>
<rectangle x1="0.15" y1="-0.15" x2="0.3" y2="0.15" layer="51"/>
<rectangle x1="-0.15" y1="-0.15" x2="0.15" y2="0.15" layer="21"/>
</package>
<package name="VTA52">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RBR52&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-15.24" y1="0" x2="-13.97" y2="0" width="0.6096" layer="51"/>
<wire x1="12.6225" y1="0.025" x2="12.6225" y2="4.725" width="0.1524" layer="21"/>
<wire x1="12.6225" y1="4.725" x2="-12.6225" y2="4.725" width="0.1524" layer="21"/>
<wire x1="-12.6225" y1="4.725" x2="-12.6225" y2="0.025" width="0.1524" layer="21"/>
<wire x1="-12.6225" y1="0.025" x2="-12.6225" y2="-4.65" width="0.1524" layer="21"/>
<wire x1="-12.6225" y1="-4.65" x2="12.6225" y2="-4.65" width="0.1524" layer="21"/>
<wire x1="12.6225" y1="-4.65" x2="12.6225" y2="0.025" width="0.1524" layer="21"/>
<wire x1="13.97" y1="0" x2="15.24" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-15.24" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="15.24" y="0" drill="1.1" shape="octagon"/>
<text x="-3.81" y="5.08" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-13.97" y1="-0.3048" x2="-12.5675" y2="0.3048" layer="21"/>
<rectangle x1="12.5675" y1="-0.3048" x2="13.97" y2="0.3048" layer="21"/>
</package>
<package name="VTA53">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RBR53&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-12.065" y1="0" x2="-10.795" y2="0" width="0.6096" layer="51"/>
<wire x1="9.8975" y1="0" x2="9.8975" y2="4.7" width="0.1524" layer="21"/>
<wire x1="9.8975" y1="4.7" x2="-9.8975" y2="4.7" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="4.7" x2="-9.8975" y2="0" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="0" x2="-9.8975" y2="-4.675" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="-4.675" x2="9.8975" y2="-4.675" width="0.1524" layer="21"/>
<wire x1="9.8975" y1="-4.675" x2="9.8975" y2="0" width="0.1524" layer="21"/>
<wire x1="10.795" y1="0" x2="12.065" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-12.065" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="12.065" y="0" drill="1.1" shape="octagon"/>
<text x="-3.81" y="5.08" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-10.795" y1="-0.3048" x2="-9.8425" y2="0.3048" layer="21"/>
<rectangle x1="9.8425" y1="-0.3048" x2="10.795" y2="0.3048" layer="21"/>
</package>
<package name="VTA54">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RBR54&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-12.065" y1="0" x2="-10.795" y2="0" width="0.6096" layer="51"/>
<wire x1="9.8975" y1="0" x2="9.8975" y2="3.3" width="0.1524" layer="21"/>
<wire x1="9.8975" y1="3.3" x2="-9.8975" y2="3.3" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="3.3" x2="-9.8975" y2="0" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="0" x2="-9.8975" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="-3.3" x2="9.8975" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="9.8975" y1="-3.3" x2="9.8975" y2="0" width="0.1524" layer="21"/>
<wire x1="10.795" y1="0" x2="12.065" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-12.065" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="12.065" y="0" drill="1.1" shape="octagon"/>
<text x="-3.81" y="3.81" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-10.795" y1="-0.3048" x2="-9.8425" y2="0.3048" layer="21"/>
<rectangle x1="9.8425" y1="-0.3048" x2="10.795" y2="0.3048" layer="21"/>
</package>
<package name="VTA55">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RBR55&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-8.255" y1="0" x2="-6.985" y2="0" width="0.6096" layer="51"/>
<wire x1="6.405" y1="0" x2="6.405" y2="3.3" width="0.1524" layer="21"/>
<wire x1="6.405" y1="3.3" x2="-6.405" y2="3.3" width="0.1524" layer="21"/>
<wire x1="-6.405" y1="3.3" x2="-6.405" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.405" y1="0" x2="-6.405" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="-6.405" y1="-3.3" x2="6.405" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="6.405" y1="-3.3" x2="6.405" y2="0" width="0.1524" layer="21"/>
<wire x1="6.985" y1="0" x2="8.255" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-8.255" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="8.255" y="0" drill="1.1" shape="octagon"/>
<text x="-3.81" y="3.81" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-6.985" y1="-0.3048" x2="-6.35" y2="0.3048" layer="21"/>
<rectangle x1="6.35" y1="-0.3048" x2="6.985" y2="0.3048" layer="21"/>
</package>
<package name="VTA56">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RBR56&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-6.35" y1="0" x2="-5.08" y2="0" width="0.6096" layer="51"/>
<wire x1="4.5" y1="0" x2="4.5" y2="3.3" width="0.1524" layer="21"/>
<wire x1="4.5" y1="3.3" x2="-4.5" y2="3.3" width="0.1524" layer="21"/>
<wire x1="-4.5" y1="3.3" x2="-4.5" y2="0" width="0.1524" layer="21"/>
<wire x1="-4.5" y1="0" x2="-4.5" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="-4.5" y1="-3.3" x2="4.5" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="4.5" y1="-3.3" x2="4.5" y2="0" width="0.1524" layer="21"/>
<wire x1="5.08" y1="0" x2="6.35" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-6.35" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="1.1" shape="octagon"/>
<text x="-3.81" y="3.81" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-5.08" y1="-0.3048" x2="-4.445" y2="0.3048" layer="21"/>
<rectangle x1="4.445" y1="-0.3048" x2="5.08" y2="0.3048" layer="21"/>
</package>
<package name="VMTA55">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RNC55&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-5.08" y1="0" x2="-4.26" y2="0" width="0.6096" layer="51"/>
<wire x1="3.3375" y1="-1.45" x2="3.3375" y2="1.45" width="0.1524" layer="21"/>
<wire x1="3.3375" y1="1.45" x2="-3.3625" y2="1.45" width="0.1524" layer="21"/>
<wire x1="-3.3625" y1="1.45" x2="-3.3625" y2="-1.45" width="0.1524" layer="21"/>
<wire x1="-3.3625" y1="-1.45" x2="3.3375" y2="-1.45" width="0.1524" layer="21"/>
<wire x1="4.235" y1="0" x2="5.08" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-5.08" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="1.1" shape="octagon"/>
<text x="-3.175" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-4.26" y1="-0.3048" x2="-3.3075" y2="0.3048" layer="21"/>
<rectangle x1="3.2825" y1="-0.3048" x2="4.235" y2="0.3048" layer="21"/>
</package>
<package name="VMTB60">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RNC60&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-6.35" y1="0" x2="-5.585" y2="0" width="0.6096" layer="51"/>
<wire x1="4.6875" y1="-1.95" x2="4.6875" y2="1.95" width="0.1524" layer="21"/>
<wire x1="4.6875" y1="1.95" x2="-4.6875" y2="1.95" width="0.1524" layer="21"/>
<wire x1="-4.6875" y1="1.95" x2="-4.6875" y2="-1.95" width="0.1524" layer="21"/>
<wire x1="-4.6875" y1="-1.95" x2="4.6875" y2="-1.95" width="0.1524" layer="21"/>
<wire x1="5.585" y1="0" x2="6.35" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-6.35" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="1.1" shape="octagon"/>
<text x="-4.445" y="2.54" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.445" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-5.585" y1="-0.3048" x2="-4.6325" y2="0.3048" layer="21"/>
<rectangle x1="4.6325" y1="-0.3048" x2="5.585" y2="0.3048" layer="21"/>
</package>
<package name="R4527">
<description>&lt;b&gt;Package 4527&lt;/b&gt;&lt;p&gt;
Source: http://www.vishay.com/docs/31059/wsrhigh.pdf</description>
<wire x1="-5.675" y1="-3.375" x2="5.65" y2="-3.375" width="0.2032" layer="21"/>
<wire x1="5.65" y1="-3.375" x2="5.65" y2="3.375" width="0.2032" layer="51"/>
<wire x1="5.65" y1="3.375" x2="-5.675" y2="3.375" width="0.2032" layer="21"/>
<wire x1="-5.675" y1="3.375" x2="-5.675" y2="-3.375" width="0.2032" layer="51"/>
<smd name="1" x="-4.575" y="0" dx="3.94" dy="5.84" layer="1"/>
<smd name="2" x="4.575" y="0" dx="3.94" dy="5.84" layer="1"/>
<text x="-5.715" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-5.715" y="-5.08" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC0001">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-3.075" y1="1.8" x2="-3.075" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="-3.075" y1="-1.8" x2="3.075" y2="-1.8" width="0.2032" layer="21"/>
<wire x1="3.075" y1="-1.8" x2="3.075" y2="1.8" width="0.2032" layer="51"/>
<wire x1="3.075" y1="1.8" x2="-3.075" y2="1.8" width="0.2032" layer="21"/>
<wire x1="-3.075" y1="1.8" x2="-3.075" y2="1.606" width="0.2032" layer="21"/>
<wire x1="-3.075" y1="-1.606" x2="-3.075" y2="-1.8" width="0.2032" layer="21"/>
<wire x1="3.075" y1="1.606" x2="3.075" y2="1.8" width="0.2032" layer="21"/>
<wire x1="3.075" y1="-1.8" x2="3.075" y2="-1.606" width="0.2032" layer="21"/>
<smd name="1" x="-2.675" y="0" dx="2.29" dy="2.92" layer="1"/>
<smd name="2" x="2.675" y="0" dx="2.29" dy="2.92" layer="1"/>
<text x="-2.544" y="2.229" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.544" y="-3.501" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC0002">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-5.55" y1="3.375" x2="-5.55" y2="-3.375" width="0.2032" layer="51"/>
<wire x1="-5.55" y1="-3.375" x2="5.55" y2="-3.375" width="0.2032" layer="21"/>
<wire x1="5.55" y1="-3.375" x2="5.55" y2="3.375" width="0.2032" layer="51"/>
<wire x1="5.55" y1="3.375" x2="-5.55" y2="3.375" width="0.2032" layer="21"/>
<smd name="1" x="-4.575" y="0.025" dx="3.94" dy="5.84" layer="1"/>
<smd name="2" x="4.575" y="0" dx="3.94" dy="5.84" layer="1"/>
<text x="-5.65" y="3.9" size="1.27" layer="25">&gt;NAME</text>
<text x="-5.65" y="-5.15" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC01/2">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-2.45" y1="1.475" x2="-2.45" y2="-1.475" width="0.2032" layer="51"/>
<wire x1="-2.45" y1="-1.475" x2="2.45" y2="-1.475" width="0.2032" layer="21"/>
<wire x1="2.45" y1="-1.475" x2="2.45" y2="1.475" width="0.2032" layer="51"/>
<wire x1="2.45" y1="1.475" x2="-2.45" y2="1.475" width="0.2032" layer="21"/>
<wire x1="-2.45" y1="1.475" x2="-2.45" y2="1.106" width="0.2032" layer="21"/>
<wire x1="-2.45" y1="-1.106" x2="-2.45" y2="-1.475" width="0.2032" layer="21"/>
<wire x1="2.45" y1="1.106" x2="2.45" y2="1.475" width="0.2032" layer="21"/>
<wire x1="2.45" y1="-1.475" x2="2.45" y2="-1.106" width="0.2032" layer="21"/>
<smd name="1" x="-2.1" y="0" dx="2.16" dy="1.78" layer="1"/>
<smd name="2" x="2.1" y="0" dx="2.16" dy="1.78" layer="1"/>
<text x="-2.544" y="1.904" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.544" y="-3.176" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC2515">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-3.075" y1="1.8" x2="-3.075" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="-3.075" y1="-1.8" x2="3.05" y2="-1.8" width="0.2032" layer="21"/>
<wire x1="3.05" y1="-1.8" x2="3.05" y2="1.8" width="0.2032" layer="51"/>
<wire x1="3.05" y1="1.8" x2="-3.075" y2="1.8" width="0.2032" layer="21"/>
<wire x1="-3.075" y1="1.8" x2="-3.075" y2="1.606" width="0.2032" layer="21"/>
<wire x1="-3.075" y1="-1.606" x2="-3.075" y2="-1.8" width="0.2032" layer="21"/>
<wire x1="3.05" y1="1.606" x2="3.05" y2="1.8" width="0.2032" layer="21"/>
<wire x1="3.05" y1="-1.8" x2="3.05" y2="-1.606" width="0.2032" layer="21"/>
<smd name="1" x="-2.675" y="0" dx="2.29" dy="2.92" layer="1"/>
<smd name="2" x="2.675" y="0" dx="2.29" dy="2.92" layer="1"/>
<text x="-3.2" y="2.15" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.2" y="-3.4" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC4527">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-5.675" y1="3.4" x2="-5.675" y2="-3.375" width="0.2032" layer="51"/>
<wire x1="-5.675" y1="-3.375" x2="5.675" y2="-3.375" width="0.2032" layer="21"/>
<wire x1="5.675" y1="-3.375" x2="5.675" y2="3.4" width="0.2032" layer="51"/>
<wire x1="5.675" y1="3.4" x2="-5.675" y2="3.4" width="0.2032" layer="21"/>
<smd name="1" x="-4.575" y="0.025" dx="3.94" dy="5.84" layer="1"/>
<smd name="2" x="4.575" y="0" dx="3.94" dy="5.84" layer="1"/>
<text x="-5.775" y="3.925" size="1.27" layer="25">&gt;NAME</text>
<text x="-5.775" y="-5.15" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC6927">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-8.65" y1="3.375" x2="-8.65" y2="-3.375" width="0.2032" layer="51"/>
<wire x1="-8.65" y1="-3.375" x2="8.65" y2="-3.375" width="0.2032" layer="21"/>
<wire x1="8.65" y1="-3.375" x2="8.65" y2="3.375" width="0.2032" layer="51"/>
<wire x1="8.65" y1="3.375" x2="-8.65" y2="3.375" width="0.2032" layer="21"/>
<smd name="1" x="-7.95" y="0.025" dx="3.94" dy="5.97" layer="1"/>
<smd name="2" x="7.95" y="0" dx="3.94" dy="5.97" layer="1"/>
<text x="-8.75" y="3.9" size="1.27" layer="25">&gt;NAME</text>
<text x="-8.75" y="-5.15" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="R1218">
<description>&lt;b&gt;CRCW1218 Thick Film, Rectangular Chip Resistors&lt;/b&gt;&lt;p&gt;
Source: http://www.vishay.com .. dcrcw.pdf</description>
<wire x1="-0.913" y1="-2.219" x2="0.939" y2="-2.219" width="0.1524" layer="51"/>
<wire x1="0.913" y1="2.219" x2="-0.939" y2="2.219" width="0.1524" layer="51"/>
<smd name="1" x="-1.475" y="0" dx="1.05" dy="4.9" layer="1"/>
<smd name="2" x="1.475" y="0" dx="1.05" dy="4.9" layer="1"/>
<text x="-2.54" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-2.3" x2="-0.9009" y2="2.3" layer="51"/>
<rectangle x1="0.9144" y1="-2.3" x2="1.6645" y2="2.3" layer="51"/>
</package>
<package name="1812X7R">
<description>&lt;b&gt;Chip Monolithic Ceramic Capacitors&lt;/b&gt; Medium Voltage High Capacitance for General Use&lt;p&gt;
Source: http://www.murata.com .. GRM43DR72E224KW01.pdf</description>
<wire x1="-1.1" y1="1.5" x2="1.1" y2="1.5" width="0.2032" layer="51"/>
<wire x1="1.1" y1="-1.5" x2="-1.1" y2="-1.5" width="0.2032" layer="51"/>
<wire x1="-0.6" y1="1.5" x2="0.6" y2="1.5" width="0.2032" layer="21"/>
<wire x1="0.6" y1="-1.5" x2="-0.6" y2="-1.5" width="0.2032" layer="21"/>
<smd name="1" x="-1.425" y="0" dx="0.8" dy="3.5" layer="1"/>
<smd name="2" x="1.425" y="0" dx="0.8" dy="3.5" layer="1" rot="R180"/>
<text x="-1.9456" y="1.9958" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.9456" y="-3.7738" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.4" y1="-1.6" x2="-1.1" y2="1.6" layer="51"/>
<rectangle x1="1.1" y1="-1.6" x2="1.4" y2="1.6" layer="51" rot="R180"/>
</package>
<package name="C0402">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="C0504">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.294" y1="0.559" x2="0.294" y2="0.559" width="0.1016" layer="51"/>
<wire x1="-0.294" y1="-0.559" x2="0.294" y2="-0.559" width="0.1016" layer="51"/>
<smd name="1" x="-0.7" y="0" dx="1" dy="1.3" layer="1"/>
<smd name="2" x="0.7" y="0" dx="1" dy="1.3" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.6604" y1="-0.6223" x2="-0.2804" y2="0.6276" layer="51"/>
<rectangle x1="0.2794" y1="-0.6223" x2="0.6594" y2="0.6276" layer="51"/>
<rectangle x1="-0.1001" y1="-0.4001" x2="0.1001" y2="0.4001" layer="35"/>
</package>
<package name="C0603">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="C0805">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;</description>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.381" y1="0.66" x2="0.381" y2="0.66" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.66" x2="0.381" y2="-0.66" width="0.1016" layer="51"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.0922" y1="-0.7239" x2="-0.3421" y2="0.7262" layer="51"/>
<rectangle x1="0.3556" y1="-0.7239" x2="1.1057" y2="0.7262" layer="51"/>
<rectangle x1="-0.1001" y1="-0.4001" x2="0.1001" y2="0.4001" layer="35"/>
</package>
<package name="C1005">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="C1206">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="C1210">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="-0.9652" y1="1.2446" x2="0.9652" y2="1.2446" width="0.1016" layer="51"/>
<wire x1="-0.9652" y1="-1.2446" x2="0.9652" y2="-1.2446" width="0.1016" layer="51"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-1.2954" x2="-0.9517" y2="1.3045" layer="51"/>
<rectangle x1="0.9517" y1="-1.3045" x2="1.7018" y2="1.2954" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="C1310">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.294" y1="0.559" x2="0.294" y2="0.559" width="0.1016" layer="51"/>
<wire x1="-0.294" y1="-0.559" x2="0.294" y2="-0.559" width="0.1016" layer="51"/>
<smd name="1" x="-0.7" y="0" dx="1" dy="1.3" layer="1"/>
<smd name="2" x="0.7" y="0" dx="1" dy="1.3" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.6604" y1="-0.6223" x2="-0.2804" y2="0.6276" layer="51"/>
<rectangle x1="0.2794" y1="-0.6223" x2="0.6594" y2="0.6276" layer="51"/>
<rectangle x1="-0.1001" y1="-0.3" x2="0.1001" y2="0.3" layer="35"/>
</package>
<package name="C1608">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="C1812">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.973" y1="1.983" x2="2.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-1.983" x2="-2.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-1.983" x2="-2.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="-1.4732" y1="1.6002" x2="1.4732" y2="1.6002" width="0.1016" layer="51"/>
<wire x1="-1.4478" y1="-1.6002" x2="1.4732" y2="-1.6002" width="0.1016" layer="51"/>
<wire x1="2.973" y1="1.983" x2="2.973" y2="-1.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.95" y="0" dx="1.9" dy="3.4" layer="1"/>
<smd name="2" x="1.95" y="0" dx="1.9" dy="3.4" layer="1"/>
<text x="-1.905" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.3876" y1="-1.651" x2="-1.4376" y2="1.649" layer="51"/>
<rectangle x1="1.4478" y1="-1.651" x2="2.3978" y2="1.649" layer="51"/>
<rectangle x1="-0.3" y1="-0.4001" x2="0.3" y2="0.4001" layer="35"/>
</package>
<package name="C1825">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.973" y1="3.483" x2="2.973" y2="3.483" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-3.483" x2="-2.973" y2="-3.483" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-3.483" x2="-2.973" y2="3.483" width="0.0508" layer="39"/>
<wire x1="-1.4986" y1="3.2766" x2="1.4732" y2="3.2766" width="0.1016" layer="51"/>
<wire x1="-1.4732" y1="-3.2766" x2="1.4986" y2="-3.2766" width="0.1016" layer="51"/>
<wire x1="2.973" y1="3.483" x2="2.973" y2="-3.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.95" y="0" dx="1.9" dy="6.8" layer="1"/>
<smd name="2" x="1.95" y="0" dx="1.9" dy="6.8" layer="1"/>
<text x="-1.905" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-5.08" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.413" y1="-3.3528" x2="-1.463" y2="3.3472" layer="51"/>
<rectangle x1="1.4478" y1="-3.3528" x2="2.3978" y2="3.3472" layer="51"/>
<rectangle x1="-0.7" y1="-0.7" x2="0.7" y2="0.7" layer="35"/>
</package>
<package name="C2012">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.381" y1="0.66" x2="0.381" y2="0.66" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.66" x2="0.381" y2="-0.66" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.0922" y1="-0.7239" x2="-0.3421" y2="0.7262" layer="51"/>
<rectangle x1="0.3556" y1="-0.7239" x2="1.1057" y2="0.7262" layer="51"/>
<rectangle x1="-0.1001" y1="-0.4001" x2="0.1001" y2="0.4001" layer="35"/>
</package>
<package name="C3216">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.3" y1="-0.5001" x2="0.3" y2="0.5001" layer="35"/>
</package>
<package name="C3225">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="-0.9652" y1="1.2446" x2="0.9652" y2="1.2446" width="0.1016" layer="51"/>
<wire x1="-0.9652" y1="-1.2446" x2="0.9652" y2="-1.2446" width="0.1016" layer="51"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-1.2954" x2="-0.9517" y2="1.3045" layer="51"/>
<rectangle x1="0.9517" y1="-1.3045" x2="1.7018" y2="1.2954" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
</package>
<package name="C4532">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.973" y1="1.983" x2="2.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-1.983" x2="-2.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-1.983" x2="-2.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="-1.4732" y1="1.6002" x2="1.4732" y2="1.6002" width="0.1016" layer="51"/>
<wire x1="-1.4478" y1="-1.6002" x2="1.4732" y2="-1.6002" width="0.1016" layer="51"/>
<wire x1="2.973" y1="1.983" x2="2.973" y2="-1.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.95" y="0" dx="1.9" dy="3.4" layer="1"/>
<smd name="2" x="1.95" y="0" dx="1.9" dy="3.4" layer="1"/>
<text x="-1.905" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.3876" y1="-1.651" x2="-1.4376" y2="1.649" layer="51"/>
<rectangle x1="1.4478" y1="-1.651" x2="2.3978" y2="1.649" layer="51"/>
<rectangle x1="-0.4001" y1="-0.7" x2="0.4001" y2="0.7" layer="35"/>
</package>
<package name="C4564">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.973" y1="3.483" x2="2.973" y2="3.483" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-3.483" x2="-2.973" y2="-3.483" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-3.483" x2="-2.973" y2="3.483" width="0.0508" layer="39"/>
<wire x1="-1.4986" y1="3.2766" x2="1.4732" y2="3.2766" width="0.1016" layer="51"/>
<wire x1="-1.4732" y1="-3.2766" x2="1.4986" y2="-3.2766" width="0.1016" layer="51"/>
<wire x1="2.973" y1="3.483" x2="2.973" y2="-3.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.95" y="0" dx="1.9" dy="6.8" layer="1"/>
<smd name="2" x="1.95" y="0" dx="1.9" dy="6.8" layer="1"/>
<text x="-1.905" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-5.08" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.413" y1="-3.3528" x2="-1.463" y2="3.3472" layer="51"/>
<rectangle x1="1.4478" y1="-3.3528" x2="2.3978" y2="3.3472" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="C025-024X044">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 2.4 x 4.4 mm</description>
<wire x1="-2.159" y1="-0.635" x2="-2.159" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="0.635" x2="-1.651" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.159" y1="-0.635" x2="-1.651" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="1.651" y1="1.143" x2="-1.651" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-0.635" x2="2.159" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.651" y1="-1.143" x2="-1.651" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="1.651" y1="1.143" x2="2.159" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="1.651" y1="-1.143" x2="2.159" y2="-0.635" width="0.1524" layer="21" curve="90"/>
<wire x1="-0.3048" y1="0.762" x2="-0.3048" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0.762" x2="0.3302" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="1.27" y1="0" x2="0.3302" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="0" x2="-0.3048" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-1.778" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.778" y="-2.667" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025-025X050">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 2.5 x 5 mm</description>
<wire x1="-2.159" y1="1.27" x2="2.159" y2="1.27" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-1.27" x2="-2.159" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.159" y1="1.27" x2="2.413" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="1.016" x2="-2.159" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-1.27" x2="2.413" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-1.016" x2="-2.159" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="1.524" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-2.794" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025-030X050">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 3 x 5 mm</description>
<wire x1="-2.159" y1="1.524" x2="2.159" y2="1.524" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-1.524" x2="-2.159" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.27" x2="2.413" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.27" x2="-2.413" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.159" y1="1.524" x2="2.413" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="1.27" x2="-2.159" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-1.524" x2="2.413" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-1.27" x2="-2.159" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-3.048" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025-040X050">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 4 x 5 mm</description>
<wire x1="-2.159" y1="1.905" x2="2.159" y2="1.905" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-1.905" x2="-2.159" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.651" x2="2.413" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.651" x2="-2.413" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="2.159" y1="1.905" x2="2.413" y2="1.651" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="1.651" x2="-2.159" y2="1.905" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-1.905" x2="2.413" y2="-1.651" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-1.651" x2="-2.159" y2="-1.905" width="0.1524" layer="21" curve="90"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="2.159" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-3.429" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025-050X050">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 5 x 5 mm</description>
<wire x1="-2.159" y1="2.286" x2="2.159" y2="2.286" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-2.286" x2="-2.159" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="2.413" y1="2.032" x2="2.413" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="2.032" x2="-2.413" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="2.159" y1="2.286" x2="2.413" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="2.032" x2="-2.159" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-2.286" x2="2.413" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-2.032" x2="-2.159" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="2.54" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-3.81" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025-060X050">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 6 x 5 mm</description>
<wire x1="-2.159" y1="2.794" x2="2.159" y2="2.794" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-2.794" x2="-2.159" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="2.413" y1="2.54" x2="2.413" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="2.54" x2="-2.413" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="2.159" y1="2.794" x2="2.413" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="2.54" x2="-2.159" y2="2.794" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-2.794" x2="2.413" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-2.54" x2="-2.159" y2="-2.794" width="0.1524" layer="21" curve="90"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="3.048" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.032" y="-2.413" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025_050-024X070">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm + 5 mm, outline 2.4 x 7 mm</description>
<wire x1="-2.159" y1="-0.635" x2="-2.159" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-2.159" y1="0.635" x2="-1.651" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.159" y1="-0.635" x2="-1.651" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="1.651" y1="1.143" x2="-1.651" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-0.635" x2="2.159" y2="0.635" width="0.1524" layer="51"/>
<wire x1="1.651" y1="-1.143" x2="-1.651" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="1.651" y1="1.143" x2="2.159" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.191" y1="-1.143" x2="-3.9624" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-4.191" y1="1.143" x2="-3.9624" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="-0.635" x2="-4.191" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="1.651" y1="-1.143" x2="2.159" y2="-0.635" width="0.1524" layer="21" curve="90"/>
<wire x1="-4.699" y1="0.635" x2="-4.191" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.699" y1="-0.635" x2="-4.699" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="1.143" x2="-2.5654" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-1.143" x2="-2.5654" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-0.3048" y1="0.762" x2="-0.3048" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0.762" x2="0.3302" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="1.27" y1="0" x2="0.3302" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="0" x2="-0.3048" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="3" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.81" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.81" y="-2.667" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025_050-025X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 + 5 mm, outline 2.5 x 7.5 mm</description>
<wire x1="-2.159" y1="1.27" x2="2.159" y2="1.27" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-1.27" x2="-2.159" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.159" y1="1.27" x2="2.413" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="1.016" x2="-2.159" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-1.27" x2="2.413" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-1.016" x2="-2.159" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="4.953" y1="1.016" x2="4.953" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="4.699" y1="1.27" x2="4.953" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="-1.27" x2="4.953" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="2.794" y1="1.27" x2="4.699" y2="1.27" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-1.27" x2="2.794" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.413" y2="0.762" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-0.762" x2="2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="0.254" x2="2.413" y2="-0.254" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0" x2="2.286" y2="0" width="0.1524" layer="51"/>
<wire x1="2.286" y1="0" x2="2.794" y2="0" width="0.1524" layer="21"/>
<wire x1="2.794" y1="0" x2="3.302" y2="0" width="0.1524" layer="51"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="3" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.159" y="1.651" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.159" y="-2.794" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025_050-035X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 + 5 mm, outline 3.5 x 7.5 mm</description>
<wire x1="-2.159" y1="1.778" x2="2.159" y2="1.778" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-1.778" x2="-2.159" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.524" x2="-2.413" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="2.159" y1="1.778" x2="2.413" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="1.524" x2="-2.159" y2="1.778" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-1.778" x2="2.413" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-1.524" x2="-2.159" y2="-1.778" width="0.1524" layer="21" curve="90"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="4.953" y1="1.524" x2="4.953" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="4.699" y1="1.778" x2="4.953" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="-1.778" x2="4.953" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="2.794" y1="1.778" x2="4.699" y2="1.778" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-1.778" x2="2.794" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.524" x2="2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.413" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="2.413" y1="0.508" x2="2.413" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="0.381" y1="0" x2="0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="2.286" y1="0" x2="2.794" y2="0" width="0.1524" layer="21"/>
<wire x1="2.794" y1="0" x2="3.302" y2="0" width="0.1524" layer="51"/>
<wire x1="2.286" y1="0" x2="1.778" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="3" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="2.159" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-3.302" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025_050-045X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 + 5 mm, outline 4.5 x 7.5 mm</description>
<wire x1="-2.159" y1="2.286" x2="2.159" y2="2.286" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-2.286" x2="-2.159" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="2.032" x2="-2.413" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="2.159" y1="2.286" x2="2.413" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="2.032" x2="-2.159" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-2.286" x2="2.413" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-2.032" x2="-2.159" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="4.953" y1="2.032" x2="4.953" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="4.699" y1="2.286" x2="4.953" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="-2.286" x2="4.953" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="2.794" y1="2.286" x2="4.699" y2="2.286" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-2.286" x2="2.794" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="2.413" y1="2.032" x2="2.413" y2="1.397" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.397" x2="2.413" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="2.413" y1="0.762" x2="2.413" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="2.286" y1="0" x2="2.794" y2="0" width="0.1524" layer="21"/>
<wire x1="2.794" y1="0" x2="3.302" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="2.286" y1="0" x2="1.778" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="3" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="2.667" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-3.81" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025_050-055X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 + 5 mm, outline 5.5 x 7.5 mm</description>
<wire x1="-2.159" y1="2.794" x2="2.159" y2="2.794" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-2.794" x2="-2.159" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="2.54" x2="-2.413" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="2.159" y1="2.794" x2="2.413" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="2.54" x2="-2.159" y2="2.794" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-2.794" x2="2.413" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-2.54" x2="-2.159" y2="-2.794" width="0.1524" layer="21" curve="90"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="4.953" y1="2.54" x2="4.953" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="4.699" y1="2.794" x2="4.953" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="-2.794" x2="4.953" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="2.794" y1="2.794" x2="4.699" y2="2.794" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-2.794" x2="2.794" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="2.413" y1="2.54" x2="2.413" y2="2.032" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-2.032" x2="2.413" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="2.413" y1="0.762" x2="2.413" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0" x2="2.286" y2="0" width="0.1524" layer="51"/>
<wire x1="2.286" y1="0" x2="2.794" y2="0" width="0.1524" layer="21"/>
<wire x1="2.794" y1="0" x2="3.302" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="3" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="3.175" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.032" y="-2.286" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-024X044">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 2.4 x 4.4 mm</description>
<wire x1="-2.159" y1="-0.635" x2="-2.159" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-2.159" y1="0.635" x2="-1.651" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.159" y1="-0.635" x2="-1.651" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="1.651" y1="1.143" x2="-1.651" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-0.635" x2="2.159" y2="0.635" width="0.1524" layer="51"/>
<wire x1="1.651" y1="-1.143" x2="-1.651" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="1.651" y1="1.143" x2="2.159" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="1.651" y1="-1.143" x2="2.159" y2="-0.635" width="0.1524" layer="21" curve="90"/>
<wire x1="-0.3048" y1="0.762" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0.762" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="1.27" y1="0" x2="0.3302" y2="0" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0" x2="-0.3048" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.159" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.159" y="-2.667" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="2.159" y1="-0.381" x2="2.54" y2="0.381" layer="51"/>
<rectangle x1="-2.54" y1="-0.381" x2="-2.159" y2="0.381" layer="51"/>
</package>
<package name="C050-025X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 2.5 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="1.016" x2="-3.683" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-1.27" x2="3.429" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-1.016" x2="3.683" y2="1.016" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.27" x2="-3.429" y2="1.27" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.27" x2="3.683" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-1.27" x2="3.683" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-1.016" x2="-3.429" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="1.016" x2="-3.429" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.429" y="1.651" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.794" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-045X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 4.5 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="2.032" x2="-3.683" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-2.286" x2="3.429" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-2.032" x2="3.683" y2="2.032" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.286" x2="-3.429" y2="2.286" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.286" x2="3.683" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-2.286" x2="3.683" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-2.032" x2="-3.429" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="2.032" x2="-3.429" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.556" y="2.667" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.556" y="-3.81" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-030X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 3 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="1.27" x2="-3.683" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-1.524" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-1.27" x2="3.683" y2="1.27" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.524" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.524" x2="3.683" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-1.524" x2="3.683" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-1.27" x2="-3.429" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="1.27" x2="-3.429" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.556" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.556" y="-3.048" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-050X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 5 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="2.286" x2="-3.683" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-2.54" x2="3.429" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-2.286" x2="3.683" y2="2.286" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.54" x2="-3.429" y2="2.54" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.54" x2="3.683" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-2.54" x2="3.683" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-2.286" x2="-3.429" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="2.286" x2="-3.429" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.429" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-2.159" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-055X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 5.5 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="2.54" x2="-3.683" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-2.794" x2="3.429" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-2.54" x2="3.683" y2="2.54" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.794" x2="-3.429" y2="2.794" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.794" x2="3.683" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-2.794" x2="3.683" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-2.54" x2="-3.429" y2="-2.794" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="2.54" x2="-3.429" y2="2.794" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.429" y="3.175" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.302" y="-2.286" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-075X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 7.5 x 7.5 mm</description>
<wire x1="-1.524" y1="0" x2="-0.4572" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.4572" y1="0" x2="-0.4572" y2="0.762" width="0.4064" layer="21"/>
<wire x1="-0.4572" y1="0" x2="-0.4572" y2="-0.762" width="0.4064" layer="21"/>
<wire x1="0.4318" y1="0.762" x2="0.4318" y2="0" width="0.4064" layer="21"/>
<wire x1="0.4318" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.4318" y1="0" x2="0.4318" y2="-0.762" width="0.4064" layer="21"/>
<wire x1="-3.683" y1="3.429" x2="-3.683" y2="-3.429" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-3.683" x2="3.429" y2="-3.683" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-3.429" x2="3.683" y2="3.429" width="0.1524" layer="21"/>
<wire x1="3.429" y1="3.683" x2="-3.429" y2="3.683" width="0.1524" layer="21"/>
<wire x1="3.429" y1="3.683" x2="3.683" y2="3.429" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-3.683" x2="3.683" y2="-3.429" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-3.429" x2="-3.429" y2="-3.683" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="3.429" x2="-3.429" y2="3.683" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.429" y="4.064" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-2.921" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050H075X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
Horizontal, grid 5 mm, outline 7.5 x 7.5 mm</description>
<wire x1="-3.683" y1="7.112" x2="-3.683" y2="0.508" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="0.508" x2="-3.302" y2="0.508" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="0.508" x2="-1.778" y2="0.508" width="0.1524" layer="51"/>
<wire x1="-1.778" y1="0.508" x2="1.778" y2="0.508" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0.508" x2="3.302" y2="0.508" width="0.1524" layer="51"/>
<wire x1="3.302" y1="0.508" x2="3.683" y2="0.508" width="0.1524" layer="21"/>
<wire x1="3.683" y1="0.508" x2="3.683" y2="7.112" width="0.1524" layer="21"/>
<wire x1="3.175" y1="7.62" x2="-3.175" y2="7.62" width="0.1524" layer="21"/>
<wire x1="-0.3048" y1="2.413" x2="-0.3048" y2="1.778" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="1.778" x2="-0.3048" y2="1.143" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="1.778" x2="-1.651" y2="1.778" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="2.413" x2="0.3302" y2="1.778" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="1.778" x2="0.3302" y2="1.143" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="1.778" x2="1.651" y2="1.778" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="7.112" x2="-3.175" y2="7.62" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.175" y1="7.62" x2="3.683" y2="7.112" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="0.254" width="0.508" layer="51"/>
<wire x1="2.54" y1="0" x2="2.54" y2="0.254" width="0.508" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.302" y="8.001" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="3.175" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.794" y1="0.127" x2="-2.286" y2="0.508" layer="51"/>
<rectangle x1="2.286" y1="0.127" x2="2.794" y2="0.508" layer="51"/>
</package>
<package name="C075-032X103">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 7.5 mm, outline 3.2 x 10.3 mm</description>
<wire x1="4.826" y1="1.524" x2="-4.826" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="-1.524" x2="4.826" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="21"/>
<wire x1="4.826" y1="1.524" x2="5.08" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.826" y1="-1.524" x2="5.08" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="-1.27" x2="-4.826" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="1.27" x2="-4.826" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="0.508" y1="0" x2="2.54" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0" x2="-0.508" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="0.889" x2="-0.508" y2="0" width="0.4064" layer="21"/>
<wire x1="-0.508" y1="0" x2="-0.508" y2="-0.889" width="0.4064" layer="21"/>
<wire x1="0.508" y1="0.889" x2="0.508" y2="0" width="0.4064" layer="21"/>
<wire x1="0.508" y1="0" x2="0.508" y2="-0.889" width="0.4064" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.9144" shape="octagon"/>
<text x="-4.826" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.826" y="-3.048" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C075-042X103">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 7.5 mm, outline 4.2 x 10.3 mm</description>
<wire x1="4.826" y1="2.032" x2="-4.826" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.778" x2="-5.08" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="-2.032" x2="4.826" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.778" x2="5.08" y2="1.778" width="0.1524" layer="21"/>
<wire x1="4.826" y1="2.032" x2="5.08" y2="1.778" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.826" y1="-2.032" x2="5.08" y2="-1.778" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="-1.778" x2="-4.826" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="1.778" x2="-4.826" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.27" y1="0" x2="2.667" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.667" y1="0" x2="-2.159" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="1.27" x2="-2.159" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.159" y1="0" x2="-2.159" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-1.27" width="0.4064" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.9144" shape="octagon"/>
<text x="-4.699" y="2.413" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.635" y="-1.651" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C075-052X106">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 7.5 mm, outline 5.2 x 10.6 mm</description>
<wire x1="4.953" y1="2.54" x2="-4.953" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="2.286" x2="-5.207" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="-2.54" x2="4.953" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="5.207" y1="-2.286" x2="5.207" y2="2.286" width="0.1524" layer="21"/>
<wire x1="4.953" y1="2.54" x2="5.207" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.953" y1="-2.54" x2="5.207" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.207" y1="-2.286" x2="-4.953" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.207" y1="2.286" x2="-4.953" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.27" y1="0" x2="2.667" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.667" y1="0" x2="-2.159" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="1.27" x2="-2.159" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.159" y1="0" x2="-2.159" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-1.27" width="0.4064" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.9144" shape="octagon"/>
<text x="-4.826" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.635" y="-2.032" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C102-043X133">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 10.2 mm, outline 4.3 x 13.3 mm</description>
<wire x1="-3.175" y1="1.27" x2="-3.175" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.286" y1="1.27" x2="-2.286" y2="0" width="0.4064" layer="21"/>
<wire x1="3.81" y1="0" x2="-2.286" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="0" x2="-2.286" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-3.81" y1="0" x2="-3.175" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="0" x2="-3.175" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-6.096" y1="2.032" x2="6.096" y2="2.032" width="0.1524" layer="21"/>
<wire x1="6.604" y1="1.524" x2="6.604" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-2.032" x2="-6.096" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-6.604" y1="-1.524" x2="-6.604" y2="1.524" width="0.1524" layer="21"/>
<wire x1="6.096" y1="2.032" x2="6.604" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.096" y1="-2.032" x2="6.604" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="-1.524" x2="-6.096" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="1.524" x2="-6.096" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="1.016" shape="octagon"/>
<text x="-6.096" y="2.413" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.524" y="-1.651" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C102-054X133">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 10.2 mm, outline 5.4 x 13.3 mm</description>
<wire x1="-3.175" y1="1.27" x2="-3.175" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.286" y1="1.27" x2="-2.286" y2="0" width="0.4064" layer="21"/>
<wire x1="3.81" y1="0" x2="-2.286" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="0" x2="-2.286" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-3.81" y1="0" x2="-3.175" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="0" x2="-3.175" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-6.096" y1="2.54" x2="6.096" y2="2.54" width="0.1524" layer="21"/>
<wire x1="6.604" y1="2.032" x2="6.604" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-2.54" x2="-6.096" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-6.604" y1="-2.032" x2="-6.604" y2="2.032" width="0.1524" layer="21"/>
<wire x1="6.096" y1="2.54" x2="6.604" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.096" y1="-2.54" x2="6.604" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="-2.032" x2="-6.096" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="2.032" x2="-6.096" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="1.016" shape="octagon"/>
<text x="-6.096" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.524" y="-1.905" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C102-064X133">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 10.2 mm, outline 6.4 x 13.3 mm</description>
<wire x1="-3.175" y1="1.27" x2="-3.175" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.286" y1="1.27" x2="-2.286" y2="0" width="0.4064" layer="21"/>
<wire x1="3.81" y1="0" x2="-2.286" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="0" x2="-2.286" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-3.81" y1="0" x2="-3.175" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="0" x2="-3.175" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-6.096" y1="3.048" x2="6.096" y2="3.048" width="0.1524" layer="21"/>
<wire x1="6.604" y1="2.54" x2="6.604" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-3.048" x2="-6.096" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-6.604" y1="-2.54" x2="-6.604" y2="2.54" width="0.1524" layer="21"/>
<wire x1="6.096" y1="3.048" x2="6.604" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.096" y1="-3.048" x2="6.604" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="-2.54" x2="-6.096" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="2.54" x2="-6.096" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="1.016" shape="octagon"/>
<text x="-6.096" y="3.429" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.524" y="-2.032" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C102_152-062X184">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 10.2 mm + 15.2 mm, outline 6.2 x 18.4 mm</description>
<wire x1="-2.286" y1="1.27" x2="-2.286" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.286" y1="0" x2="-2.286" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-3.175" y1="1.27" x2="-3.175" y2="0" width="0.4064" layer="21"/>
<wire x1="-3.175" y1="0" x2="-3.175" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-3.683" y1="0" x2="-3.175" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="0" x2="3.683" y2="0" width="0.1524" layer="21"/>
<wire x1="6.477" y1="0" x2="8.636" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.096" y1="3.048" x2="6.223" y2="3.048" width="0.1524" layer="21"/>
<wire x1="6.223" y1="-3.048" x2="-6.096" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-6.604" y1="-2.54" x2="-6.604" y2="2.54" width="0.1524" layer="21"/>
<wire x1="6.223" y1="3.048" x2="6.731" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.223" y1="-3.048" x2="6.731" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="-2.54" x2="-6.096" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="2.54" x2="-6.096" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.731" y1="2.54" x2="6.731" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="11.176" y1="3.048" x2="11.684" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="11.176" y1="-3.048" x2="11.684" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="11.176" y1="-3.048" x2="7.112" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="7.112" y1="3.048" x2="11.176" y2="3.048" width="0.1524" layer="21"/>
<wire x1="11.684" y1="2.54" x2="11.684" y2="-2.54" width="0.1524" layer="21"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="3" x="10.033" y="0" drill="1.016" shape="octagon"/>
<text x="-5.969" y="3.429" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.524" y="-2.286" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C150-054X183">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 15 mm, outline 5.4 x 18.3 mm</description>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="0" width="0.4064" layer="21"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="1.27" x2="-4.191" y2="0" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="-4.191" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0" x2="-6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="9.017" y1="2.032" x2="9.017" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-2.54" x2="-8.509" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-9.017" y1="-2.032" x2="-9.017" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-8.509" y1="2.54" x2="8.509" y2="2.54" width="0.1524" layer="21"/>
<wire x1="8.509" y1="2.54" x2="9.017" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.509" y1="-2.54" x2="9.017" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="-2.032" x2="-8.509" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="2.032" x2="-8.509" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-7.493" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.493" y="0" drill="1.016" shape="octagon"/>
<text x="-8.382" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.032" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C150-064X183">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 15 mm, outline 6.4 x 18.3 mm</description>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="0" width="0.4064" layer="21"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="1.27" x2="-4.191" y2="0" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="-4.191" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0" x2="-6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="9.017" y1="2.54" x2="9.017" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-3.048" x2="-8.509" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-9.017" y1="-2.54" x2="-9.017" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-8.509" y1="3.048" x2="8.509" y2="3.048" width="0.1524" layer="21"/>
<wire x1="8.509" y1="3.048" x2="9.017" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.509" y1="-3.048" x2="9.017" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="-2.54" x2="-8.509" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="2.54" x2="-8.509" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-7.493" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.493" y="0" drill="1.016" shape="octagon"/>
<text x="-8.509" y="3.429" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.032" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C150-072X183">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 15 mm, outline 7.2 x 18.3 mm</description>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="0" width="0.4064" layer="21"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="1.27" x2="-4.191" y2="0" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="-4.191" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0" x2="-6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="9.017" y1="3.048" x2="9.017" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-3.556" x2="-8.509" y2="-3.556" width="0.1524" layer="21"/>
<wire x1="-9.017" y1="-3.048" x2="-9.017" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-8.509" y1="3.556" x2="8.509" y2="3.556" width="0.1524" layer="21"/>
<wire x1="8.509" y1="3.556" x2="9.017" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.509" y1="-3.556" x2="9.017" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="-3.048" x2="-8.509" y2="-3.556" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="3.048" x2="-8.509" y2="3.556" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-7.493" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.493" y="0" drill="1.016" shape="octagon"/>
<text x="-8.509" y="3.937" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.286" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C150-084X183">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 15 mm, outline 8.4 x 18.3 mm</description>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="0" width="0.4064" layer="21"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="1.27" x2="-4.191" y2="0" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="-4.191" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0" x2="-6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="9.017" y1="3.556" x2="9.017" y2="-3.556" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-4.064" x2="-8.509" y2="-4.064" width="0.1524" layer="21"/>
<wire x1="-9.017" y1="-3.556" x2="-9.017" y2="3.556" width="0.1524" layer="21"/>
<wire x1="-8.509" y1="4.064" x2="8.509" y2="4.064" width="0.1524" layer="21"/>
<wire x1="8.509" y1="4.064" x2="9.017" y2="3.556" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.509" y1="-4.064" x2="9.017" y2="-3.556" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="-3.556" x2="-8.509" y2="-4.064" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="3.556" x2="-8.509" y2="4.064" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-7.493" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.493" y="0" drill="1.016" shape="octagon"/>
<text x="-8.509" y="4.445" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.54" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C150-091X182">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 15 mm, outline 9.1 x 18.2 mm</description>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="0" width="0.4064" layer="21"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="1.27" x2="-4.191" y2="0" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="-4.191" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0" x2="-6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="9.017" y1="3.937" x2="9.017" y2="-3.937" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-4.445" x2="-8.509" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="-9.017" y1="-3.937" x2="-9.017" y2="3.937" width="0.1524" layer="21"/>
<wire x1="-8.509" y1="4.445" x2="8.509" y2="4.445" width="0.1524" layer="21"/>
<wire x1="8.509" y1="4.445" x2="9.017" y2="3.937" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.509" y1="-4.445" x2="9.017" y2="-3.937" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="-3.937" x2="-8.509" y2="-4.445" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="3.937" x2="-8.509" y2="4.445" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-7.493" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.493" y="0" drill="1.016" shape="octagon"/>
<text x="-8.509" y="4.826" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.54" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C225-062X268">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 22.5 mm, outline 6.2 x 26.8 mm</description>
<wire x1="-12.827" y1="3.048" x2="12.827" y2="3.048" width="0.1524" layer="21"/>
<wire x1="13.335" y1="2.54" x2="13.335" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="12.827" y1="-3.048" x2="-12.827" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-2.54" x2="-13.335" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="12.827" y1="3.048" x2="13.335" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="12.827" y1="-3.048" x2="13.335" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="-2.54" x2="-12.827" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="2.54" x2="-12.827" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="-9.652" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="9.652" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-11.303" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.303" y="0" drill="1.016" shape="octagon"/>
<text x="-12.7" y="3.429" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C225-074X268">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 22.5 mm, outline 7.4 x 26.8 mm</description>
<wire x1="-12.827" y1="3.556" x2="12.827" y2="3.556" width="0.1524" layer="21"/>
<wire x1="13.335" y1="3.048" x2="13.335" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="12.827" y1="-3.556" x2="-12.827" y2="-3.556" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-3.048" x2="-13.335" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="12.827" y1="3.556" x2="13.335" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="12.827" y1="-3.556" x2="13.335" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="-3.048" x2="-12.827" y2="-3.556" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="3.048" x2="-12.827" y2="3.556" width="0.1524" layer="21" curve="-90"/>
<wire x1="-9.652" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="9.652" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-11.303" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.303" y="0" drill="1.016" shape="octagon"/>
<text x="-12.827" y="3.937" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C225-087X268">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 22.5 mm, outline 8.7 x 26.8 mm</description>
<wire x1="-12.827" y1="4.318" x2="12.827" y2="4.318" width="0.1524" layer="21"/>
<wire x1="13.335" y1="3.81" x2="13.335" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="12.827" y1="-4.318" x2="-12.827" y2="-4.318" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-3.81" x2="-13.335" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="12.827" y1="4.318" x2="13.335" y2="3.81" width="0.1524" layer="21" curve="-90"/>
<wire x1="12.827" y1="-4.318" x2="13.335" y2="-3.81" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="-3.81" x2="-12.827" y2="-4.318" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="3.81" x2="-12.827" y2="4.318" width="0.1524" layer="21" curve="-90"/>
<wire x1="-9.652" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="9.652" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-11.303" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.303" y="0" drill="1.016" shape="octagon"/>
<text x="-12.827" y="4.699" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C225-108X268">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 22.5 mm, outline 10.8 x 26.8 mm</description>
<wire x1="-12.827" y1="5.334" x2="12.827" y2="5.334" width="0.1524" layer="21"/>
<wire x1="13.335" y1="4.826" x2="13.335" y2="-4.826" width="0.1524" layer="21"/>
<wire x1="12.827" y1="-5.334" x2="-12.827" y2="-5.334" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-4.826" x2="-13.335" y2="4.826" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="12.827" y1="5.334" x2="13.335" y2="4.826" width="0.1524" layer="21" curve="-90"/>
<wire x1="12.827" y1="-5.334" x2="13.335" y2="-4.826" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="-4.826" x2="-12.827" y2="-5.334" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="4.826" x2="-12.827" y2="5.334" width="0.1524" layer="21" curve="-90"/>
<wire x1="-9.652" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="9.652" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-11.303" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.303" y="0" drill="1.016" shape="octagon"/>
<text x="-12.954" y="5.715" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C225-113X268">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 22.5 mm, outline 11.3 x 26.8 mm</description>
<wire x1="-12.827" y1="5.588" x2="12.827" y2="5.588" width="0.1524" layer="21"/>
<wire x1="13.335" y1="5.08" x2="13.335" y2="-5.08" width="0.1524" layer="21"/>
<wire x1="12.827" y1="-5.588" x2="-12.827" y2="-5.588" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-5.08" x2="-13.335" y2="5.08" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="12.827" y1="5.588" x2="13.335" y2="5.08" width="0.1524" layer="21" curve="-90"/>
<wire x1="12.827" y1="-5.588" x2="13.335" y2="-5.08" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="-5.08" x2="-12.827" y2="-5.588" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="5.08" x2="-12.827" y2="5.588" width="0.1524" layer="21" curve="-90"/>
<wire x1="-9.652" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="9.652" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-11.303" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.303" y="0" drill="1.016" shape="octagon"/>
<text x="-12.954" y="5.969" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-093X316">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 9.3 x 31.6 mm</description>
<wire x1="-15.24" y1="4.572" x2="15.24" y2="4.572" width="0.1524" layer="21"/>
<wire x1="15.748" y1="4.064" x2="15.748" y2="-4.064" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-4.572" x2="-15.24" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-4.064" x2="-15.748" y2="4.064" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="4.572" x2="15.748" y2="4.064" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-4.572" x2="15.748" y2="-4.064" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-4.064" x2="-15.24" y2="-4.572" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="4.064" x2="-15.24" y2="4.572" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="4.953" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-113X316">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 11.3 x 31.6 mm</description>
<wire x1="-15.24" y1="5.588" x2="15.24" y2="5.588" width="0.1524" layer="21"/>
<wire x1="15.748" y1="5.08" x2="15.748" y2="-5.08" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-5.588" x2="-15.24" y2="-5.588" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-5.08" x2="-15.748" y2="5.08" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="5.588" x2="15.748" y2="5.08" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-5.588" x2="15.748" y2="-5.08" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-5.08" x2="-15.24" y2="-5.588" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="5.08" x2="-15.24" y2="5.588" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="5.969" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-134X316">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 13.4 x 31.6 mm</description>
<wire x1="-15.24" y1="6.604" x2="15.24" y2="6.604" width="0.1524" layer="21"/>
<wire x1="15.748" y1="6.096" x2="15.748" y2="-6.096" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-6.604" x2="-15.24" y2="-6.604" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-6.096" x2="-15.748" y2="6.096" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="6.604" x2="15.748" y2="6.096" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-6.604" x2="15.748" y2="-6.096" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-6.096" x2="-15.24" y2="-6.604" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="6.096" x2="-15.24" y2="6.604" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="6.985" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-205X316">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 20.5 x 31.6 mm</description>
<wire x1="-15.24" y1="10.16" x2="15.24" y2="10.16" width="0.1524" layer="21"/>
<wire x1="15.748" y1="9.652" x2="15.748" y2="-9.652" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-10.16" x2="-15.24" y2="-10.16" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-9.652" x2="-15.748" y2="9.652" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="10.16" x2="15.748" y2="9.652" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-10.16" x2="15.748" y2="-9.652" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-9.652" x2="-15.24" y2="-10.16" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="9.652" x2="-15.24" y2="10.16" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="10.541" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-4.318" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C325-137X374">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 32.5 mm, outline 13.7 x 37.4 mm</description>
<wire x1="-14.2748" y1="0" x2="-12.7" y2="0" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="1.905" x2="-12.7" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="1.905" x2="-11.811" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="0" x2="14.2748" y2="0" width="0.1524" layer="21"/>
<wire x1="-11.811" y1="0" x2="-11.811" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-12.7" y1="0" x2="-12.7" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="18.542" y1="6.731" x2="18.542" y2="-6.731" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="6.731" x2="-18.542" y2="-6.731" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="-6.731" x2="18.542" y2="-6.731" width="0.1524" layer="21"/>
<wire x1="18.542" y1="6.731" x2="-18.542" y2="6.731" width="0.1524" layer="21"/>
<pad name="1" x="-16.256" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="16.256" y="0" drill="1.1938" shape="octagon"/>
<text x="-18.2372" y="7.0612" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-10.8458" y="-2.8702" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C325-162X374">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 32.5 mm, outline 16.2 x 37.4 mm</description>
<wire x1="-14.2748" y1="0" x2="-12.7" y2="0" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="1.905" x2="-12.7" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="1.905" x2="-11.811" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="0" x2="14.2748" y2="0" width="0.1524" layer="21"/>
<wire x1="-11.811" y1="0" x2="-11.811" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-12.7" y1="0" x2="-12.7" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="18.542" y1="8.001" x2="18.542" y2="-8.001" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="8.001" x2="-18.542" y2="-8.001" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="-8.001" x2="18.542" y2="-8.001" width="0.1524" layer="21"/>
<wire x1="18.542" y1="8.001" x2="-18.542" y2="8.001" width="0.1524" layer="21"/>
<pad name="1" x="-16.256" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="16.256" y="0" drill="1.1938" shape="octagon"/>
<text x="-18.3642" y="8.3312" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-10.8458" y="-2.8702" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C325-182X374">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 32.5 mm, outline 18.2 x 37.4 mm</description>
<wire x1="-14.2748" y1="0" x2="-12.7" y2="0" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="1.905" x2="-12.7" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="1.905" x2="-11.811" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="0" x2="14.2748" y2="0" width="0.1524" layer="21"/>
<wire x1="-11.811" y1="0" x2="-11.811" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-12.7" y1="0" x2="-12.7" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="18.542" y1="9.017" x2="18.542" y2="-9.017" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="9.017" x2="-18.542" y2="-9.017" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="-9.017" x2="18.542" y2="-9.017" width="0.1524" layer="21"/>
<wire x1="18.542" y1="9.017" x2="-18.542" y2="9.017" width="0.1524" layer="21"/>
<pad name="1" x="-16.256" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="16.256" y="0" drill="1.1938" shape="octagon"/>
<text x="-18.3642" y="9.3472" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-10.8458" y="-2.8702" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C375-192X418">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 37.5 mm, outline 19.2 x 41.8 mm</description>
<wire x1="-20.32" y1="8.509" x2="20.32" y2="8.509" width="0.1524" layer="21"/>
<wire x1="20.828" y1="8.001" x2="20.828" y2="-8.001" width="0.1524" layer="21"/>
<wire x1="20.32" y1="-8.509" x2="-20.32" y2="-8.509" width="0.1524" layer="21"/>
<wire x1="-20.828" y1="-8.001" x2="-20.828" y2="8.001" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="20.32" y1="8.509" x2="20.828" y2="8.001" width="0.1524" layer="21" curve="-90"/>
<wire x1="20.32" y1="-8.509" x2="20.828" y2="-8.001" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="-8.001" x2="-20.32" y2="-8.509" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="8.001" x2="-20.32" y2="8.509" width="0.1524" layer="21" curve="-90"/>
<wire x1="-16.002" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="16.002" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-18.796" y="0" drill="1.3208" shape="octagon"/>
<pad name="2" x="18.796" y="0" drill="1.3208" shape="octagon"/>
<text x="-20.447" y="8.89" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C375-203X418">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 37.5 mm, outline 20.3 x 41.8 mm</description>
<wire x1="-20.32" y1="10.16" x2="20.32" y2="10.16" width="0.1524" layer="21"/>
<wire x1="20.828" y1="9.652" x2="20.828" y2="-9.652" width="0.1524" layer="21"/>
<wire x1="20.32" y1="-10.16" x2="-20.32" y2="-10.16" width="0.1524" layer="21"/>
<wire x1="-20.828" y1="-9.652" x2="-20.828" y2="9.652" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="20.32" y1="10.16" x2="20.828" y2="9.652" width="0.1524" layer="21" curve="-90"/>
<wire x1="20.32" y1="-10.16" x2="20.828" y2="-9.652" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="-9.652" x2="-20.32" y2="-10.16" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="9.652" x2="-20.32" y2="10.16" width="0.1524" layer="21" curve="-90"/>
<wire x1="-16.002" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="16.002" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-18.796" y="0" drill="1.3208" shape="octagon"/>
<pad name="2" x="18.796" y="0" drill="1.3208" shape="octagon"/>
<text x="-20.32" y="10.541" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-035X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 3.5 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="1.524" x2="-3.683" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-1.778" x2="3.429" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-1.524" x2="3.683" y2="1.524" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.778" x2="-3.429" y2="1.778" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.778" x2="3.683" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-1.778" x2="3.683" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-1.524" x2="-3.429" y2="-1.778" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="1.524" x2="-3.429" y2="1.778" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.556" y="2.159" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.556" y="-3.429" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C375-155X418">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 37.5 mm, outline 15.5 x 41.8 mm</description>
<wire x1="-20.32" y1="7.62" x2="20.32" y2="7.62" width="0.1524" layer="21"/>
<wire x1="20.828" y1="7.112" x2="20.828" y2="-7.112" width="0.1524" layer="21"/>
<wire x1="20.32" y1="-7.62" x2="-20.32" y2="-7.62" width="0.1524" layer="21"/>
<wire x1="-20.828" y1="-7.112" x2="-20.828" y2="7.112" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="20.32" y1="7.62" x2="20.828" y2="7.112" width="0.1524" layer="21" curve="-90"/>
<wire x1="20.32" y1="-7.62" x2="20.828" y2="-7.112" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="-7.112" x2="-20.32" y2="-7.62" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="7.112" x2="-20.32" y2="7.62" width="0.1524" layer="21" curve="-90"/>
<wire x1="-16.002" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="16.002" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-18.796" y="0" drill="1.3208" shape="octagon"/>
<pad name="2" x="18.796" y="0" drill="1.3208" shape="octagon"/>
<text x="-20.447" y="8.001" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C075-063X106">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 7.5 mm, outline 6.3 x 10.6 mm</description>
<wire x1="4.953" y1="3.048" x2="-4.953" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="2.794" x2="-5.207" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="-3.048" x2="4.953" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="5.207" y1="-2.794" x2="5.207" y2="2.794" width="0.1524" layer="21"/>
<wire x1="4.953" y1="3.048" x2="5.207" y2="2.794" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.953" y1="-3.048" x2="5.207" y2="-2.794" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.207" y1="-2.794" x2="-4.953" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.207" y1="2.794" x2="-4.953" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.27" y1="0" x2="2.667" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.667" y1="0" x2="-2.159" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="1.27" x2="-2.159" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.159" y1="0" x2="-2.159" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-1.27" width="0.4064" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.9144" shape="octagon"/>
<text x="-4.826" y="3.429" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-154X316">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 15.4 x 31.6 mm</description>
<wire x1="-15.24" y1="7.62" x2="15.24" y2="7.62" width="0.1524" layer="21"/>
<wire x1="15.748" y1="7.112" x2="15.748" y2="-7.112" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-7.62" x2="-15.24" y2="-7.62" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-7.112" x2="-15.748" y2="7.112" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="7.62" x2="15.748" y2="7.112" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-7.62" x2="15.748" y2="-7.112" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-7.112" x2="-15.24" y2="-7.62" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="7.112" x2="-15.24" y2="7.62" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="8.001" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-173X316">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 17.3 x 31.6 mm</description>
<wire x1="-15.24" y1="8.509" x2="15.24" y2="8.509" width="0.1524" layer="21"/>
<wire x1="15.748" y1="8.001" x2="15.748" y2="-8.001" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-8.509" x2="-15.24" y2="-8.509" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-8.001" x2="-15.748" y2="8.001" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="8.509" x2="15.748" y2="8.001" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-8.509" x2="15.748" y2="-8.001" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-8.001" x2="-15.24" y2="-8.509" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="8.001" x2="-15.24" y2="8.509" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="8.89" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C0402K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 0204 reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 1005</description>
<wire x1="-0.425" y1="0.2" x2="0.425" y2="0.2" width="0.1016" layer="51"/>
<wire x1="0.425" y1="-0.2" x2="-0.425" y2="-0.2" width="0.1016" layer="51"/>
<smd name="1" x="-0.6" y="0" dx="0.925" dy="0.74" layer="1"/>
<smd name="2" x="0.6" y="0" dx="0.925" dy="0.74" layer="1"/>
<text x="-0.5" y="0.425" size="1.016" layer="25">&gt;NAME</text>
<text x="-0.5" y="-1.45" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.5" y1="-0.25" x2="-0.225" y2="0.25" layer="51"/>
<rectangle x1="0.225" y1="-0.25" x2="0.5" y2="0.25" layer="51"/>
</package>
<package name="C0603K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 0603 reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 1608</description>
<wire x1="-0.725" y1="0.35" x2="0.725" y2="0.35" width="0.1016" layer="51"/>
<wire x1="0.725" y1="-0.35" x2="-0.725" y2="-0.35" width="0.1016" layer="51"/>
<smd name="1" x="-0.875" y="0" dx="1.05" dy="1.08" layer="1"/>
<smd name="2" x="0.875" y="0" dx="1.05" dy="1.08" layer="1"/>
<text x="-0.8" y="0.65" size="1.016" layer="25">&gt;NAME</text>
<text x="-0.8" y="-1.65" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.8" y1="-0.4" x2="-0.45" y2="0.4" layer="51"/>
<rectangle x1="0.45" y1="-0.4" x2="0.8" y2="0.4" layer="51"/>
</package>
<package name="C0805K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 0805 reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 2012</description>
<wire x1="-0.925" y1="0.6" x2="0.925" y2="0.6" width="0.1016" layer="51"/>
<wire x1="0.925" y1="-0.6" x2="-0.925" y2="-0.6" width="0.1016" layer="51"/>
<smd name="1" x="-1" y="0" dx="1.3" dy="1.6" layer="1"/>
<smd name="2" x="1" y="0" dx="1.3" dy="1.6" layer="1"/>
<text x="-1" y="0.875" size="1.016" layer="25">&gt;NAME</text>
<text x="-1" y="-1.9" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1" y1="-0.65" x2="-0.5" y2="0.65" layer="51"/>
<rectangle x1="0.5" y1="-0.65" x2="1" y2="0.65" layer="51"/>
</package>
<package name="C1206K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 1206 reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 3216</description>
<wire x1="-1.525" y1="0.75" x2="1.525" y2="0.75" width="0.1016" layer="51"/>
<wire x1="1.525" y1="-0.75" x2="-1.525" y2="-0.75" width="0.1016" layer="51"/>
<smd name="1" x="-1.5" y="0" dx="1.5" dy="2" layer="1"/>
<smd name="2" x="1.5" y="0" dx="1.5" dy="2" layer="1"/>
<text x="-1.6" y="1.1" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.6" y="-2.1" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6" y1="-0.8" x2="-1.1" y2="0.8" layer="51"/>
<rectangle x1="1.1" y1="-0.8" x2="1.6" y2="0.8" layer="51"/>
</package>
<package name="C1210K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 1210 reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 3225</description>
<wire x1="-1.525" y1="1.175" x2="1.525" y2="1.175" width="0.1016" layer="51"/>
<wire x1="1.525" y1="-1.175" x2="-1.525" y2="-1.175" width="0.1016" layer="51"/>
<smd name="1" x="-1.5" y="0" dx="1.5" dy="2.9" layer="1"/>
<smd name="2" x="1.5" y="0" dx="1.5" dy="2.9" layer="1"/>
<text x="-1.6" y="1.55" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.6" y="-2.575" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6" y1="-1.25" x2="-1.1" y2="1.25" layer="51"/>
<rectangle x1="1.1" y1="-1.25" x2="1.6" y2="1.25" layer="51"/>
</package>
<package name="C1812K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 1812 reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 4532</description>
<wire x1="-2.175" y1="1.525" x2="2.175" y2="1.525" width="0.1016" layer="51"/>
<wire x1="2.175" y1="-1.525" x2="-2.175" y2="-1.525" width="0.1016" layer="51"/>
<smd name="1" x="-2.05" y="0" dx="1.8" dy="3.7" layer="1"/>
<smd name="2" x="2.05" y="0" dx="1.8" dy="3.7" layer="1"/>
<text x="-2.25" y="1.95" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.25" y="-2.975" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-2.25" y1="-1.6" x2="-1.65" y2="1.6" layer="51"/>
<rectangle x1="1.65" y1="-1.6" x2="2.25" y2="1.6" layer="51"/>
</package>
<package name="C1825K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 1825 reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 4564</description>
<wire x1="-1.525" y1="3.125" x2="1.525" y2="3.125" width="0.1016" layer="51"/>
<wire x1="1.525" y1="-3.125" x2="-1.525" y2="-3.125" width="0.1016" layer="51"/>
<smd name="1" x="-1.5" y="0" dx="1.8" dy="6.9" layer="1"/>
<smd name="2" x="1.5" y="0" dx="1.8" dy="6.9" layer="1"/>
<text x="-1.6" y="3.55" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.6" y="-4.625" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6" y1="-3.2" x2="-1.1" y2="3.2" layer="51"/>
<rectangle x1="1.1" y1="-3.2" x2="1.6" y2="3.2" layer="51"/>
</package>
<package name="C2220K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 2220 reflow solder&lt;/b&gt;&lt;p&gt;Metric Code Size 5650</description>
<wire x1="-2.725" y1="2.425" x2="2.725" y2="2.425" width="0.1016" layer="51"/>
<wire x1="2.725" y1="-2.425" x2="-2.725" y2="-2.425" width="0.1016" layer="51"/>
<smd name="1" x="-2.55" y="0" dx="1.85" dy="5.5" layer="1"/>
<smd name="2" x="2.55" y="0" dx="1.85" dy="5.5" layer="1"/>
<text x="-2.8" y="2.95" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.8" y="-3.975" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-2.8" y1="-2.5" x2="-2.2" y2="2.5" layer="51"/>
<rectangle x1="2.2" y1="-2.5" x2="2.8" y2="2.5" layer="51"/>
</package>
<package name="C2225K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 2225 reflow solder&lt;/b&gt;&lt;p&gt;Metric Code Size 5664</description>
<wire x1="-2.725" y1="3.075" x2="2.725" y2="3.075" width="0.1016" layer="51"/>
<wire x1="2.725" y1="-3.075" x2="-2.725" y2="-3.075" width="0.1016" layer="51"/>
<smd name="1" x="-2.55" y="0" dx="1.85" dy="6.8" layer="1"/>
<smd name="2" x="2.55" y="0" dx="1.85" dy="6.8" layer="1"/>
<text x="-2.8" y="3.6" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.8" y="-4.575" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-2.8" y1="-3.15" x2="-2.2" y2="3.15" layer="51"/>
<rectangle x1="2.2" y1="-3.15" x2="2.8" y2="3.15" layer="51"/>
</package>
<package name="HPC0201">
<description>&lt;b&gt; &lt;/b&gt;&lt;p&gt;
Source: http://www.vishay.com/docs/10129/hpc0201a.pdf</description>
<smd name="1" x="-0.18" y="0" dx="0.2" dy="0.35" layer="1"/>
<smd name="2" x="0.18" y="0" dx="0.2" dy="0.35" layer="1"/>
<text x="-0.75" y="0.74" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.785" y="-1.865" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.305" y1="-0.15" x2="0.305" y2="0.15" layer="51"/>
</package>
<package name="C0201">
<description>Source: http://www.avxcorp.com/docs/catalogs/cx5r.pdf</description>
<smd name="1" x="-0.25" y="0" dx="0.25" dy="0.35" layer="1"/>
<smd name="2" x="0.25" y="0" dx="0.25" dy="0.35" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.3" y1="-0.15" x2="-0.15" y2="0.15" layer="51"/>
<rectangle x1="0.15" y1="-0.15" x2="0.3" y2="0.15" layer="51"/>
<rectangle x1="-0.15" y1="0.1" x2="0.15" y2="0.15" layer="51"/>
<rectangle x1="-0.15" y1="-0.15" x2="0.15" y2="-0.1" layer="51"/>
</package>
<package name="C1808">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
Source: AVX .. aphvc.pdf</description>
<wire x1="-1.4732" y1="0.9502" x2="1.4732" y2="0.9502" width="0.1016" layer="51"/>
<wire x1="-1.4478" y1="-0.9502" x2="1.4732" y2="-0.9502" width="0.1016" layer="51"/>
<smd name="1" x="-1.95" y="0" dx="1.6" dy="2.2" layer="1"/>
<smd name="2" x="1.95" y="0" dx="1.6" dy="2.2" layer="1"/>
<text x="-2.233" y="1.827" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.233" y="-2.842" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.275" y1="-1.015" x2="-1.225" y2="1.015" layer="51"/>
<rectangle x1="1.225" y1="-1.015" x2="2.275" y2="1.015" layer="51"/>
</package>
<package name="C3640">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
Source: AVX .. aphvc.pdf</description>
<wire x1="-3.8322" y1="5.0496" x2="3.8322" y2="5.0496" width="0.1016" layer="51"/>
<wire x1="-3.8322" y1="-5.0496" x2="3.8322" y2="-5.0496" width="0.1016" layer="51"/>
<smd name="1" x="-4.267" y="0" dx="2.6" dy="10.7" layer="1"/>
<smd name="2" x="4.267" y="0" dx="2.6" dy="10.7" layer="1"/>
<text x="-4.647" y="6.465" size="1.27" layer="25">&gt;NAME</text>
<text x="-4.647" y="-7.255" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-4.57" y1="-5.1" x2="-3.05" y2="5.1" layer="51"/>
<rectangle x1="3.05" y1="-5.1" x2="4.5688" y2="5.1" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="R-EU">
<wire x1="-2.54" y1="-0.889" x2="2.54" y2="-0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="-0.889" x2="2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
<symbol name="C-EU">
<wire x1="0" y1="0" x2="0" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-2.032" width="0.1524" layer="94"/>
<text x="1.524" y="0.381" size="1.778" layer="95">&gt;NAME</text>
<text x="1.524" y="-4.699" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.032" y1="-2.032" x2="2.032" y2="-1.524" layer="94"/>
<rectangle x1="-2.032" y1="-1.016" x2="2.032" y2="-0.508" layer="94"/>
<pin name="1" x="0" y="2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="R-EU_" prefix="R" uservalue="yes">
<description>&lt;B&gt;RESISTOR&lt;/B&gt;, European symbol</description>
<gates>
<gate name="G$1" symbol="R-EU" x="0" y="0"/>
</gates>
<devices>
<device name="R0402" package="R0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R0603" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R0805" package="R0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R0805W" package="R0805W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R1005" package="R1005">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R1206" package="R1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R1206W" package="R1206W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R1210" package="R1210">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R1210W" package="R1210W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2010" package="R2010">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2010W" package="R2010W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2012" package="R2012">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2012W" package="R2012W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2512" package="R2512">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2512W" package="R2512W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R3216" package="R3216">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R3216W" package="R3216W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R3225" package="R3225">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R3225W" package="R3225W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R5025" package="R5025">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R5025W" package="R5025W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R6332" package="R6332">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R6332W" package="R6332W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M0805" package="M0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M1206" package="M1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M1406" package="M1406">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M2012" package="M2012">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M2309" package="M2309">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M3216" package="M3216">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M3516" package="M3516">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M5923" package="M5923">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0204/5" package="0204/5">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0204/7" package="0204/7">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0204/2V" package="0204V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/10" package="0207/10">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/12" package="0207/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/15" package="0207/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/2V" package="0207/2V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/5V" package="0207/5V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/7" package="0207/7">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0309/10" package="0309/10">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0309/12" package="0309/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0309/V" package="0309V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0411/12" package="0411/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0411/15" package="0411/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0411/3V" package="0411V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0414/15" package="0414/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0414/5V" package="0414V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0617/17" package="0617/17">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0617/22" package="0617/22">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0617/5V" package="0617V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0922/22" package="0922/22">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0613/5V" package="P0613V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0613/15" package="P0613/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0817/22" package="P0817/22">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0817/7V" package="P0817V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="V234/12" package="V234/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="V235/17" package="V235/17">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="V526-0" package="V526-0">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0102R" package="MINI_MELF-0102R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0102W" package="MINI_MELF-0102W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0204R" package="MINI_MELF-0204R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0204W" package="MINI_MELF-0204W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0207R" package="MINI_MELF-0207R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0207W" package="MINI_MELF-0207W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0922V" package="0922V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="RDH/15" package="RDH/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0102AX" package="MINI_MELF-0102AX">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R0201" package="R0201">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VTA52" package="VTA52">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VTA53" package="VTA53">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VTA54" package="VTA54">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VTA55" package="VTA55">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VTA56" package="VTA56">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VMTA55" package="VMTA55">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VMTB60" package="VMTB60">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R4527" package="R4527">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WSC0001" package="WSC0001">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WSC0002" package="WSC0002">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WSC01/2" package="WSC01/2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WSC2515" package="WSC2515">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WSC4527" package="WSC4527">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WSC6927" package="WSC6927">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R1218" package="R1218">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1812X7R" package="1812X7R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="C-EU" prefix="C" uservalue="yes">
<description>&lt;B&gt;CAPACITOR&lt;/B&gt;, European symbol</description>
<gates>
<gate name="G$1" symbol="C-EU" x="0" y="0"/>
</gates>
<devices>
<device name="C0402" package="C0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0504" package="C0504">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0603" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0805" package="C0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1005" package="C1005">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1206" package="C1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1210" package="C1210">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1310" package="C1310">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1608" package="C1608">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1812" package="C1812">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1825" package="C1825">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C2012" package="C2012">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C3216" package="C3216">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C3225" package="C3225">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C4532" package="C4532">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C4564" package="C4564">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025-024X044" package="C025-024X044">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025-025X050" package="C025-025X050">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025-030X050" package="C025-030X050">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025-040X050" package="C025-040X050">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025-050X050" package="C025-050X050">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025-060X050" package="C025-060X050">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C025_050-024X070" package="C025_050-024X070">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025_050-025X075" package="C025_050-025X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025_050-035X075" package="C025_050-035X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025_050-045X075" package="C025_050-045X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025_050-055X075" package="C025_050-055X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-024X044" package="C050-024X044">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-025X075" package="C050-025X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-045X075" package="C050-045X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-030X075" package="C050-030X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-050X075" package="C050-050X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-055X075" package="C050-055X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-075X075" package="C050-075X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050H075X075" package="C050H075X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="075-032X103" package="C075-032X103">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="075-042X103" package="C075-042X103">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="075-052X106" package="C075-052X106">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="102-043X133" package="C102-043X133">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="102-054X133" package="C102-054X133">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="102-064X133" package="C102-064X133">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="102_152-062X184" package="C102_152-062X184">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="150-054X183" package="C150-054X183">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="150-064X183" package="C150-064X183">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="150-072X183" package="C150-072X183">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="150-084X183" package="C150-084X183">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="150-091X182" package="C150-091X182">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="225-062X268" package="C225-062X268">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="225-074X268" package="C225-074X268">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="225-087X268" package="C225-087X268">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="225-108X268" package="C225-108X268">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="225-113X268" package="C225-113X268">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="275-093X316" package="C275-093X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="275-113X316" package="C275-113X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="275-134X316" package="C275-134X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="275-205X316" package="C275-205X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="325-137X374" package="C325-137X374">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="325-162X374" package="C325-162X374">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="325-182X374" package="C325-182X374">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="375-192X418" package="C375-192X418">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="375-203X418" package="C375-203X418">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-035X075" package="C050-035X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="375-155X418" package="C375-155X418">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="075-063X106" package="C075-063X106">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="275-154X316" package="C275-154X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="275-173X316" package="C275-173X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0402K" package="C0402K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0603K" package="C0603K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0805K" package="C0805K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1206K" package="C1206K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1210K" package="C1210K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1812K" package="C1812K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1825K" package="C1825K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C2220K" package="C2220K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C2225K" package="C2225K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="HPC0201" package="HPC0201">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0201" package="C0201">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1808" package="C1808">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C3640" package="C3640">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="jumper">
<packages>
<package name="JP2">
<description>&lt;b&gt;JUMPER&lt;/b&gt;</description>
<wire x1="-3.81" y1="-1.016" x2="-3.81" y2="1.016" width="0.1524" layer="21"/>
<wire x1="3.556" y1="1.27" x2="1.524" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.016" x2="1.524" y2="1.27" width="0.1524" layer="21"/>
<wire x1="3.556" y1="-1.27" x2="1.524" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-1.016" x2="1.524" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.556" y1="-1.27" x2="3.81" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="3.556" y1="1.27" x2="3.81" y2="1.016" width="0.1524" layer="21"/>
<wire x1="3.81" y1="1.016" x2="3.81" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="1.016" y1="1.27" x2="1.27" y2="1.016" width="0.1524" layer="21"/>
<wire x1="1.016" y1="1.27" x2="-1.016" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="1.016" x2="-1.016" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="1.27" x2="-1.27" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="1.016" x2="-3.556" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="1.27" x2="-3.556" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.016" y1="-1.27" x2="1.27" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.016" x2="-1.016" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="-1.27" x2="-1.27" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-1.016" x2="-3.556" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.016" y1="-1.27" x2="-1.016" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="-1.27" x2="-3.556" y2="-1.27" width="0.1524" layer="21"/>
<pad name="1" x="-2.54" y="0" drill="0.9144" shape="long" rot="R90"/>
<pad name="2" x="0" y="0" drill="0.9144" shape="long" rot="R90"/>
<pad name="3" x="2.54" y="0" drill="0.9144" shape="long" rot="R90"/>
<text x="-3.556" y="1.651" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-2.667" size="0.9906" layer="21" ratio="12">1</text>
<text x="0" y="-2.667" size="0.9906" layer="21" ratio="12">2</text>
<text x="2.54" y="-2.667" size="0.9906" layer="21" ratio="12">3</text>
<text x="-3.556" y="-4.318" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.8448" y1="-0.3048" x2="-2.2352" y2="0.3048" layer="51"/>
<rectangle x1="-0.3048" y1="-0.3048" x2="0.3048" y2="0.3048" layer="51"/>
<rectangle x1="2.2352" y1="-0.3048" x2="2.8448" y2="0.3048" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="JP3E">
<wire x1="2.54" y1="0" x2="2.54" y2="1.27" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="1.27" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="1.27" width="0.1524" layer="94"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="1.27" width="0.4064" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="1.27" width="0.4064" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-2.54" y2="1.27" width="0.4064" layer="94"/>
<wire x1="-3.175" y1="0" x2="3.175" y2="0" width="0.4064" layer="94"/>
<wire x1="3.175" y1="0" x2="3.175" y2="0.635" width="0.4064" layer="94"/>
<wire x1="3.175" y1="0.635" x2="-3.175" y2="0.635" width="0.4064" layer="94"/>
<wire x1="-3.175" y1="0.635" x2="-3.175" y2="0" width="0.4064" layer="94"/>
<text x="-3.81" y="0" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="5.715" y="0" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="1" x="-2.54" y="-2.54" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="2" x="0" y="-2.54" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="3" x="2.54" y="-2.54" visible="pad" length="short" direction="pas" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="JP2E" prefix="JP" uservalue="yes">
<description>&lt;b&gt;JUMPER&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="JP3E" x="0" y="0"/>
</gates>
<devices>
<device name="" package="JP2">
<connects>
<connect gate="1" pin="1" pad="1"/>
<connect gate="1" pin="2" pad="2"/>
<connect gate="1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="FMC-LPC">
<packages>
<package name="ASP-134604-01">
<wire x1="-27.8892" y1="7.3406" x2="27.8892" y2="7.3406" width="0.127" layer="51"/>
<wire x1="27.8892" y1="7.3406" x2="27.8892" y2="0.762" width="0.127" layer="51"/>
<wire x1="27.8892" y1="0.762" x2="28.7782" y2="0.762" width="0.127" layer="51"/>
<wire x1="28.7782" y1="0.762" x2="28.7782" y2="-0.762" width="0.127" layer="51"/>
<wire x1="28.7782" y1="-0.762" x2="27.8892" y2="-0.762" width="0.127" layer="51"/>
<wire x1="27.8892" y1="-0.762" x2="27.8892" y2="-7.3406" width="0.127" layer="51"/>
<wire x1="27.8892" y1="-7.3406" x2="-27.8892" y2="-7.3406" width="0.127" layer="51"/>
<wire x1="-27.8892" y1="-7.3406" x2="-27.8892" y2="7.3406" width="0.127" layer="51"/>
<wire x1="-28.575" y1="8.255" x2="28.575" y2="8.255" width="0.127" layer="21"/>
<wire x1="28.575" y1="8.255" x2="28.575" y2="1.27" width="0.127" layer="21"/>
<wire x1="28.575" y1="1.27" x2="29.21" y2="1.27" width="0.127" layer="21"/>
<wire x1="29.21" y1="1.27" x2="29.21" y2="-1.27" width="0.127" layer="21"/>
<wire x1="29.21" y1="-1.27" x2="28.575" y2="-1.27" width="0.127" layer="21"/>
<wire x1="28.575" y1="-1.27" x2="28.575" y2="-8.255" width="0.127" layer="21"/>
<wire x1="28.575" y1="-8.255" x2="-28.575" y2="-8.255" width="0.127" layer="21"/>
<wire x1="-28.575" y1="-8.255" x2="-28.575" y2="8.255" width="0.127" layer="21"/>
<wire x1="27.94" y1="8.255" x2="28.575" y2="7.62" width="0.127" layer="25"/>
<smd name="C1" x="24.765" y="3.175" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="C2" x="23.495" y="3.175" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="C3" x="22.225" y="3.175" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="C4" x="20.955" y="3.175" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="C5" x="19.685" y="3.175" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="C6" x="18.415" y="3.175" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="C7" x="17.145" y="3.175" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="C8" x="15.875" y="3.175" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="C9" x="14.605" y="3.175" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="C10" x="13.335" y="3.175" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="C11" x="12.065" y="3.175" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="C12" x="10.795" y="3.175" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="C13" x="9.525" y="3.175" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="C14" x="8.255" y="3.175" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="C15" x="6.985" y="3.175" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="C16" x="5.715" y="3.175" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="C17" x="4.445" y="3.175" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="C18" x="3.175" y="3.175" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="C19" x="1.905" y="3.175" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="C20" x="0.635" y="3.175" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="C21" x="-0.635" y="3.175" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="C22" x="-1.905" y="3.175" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="C23" x="-3.175" y="3.175" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="C24" x="-4.445" y="3.175" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="C25" x="-5.715" y="3.175" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="C26" x="-6.985" y="3.175" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="C27" x="-8.255" y="3.175" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="C28" x="-9.525" y="3.175" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="C29" x="-10.795" y="3.175" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="C30" x="-12.065" y="3.175" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="C31" x="-13.335" y="3.175" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="C32" x="-14.605" y="3.175" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="C33" x="-15.875" y="3.175" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="C34" x="-17.145" y="3.175" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="C35" x="-18.415" y="3.175" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="C36" x="-19.685" y="3.175" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="C37" x="-20.955" y="3.175" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="C38" x="-22.225" y="3.175" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="C39" x="-23.495" y="3.175" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="C40" x="-24.765" y="3.175" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="D1" x="24.765" y="1.905" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="D2" x="23.495" y="1.905" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="D3" x="22.225" y="1.905" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="D4" x="20.955" y="1.905" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="D5" x="19.685" y="1.905" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="D6" x="18.415" y="1.905" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="D7" x="17.145" y="1.905" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="D8" x="15.875" y="1.905" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="D9" x="14.605" y="1.905" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="D10" x="13.335" y="1.905" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="D11" x="12.065" y="1.905" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="D12" x="10.795" y="1.905" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="D13" x="9.525" y="1.905" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="D14" x="8.255" y="1.905" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="D15" x="6.985" y="1.905" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="D16" x="5.715" y="1.905" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="D17" x="4.445" y="1.905" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="D18" x="3.175" y="1.905" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="D19" x="1.905" y="1.905" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="D20" x="0.635" y="1.905" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="D21" x="-0.635" y="1.905" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="D22" x="-1.905" y="1.905" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="D23" x="-3.175" y="1.905" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="D24" x="-4.445" y="1.905" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="D25" x="-5.715" y="1.905" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="D26" x="-6.985" y="1.905" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="D27" x="-8.255" y="1.905" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="D28" x="-9.525" y="1.905" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="D29" x="-10.795" y="1.905" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="D30" x="-12.065" y="1.905" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="D31" x="-13.335" y="1.905" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="D32" x="-14.605" y="1.905" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="D33" x="-15.875" y="1.905" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="D34" x="-17.145" y="1.905" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="D35" x="-18.415" y="1.905" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="D36" x="-19.685" y="1.905" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="D37" x="-20.955" y="1.905" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="D38" x="-22.225" y="1.905" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="D39" x="-23.495" y="1.905" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="D40" x="-24.765" y="1.905" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="G1" x="24.765" y="-1.905" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="G2" x="23.495" y="-1.905" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="G3" x="22.225" y="-1.905" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="G4" x="20.955" y="-1.905" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="G5" x="19.685" y="-1.905" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="G6" x="18.415" y="-1.905" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="G7" x="17.145" y="-1.905" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="G8" x="15.875" y="-1.905" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="G9" x="14.605" y="-1.905" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="G10" x="13.335" y="-1.905" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="G11" x="12.065" y="-1.905" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="G12" x="10.795" y="-1.905" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="G13" x="9.525" y="-1.905" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="G14" x="8.255" y="-1.905" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="G15" x="6.985" y="-1.905" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="G16" x="5.715" y="-1.905" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="G17" x="4.445" y="-1.905" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="G18" x="3.175" y="-1.905" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="G19" x="1.905" y="-1.905" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="G20" x="0.635" y="-1.905" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="G21" x="-0.635" y="-1.905" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="G22" x="-1.905" y="-1.905" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="G23" x="-3.175" y="-1.905" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="G24" x="-4.445" y="-1.905" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="G25" x="-5.715" y="-1.905" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="G26" x="-6.985" y="-1.905" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="G27" x="-8.255" y="-1.905" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="G28" x="-9.525" y="-1.905" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="G29" x="-10.795" y="-1.905" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="G30" x="-12.065" y="-1.905" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="G31" x="-13.335" y="-1.905" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="G32" x="-14.605" y="-1.905" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="G33" x="-15.875" y="-1.905" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="G34" x="-17.145" y="-1.905" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="G35" x="-18.415" y="-1.905" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="G36" x="-19.685" y="-1.905" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="G37" x="-20.955" y="-1.905" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="G38" x="-22.225" y="-1.905" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="G39" x="-23.495" y="-1.905" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="G40" x="-24.765" y="-1.905" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="H1" x="24.765" y="-3.175" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="H2" x="23.495" y="-3.175" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="H3" x="22.225" y="-3.175" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="H4" x="20.955" y="-3.175" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="H5" x="19.685" y="-3.175" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="H6" x="18.415" y="-3.175" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="H7" x="17.145" y="-3.175" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="H8" x="15.875" y="-3.175" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="H9" x="14.605" y="-3.175" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="H10" x="13.335" y="-3.175" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="H11" x="12.065" y="-3.175" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="H12" x="10.795" y="-3.175" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="H13" x="9.525" y="-3.175" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="H14" x="8.255" y="-3.175" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="H15" x="6.985" y="-3.175" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="H16" x="5.715" y="-3.175" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="H17" x="4.445" y="-3.175" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="H18" x="3.175" y="-3.175" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="H19" x="1.905" y="-3.175" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="H20" x="0.635" y="-3.175" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="H21" x="-0.635" y="-3.175" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="H22" x="-1.905" y="-3.175" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="H23" x="-3.175" y="-3.175" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="H24" x="-4.445" y="-3.175" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="H25" x="-5.715" y="-3.175" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="H26" x="-6.985" y="-3.175" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="H27" x="-8.255" y="-3.175" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="H28" x="-9.525" y="-3.175" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="H29" x="-10.795" y="-3.175" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="H30" x="-12.065" y="-3.175" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="H31" x="-13.335" y="-3.175" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="H32" x="-14.605" y="-3.175" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="H33" x="-15.875" y="-3.175" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="H34" x="-17.145" y="-3.175" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="H35" x="-18.415" y="-3.175" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="H36" x="-19.685" y="-3.175" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="H37" x="-20.955" y="-3.175" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="H38" x="-22.225" y="-3.175" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="H39" x="-23.495" y="-3.175" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<smd name="H40" x="-24.765" y="-3.175" dx="0.635" dy="0.635" layer="1" roundness="100"/>
<text x="25.4" y="4.445" size="1.016" layer="21" ratio="15" rot="R90">1</text>
<text x="-24.13" y="4.445" size="1.016" layer="21" ratio="15" rot="R90">40</text>
<text x="26.035" y="2.54" size="1.016" layer="21" ratio="15">C</text>
<text x="26.035" y="1.27" size="1.016" layer="21" ratio="15">D</text>
<text x="26.035" y="-2.54" size="1.016" layer="21" ratio="15">G</text>
<text x="26.035" y="-3.81" size="1.016" layer="21" ratio="15">H</text>
<text x="-27.305" y="8.89" size="1.27" layer="25" ratio="15">&gt;NAME</text>
<text x="25.4" y="4.445" size="1.016" layer="21" ratio="15" rot="R90">1</text>
<text x="-24.13" y="4.445" size="1.016" layer="21" ratio="15" rot="R90">40</text>
<text x="26.035" y="2.54" size="1.016" layer="21" ratio="15">C</text>
<text x="26.035" y="1.27" size="1.016" layer="21" ratio="15">D</text>
<text x="26.035" y="-2.54" size="1.016" layer="21" ratio="15">G</text>
<text x="26.035" y="-3.81" size="1.016" layer="21" ratio="15">H</text>
<text x="-27.305" y="8.89" size="1.27" layer="25" ratio="15">&gt;NAME</text>
<text x="-27.305" y="-10.16" size="1.27" layer="27" ratio="15">&gt;VALUE</text>
<hole x="27.1907" y="0" drill="1.27"/>
<hole x="-27.1907" y="-3.048" drill="1.27"/>
</package>
</packages>
<symbols>
<symbol name="BANK01">
<wire x1="-10.16" y1="53.34" x2="10.16" y2="53.34" width="0.254" layer="94"/>
<wire x1="10.16" y1="53.34" x2="10.16" y2="-50.8" width="0.254" layer="94"/>
<wire x1="10.16" y1="-50.8" x2="-10.16" y2="-50.8" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-50.8" x2="-10.16" y2="53.34" width="0.254" layer="94"/>
<text x="-10.16" y="53.34" size="1.778" layer="95">&gt;NAME</text>
<text x="-10.16" y="-53.34" size="1.778" layer="96">&gt;VALUE</text>
<pin name="C1" x="-15.24" y="50.8" length="middle"/>
<pin name="C2" x="-15.24" y="48.26" length="middle"/>
<pin name="C3" x="-15.24" y="45.72" length="middle"/>
<pin name="C4" x="-15.24" y="43.18" length="middle"/>
<pin name="C5" x="-15.24" y="40.64" length="middle"/>
<pin name="C6" x="-15.24" y="38.1" length="middle"/>
<pin name="C7" x="-15.24" y="35.56" length="middle"/>
<pin name="C8" x="-15.24" y="33.02" length="middle"/>
<pin name="C9" x="-15.24" y="30.48" length="middle"/>
<pin name="C10" x="-15.24" y="27.94" length="middle"/>
<pin name="C11" x="-15.24" y="25.4" length="middle"/>
<pin name="C12" x="-15.24" y="22.86" length="middle"/>
<pin name="C13" x="-15.24" y="20.32" length="middle"/>
<pin name="C14" x="-15.24" y="17.78" length="middle"/>
<pin name="C15" x="-15.24" y="15.24" length="middle"/>
<pin name="C16" x="-15.24" y="12.7" length="middle"/>
<pin name="C17" x="-15.24" y="10.16" length="middle"/>
<pin name="C18" x="-15.24" y="7.62" length="middle"/>
<pin name="C19" x="-15.24" y="5.08" length="middle"/>
<pin name="C20" x="-15.24" y="2.54" length="middle"/>
<pin name="C21" x="-15.24" y="0" length="middle"/>
<pin name="C22" x="-15.24" y="-2.54" length="middle"/>
<pin name="C23" x="-15.24" y="-5.08" length="middle"/>
<pin name="C24" x="-15.24" y="-7.62" length="middle"/>
<pin name="C25" x="-15.24" y="-10.16" length="middle"/>
<pin name="C26" x="-15.24" y="-12.7" length="middle"/>
<pin name="C27" x="-15.24" y="-15.24" length="middle"/>
<pin name="C28" x="-15.24" y="-17.78" length="middle"/>
<pin name="C29" x="-15.24" y="-20.32" length="middle"/>
<pin name="C30" x="-15.24" y="-22.86" length="middle"/>
<pin name="C31" x="-15.24" y="-25.4" length="middle"/>
<pin name="C32" x="-15.24" y="-27.94" length="middle"/>
<pin name="C33" x="-15.24" y="-30.48" length="middle"/>
<pin name="C34" x="-15.24" y="-33.02" length="middle"/>
<pin name="C35" x="-15.24" y="-35.56" length="middle"/>
<pin name="C36" x="-15.24" y="-38.1" length="middle"/>
<pin name="C37" x="-15.24" y="-40.64" length="middle"/>
<pin name="C38" x="-15.24" y="-43.18" length="middle"/>
<pin name="C39" x="-15.24" y="-45.72" length="middle"/>
<pin name="C40" x="-15.24" y="-48.26" length="middle"/>
<pin name="D1" x="15.24" y="50.8" length="middle" rot="R180"/>
<pin name="D2" x="15.24" y="48.26" length="middle" rot="R180"/>
<pin name="D3" x="15.24" y="45.72" length="middle" rot="R180"/>
<pin name="D4" x="15.24" y="43.18" length="middle" rot="R180"/>
<pin name="D5" x="15.24" y="40.64" length="middle" rot="R180"/>
<pin name="D6" x="15.24" y="38.1" length="middle" rot="R180"/>
<pin name="D7" x="15.24" y="35.56" length="middle" rot="R180"/>
<pin name="D8" x="15.24" y="33.02" length="middle" rot="R180"/>
<pin name="D9" x="15.24" y="30.48" length="middle" rot="R180"/>
<pin name="D10" x="15.24" y="27.94" length="middle" rot="R180"/>
<pin name="D11" x="15.24" y="25.4" length="middle" rot="R180"/>
<pin name="D12" x="15.24" y="22.86" length="middle" rot="R180"/>
<pin name="D13" x="15.24" y="20.32" length="middle" rot="R180"/>
<pin name="D14" x="15.24" y="17.78" length="middle" rot="R180"/>
<pin name="D15" x="15.24" y="15.24" length="middle" rot="R180"/>
<pin name="D16" x="15.24" y="12.7" length="middle" rot="R180"/>
<pin name="D17" x="15.24" y="10.16" length="middle" rot="R180"/>
<pin name="D18" x="15.24" y="7.62" length="middle" rot="R180"/>
<pin name="D19" x="15.24" y="5.08" length="middle" rot="R180"/>
<pin name="D20" x="15.24" y="2.54" length="middle" rot="R180"/>
<pin name="D21" x="15.24" y="0" length="middle" rot="R180"/>
<pin name="D22" x="15.24" y="-2.54" length="middle" rot="R180"/>
<pin name="D23" x="15.24" y="-5.08" length="middle" rot="R180"/>
<pin name="D24" x="15.24" y="-7.62" length="middle" rot="R180"/>
<pin name="D25" x="15.24" y="-10.16" length="middle" rot="R180"/>
<pin name="D26" x="15.24" y="-12.7" length="middle" rot="R180"/>
<pin name="D27" x="15.24" y="-15.24" length="middle" rot="R180"/>
<pin name="D28" x="15.24" y="-17.78" length="middle" rot="R180"/>
<pin name="D29" x="15.24" y="-20.32" length="middle" rot="R180"/>
<pin name="D30" x="15.24" y="-22.86" length="middle" rot="R180"/>
<pin name="D31" x="15.24" y="-25.4" length="middle" rot="R180"/>
<pin name="D32" x="15.24" y="-27.94" length="middle" rot="R180"/>
<pin name="D33" x="15.24" y="-30.48" length="middle" rot="R180"/>
<pin name="D34" x="15.24" y="-33.02" length="middle" rot="R180"/>
<pin name="D35" x="15.24" y="-35.56" length="middle" rot="R180"/>
<pin name="D36" x="15.24" y="-38.1" length="middle" rot="R180"/>
<pin name="D37" x="15.24" y="-40.64" length="middle" rot="R180"/>
<pin name="D38" x="15.24" y="-43.18" length="middle" rot="R180"/>
<pin name="D39" x="15.24" y="-45.72" length="middle" rot="R180"/>
<pin name="D40" x="15.24" y="-48.26" length="middle" rot="R180"/>
</symbol>
<symbol name="BANK02">
<wire x1="-10.16" y1="53.34" x2="10.16" y2="53.34" width="0.254" layer="94"/>
<wire x1="10.16" y1="53.34" x2="10.16" y2="-50.8" width="0.254" layer="94"/>
<wire x1="10.16" y1="-50.8" x2="-10.16" y2="-50.8" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-50.8" x2="-10.16" y2="53.34" width="0.254" layer="94"/>
<text x="-10.16" y="53.34" size="1.778" layer="95">&gt;NAME</text>
<text x="-10.16" y="-53.34" size="1.778" layer="96">&gt;VALUE</text>
<pin name="G1" x="-15.24" y="50.8" length="middle"/>
<pin name="G2" x="-15.24" y="48.26" length="middle"/>
<pin name="G3" x="-15.24" y="45.72" length="middle"/>
<pin name="G4" x="-15.24" y="43.18" length="middle"/>
<pin name="G5" x="-15.24" y="40.64" length="middle"/>
<pin name="G6" x="-15.24" y="38.1" length="middle"/>
<pin name="G7" x="-15.24" y="35.56" length="middle"/>
<pin name="G8" x="-15.24" y="33.02" length="middle"/>
<pin name="G9" x="-15.24" y="30.48" length="middle"/>
<pin name="G10" x="-15.24" y="27.94" length="middle"/>
<pin name="G11" x="-15.24" y="25.4" length="middle"/>
<pin name="G12" x="-15.24" y="22.86" length="middle"/>
<pin name="G13" x="-15.24" y="20.32" length="middle"/>
<pin name="G14" x="-15.24" y="17.78" length="middle"/>
<pin name="G15" x="-15.24" y="15.24" length="middle"/>
<pin name="G16" x="-15.24" y="12.7" length="middle"/>
<pin name="G17" x="-15.24" y="10.16" length="middle"/>
<pin name="G18" x="-15.24" y="7.62" length="middle"/>
<pin name="G19" x="-15.24" y="5.08" length="middle"/>
<pin name="G20" x="-15.24" y="2.54" length="middle"/>
<pin name="G21" x="-15.24" y="0" length="middle"/>
<pin name="G22" x="-15.24" y="-2.54" length="middle"/>
<pin name="G23" x="-15.24" y="-5.08" length="middle"/>
<pin name="G24" x="-15.24" y="-7.62" length="middle"/>
<pin name="G25" x="-15.24" y="-10.16" length="middle"/>
<pin name="G26" x="-15.24" y="-12.7" length="middle"/>
<pin name="G27" x="-15.24" y="-15.24" length="middle"/>
<pin name="G28" x="-15.24" y="-17.78" length="middle"/>
<pin name="G29" x="-15.24" y="-20.32" length="middle"/>
<pin name="G30" x="-15.24" y="-22.86" length="middle"/>
<pin name="G31" x="-15.24" y="-25.4" length="middle"/>
<pin name="G32" x="-15.24" y="-27.94" length="middle"/>
<pin name="G33" x="-15.24" y="-30.48" length="middle"/>
<pin name="G34" x="-15.24" y="-33.02" length="middle"/>
<pin name="G35" x="-15.24" y="-35.56" length="middle"/>
<pin name="G36" x="-15.24" y="-38.1" length="middle"/>
<pin name="G37" x="-15.24" y="-40.64" length="middle"/>
<pin name="G38" x="-15.24" y="-43.18" length="middle"/>
<pin name="G39" x="-15.24" y="-45.72" length="middle"/>
<pin name="G40" x="-15.24" y="-48.26" length="middle"/>
<pin name="H1" x="15.24" y="50.8" length="middle" rot="R180"/>
<pin name="H2" x="15.24" y="48.26" length="middle" rot="R180"/>
<pin name="H3" x="15.24" y="45.72" length="middle" rot="R180"/>
<pin name="H4" x="15.24" y="43.18" length="middle" rot="R180"/>
<pin name="H5" x="15.24" y="40.64" length="middle" rot="R180"/>
<pin name="H6" x="15.24" y="38.1" length="middle" rot="R180"/>
<pin name="H7" x="15.24" y="35.56" length="middle" rot="R180"/>
<pin name="H8" x="15.24" y="33.02" length="middle" rot="R180"/>
<pin name="H9" x="15.24" y="30.48" length="middle" rot="R180"/>
<pin name="H10" x="15.24" y="27.94" length="middle" rot="R180"/>
<pin name="H11" x="15.24" y="25.4" length="middle" rot="R180"/>
<pin name="H12" x="15.24" y="22.86" length="middle" rot="R180"/>
<pin name="H13" x="15.24" y="20.32" length="middle" rot="R180"/>
<pin name="H14" x="15.24" y="17.78" length="middle" rot="R180"/>
<pin name="H15" x="15.24" y="15.24" length="middle" rot="R180"/>
<pin name="H16" x="15.24" y="12.7" length="middle" rot="R180"/>
<pin name="H17" x="15.24" y="10.16" length="middle" rot="R180"/>
<pin name="H18" x="15.24" y="7.62" length="middle" rot="R180"/>
<pin name="H19" x="15.24" y="5.08" length="middle" rot="R180"/>
<pin name="H20" x="15.24" y="2.54" length="middle" rot="R180"/>
<pin name="H21" x="15.24" y="0" length="middle" rot="R180"/>
<pin name="H22" x="15.24" y="-2.54" length="middle" rot="R180"/>
<pin name="H23" x="15.24" y="-5.08" length="middle" rot="R180"/>
<pin name="H24" x="15.24" y="-7.62" length="middle" rot="R180"/>
<pin name="H25" x="15.24" y="-10.16" length="middle" rot="R180"/>
<pin name="H26" x="15.24" y="-12.7" length="middle" rot="R180"/>
<pin name="H27" x="15.24" y="-15.24" length="middle" rot="R180"/>
<pin name="H28" x="15.24" y="-17.78" length="middle" rot="R180"/>
<pin name="H29" x="15.24" y="-20.32" length="middle" rot="R180"/>
<pin name="H30" x="15.24" y="-22.86" length="middle" rot="R180"/>
<pin name="H31" x="15.24" y="-25.4" length="middle" rot="R180"/>
<pin name="H32" x="15.24" y="-27.94" length="middle" rot="R180"/>
<pin name="H33" x="15.24" y="-30.48" length="middle" rot="R180"/>
<pin name="H34" x="15.24" y="-33.02" length="middle" rot="R180"/>
<pin name="H35" x="15.24" y="-35.56" length="middle" rot="R180"/>
<pin name="H36" x="15.24" y="-38.1" length="middle" rot="R180"/>
<pin name="H37" x="15.24" y="-40.64" length="middle" rot="R180"/>
<pin name="H38" x="15.24" y="-43.18" length="middle" rot="R180"/>
<pin name="H39" x="15.24" y="-45.72" length="middle" rot="R180"/>
<pin name="H40" x="15.24" y="-48.26" length="middle" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="FMC-LPC" prefix="ZED">
<description>FMC-LPC 160 Pins 
ASP-134604-01</description>
<gates>
<gate name="-A" symbol="BANK01" x="-30.48" y="0"/>
<gate name="-B" symbol="BANK02" x="30.48" y="0"/>
</gates>
<devices>
<device name="" package="ASP-134604-01">
<connects>
<connect gate="-A" pin="C1" pad="C1"/>
<connect gate="-A" pin="C10" pad="C10"/>
<connect gate="-A" pin="C11" pad="C11"/>
<connect gate="-A" pin="C12" pad="C12"/>
<connect gate="-A" pin="C13" pad="C13"/>
<connect gate="-A" pin="C14" pad="C14"/>
<connect gate="-A" pin="C15" pad="C15"/>
<connect gate="-A" pin="C16" pad="C16"/>
<connect gate="-A" pin="C17" pad="C17"/>
<connect gate="-A" pin="C18" pad="C18"/>
<connect gate="-A" pin="C19" pad="C19"/>
<connect gate="-A" pin="C2" pad="C2"/>
<connect gate="-A" pin="C20" pad="C20"/>
<connect gate="-A" pin="C21" pad="C21"/>
<connect gate="-A" pin="C22" pad="C22"/>
<connect gate="-A" pin="C23" pad="C23"/>
<connect gate="-A" pin="C24" pad="C24"/>
<connect gate="-A" pin="C25" pad="C25"/>
<connect gate="-A" pin="C26" pad="C26"/>
<connect gate="-A" pin="C27" pad="C27"/>
<connect gate="-A" pin="C28" pad="C28"/>
<connect gate="-A" pin="C29" pad="C29"/>
<connect gate="-A" pin="C3" pad="C3"/>
<connect gate="-A" pin="C30" pad="C30"/>
<connect gate="-A" pin="C31" pad="C31"/>
<connect gate="-A" pin="C32" pad="C32"/>
<connect gate="-A" pin="C33" pad="C33"/>
<connect gate="-A" pin="C34" pad="C34"/>
<connect gate="-A" pin="C35" pad="C35"/>
<connect gate="-A" pin="C36" pad="C36"/>
<connect gate="-A" pin="C37" pad="C37"/>
<connect gate="-A" pin="C38" pad="C38"/>
<connect gate="-A" pin="C39" pad="C39"/>
<connect gate="-A" pin="C4" pad="C4"/>
<connect gate="-A" pin="C40" pad="C40"/>
<connect gate="-A" pin="C5" pad="C5"/>
<connect gate="-A" pin="C6" pad="C6"/>
<connect gate="-A" pin="C7" pad="C7"/>
<connect gate="-A" pin="C8" pad="C8"/>
<connect gate="-A" pin="C9" pad="C9"/>
<connect gate="-A" pin="D1" pad="D1"/>
<connect gate="-A" pin="D10" pad="D10"/>
<connect gate="-A" pin="D11" pad="D11"/>
<connect gate="-A" pin="D12" pad="D12"/>
<connect gate="-A" pin="D13" pad="D13"/>
<connect gate="-A" pin="D14" pad="D14"/>
<connect gate="-A" pin="D15" pad="D15"/>
<connect gate="-A" pin="D16" pad="D16"/>
<connect gate="-A" pin="D17" pad="D17"/>
<connect gate="-A" pin="D18" pad="D18"/>
<connect gate="-A" pin="D19" pad="D19"/>
<connect gate="-A" pin="D2" pad="D2"/>
<connect gate="-A" pin="D20" pad="D20"/>
<connect gate="-A" pin="D21" pad="D21"/>
<connect gate="-A" pin="D22" pad="D22"/>
<connect gate="-A" pin="D23" pad="D23"/>
<connect gate="-A" pin="D24" pad="D24"/>
<connect gate="-A" pin="D25" pad="D25"/>
<connect gate="-A" pin="D26" pad="D26"/>
<connect gate="-A" pin="D27" pad="D27"/>
<connect gate="-A" pin="D28" pad="D28"/>
<connect gate="-A" pin="D29" pad="D29"/>
<connect gate="-A" pin="D3" pad="D3"/>
<connect gate="-A" pin="D30" pad="D30"/>
<connect gate="-A" pin="D31" pad="D31"/>
<connect gate="-A" pin="D32" pad="D32"/>
<connect gate="-A" pin="D33" pad="D33"/>
<connect gate="-A" pin="D34" pad="D34"/>
<connect gate="-A" pin="D35" pad="D35"/>
<connect gate="-A" pin="D36" pad="D36"/>
<connect gate="-A" pin="D37" pad="D37"/>
<connect gate="-A" pin="D38" pad="D38"/>
<connect gate="-A" pin="D39" pad="D39"/>
<connect gate="-A" pin="D4" pad="D4"/>
<connect gate="-A" pin="D40" pad="D40"/>
<connect gate="-A" pin="D5" pad="D5"/>
<connect gate="-A" pin="D6" pad="D6"/>
<connect gate="-A" pin="D7" pad="D7"/>
<connect gate="-A" pin="D8" pad="D8"/>
<connect gate="-A" pin="D9" pad="D9"/>
<connect gate="-B" pin="G1" pad="G1"/>
<connect gate="-B" pin="G10" pad="G10"/>
<connect gate="-B" pin="G11" pad="G11"/>
<connect gate="-B" pin="G12" pad="G12"/>
<connect gate="-B" pin="G13" pad="G13"/>
<connect gate="-B" pin="G14" pad="G14"/>
<connect gate="-B" pin="G15" pad="G15"/>
<connect gate="-B" pin="G16" pad="G16"/>
<connect gate="-B" pin="G17" pad="G17"/>
<connect gate="-B" pin="G18" pad="G18"/>
<connect gate="-B" pin="G19" pad="G19"/>
<connect gate="-B" pin="G2" pad="G2"/>
<connect gate="-B" pin="G20" pad="G20"/>
<connect gate="-B" pin="G21" pad="G21"/>
<connect gate="-B" pin="G22" pad="G22"/>
<connect gate="-B" pin="G23" pad="G23"/>
<connect gate="-B" pin="G24" pad="G24"/>
<connect gate="-B" pin="G25" pad="G25"/>
<connect gate="-B" pin="G26" pad="G26"/>
<connect gate="-B" pin="G27" pad="G27"/>
<connect gate="-B" pin="G28" pad="G28"/>
<connect gate="-B" pin="G29" pad="G29"/>
<connect gate="-B" pin="G3" pad="G3"/>
<connect gate="-B" pin="G30" pad="G30"/>
<connect gate="-B" pin="G31" pad="G31"/>
<connect gate="-B" pin="G32" pad="G32"/>
<connect gate="-B" pin="G33" pad="G33"/>
<connect gate="-B" pin="G34" pad="G34"/>
<connect gate="-B" pin="G35" pad="G35"/>
<connect gate="-B" pin="G36" pad="G36"/>
<connect gate="-B" pin="G37" pad="G37"/>
<connect gate="-B" pin="G38" pad="G38"/>
<connect gate="-B" pin="G39" pad="G39"/>
<connect gate="-B" pin="G4" pad="G4"/>
<connect gate="-B" pin="G40" pad="G40"/>
<connect gate="-B" pin="G5" pad="G5"/>
<connect gate="-B" pin="G6" pad="G6"/>
<connect gate="-B" pin="G7" pad="G7"/>
<connect gate="-B" pin="G8" pad="G8"/>
<connect gate="-B" pin="G9" pad="G9"/>
<connect gate="-B" pin="H1" pad="H1"/>
<connect gate="-B" pin="H10" pad="H10"/>
<connect gate="-B" pin="H11" pad="H11"/>
<connect gate="-B" pin="H12" pad="H12"/>
<connect gate="-B" pin="H13" pad="H13"/>
<connect gate="-B" pin="H14" pad="H14"/>
<connect gate="-B" pin="H15" pad="H15"/>
<connect gate="-B" pin="H16" pad="H16"/>
<connect gate="-B" pin="H17" pad="H17"/>
<connect gate="-B" pin="H18" pad="H18"/>
<connect gate="-B" pin="H19" pad="H19"/>
<connect gate="-B" pin="H2" pad="H2"/>
<connect gate="-B" pin="H20" pad="H20"/>
<connect gate="-B" pin="H21" pad="H21"/>
<connect gate="-B" pin="H22" pad="H22"/>
<connect gate="-B" pin="H23" pad="H23"/>
<connect gate="-B" pin="H24" pad="H24"/>
<connect gate="-B" pin="H25" pad="H25"/>
<connect gate="-B" pin="H26" pad="H26"/>
<connect gate="-B" pin="H27" pad="H27"/>
<connect gate="-B" pin="H28" pad="H28"/>
<connect gate="-B" pin="H29" pad="H29"/>
<connect gate="-B" pin="H3" pad="H3"/>
<connect gate="-B" pin="H30" pad="H30"/>
<connect gate="-B" pin="H31" pad="H31"/>
<connect gate="-B" pin="H32" pad="H32"/>
<connect gate="-B" pin="H33" pad="H33"/>
<connect gate="-B" pin="H34" pad="H34"/>
<connect gate="-B" pin="H35" pad="H35"/>
<connect gate="-B" pin="H36" pad="H36"/>
<connect gate="-B" pin="H37" pad="H37"/>
<connect gate="-B" pin="H38" pad="H38"/>
<connect gate="-B" pin="H39" pad="H39"/>
<connect gate="-B" pin="H4" pad="H4"/>
<connect gate="-B" pin="H40" pad="H40"/>
<connect gate="-B" pin="H5" pad="H5"/>
<connect gate="-B" pin="H6" pad="H6"/>
<connect gate="-B" pin="H7" pad="H7"/>
<connect gate="-B" pin="H8" pad="H8"/>
<connect gate="-B" pin="H9" pad="H9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="resistor">
<packages>
<package name="C0402">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.635" y="0.762" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="C0504">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.294" y1="0.559" x2="0.294" y2="0.559" width="0.1016" layer="51"/>
<wire x1="-0.294" y1="-0.559" x2="0.294" y2="-0.559" width="0.1016" layer="51"/>
<smd name="1" x="-0.7" y="0" dx="1" dy="1.3" layer="1"/>
<smd name="2" x="0.7" y="0" dx="1" dy="1.3" layer="1"/>
<text x="-0.635" y="0.889" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.159" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.6604" y1="-0.6223" x2="-0.2804" y2="0.6276" layer="51"/>
<rectangle x1="0.2794" y1="-0.6223" x2="0.6594" y2="0.6276" layer="51"/>
<rectangle x1="-0.1001" y1="-0.4001" x2="0.1001" y2="0.4001" layer="35"/>
</package>
<package name="C0603">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.889" y="0.762" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.889" y="-2.032" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="C0805">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.381" y1="0.66" x2="0.381" y2="0.66" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.66" x2="0.381" y2="-0.66" width="0.1016" layer="51"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-0.889" y="1.016" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.889" y="-2.286" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.0922" y1="-0.7239" x2="-0.3421" y2="0.7262" layer="51"/>
<rectangle x1="0.3556" y1="-0.7239" x2="1.1057" y2="0.7262" layer="51"/>
<rectangle x1="-0.1001" y1="-0.4001" x2="0.1001" y2="0.4001" layer="35"/>
</package>
<package name="C1005">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.635" y="0.762" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="C1206">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.27" y="1.143" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.413" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="C1210">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="-0.9652" y1="1.2446" x2="0.9652" y2="1.2446" width="0.1016" layer="51"/>
<wire x1="-0.9652" y1="-1.2446" x2="0.9652" y2="-1.2446" width="0.1016" layer="51"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<text x="-1.397" y="1.651" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.397" y="-2.921" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-1.2954" x2="-0.9517" y2="1.3045" layer="51"/>
<rectangle x1="0.9517" y1="-1.3045" x2="1.7018" y2="1.2954" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="C1310">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.294" y1="0.559" x2="0.294" y2="0.559" width="0.1016" layer="51"/>
<wire x1="-0.294" y1="-0.559" x2="0.294" y2="-0.559" width="0.1016" layer="51"/>
<smd name="1" x="-0.7" y="0" dx="1" dy="1.3" layer="1"/>
<smd name="2" x="0.7" y="0" dx="1" dy="1.3" layer="1"/>
<text x="-0.635" y="0.889" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.159" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.6604" y1="-0.6223" x2="-0.2804" y2="0.6276" layer="51"/>
<rectangle x1="0.2794" y1="-0.6223" x2="0.6594" y2="0.6276" layer="51"/>
<rectangle x1="-0.1001" y1="-0.3" x2="0.1001" y2="0.3" layer="35"/>
</package>
<package name="C1608">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.635" y="0.762" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.032" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="C1812">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-2.973" y1="1.983" x2="2.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-1.983" x2="-2.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-1.983" x2="-2.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="-1.4732" y1="1.6002" x2="1.4732" y2="1.6002" width="0.1016" layer="51"/>
<wire x1="-1.4478" y1="-1.6002" x2="1.4732" y2="-1.6002" width="0.1016" layer="51"/>
<wire x1="2.973" y1="1.983" x2="2.973" y2="-1.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.95" y="0" dx="1.9" dy="3.4" layer="1"/>
<smd name="2" x="1.95" y="0" dx="1.9" dy="3.4" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.3876" y1="-1.651" x2="-1.4376" y2="1.649" layer="51"/>
<rectangle x1="1.4478" y1="-1.651" x2="2.3978" y2="1.649" layer="51"/>
<rectangle x1="-0.3" y1="-0.4001" x2="0.3" y2="0.4001" layer="35"/>
</package>
<package name="C1825">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-2.973" y1="3.483" x2="2.973" y2="3.483" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-3.483" x2="-2.973" y2="-3.483" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-3.483" x2="-2.973" y2="3.483" width="0.0508" layer="39"/>
<wire x1="-1.4986" y1="3.2766" x2="1.4732" y2="3.2766" width="0.1016" layer="51"/>
<wire x1="-1.4732" y1="-3.2766" x2="1.4986" y2="-3.2766" width="0.1016" layer="51"/>
<wire x1="2.973" y1="3.483" x2="2.973" y2="-3.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.95" y="0" dx="1.9" dy="6.8" layer="1"/>
<smd name="2" x="1.95" y="0" dx="1.9" dy="6.8" layer="1"/>
<text x="-1.905" y="3.683" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-4.826" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.413" y1="-3.3528" x2="-1.463" y2="3.3472" layer="51"/>
<rectangle x1="1.4478" y1="-3.3528" x2="2.3978" y2="3.3472" layer="51"/>
<rectangle x1="-0.7" y1="-0.7" x2="0.7" y2="0.7" layer="35"/>
</package>
<package name="C2012">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.381" y1="0.66" x2="0.381" y2="0.66" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.66" x2="0.381" y2="-0.66" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-0.889" y="1.016" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.889" y="-2.286" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.0922" y1="-0.7239" x2="-0.3421" y2="0.7262" layer="51"/>
<rectangle x1="0.3556" y1="-0.7239" x2="1.1057" y2="0.7262" layer="51"/>
<rectangle x1="-0.1001" y1="-0.4001" x2="0.1001" y2="0.4001" layer="35"/>
</package>
<package name="C3216">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.27" y="1.143" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.413" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.3" y1="-0.5001" x2="0.3" y2="0.5001" layer="35"/>
</package>
<package name="C3225">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="-0.9652" y1="1.2446" x2="0.9652" y2="1.2446" width="0.1016" layer="51"/>
<wire x1="-0.9652" y1="-1.2446" x2="0.9652" y2="-1.2446" width="0.1016" layer="51"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<text x="-1.397" y="1.651" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.397" y="-2.921" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-1.2954" x2="-0.9517" y2="1.3045" layer="51"/>
<rectangle x1="0.9517" y1="-1.3045" x2="1.7018" y2="1.2954" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
</package>
<package name="C4532">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-2.973" y1="1.983" x2="2.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-1.983" x2="-2.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-1.983" x2="-2.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="-1.4732" y1="1.6002" x2="1.4732" y2="1.6002" width="0.1016" layer="51"/>
<wire x1="-1.4478" y1="-1.6002" x2="1.4732" y2="-1.6002" width="0.1016" layer="51"/>
<wire x1="2.973" y1="1.983" x2="2.973" y2="-1.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.95" y="0" dx="1.9" dy="3.4" layer="1"/>
<smd name="2" x="1.95" y="0" dx="1.9" dy="3.4" layer="1"/>
<text x="-1.905" y="2.032" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.3876" y1="-1.651" x2="-1.4376" y2="1.649" layer="51"/>
<rectangle x1="1.4478" y1="-1.651" x2="2.3978" y2="1.649" layer="51"/>
<rectangle x1="-0.4001" y1="-0.7" x2="0.4001" y2="0.7" layer="35"/>
</package>
<package name="C4564">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-2.973" y1="3.483" x2="2.973" y2="3.483" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-3.483" x2="-2.973" y2="-3.483" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-3.483" x2="-2.973" y2="3.483" width="0.0508" layer="39"/>
<wire x1="-1.4986" y1="3.2766" x2="1.4732" y2="3.2766" width="0.1016" layer="51"/>
<wire x1="-1.4732" y1="-3.2766" x2="1.4986" y2="-3.2766" width="0.1016" layer="51"/>
<wire x1="2.973" y1="3.483" x2="2.973" y2="-3.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.95" y="0" dx="1.9" dy="6.8" layer="1"/>
<smd name="2" x="1.95" y="0" dx="1.9" dy="6.8" layer="1"/>
<text x="-1.905" y="3.683" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-4.826" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.413" y1="-3.3528" x2="-1.463" y2="3.3472" layer="51"/>
<rectangle x1="1.4478" y1="-3.3528" x2="2.3978" y2="3.3472" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="C025-024X044">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 2.4 x 4.4 mm</description>
<wire x1="-2.159" y1="-0.635" x2="-2.159" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="0.635" x2="-1.651" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.159" y1="-0.635" x2="-1.651" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="1.651" y1="1.143" x2="-1.651" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-0.635" x2="2.159" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.651" y1="-1.143" x2="-1.651" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="1.651" y1="1.143" x2="2.159" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="1.651" y1="-1.143" x2="2.159" y2="-0.635" width="0.1524" layer="21" curve="90"/>
<wire x1="-0.3048" y1="0.762" x2="-0.3048" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0.762" x2="0.3302" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="1.27" y1="0" x2="0.3302" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="0" x2="-0.3048" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-1.778" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.778" y="-2.667" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025-025X050">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 2.5 x 5 mm</description>
<wire x1="-2.159" y1="1.27" x2="2.159" y2="1.27" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-1.27" x2="-2.159" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.159" y1="1.27" x2="2.413" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="1.016" x2="-2.159" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-1.27" x2="2.413" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-1.016" x2="-2.159" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="1.524" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-2.794" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025-030X050">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 3 x 5 mm</description>
<wire x1="-2.159" y1="1.524" x2="2.159" y2="1.524" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-1.524" x2="-2.159" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.27" x2="2.413" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.27" x2="-2.413" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.159" y1="1.524" x2="2.413" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="1.27" x2="-2.159" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-1.524" x2="2.413" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-1.27" x2="-2.159" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-3.048" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025-040X050">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 4 x 5 mm</description>
<wire x1="-2.159" y1="1.905" x2="2.159" y2="1.905" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-1.905" x2="-2.159" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.651" x2="2.413" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.651" x2="-2.413" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="2.159" y1="1.905" x2="2.413" y2="1.651" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="1.651" x2="-2.159" y2="1.905" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-1.905" x2="2.413" y2="-1.651" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-1.651" x2="-2.159" y2="-1.905" width="0.1524" layer="21" curve="90"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="2.159" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-3.429" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025-050X050">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 5 x 5 mm</description>
<wire x1="-2.159" y1="2.286" x2="2.159" y2="2.286" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-2.286" x2="-2.159" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="2.413" y1="2.032" x2="2.413" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="2.032" x2="-2.413" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="2.159" y1="2.286" x2="2.413" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="2.032" x2="-2.159" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-2.286" x2="2.413" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-2.032" x2="-2.159" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="2.54" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-3.81" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025-060X050">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 6 x 5 mm</description>
<wire x1="-2.159" y1="2.794" x2="2.159" y2="2.794" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-2.794" x2="-2.159" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="2.413" y1="2.54" x2="2.413" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="2.54" x2="-2.413" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="2.159" y1="2.794" x2="2.413" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="2.54" x2="-2.159" y2="2.794" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-2.794" x2="2.413" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-2.54" x2="-2.159" y2="-2.794" width="0.1524" layer="21" curve="90"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="3.048" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.032" y="-2.413" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025_050-024X070">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm + 5 mm, outline 2.4 x 7 mm</description>
<wire x1="-2.159" y1="-0.635" x2="-2.159" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-2.159" y1="0.635" x2="-1.651" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.159" y1="-0.635" x2="-1.651" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="1.651" y1="1.143" x2="-1.651" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-0.635" x2="2.159" y2="0.635" width="0.1524" layer="51"/>
<wire x1="1.651" y1="-1.143" x2="-1.651" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="1.651" y1="1.143" x2="2.159" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.191" y1="-1.143" x2="-3.9624" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-4.191" y1="1.143" x2="-3.9624" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="-0.635" x2="-4.191" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="1.651" y1="-1.143" x2="2.159" y2="-0.635" width="0.1524" layer="21" curve="90"/>
<wire x1="-4.699" y1="0.635" x2="-4.191" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.699" y1="-0.635" x2="-4.699" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="1.143" x2="-2.5654" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-1.143" x2="-2.5654" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-0.3048" y1="0.762" x2="-0.3048" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0.762" x2="0.3302" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="1.27" y1="0" x2="0.3302" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="0" x2="-0.3048" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="3" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.81" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.81" y="-2.667" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025_050-025X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 + 5 mm, outline 2.5 x 7.5 mm</description>
<wire x1="-2.159" y1="1.27" x2="2.159" y2="1.27" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-1.27" x2="-2.159" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.159" y1="1.27" x2="2.413" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="1.016" x2="-2.159" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-1.27" x2="2.413" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-1.016" x2="-2.159" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="4.953" y1="1.016" x2="4.953" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="4.699" y1="1.27" x2="4.953" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="-1.27" x2="4.953" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="2.794" y1="1.27" x2="4.699" y2="1.27" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-1.27" x2="2.794" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.413" y2="0.762" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-0.762" x2="2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="0.254" x2="2.413" y2="-0.254" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0" x2="2.286" y2="0" width="0.1524" layer="51"/>
<wire x1="2.286" y1="0" x2="2.794" y2="0" width="0.1524" layer="21"/>
<wire x1="2.794" y1="0" x2="3.302" y2="0" width="0.1524" layer="51"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="3" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.159" y="1.651" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.159" y="-2.794" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025_050-035X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 + 5 mm, outline 3.5 x 7.5 mm</description>
<wire x1="-2.159" y1="1.778" x2="2.159" y2="1.778" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-1.778" x2="-2.159" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.524" x2="-2.413" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="2.159" y1="1.778" x2="2.413" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="1.524" x2="-2.159" y2="1.778" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-1.778" x2="2.413" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-1.524" x2="-2.159" y2="-1.778" width="0.1524" layer="21" curve="90"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="4.953" y1="1.524" x2="4.953" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="4.699" y1="1.778" x2="4.953" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="-1.778" x2="4.953" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="2.794" y1="1.778" x2="4.699" y2="1.778" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-1.778" x2="2.794" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.524" x2="2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.413" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="2.413" y1="0.508" x2="2.413" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="0.381" y1="0" x2="0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="2.286" y1="0" x2="2.794" y2="0" width="0.1524" layer="21"/>
<wire x1="2.794" y1="0" x2="3.302" y2="0" width="0.1524" layer="51"/>
<wire x1="2.286" y1="0" x2="1.778" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="3" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="2.159" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-3.302" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025_050-045X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 + 5 mm, outline 4.5 x 7.5 mm</description>
<wire x1="-2.159" y1="2.286" x2="2.159" y2="2.286" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-2.286" x2="-2.159" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="2.032" x2="-2.413" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="2.159" y1="2.286" x2="2.413" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="2.032" x2="-2.159" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-2.286" x2="2.413" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-2.032" x2="-2.159" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="4.953" y1="2.032" x2="4.953" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="4.699" y1="2.286" x2="4.953" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="-2.286" x2="4.953" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="2.794" y1="2.286" x2="4.699" y2="2.286" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-2.286" x2="2.794" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="2.413" y1="2.032" x2="2.413" y2="1.397" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.397" x2="2.413" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="2.413" y1="0.762" x2="2.413" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="2.286" y1="0" x2="2.794" y2="0" width="0.1524" layer="21"/>
<wire x1="2.794" y1="0" x2="3.302" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="2.286" y1="0" x2="1.778" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="3" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="2.667" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-3.81" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025_050-055X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 + 5 mm, outline 5.5 x 7.5 mm</description>
<wire x1="-2.159" y1="2.794" x2="2.159" y2="2.794" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-2.794" x2="-2.159" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="2.54" x2="-2.413" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="2.159" y1="2.794" x2="2.413" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="2.54" x2="-2.159" y2="2.794" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-2.794" x2="2.413" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-2.54" x2="-2.159" y2="-2.794" width="0.1524" layer="21" curve="90"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="4.953" y1="2.54" x2="4.953" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="4.699" y1="2.794" x2="4.953" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="-2.794" x2="4.953" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="2.794" y1="2.794" x2="4.699" y2="2.794" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-2.794" x2="2.794" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="2.413" y1="2.54" x2="2.413" y2="2.032" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-2.032" x2="2.413" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="2.413" y1="0.762" x2="2.413" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0" x2="2.286" y2="0" width="0.1524" layer="51"/>
<wire x1="2.286" y1="0" x2="2.794" y2="0" width="0.1524" layer="21"/>
<wire x1="2.794" y1="0" x2="3.302" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="3" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="3.175" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.032" y="-2.286" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-024X044">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 2.4 x 4.4 mm</description>
<wire x1="-2.159" y1="-0.635" x2="-2.159" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-2.159" y1="0.635" x2="-1.651" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.159" y1="-0.635" x2="-1.651" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="1.651" y1="1.143" x2="-1.651" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-0.635" x2="2.159" y2="0.635" width="0.1524" layer="51"/>
<wire x1="1.651" y1="-1.143" x2="-1.651" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="1.651" y1="1.143" x2="2.159" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="1.651" y1="-1.143" x2="2.159" y2="-0.635" width="0.1524" layer="21" curve="90"/>
<wire x1="-0.3048" y1="0.762" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0.762" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="1.27" y1="0" x2="0.3302" y2="0" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0" x2="-0.3048" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.159" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.159" y="-2.667" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="2.159" y1="-0.381" x2="2.54" y2="0.381" layer="51"/>
<rectangle x1="-2.54" y1="-0.381" x2="-2.159" y2="0.381" layer="51"/>
</package>
<package name="C050-025X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 2.5 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="1.016" x2="-3.683" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-1.27" x2="3.429" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-1.016" x2="3.683" y2="1.016" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.27" x2="-3.429" y2="1.27" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.27" x2="3.683" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-1.27" x2="3.683" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-1.016" x2="-3.429" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="1.016" x2="-3.429" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.429" y="1.651" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.794" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-045X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 4.5 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="2.032" x2="-3.683" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-2.286" x2="3.429" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-2.032" x2="3.683" y2="2.032" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.286" x2="-3.429" y2="2.286" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.286" x2="3.683" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-2.286" x2="3.683" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-2.032" x2="-3.429" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="2.032" x2="-3.429" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.556" y="2.667" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.556" y="-3.81" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-030X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 3 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="1.27" x2="-3.683" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-1.524" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-1.27" x2="3.683" y2="1.27" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.524" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.524" x2="3.683" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-1.524" x2="3.683" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-1.27" x2="-3.429" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="1.27" x2="-3.429" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.556" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.556" y="-3.048" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-050X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 5 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="2.286" x2="-3.683" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-2.54" x2="3.429" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-2.286" x2="3.683" y2="2.286" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.54" x2="-3.429" y2="2.54" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.54" x2="3.683" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-2.54" x2="3.683" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-2.286" x2="-3.429" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="2.286" x2="-3.429" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.429" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-2.159" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-055X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 5.5 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="2.54" x2="-3.683" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-2.794" x2="3.429" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-2.54" x2="3.683" y2="2.54" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.794" x2="-3.429" y2="2.794" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.794" x2="3.683" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-2.794" x2="3.683" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-2.54" x2="-3.429" y2="-2.794" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="2.54" x2="-3.429" y2="2.794" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.429" y="3.175" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.302" y="-2.286" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-075X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 7.5 x 7.5 mm</description>
<wire x1="-1.524" y1="0" x2="-0.4572" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.4572" y1="0" x2="-0.4572" y2="0.762" width="0.4064" layer="21"/>
<wire x1="-0.4572" y1="0" x2="-0.4572" y2="-0.762" width="0.4064" layer="21"/>
<wire x1="0.4318" y1="0.762" x2="0.4318" y2="0" width="0.4064" layer="21"/>
<wire x1="0.4318" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.4318" y1="0" x2="0.4318" y2="-0.762" width="0.4064" layer="21"/>
<wire x1="-3.683" y1="3.429" x2="-3.683" y2="-3.429" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-3.683" x2="3.429" y2="-3.683" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-3.429" x2="3.683" y2="3.429" width="0.1524" layer="21"/>
<wire x1="3.429" y1="3.683" x2="-3.429" y2="3.683" width="0.1524" layer="21"/>
<wire x1="3.429" y1="3.683" x2="3.683" y2="3.429" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-3.683" x2="3.683" y2="-3.429" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-3.429" x2="-3.429" y2="-3.683" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="3.429" x2="-3.429" y2="3.683" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.429" y="4.064" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-2.921" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050H075X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
Horizontal, grid 5 mm, outline 7.5 x 7.5 mm</description>
<wire x1="-3.683" y1="7.112" x2="-3.683" y2="0.508" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="0.508" x2="-3.302" y2="0.508" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="0.508" x2="-1.778" y2="0.508" width="0.1524" layer="51"/>
<wire x1="-1.778" y1="0.508" x2="1.778" y2="0.508" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0.508" x2="3.302" y2="0.508" width="0.1524" layer="51"/>
<wire x1="3.302" y1="0.508" x2="3.683" y2="0.508" width="0.1524" layer="21"/>
<wire x1="3.683" y1="0.508" x2="3.683" y2="7.112" width="0.1524" layer="21"/>
<wire x1="3.175" y1="7.62" x2="-3.175" y2="7.62" width="0.1524" layer="21"/>
<wire x1="-0.3048" y1="2.413" x2="-0.3048" y2="1.778" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="1.778" x2="-0.3048" y2="1.143" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="1.778" x2="-1.651" y2="1.778" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="2.413" x2="0.3302" y2="1.778" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="1.778" x2="0.3302" y2="1.143" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="1.778" x2="1.651" y2="1.778" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="7.112" x2="-3.175" y2="7.62" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.175" y1="7.62" x2="3.683" y2="7.112" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="0.254" width="0.508" layer="51"/>
<wire x1="2.54" y1="0" x2="2.54" y2="0.254" width="0.508" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.302" y="8.001" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="3.175" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.794" y1="0.127" x2="-2.286" y2="0.508" layer="51"/>
<rectangle x1="2.286" y1="0.127" x2="2.794" y2="0.508" layer="51"/>
</package>
<package name="C075-032X103">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 7.5 mm, outline 3.2 x 10.3 mm</description>
<wire x1="4.826" y1="1.524" x2="-4.826" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="-1.524" x2="4.826" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="21"/>
<wire x1="4.826" y1="1.524" x2="5.08" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.826" y1="-1.524" x2="5.08" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="-1.27" x2="-4.826" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="1.27" x2="-4.826" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="0.508" y1="0" x2="2.54" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0" x2="-0.508" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="0.889" x2="-0.508" y2="0" width="0.4064" layer="21"/>
<wire x1="-0.508" y1="0" x2="-0.508" y2="-0.889" width="0.4064" layer="21"/>
<wire x1="0.508" y1="0.889" x2="0.508" y2="0" width="0.4064" layer="21"/>
<wire x1="0.508" y1="0" x2="0.508" y2="-0.889" width="0.4064" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.9144" shape="octagon"/>
<text x="-4.826" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.826" y="-3.048" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C075-042X103">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 7.5 mm, outline 4.2 x 10.3 mm</description>
<wire x1="4.826" y1="2.032" x2="-4.826" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.778" x2="-5.08" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="-2.032" x2="4.826" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.778" x2="5.08" y2="1.778" width="0.1524" layer="21"/>
<wire x1="4.826" y1="2.032" x2="5.08" y2="1.778" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.826" y1="-2.032" x2="5.08" y2="-1.778" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="-1.778" x2="-4.826" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="1.778" x2="-4.826" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.27" y1="0" x2="2.667" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.667" y1="0" x2="-2.159" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="1.27" x2="-2.159" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.159" y1="0" x2="-2.159" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-1.27" width="0.4064" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.9144" shape="octagon"/>
<text x="-4.699" y="2.413" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.635" y="-1.651" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C075-052X106">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 7.5 mm, outline 5.2 x 10.6 mm</description>
<wire x1="4.953" y1="2.54" x2="-4.953" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="2.286" x2="-5.207" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="-2.54" x2="4.953" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="5.207" y1="-2.286" x2="5.207" y2="2.286" width="0.1524" layer="21"/>
<wire x1="4.953" y1="2.54" x2="5.207" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.953" y1="-2.54" x2="5.207" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.207" y1="-2.286" x2="-4.953" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.207" y1="2.286" x2="-4.953" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.27" y1="0" x2="2.667" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.667" y1="0" x2="-2.159" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="1.27" x2="-2.159" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.159" y1="0" x2="-2.159" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-1.27" width="0.4064" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.9144" shape="octagon"/>
<text x="-4.826" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.635" y="-2.032" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C102-043X133">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 10.2 mm, outline 4.3 x 13.3 mm</description>
<wire x1="-3.175" y1="1.27" x2="-3.175" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.286" y1="1.27" x2="-2.286" y2="0" width="0.4064" layer="21"/>
<wire x1="3.81" y1="0" x2="-2.286" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="0" x2="-2.286" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-3.81" y1="0" x2="-3.175" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="0" x2="-3.175" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-6.096" y1="2.032" x2="6.096" y2="2.032" width="0.1524" layer="21"/>
<wire x1="6.604" y1="1.524" x2="6.604" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-2.032" x2="-6.096" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-6.604" y1="-1.524" x2="-6.604" y2="1.524" width="0.1524" layer="21"/>
<wire x1="6.096" y1="2.032" x2="6.604" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.096" y1="-2.032" x2="6.604" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="-1.524" x2="-6.096" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="1.524" x2="-6.096" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="1.016" shape="octagon"/>
<text x="-6.096" y="2.413" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.524" y="-1.651" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C102-054X133">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 10.2 mm, outline 5.4 x 13.3 mm</description>
<wire x1="-3.175" y1="1.27" x2="-3.175" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.286" y1="1.27" x2="-2.286" y2="0" width="0.4064" layer="21"/>
<wire x1="3.81" y1="0" x2="-2.286" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="0" x2="-2.286" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-3.81" y1="0" x2="-3.175" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="0" x2="-3.175" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-6.096" y1="2.54" x2="6.096" y2="2.54" width="0.1524" layer="21"/>
<wire x1="6.604" y1="2.032" x2="6.604" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-2.54" x2="-6.096" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-6.604" y1="-2.032" x2="-6.604" y2="2.032" width="0.1524" layer="21"/>
<wire x1="6.096" y1="2.54" x2="6.604" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.096" y1="-2.54" x2="6.604" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="-2.032" x2="-6.096" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="2.032" x2="-6.096" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="1.016" shape="octagon"/>
<text x="-6.096" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.524" y="-1.905" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C102-064X133">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 10.2 mm, outline 6.4 x 13.3 mm</description>
<wire x1="-3.175" y1="1.27" x2="-3.175" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.286" y1="1.27" x2="-2.286" y2="0" width="0.4064" layer="21"/>
<wire x1="3.81" y1="0" x2="-2.286" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="0" x2="-2.286" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-3.81" y1="0" x2="-3.175" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="0" x2="-3.175" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-6.096" y1="3.048" x2="6.096" y2="3.048" width="0.1524" layer="21"/>
<wire x1="6.604" y1="2.54" x2="6.604" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-3.048" x2="-6.096" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-6.604" y1="-2.54" x2="-6.604" y2="2.54" width="0.1524" layer="21"/>
<wire x1="6.096" y1="3.048" x2="6.604" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.096" y1="-3.048" x2="6.604" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="-2.54" x2="-6.096" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="2.54" x2="-6.096" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="1.016" shape="octagon"/>
<text x="-6.096" y="3.429" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.524" y="-2.032" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C102_152-062X184">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 10.2 mm + 15.2 mm, outline 6.2 x 18.4 mm</description>
<wire x1="-2.286" y1="1.27" x2="-2.286" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.286" y1="0" x2="-2.286" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-3.175" y1="1.27" x2="-3.175" y2="0" width="0.4064" layer="21"/>
<wire x1="-3.175" y1="0" x2="-3.175" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-3.683" y1="0" x2="-3.175" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="0" x2="3.683" y2="0" width="0.1524" layer="21"/>
<wire x1="6.477" y1="0" x2="8.636" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.096" y1="3.048" x2="6.223" y2="3.048" width="0.1524" layer="21"/>
<wire x1="6.223" y1="-3.048" x2="-6.096" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-6.604" y1="-2.54" x2="-6.604" y2="2.54" width="0.1524" layer="21"/>
<wire x1="6.223" y1="3.048" x2="6.731" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.223" y1="-3.048" x2="6.731" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="-2.54" x2="-6.096" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="2.54" x2="-6.096" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.731" y1="2.54" x2="6.731" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="11.176" y1="3.048" x2="11.684" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="11.176" y1="-3.048" x2="11.684" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="11.176" y1="-3.048" x2="7.112" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="7.112" y1="3.048" x2="11.176" y2="3.048" width="0.1524" layer="21"/>
<wire x1="11.684" y1="2.54" x2="11.684" y2="-2.54" width="0.1524" layer="21"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="3" x="10.033" y="0" drill="1.016" shape="octagon"/>
<text x="-5.969" y="3.429" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.524" y="-2.286" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C150-054X183">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 15 mm, outline 5.4 x 18.3 mm</description>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="0" width="0.4064" layer="21"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="1.27" x2="-4.191" y2="0" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="-4.191" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0" x2="-6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="9.017" y1="2.032" x2="9.017" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-2.54" x2="-8.509" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-9.017" y1="-2.032" x2="-9.017" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-8.509" y1="2.54" x2="8.509" y2="2.54" width="0.1524" layer="21"/>
<wire x1="8.509" y1="2.54" x2="9.017" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.509" y1="-2.54" x2="9.017" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="-2.032" x2="-8.509" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="2.032" x2="-8.509" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-7.493" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.493" y="0" drill="1.016" shape="octagon"/>
<text x="-8.382" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.032" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C150-064X183">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 15 mm, outline 6.4 x 18.3 mm</description>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="0" width="0.4064" layer="21"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="1.27" x2="-4.191" y2="0" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="-4.191" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0" x2="-6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="9.017" y1="2.54" x2="9.017" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-3.048" x2="-8.509" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-9.017" y1="-2.54" x2="-9.017" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-8.509" y1="3.048" x2="8.509" y2="3.048" width="0.1524" layer="21"/>
<wire x1="8.509" y1="3.048" x2="9.017" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.509" y1="-3.048" x2="9.017" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="-2.54" x2="-8.509" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="2.54" x2="-8.509" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-7.493" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.493" y="0" drill="1.016" shape="octagon"/>
<text x="-8.509" y="3.429" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.032" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C150-072X183">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 15 mm, outline 7.2 x 18.3 mm</description>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="0" width="0.4064" layer="21"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="1.27" x2="-4.191" y2="0" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="-4.191" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0" x2="-6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="9.017" y1="3.048" x2="9.017" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-3.556" x2="-8.509" y2="-3.556" width="0.1524" layer="21"/>
<wire x1="-9.017" y1="-3.048" x2="-9.017" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-8.509" y1="3.556" x2="8.509" y2="3.556" width="0.1524" layer="21"/>
<wire x1="8.509" y1="3.556" x2="9.017" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.509" y1="-3.556" x2="9.017" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="-3.048" x2="-8.509" y2="-3.556" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="3.048" x2="-8.509" y2="3.556" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-7.493" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.493" y="0" drill="1.016" shape="octagon"/>
<text x="-8.509" y="3.937" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.286" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C150-084X183">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 15 mm, outline 8.4 x 18.3 mm</description>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="0" width="0.4064" layer="21"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="1.27" x2="-4.191" y2="0" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="-4.191" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0" x2="-6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="9.017" y1="3.556" x2="9.017" y2="-3.556" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-4.064" x2="-8.509" y2="-4.064" width="0.1524" layer="21"/>
<wire x1="-9.017" y1="-3.556" x2="-9.017" y2="3.556" width="0.1524" layer="21"/>
<wire x1="-8.509" y1="4.064" x2="8.509" y2="4.064" width="0.1524" layer="21"/>
<wire x1="8.509" y1="4.064" x2="9.017" y2="3.556" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.509" y1="-4.064" x2="9.017" y2="-3.556" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="-3.556" x2="-8.509" y2="-4.064" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="3.556" x2="-8.509" y2="4.064" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-7.493" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.493" y="0" drill="1.016" shape="octagon"/>
<text x="-8.509" y="4.445" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.54" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C150-091X182">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 15 mm, outline 9.1 x 18.2 mm</description>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="0" width="0.4064" layer="21"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="1.27" x2="-4.191" y2="0" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="-4.191" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0" x2="-6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="9.017" y1="3.937" x2="9.017" y2="-3.937" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-4.445" x2="-8.509" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="-9.017" y1="-3.937" x2="-9.017" y2="3.937" width="0.1524" layer="21"/>
<wire x1="-8.509" y1="4.445" x2="8.509" y2="4.445" width="0.1524" layer="21"/>
<wire x1="8.509" y1="4.445" x2="9.017" y2="3.937" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.509" y1="-4.445" x2="9.017" y2="-3.937" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="-3.937" x2="-8.509" y2="-4.445" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="3.937" x2="-8.509" y2="4.445" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-7.493" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.493" y="0" drill="1.016" shape="octagon"/>
<text x="-8.509" y="4.826" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.54" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C225-062X268">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 22.5 mm, outline 6.2 x 26.8 mm</description>
<wire x1="-12.827" y1="3.048" x2="12.827" y2="3.048" width="0.1524" layer="21"/>
<wire x1="13.335" y1="2.54" x2="13.335" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="12.827" y1="-3.048" x2="-12.827" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-2.54" x2="-13.335" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="12.827" y1="3.048" x2="13.335" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="12.827" y1="-3.048" x2="13.335" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="-2.54" x2="-12.827" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="2.54" x2="-12.827" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="-9.652" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="9.652" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-11.303" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.303" y="0" drill="1.016" shape="octagon"/>
<text x="-12.7" y="3.429" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C225-074X268">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 22.5 mm, outline 7.4 x 26.8 mm</description>
<wire x1="-12.827" y1="3.556" x2="12.827" y2="3.556" width="0.1524" layer="21"/>
<wire x1="13.335" y1="3.048" x2="13.335" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="12.827" y1="-3.556" x2="-12.827" y2="-3.556" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-3.048" x2="-13.335" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="12.827" y1="3.556" x2="13.335" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="12.827" y1="-3.556" x2="13.335" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="-3.048" x2="-12.827" y2="-3.556" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="3.048" x2="-12.827" y2="3.556" width="0.1524" layer="21" curve="-90"/>
<wire x1="-9.652" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="9.652" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-11.303" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.303" y="0" drill="1.016" shape="octagon"/>
<text x="-12.827" y="3.937" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C225-087X268">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 22.5 mm, outline 8.7 x 26.8 mm</description>
<wire x1="-12.827" y1="4.318" x2="12.827" y2="4.318" width="0.1524" layer="21"/>
<wire x1="13.335" y1="3.81" x2="13.335" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="12.827" y1="-4.318" x2="-12.827" y2="-4.318" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-3.81" x2="-13.335" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="12.827" y1="4.318" x2="13.335" y2="3.81" width="0.1524" layer="21" curve="-90"/>
<wire x1="12.827" y1="-4.318" x2="13.335" y2="-3.81" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="-3.81" x2="-12.827" y2="-4.318" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="3.81" x2="-12.827" y2="4.318" width="0.1524" layer="21" curve="-90"/>
<wire x1="-9.652" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="9.652" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-11.303" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.303" y="0" drill="1.016" shape="octagon"/>
<text x="-12.827" y="4.699" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C225-108X268">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 22.5 mm, outline 10.8 x 26.8 mm</description>
<wire x1="-12.827" y1="5.334" x2="12.827" y2="5.334" width="0.1524" layer="21"/>
<wire x1="13.335" y1="4.826" x2="13.335" y2="-4.826" width="0.1524" layer="21"/>
<wire x1="12.827" y1="-5.334" x2="-12.827" y2="-5.334" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-4.826" x2="-13.335" y2="4.826" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="12.827" y1="5.334" x2="13.335" y2="4.826" width="0.1524" layer="21" curve="-90"/>
<wire x1="12.827" y1="-5.334" x2="13.335" y2="-4.826" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="-4.826" x2="-12.827" y2="-5.334" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="4.826" x2="-12.827" y2="5.334" width="0.1524" layer="21" curve="-90"/>
<wire x1="-9.652" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="9.652" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-11.303" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.303" y="0" drill="1.016" shape="octagon"/>
<text x="-12.954" y="5.715" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C225-113X268">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 22.5 mm, outline 11.3 x 26.8 mm</description>
<wire x1="-12.827" y1="5.588" x2="12.827" y2="5.588" width="0.1524" layer="21"/>
<wire x1="13.335" y1="5.08" x2="13.335" y2="-5.08" width="0.1524" layer="21"/>
<wire x1="12.827" y1="-5.588" x2="-12.827" y2="-5.588" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-5.08" x2="-13.335" y2="5.08" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="12.827" y1="5.588" x2="13.335" y2="5.08" width="0.1524" layer="21" curve="-90"/>
<wire x1="12.827" y1="-5.588" x2="13.335" y2="-5.08" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="-5.08" x2="-12.827" y2="-5.588" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="5.08" x2="-12.827" y2="5.588" width="0.1524" layer="21" curve="-90"/>
<wire x1="-9.652" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="9.652" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-11.303" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.303" y="0" drill="1.016" shape="octagon"/>
<text x="-12.954" y="5.969" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-093X316">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 9.3 x 31.6 mm</description>
<wire x1="-15.24" y1="4.572" x2="15.24" y2="4.572" width="0.1524" layer="21"/>
<wire x1="15.748" y1="4.064" x2="15.748" y2="-4.064" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-4.572" x2="-15.24" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-4.064" x2="-15.748" y2="4.064" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="4.572" x2="15.748" y2="4.064" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-4.572" x2="15.748" y2="-4.064" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-4.064" x2="-15.24" y2="-4.572" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="4.064" x2="-15.24" y2="4.572" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="4.953" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-113X316">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 11.3 x 31.6 mm</description>
<wire x1="-15.24" y1="5.588" x2="15.24" y2="5.588" width="0.1524" layer="21"/>
<wire x1="15.748" y1="5.08" x2="15.748" y2="-5.08" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-5.588" x2="-15.24" y2="-5.588" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-5.08" x2="-15.748" y2="5.08" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="5.588" x2="15.748" y2="5.08" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-5.588" x2="15.748" y2="-5.08" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-5.08" x2="-15.24" y2="-5.588" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="5.08" x2="-15.24" y2="5.588" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="5.969" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-134X316">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 13.4 x 31.6 mm</description>
<wire x1="-15.24" y1="6.604" x2="15.24" y2="6.604" width="0.1524" layer="21"/>
<wire x1="15.748" y1="6.096" x2="15.748" y2="-6.096" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-6.604" x2="-15.24" y2="-6.604" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-6.096" x2="-15.748" y2="6.096" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="6.604" x2="15.748" y2="6.096" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-6.604" x2="15.748" y2="-6.096" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-6.096" x2="-15.24" y2="-6.604" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="6.096" x2="-15.24" y2="6.604" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="6.985" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-205X316">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 20.5 x 31.6 mm</description>
<wire x1="-15.24" y1="10.16" x2="15.24" y2="10.16" width="0.1524" layer="21"/>
<wire x1="15.748" y1="9.652" x2="15.748" y2="-9.652" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-10.16" x2="-15.24" y2="-10.16" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-9.652" x2="-15.748" y2="9.652" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="10.16" x2="15.748" y2="9.652" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-10.16" x2="15.748" y2="-9.652" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-9.652" x2="-15.24" y2="-10.16" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="9.652" x2="-15.24" y2="10.16" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="10.541" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-4.318" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C325-137X374">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 32.5 mm, outline 13.7 x 37.4 mm</description>
<wire x1="-14.2748" y1="0" x2="-12.7" y2="0" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="1.905" x2="-12.7" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="1.905" x2="-11.811" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="0" x2="14.2748" y2="0" width="0.1524" layer="21"/>
<wire x1="-11.811" y1="0" x2="-11.811" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-12.7" y1="0" x2="-12.7" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="18.542" y1="6.731" x2="18.542" y2="-6.731" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="6.731" x2="-18.542" y2="-6.731" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="-6.731" x2="18.542" y2="-6.731" width="0.1524" layer="21"/>
<wire x1="18.542" y1="6.731" x2="-18.542" y2="6.731" width="0.1524" layer="21"/>
<pad name="1" x="-16.256" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="16.256" y="0" drill="1.1938" shape="octagon"/>
<text x="-18.2372" y="7.0612" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-10.8458" y="-2.8702" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C325-162X374">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 32.5 mm, outline 16.2 x 37.4 mm</description>
<wire x1="-14.2748" y1="0" x2="-12.7" y2="0" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="1.905" x2="-12.7" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="1.905" x2="-11.811" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="0" x2="14.2748" y2="0" width="0.1524" layer="21"/>
<wire x1="-11.811" y1="0" x2="-11.811" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-12.7" y1="0" x2="-12.7" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="18.542" y1="8.001" x2="18.542" y2="-8.001" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="8.001" x2="-18.542" y2="-8.001" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="-8.001" x2="18.542" y2="-8.001" width="0.1524" layer="21"/>
<wire x1="18.542" y1="8.001" x2="-18.542" y2="8.001" width="0.1524" layer="21"/>
<pad name="1" x="-16.256" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="16.256" y="0" drill="1.1938" shape="octagon"/>
<text x="-18.3642" y="8.3312" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-10.8458" y="-2.8702" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C325-182X374">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 32.5 mm, outline 18.2 x 37.4 mm</description>
<wire x1="-14.2748" y1="0" x2="-12.7" y2="0" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="1.905" x2="-12.7" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="1.905" x2="-11.811" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="0" x2="14.2748" y2="0" width="0.1524" layer="21"/>
<wire x1="-11.811" y1="0" x2="-11.811" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-12.7" y1="0" x2="-12.7" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="18.542" y1="9.017" x2="18.542" y2="-9.017" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="9.017" x2="-18.542" y2="-9.017" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="-9.017" x2="18.542" y2="-9.017" width="0.1524" layer="21"/>
<wire x1="18.542" y1="9.017" x2="-18.542" y2="9.017" width="0.1524" layer="21"/>
<pad name="1" x="-16.256" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="16.256" y="0" drill="1.1938" shape="octagon"/>
<text x="-18.3642" y="9.3472" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-10.8458" y="-2.8702" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C375-192X418">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 37.5 mm, outline 19.2 x 41.8 mm</description>
<wire x1="-20.32" y1="8.509" x2="20.32" y2="8.509" width="0.1524" layer="21"/>
<wire x1="20.828" y1="8.001" x2="20.828" y2="-8.001" width="0.1524" layer="21"/>
<wire x1="20.32" y1="-8.509" x2="-20.32" y2="-8.509" width="0.1524" layer="21"/>
<wire x1="-20.828" y1="-8.001" x2="-20.828" y2="8.001" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="20.32" y1="8.509" x2="20.828" y2="8.001" width="0.1524" layer="21" curve="-90"/>
<wire x1="20.32" y1="-8.509" x2="20.828" y2="-8.001" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="-8.001" x2="-20.32" y2="-8.509" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="8.001" x2="-20.32" y2="8.509" width="0.1524" layer="21" curve="-90"/>
<wire x1="-16.002" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="16.002" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-18.796" y="0" drill="1.3208" shape="octagon"/>
<pad name="2" x="18.796" y="0" drill="1.3208" shape="octagon"/>
<text x="-20.447" y="8.89" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C375-203X418">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 37.5 mm, outline 20.3 x 41.8 mm</description>
<wire x1="-20.32" y1="10.16" x2="20.32" y2="10.16" width="0.1524" layer="21"/>
<wire x1="20.828" y1="9.652" x2="20.828" y2="-9.652" width="0.1524" layer="21"/>
<wire x1="20.32" y1="-10.16" x2="-20.32" y2="-10.16" width="0.1524" layer="21"/>
<wire x1="-20.828" y1="-9.652" x2="-20.828" y2="9.652" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="20.32" y1="10.16" x2="20.828" y2="9.652" width="0.1524" layer="21" curve="-90"/>
<wire x1="20.32" y1="-10.16" x2="20.828" y2="-9.652" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="-9.652" x2="-20.32" y2="-10.16" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="9.652" x2="-20.32" y2="10.16" width="0.1524" layer="21" curve="-90"/>
<wire x1="-16.002" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="16.002" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-18.796" y="0" drill="1.3208" shape="octagon"/>
<pad name="2" x="18.796" y="0" drill="1.3208" shape="octagon"/>
<text x="-20.32" y="10.541" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-035X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 3.5 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="1.524" x2="-3.683" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-1.778" x2="3.429" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-1.524" x2="3.683" y2="1.524" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.778" x2="-3.429" y2="1.778" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.778" x2="3.683" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-1.778" x2="3.683" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-1.524" x2="-3.429" y2="-1.778" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="1.524" x2="-3.429" y2="1.778" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.556" y="2.159" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.556" y="-3.429" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C375-155X418">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 37.5 mm, outline 15.5 x 41.8 mm</description>
<wire x1="-20.32" y1="7.62" x2="20.32" y2="7.62" width="0.1524" layer="21"/>
<wire x1="20.828" y1="7.112" x2="20.828" y2="-7.112" width="0.1524" layer="21"/>
<wire x1="20.32" y1="-7.62" x2="-20.32" y2="-7.62" width="0.1524" layer="21"/>
<wire x1="-20.828" y1="-7.112" x2="-20.828" y2="7.112" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="20.32" y1="7.62" x2="20.828" y2="7.112" width="0.1524" layer="21" curve="-90"/>
<wire x1="20.32" y1="-7.62" x2="20.828" y2="-7.112" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="-7.112" x2="-20.32" y2="-7.62" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="7.112" x2="-20.32" y2="7.62" width="0.1524" layer="21" curve="-90"/>
<wire x1="-16.002" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="16.002" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-18.796" y="0" drill="1.3208" shape="octagon"/>
<pad name="2" x="18.796" y="0" drill="1.3208" shape="octagon"/>
<text x="-20.447" y="8.001" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C075-063X106">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 7.5 mm, outline 6.3 x 10.6 mm</description>
<wire x1="4.953" y1="3.048" x2="-4.953" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="2.794" x2="-5.207" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="-3.048" x2="4.953" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="5.207" y1="-2.794" x2="5.207" y2="2.794" width="0.1524" layer="21"/>
<wire x1="4.953" y1="3.048" x2="5.207" y2="2.794" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.953" y1="-3.048" x2="5.207" y2="-2.794" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.207" y1="-2.794" x2="-4.953" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.207" y1="2.794" x2="-4.953" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.27" y1="0" x2="2.667" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.667" y1="0" x2="-2.159" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="1.27" x2="-2.159" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.159" y1="0" x2="-2.159" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-1.27" width="0.4064" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.9144" shape="octagon"/>
<text x="-4.826" y="3.429" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-154X316">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 15.4 x 31.6 mm</description>
<wire x1="-15.24" y1="7.62" x2="15.24" y2="7.62" width="0.1524" layer="21"/>
<wire x1="15.748" y1="7.112" x2="15.748" y2="-7.112" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-7.62" x2="-15.24" y2="-7.62" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-7.112" x2="-15.748" y2="7.112" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="7.62" x2="15.748" y2="7.112" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-7.62" x2="15.748" y2="-7.112" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-7.112" x2="-15.24" y2="-7.62" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="7.112" x2="-15.24" y2="7.62" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="8.001" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-173X316">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 17.3 x 31.6 mm</description>
<wire x1="-15.24" y1="8.509" x2="15.24" y2="8.509" width="0.1524" layer="21"/>
<wire x1="15.748" y1="8.001" x2="15.748" y2="-8.001" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-8.509" x2="-15.24" y2="-8.509" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-8.001" x2="-15.748" y2="8.001" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="8.509" x2="15.748" y2="8.001" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-8.509" x2="15.748" y2="-8.001" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-8.001" x2="-15.24" y2="-8.509" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="8.001" x2="-15.24" y2="8.509" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="8.89" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C0402K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 0204 Reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 1005</description>
<wire x1="-0.425" y1="0.2" x2="0.425" y2="0.2" width="0.1016" layer="51"/>
<wire x1="0.425" y1="-0.2" x2="-0.425" y2="-0.2" width="0.1016" layer="51"/>
<smd name="1" x="-0.6" y="0" dx="0.925" dy="0.74" layer="1"/>
<smd name="2" x="0.6" y="0" dx="0.925" dy="0.74" layer="1"/>
<text x="-0.5" y="0.425" size="1.016" layer="25">&gt;NAME</text>
<text x="-0.5" y="-1.45" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.5" y1="-0.25" x2="-0.225" y2="0.25" layer="51"/>
<rectangle x1="0.225" y1="-0.25" x2="0.5" y2="0.25" layer="51"/>
</package>
<package name="C0603K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 0603 Reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 1608</description>
<wire x1="-0.725" y1="0.35" x2="0.725" y2="0.35" width="0.1016" layer="51"/>
<wire x1="0.725" y1="-0.35" x2="-0.725" y2="-0.35" width="0.1016" layer="51"/>
<smd name="1" x="-0.875" y="0" dx="1.05" dy="1.08" layer="1"/>
<smd name="2" x="0.875" y="0" dx="1.05" dy="1.08" layer="1"/>
<text x="-0.8" y="0.65" size="1.016" layer="25">&gt;NAME</text>
<text x="-0.8" y="-1.65" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.8" y1="-0.4" x2="-0.45" y2="0.4" layer="51"/>
<rectangle x1="0.45" y1="-0.4" x2="0.8" y2="0.4" layer="51"/>
</package>
<package name="C0805K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 0805 Reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 2012</description>
<wire x1="-0.925" y1="0.6" x2="0.925" y2="0.6" width="0.1016" layer="51"/>
<wire x1="0.925" y1="-0.6" x2="-0.925" y2="-0.6" width="0.1016" layer="51"/>
<smd name="1" x="-1" y="0" dx="1.3" dy="1.6" layer="1"/>
<smd name="2" x="1" y="0" dx="1.3" dy="1.6" layer="1"/>
<text x="-1" y="0.875" size="1.016" layer="25">&gt;NAME</text>
<text x="-1" y="-1.9" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1" y1="-0.65" x2="-0.5" y2="0.65" layer="51"/>
<rectangle x1="0.5" y1="-0.65" x2="1" y2="0.65" layer="51"/>
</package>
<package name="C1206K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 1206 Reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 3216</description>
<wire x1="-1.525" y1="0.75" x2="1.525" y2="0.75" width="0.1016" layer="51"/>
<wire x1="1.525" y1="-0.75" x2="-1.525" y2="-0.75" width="0.1016" layer="51"/>
<smd name="1" x="-1.5" y="0" dx="1.5" dy="2" layer="1"/>
<smd name="2" x="1.5" y="0" dx="1.5" dy="2" layer="1"/>
<text x="-1.6" y="1.1" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.6" y="-2.1" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6" y1="-0.8" x2="-1.1" y2="0.8" layer="51"/>
<rectangle x1="1.1" y1="-0.8" x2="1.6" y2="0.8" layer="51"/>
</package>
<package name="C1210K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 1210 Reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 3225</description>
<wire x1="-1.525" y1="1.175" x2="1.525" y2="1.175" width="0.1016" layer="51"/>
<wire x1="1.525" y1="-1.175" x2="-1.525" y2="-1.175" width="0.1016" layer="51"/>
<smd name="1" x="-1.5" y="0" dx="1.5" dy="2.9" layer="1"/>
<smd name="2" x="1.5" y="0" dx="1.5" dy="2.9" layer="1"/>
<text x="-1.6" y="1.55" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.6" y="-2.575" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6" y1="-1.25" x2="-1.1" y2="1.25" layer="51"/>
<rectangle x1="1.1" y1="-1.25" x2="1.6" y2="1.25" layer="51"/>
</package>
<package name="C1812K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 1812 Reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 4532</description>
<wire x1="-2.175" y1="1.525" x2="2.175" y2="1.525" width="0.1016" layer="51"/>
<wire x1="2.175" y1="-1.525" x2="-2.175" y2="-1.525" width="0.1016" layer="51"/>
<smd name="1" x="-2.05" y="0" dx="1.8" dy="3.7" layer="1"/>
<smd name="2" x="2.05" y="0" dx="1.8" dy="3.7" layer="1"/>
<text x="-2.25" y="1.95" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.25" y="-2.975" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-2.25" y1="-1.6" x2="-1.65" y2="1.6" layer="51"/>
<rectangle x1="1.65" y1="-1.6" x2="2.25" y2="1.6" layer="51"/>
</package>
<package name="C1825K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 1825 Reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 4564</description>
<wire x1="-1.525" y1="3.125" x2="1.525" y2="3.125" width="0.1016" layer="51"/>
<wire x1="1.525" y1="-3.125" x2="-1.525" y2="-3.125" width="0.1016" layer="51"/>
<smd name="1" x="-1.5" y="0" dx="1.8" dy="6.9" layer="1"/>
<smd name="2" x="1.5" y="0" dx="1.8" dy="6.9" layer="1"/>
<text x="-1.6" y="3.55" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.6" y="-4.625" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6" y1="-3.2" x2="-1.1" y2="3.2" layer="51"/>
<rectangle x1="1.1" y1="-3.2" x2="1.6" y2="3.2" layer="51"/>
</package>
<package name="C2220K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 2220 Reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 5650</description>
<wire x1="-2.725" y1="2.425" x2="2.725" y2="2.425" width="0.1016" layer="51"/>
<wire x1="2.725" y1="-2.425" x2="-2.725" y2="-2.425" width="0.1016" layer="51"/>
<smd name="1" x="-2.55" y="0" dx="1.85" dy="5.5" layer="1"/>
<smd name="2" x="2.55" y="0" dx="1.85" dy="5.5" layer="1"/>
<text x="-2.8" y="2.95" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.8" y="-3.975" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-2.8" y1="-2.5" x2="-2.2" y2="2.5" layer="51"/>
<rectangle x1="2.2" y1="-2.5" x2="2.8" y2="2.5" layer="51"/>
</package>
<package name="C2225K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 2225 Reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 5664</description>
<wire x1="-2.725" y1="3.075" x2="2.725" y2="3.075" width="0.1016" layer="51"/>
<wire x1="2.725" y1="-3.075" x2="-2.725" y2="-3.075" width="0.1016" layer="51"/>
<smd name="1" x="-2.55" y="0" dx="1.85" dy="6.8" layer="1"/>
<smd name="2" x="2.55" y="0" dx="1.85" dy="6.8" layer="1"/>
<text x="-2.8" y="3.6" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.8" y="-4.575" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-2.8" y1="-3.15" x2="-2.2" y2="3.15" layer="51"/>
<rectangle x1="2.2" y1="-3.15" x2="2.8" y2="3.15" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="C-EU">
<wire x1="0" y1="0" x2="0" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-2.032" width="0.1524" layer="94"/>
<text x="1.524" y="0.381" size="1.778" layer="95">&gt;NAME</text>
<text x="1.524" y="-4.699" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.032" y1="-2.032" x2="2.032" y2="-1.524" layer="94"/>
<rectangle x1="-2.032" y1="-1.016" x2="2.032" y2="-0.508" layer="94"/>
<pin name="1" x="0" y="2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="C-EU" prefix="C" uservalue="yes">
<description>&lt;B&gt;CAPACITOR&lt;/B&gt;, European symbol</description>
<gates>
<gate name="G$1" symbol="C-EU" x="0" y="0"/>
</gates>
<devices>
<device name="C0402" package="C0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0504" package="C0504">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0603" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0805" package="C0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1005" package="C1005">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1206" package="C1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1210" package="C1210">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1310" package="C1310">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1608" package="C1608">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1812" package="C1812">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1825" package="C1825">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C2012" package="C2012">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C3216" package="C3216">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C3225" package="C3225">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C4532" package="C4532">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C4564" package="C4564">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025-024X044" package="C025-024X044">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025-025X050" package="C025-025X050">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025-030X050" package="C025-030X050">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025-040X050" package="C025-040X050">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025-050X050" package="C025-050X050">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025-060X050" package="C025-060X050">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C025_050-024X070" package="C025_050-024X070">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025_050-025X075" package="C025_050-025X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025_050-035X075" package="C025_050-035X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025_050-045X075" package="C025_050-045X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025_050-055X075" package="C025_050-055X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-024X044" package="C050-024X044">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-025X075" package="C050-025X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-045X075" package="C050-045X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-030X075" package="C050-030X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-050X075" package="C050-050X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-055X075" package="C050-055X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-075X075" package="C050-075X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050H075X075" package="C050H075X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="075-032X103" package="C075-032X103">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="075-042X103" package="C075-042X103">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="075-052X106" package="C075-052X106">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="102-043X133" package="C102-043X133">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="102-054X133" package="C102-054X133">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="102-064X133" package="C102-064X133">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="102_152-062X184" package="C102_152-062X184">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="150-054X183" package="C150-054X183">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="150-064X183" package="C150-064X183">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="150-072X183" package="C150-072X183">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="150-084X183" package="C150-084X183">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="150-091X182" package="C150-091X182">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="225-062X268" package="C225-062X268">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="225-074X268" package="C225-074X268">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="225-087X268" package="C225-087X268">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="225-108X268" package="C225-108X268">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="225-113X268" package="C225-113X268">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="275-093X316" package="C275-093X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="275-113X316" package="C275-113X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="275-134X316" package="C275-134X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="275-205X316" package="C275-205X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="325-137X374" package="C325-137X374">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="325-162X374" package="C325-162X374">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="325-182X374" package="C325-182X374">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="375-192X418" package="C375-192X418">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="375-203X418" package="C375-203X418">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-035X075" package="C050-035X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="375-155X418" package="C375-155X418">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="075-063X106" package="C075-063X106">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="275-154X316" package="C275-154X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="275-173X316" package="C275-173X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0402K" package="C0402K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0603K" package="C0603K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0805K" package="C0805K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1206K" package="C1206K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1210K" package="C1210K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1812K" package="C1812K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1825K" package="C1825K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C2220K" package="C2220K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C2225K" package="C2225K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="ADS62P45">
<packages>
<package name="QFN50P900X900X100-65N">
<description>AD Converter ADS62P45 in VQFN-64 Package</description>
<wire x1="-5.2324" y1="-0.7366" x2="-6.223" y2="-0.7366" width="0.1524" layer="21"/>
<wire x1="-1.7018" y1="5.207" x2="-1.7018" y2="6.223" width="0.1524" layer="21"/>
<wire x1="3.2512" y1="5.207" x2="3.2512" y2="6.2484" width="0.1524" layer="21"/>
<wire x1="-2.2098" y1="-5.1816" x2="-2.2098" y2="-6.1976" width="0.1524" layer="21"/>
<wire x1="2.7686" y1="-5.207" x2="2.7686" y2="-6.2484" width="0.1524" layer="21"/>
<wire x1="5.2324" y1="-0.2794" x2="6.223" y2="-0.2794" width="0.1524" layer="21"/>
<wire x1="4.572" y1="4.2418" x2="4.572" y2="4.572" width="0.1524" layer="21"/>
<wire x1="-4.2418" y1="4.572" x2="-4.572" y2="4.572" width="0.1524" layer="21"/>
<wire x1="-4.572" y1="-4.572" x2="-4.2418" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="4.572" y1="-4.572" x2="4.572" y2="-4.2418" width="0.1524" layer="21"/>
<wire x1="4.572" y1="4.572" x2="4.2418" y2="4.572" width="0.1524" layer="21"/>
<wire x1="-4.572" y1="4.572" x2="-4.572" y2="4.2418" width="0.1524" layer="21"/>
<wire x1="-4.572" y1="-4.2418" x2="-4.572" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="4.2418" y1="-4.572" x2="4.572" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="-4.572" y1="3.302" x2="-3.302" y2="4.572" width="0" layer="51"/>
<wire x1="3.9116" y1="4.572" x2="3.6068" y2="4.572" width="0" layer="51"/>
<wire x1="3.4036" y1="4.572" x2="3.0988" y2="4.572" width="0" layer="51"/>
<wire x1="2.8956" y1="4.572" x2="2.5908" y2="4.572" width="0" layer="51"/>
<wire x1="2.3876" y1="4.572" x2="2.1082" y2="4.572" width="0" layer="51"/>
<wire x1="1.905" y1="4.572" x2="1.6002" y2="4.572" width="0" layer="51"/>
<wire x1="1.397" y1="4.572" x2="1.0922" y2="4.572" width="0" layer="51"/>
<wire x1="0.889" y1="4.572" x2="0.6096" y2="4.572" width="0" layer="51"/>
<wire x1="0.4064" y1="4.572" x2="0.1016" y2="4.572" width="0" layer="51"/>
<wire x1="-0.1016" y1="4.572" x2="-0.4064" y2="4.572" width="0" layer="51"/>
<wire x1="-0.6096" y1="4.572" x2="-0.889" y2="4.572" width="0" layer="51"/>
<wire x1="-1.0922" y1="4.572" x2="-1.397" y2="4.572" width="0" layer="51"/>
<wire x1="-1.6002" y1="4.572" x2="-1.905" y2="4.572" width="0" layer="51"/>
<wire x1="-2.1082" y1="4.572" x2="-2.3876" y2="4.572" width="0" layer="51"/>
<wire x1="-2.5908" y1="4.572" x2="-2.8956" y2="4.572" width="0" layer="51"/>
<wire x1="-3.0988" y1="4.572" x2="-3.4036" y2="4.572" width="0" layer="51"/>
<wire x1="-3.6068" y1="4.572" x2="-3.9116" y2="4.572" width="0" layer="51"/>
<wire x1="-4.572" y1="3.9116" x2="-4.572" y2="3.6068" width="0" layer="51"/>
<wire x1="-4.572" y1="3.4036" x2="-4.572" y2="3.0988" width="0" layer="51"/>
<wire x1="-4.572" y1="2.8956" x2="-4.572" y2="2.5908" width="0" layer="51"/>
<wire x1="-4.572" y1="2.3876" x2="-4.572" y2="2.1082" width="0" layer="51"/>
<wire x1="-4.572" y1="1.905" x2="-4.572" y2="1.6002" width="0" layer="51"/>
<wire x1="-4.572" y1="1.397" x2="-4.572" y2="1.0922" width="0" layer="51"/>
<wire x1="-4.572" y1="0.889" x2="-4.572" y2="0.6096" width="0" layer="51"/>
<wire x1="-4.572" y1="0.4064" x2="-4.572" y2="0.1016" width="0" layer="51"/>
<wire x1="-4.572" y1="-0.1016" x2="-4.572" y2="-0.4064" width="0" layer="51"/>
<wire x1="-4.572" y1="-0.6096" x2="-4.572" y2="-0.889" width="0" layer="51"/>
<wire x1="-4.572" y1="-1.0922" x2="-4.572" y2="-1.397" width="0" layer="51"/>
<wire x1="-4.572" y1="-1.6002" x2="-4.572" y2="-1.905" width="0" layer="51"/>
<wire x1="-4.572" y1="-2.1082" x2="-4.572" y2="-2.3876" width="0" layer="51"/>
<wire x1="-4.572" y1="-2.5908" x2="-4.572" y2="-2.8956" width="0" layer="51"/>
<wire x1="-4.572" y1="-3.0988" x2="-4.572" y2="-3.4036" width="0" layer="51"/>
<wire x1="-4.572" y1="-3.6068" x2="-4.572" y2="-3.9116" width="0" layer="51"/>
<wire x1="-3.9116" y1="-4.572" x2="-3.6068" y2="-4.572" width="0" layer="51"/>
<wire x1="-3.4036" y1="-4.572" x2="-3.0988" y2="-4.572" width="0" layer="51"/>
<wire x1="-2.8956" y1="-4.572" x2="-2.5908" y2="-4.572" width="0" layer="51"/>
<wire x1="-2.3876" y1="-4.572" x2="-2.1082" y2="-4.572" width="0" layer="51"/>
<wire x1="-1.905" y1="-4.572" x2="-1.6002" y2="-4.572" width="0" layer="51"/>
<wire x1="-1.397" y1="-4.572" x2="-1.0922" y2="-4.572" width="0" layer="51"/>
<wire x1="-0.889" y1="-4.572" x2="-0.6096" y2="-4.572" width="0" layer="51"/>
<wire x1="-0.4064" y1="-4.572" x2="-0.1016" y2="-4.572" width="0" layer="51"/>
<wire x1="0.1016" y1="-4.572" x2="0.4064" y2="-4.572" width="0" layer="51"/>
<wire x1="0.6096" y1="-4.572" x2="0.889" y2="-4.572" width="0" layer="51"/>
<wire x1="1.0922" y1="-4.572" x2="1.397" y2="-4.572" width="0" layer="51"/>
<wire x1="1.6002" y1="-4.572" x2="1.905" y2="-4.572" width="0" layer="51"/>
<wire x1="2.1082" y1="-4.572" x2="2.3876" y2="-4.572" width="0" layer="51"/>
<wire x1="2.5908" y1="-4.572" x2="2.8956" y2="-4.572" width="0" layer="51"/>
<wire x1="3.0988" y1="-4.572" x2="3.4036" y2="-4.572" width="0" layer="51"/>
<wire x1="3.6068" y1="-4.572" x2="3.9116" y2="-4.572" width="0" layer="51"/>
<wire x1="4.572" y1="-3.9116" x2="4.572" y2="-3.6068" width="0" layer="51"/>
<wire x1="4.572" y1="-3.4036" x2="4.572" y2="-3.0988" width="0" layer="51"/>
<wire x1="4.572" y1="-2.8956" x2="4.572" y2="-2.5908" width="0" layer="51"/>
<wire x1="4.572" y1="-2.3876" x2="4.572" y2="-2.1082" width="0" layer="51"/>
<wire x1="4.572" y1="-1.905" x2="4.572" y2="-1.6002" width="0" layer="51"/>
<wire x1="4.572" y1="-1.397" x2="4.572" y2="-1.0922" width="0" layer="51"/>
<wire x1="4.572" y1="-0.889" x2="4.572" y2="-0.6096" width="0" layer="51"/>
<wire x1="4.572" y1="-0.4064" x2="4.572" y2="-0.1016" width="0" layer="51"/>
<wire x1="4.572" y1="0.1016" x2="4.572" y2="0.4064" width="0" layer="51"/>
<wire x1="4.572" y1="0.6096" x2="4.572" y2="0.889" width="0" layer="51"/>
<wire x1="4.572" y1="1.0922" x2="4.572" y2="1.397" width="0" layer="51"/>
<wire x1="4.572" y1="1.6002" x2="4.572" y2="1.905" width="0" layer="51"/>
<wire x1="4.572" y1="2.1082" x2="4.572" y2="2.3876" width="0" layer="51"/>
<wire x1="4.572" y1="2.5908" x2="4.572" y2="2.8956" width="0" layer="51"/>
<wire x1="4.572" y1="3.0988" x2="4.572" y2="3.4036" width="0" layer="51"/>
<wire x1="4.572" y1="3.6068" x2="4.572" y2="3.9116" width="0" layer="51"/>
<wire x1="-4.572" y1="-4.572" x2="4.572" y2="-4.572" width="0" layer="51"/>
<wire x1="4.572" y1="-4.572" x2="4.572" y2="4.572" width="0" layer="51"/>
<wire x1="4.572" y1="4.572" x2="-4.572" y2="4.572" width="0" layer="51"/>
<wire x1="-4.572" y1="4.572" x2="-4.572" y2="-4.572" width="0" layer="51"/>
<smd name="1" x="-4.788" y="3.7592" dx="0.3048" dy="1.5" layer="1" rot="R270"/>
<smd name="2" x="-4.7879" y="3.2512" dx="0.3048" dy="1.5" layer="1" rot="R270"/>
<smd name="3" x="-4.7878" y="2.7432" dx="0.3048" dy="1.5" layer="1" rot="R270"/>
<smd name="4" x="-4.7878" y="2.2606" dx="0.3048" dy="1.5" layer="1" rot="R270"/>
<smd name="5" x="-4.7879" y="1.7526" dx="0.3048" dy="1.5" layer="1" rot="R270"/>
<smd name="6" x="-4.7879" y="1.2446" dx="0.3048" dy="1.5" layer="1" rot="R270"/>
<smd name="7" x="-4.7878" y="0.762" dx="0.3048" dy="1.5" layer="1" rot="R270"/>
<smd name="8" x="-4.7878" y="0.254" dx="0.3048" dy="1.5" layer="1" rot="R270"/>
<smd name="9" x="-4.7878" y="-0.254" dx="0.3048" dy="1.5" layer="1" rot="R270"/>
<smd name="10" x="-4.7878" y="-0.762" dx="0.3048" dy="1.5" layer="1" rot="R270"/>
<smd name="11" x="-4.7879" y="-1.2446" dx="0.3048" dy="1.5" layer="1" rot="R270"/>
<smd name="12" x="-4.788" y="-1.7526" dx="0.3048" dy="1.5" layer="1" rot="R270"/>
<smd name="13" x="-4.7878" y="-2.2606" dx="0.3048" dy="1.5" layer="1" rot="R270"/>
<smd name="14" x="-4.7879" y="-2.7432" dx="0.3048" dy="1.5" layer="1" rot="R270"/>
<smd name="15" x="-4.788" y="-3.2512" dx="0.3048" dy="1.5" layer="1" rot="R270"/>
<smd name="16" x="-4.7878" y="-3.7592" dx="0.3048" dy="1.5" layer="1" rot="R270"/>
<smd name="17" x="-3.7592" y="-4.7878" dx="0.3048" dy="1.5" layer="1" rot="R180"/>
<smd name="18" x="-3.2512" y="-4.7879" dx="0.3048" dy="1.5" layer="1" rot="R180"/>
<smd name="19" x="-2.7432" y="-4.7878" dx="0.3048" dy="1.5" layer="1" rot="R180"/>
<smd name="20" x="-2.2606" y="-4.7878" dx="0.3048" dy="1.5" layer="1" rot="R180"/>
<smd name="21" x="-1.7526" y="-4.7879" dx="0.3048" dy="1.5" layer="1" rot="R180"/>
<smd name="22" x="-1.2446" y="-4.7879" dx="0.3048" dy="1.5" layer="1" rot="R180"/>
<smd name="23" x="-0.762" y="-4.7879" dx="0.3048" dy="1.5" layer="1" rot="R180"/>
<smd name="24" x="-0.254" y="-4.788" dx="0.3048" dy="1.5" layer="1" rot="R180"/>
<smd name="25" x="0.254" y="-4.7879" dx="0.3048" dy="1.5" layer="1" rot="R180"/>
<smd name="26" x="0.762" y="-4.7879" dx="0.3048" dy="1.5" layer="1" rot="R180"/>
<smd name="27" x="1.2446" y="-4.7878" dx="0.3048" dy="1.5" layer="1" rot="R180"/>
<smd name="28" x="1.7526" y="-4.7879" dx="0.3048" dy="1.5" layer="1" rot="R180"/>
<smd name="29" x="2.2606" y="-4.788" dx="0.3048" dy="1.5" layer="1" rot="R180"/>
<smd name="30" x="2.7432" y="-4.7879" dx="0.3048" dy="1.5" layer="1" rot="R180"/>
<smd name="31" x="3.2512" y="-4.7878" dx="0.3048" dy="1.5" layer="1" rot="R180"/>
<smd name="32" x="3.7592" y="-4.7879" dx="0.3048" dy="1.5" layer="1" rot="R180"/>
<smd name="33" x="4.7879" y="-3.7592" dx="0.3048" dy="1.5" layer="1" rot="R270"/>
<smd name="34" x="4.7878" y="-3.2512" dx="0.3048" dy="1.5" layer="1" rot="R270"/>
<smd name="35" x="4.7879" y="-2.7432" dx="0.3048" dy="1.5" layer="1" rot="R270"/>
<smd name="36" x="4.788" y="-2.2606" dx="0.3048" dy="1.5" layer="1" rot="R270"/>
<smd name="37" x="4.7879" y="-1.7526" dx="0.3048" dy="1.5" layer="1" rot="R270"/>
<smd name="38" x="4.7879" y="-1.2446" dx="0.3048" dy="1.5" layer="1" rot="R270"/>
<smd name="39" x="4.7878" y="-0.762" dx="0.3048" dy="1.5" layer="1" rot="R270"/>
<smd name="40" x="4.7879" y="-0.254" dx="0.3048" dy="1.5" layer="1" rot="R270"/>
<smd name="41" x="4.788" y="0.254" dx="0.3048" dy="1.5" layer="1" rot="R270"/>
<smd name="42" x="4.7879" y="0.762" dx="0.3048" dy="1.5" layer="1" rot="R270"/>
<smd name="43" x="4.7879" y="1.2446" dx="0.3048" dy="1.5" layer="1" rot="R270"/>
<smd name="44" x="4.7879" y="1.7526" dx="0.3048" dy="1.5" layer="1" rot="R270"/>
<smd name="45" x="4.788" y="2.2606" dx="0.3048" dy="1.5" layer="1" rot="R270"/>
<smd name="46" x="4.7879" y="2.7432" dx="0.3048" dy="1.5" layer="1" rot="R270"/>
<smd name="47" x="4.7879" y="3.2512" dx="0.3048" dy="1.5" layer="1" rot="R270"/>
<smd name="48" x="4.7878" y="3.7592" dx="0.3048" dy="1.5" layer="1" rot="R270"/>
<smd name="49" x="3.7592" y="4.7879" dx="0.3048" dy="1.5" layer="1" rot="R180"/>
<smd name="50" x="3.2512" y="4.7879" dx="0.3048" dy="1.5" layer="1" rot="R180"/>
<smd name="51" x="2.7432" y="4.788" dx="0.3048" dy="1.5" layer="1" rot="R180"/>
<smd name="52" x="2.2606" y="4.788" dx="0.3048" dy="1.5" layer="1" rot="R180"/>
<smd name="53" x="1.7526" y="4.7878" dx="0.3048" dy="1.5" layer="1" rot="R180"/>
<smd name="54" x="1.2446" y="4.7878" dx="0.3048" dy="1.5" layer="1" rot="R180"/>
<smd name="55" x="0.762" y="4.7878" dx="0.3048" dy="1.5" layer="1" rot="R180"/>
<smd name="56" x="0.254" y="4.788" dx="0.3048" dy="1.5" layer="1" rot="R180"/>
<smd name="57" x="-0.254" y="4.7879" dx="0.3048" dy="1.5" layer="1" rot="R180"/>
<smd name="58" x="-0.762" y="4.7879" dx="0.3048" dy="1.5" layer="1" rot="R180"/>
<smd name="59" x="-1.2446" y="4.7879" dx="0.3048" dy="1.5" layer="1" rot="R180"/>
<smd name="60" x="-1.7526" y="4.7879" dx="0.3048" dy="1.5" layer="1" rot="R180"/>
<smd name="61" x="-2.2606" y="4.7879" dx="0.3048" dy="1.5" layer="1" rot="R180"/>
<smd name="62" x="-2.7432" y="4.7879" dx="0.3048" dy="1.5" layer="1" rot="R180"/>
<smd name="63" x="-3.2512" y="4.7879" dx="0.3048" dy="1.5" layer="1" rot="R180"/>
<smd name="64" x="-3.7592" y="4.788" dx="0.3048" dy="1.5" layer="1" rot="R180"/>
<smd name="65" x="0" y="0" dx="7.239" dy="7.239" layer="1"/>
<text x="-6.096" y="3.7338" size="1.27" layer="21" ratio="6" rot="SR0">*</text>
<text x="-6.096" y="3.7338" size="1.27" layer="51" ratio="6" rot="SR0">*</text>
<text x="-3.4544" y="6.985" size="2.0828" layer="25" ratio="10" rot="SR0">&gt;NAME</text>
<text x="-3.4544" y="-9.525" size="2.0828" layer="27" ratio="10" rot="SR0">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="ADS62P45">
<wire x1="-22.86" y1="29.21" x2="-22.86" y2="-74.93" width="0.254" layer="94"/>
<wire x1="-22.86" y1="-74.93" x2="1.27" y2="-74.93" width="0.254" layer="94"/>
<wire x1="1.27" y1="-74.93" x2="1.27" y2="29.21" width="0.254" layer="94"/>
<wire x1="1.27" y1="29.21" x2="-22.86" y2="29.21" width="0.254" layer="94"/>
<text x="-22.86" y="-77.47" size="1.778" layer="96">&lt;Value&gt;</text>
<text x="-22.86" y="29.48" size="1.778" layer="95">ADS62P45</text>
<pin name="AVDD_0" x="-25.4" y="26.67" length="short"/>
<pin name="AVDD_1" x="-25.4" y="24.13" length="short"/>
<pin name="AVDD_2" x="-25.4" y="21.59" length="short"/>
<pin name="AGND_0" x="-25.4" y="16.51" length="short"/>
<pin name="AGND_1" x="-25.4" y="13.97" length="short"/>
<pin name="AGND_2" x="-25.4" y="11.43" length="short"/>
<pin name="DA0M" x="3.81" y="26.67" length="short" rot="R180"/>
<pin name="DA0P" x="3.81" y="24.13" length="short" rot="R180"/>
<pin name="DA2M" x="3.81" y="21.59" length="short" rot="R180"/>
<pin name="DA2P" x="3.81" y="19.05" length="short" rot="R180"/>
<pin name="DA4M" x="3.81" y="16.51" length="short" rot="R180"/>
<pin name="DA4P" x="3.81" y="13.97" length="short" rot="R180"/>
<pin name="DA6M" x="3.81" y="11.43" length="short" rot="R180"/>
<pin name="DA6P" x="3.81" y="8.89" length="short" rot="R180"/>
<pin name="DA8M" x="3.81" y="6.35" length="short" rot="R180"/>
<pin name="DA8P" x="3.81" y="3.81" length="short" rot="R180"/>
<pin name="DA10M" x="3.81" y="1.27" length="short" rot="R180"/>
<pin name="DA10P" x="3.81" y="-1.27" length="short" rot="R180"/>
<pin name="DA12M" x="3.81" y="-3.81" length="short" rot="R180"/>
<pin name="DA12P" x="3.81" y="-6.35" length="short" rot="R180"/>
<pin name="DB0M" x="3.81" y="-11.43" length="short" rot="R180"/>
<pin name="DB0P" x="3.81" y="-13.97" length="short" rot="R180"/>
<pin name="DB2M" x="3.81" y="-16.51" length="short" rot="R180"/>
<pin name="DB2P" x="3.81" y="-19.05" length="short" rot="R180"/>
<pin name="DB4M" x="3.81" y="-21.59" length="short" rot="R180"/>
<pin name="DB4P" x="3.81" y="-24.13" length="short" rot="R180"/>
<pin name="DB6M" x="3.81" y="-26.67" length="short" rot="R180"/>
<pin name="DB6P" x="3.81" y="-29.21" length="short" rot="R180"/>
<pin name="DB8M" x="3.81" y="-31.75" length="short" rot="R180"/>
<pin name="DB8P" x="3.81" y="-34.29" length="short" rot="R180"/>
<pin name="DB10M" x="3.81" y="-36.83" length="short" rot="R180"/>
<pin name="DB10P" x="3.81" y="-39.37" length="short" rot="R180"/>
<pin name="DB12M" x="3.81" y="-41.91" length="short" rot="R180"/>
<pin name="DB12P" x="3.81" y="-44.45" length="short" rot="R180"/>
<pin name="AGND_3" x="-25.4" y="8.89" length="short"/>
<pin name="AGND_4" x="-25.4" y="6.35" length="short"/>
<pin name="AGND_5" x="-25.4" y="3.81" length="short"/>
<pin name="AGND_6" x="-25.4" y="1.27" length="short"/>
<pin name="AGND_7" x="-25.4" y="-1.27" length="short"/>
<pin name="AGND_8" x="-25.4" y="-3.81" length="short"/>
<pin name="DRVDD0" x="-25.4" y="-10.16" length="short"/>
<pin name="DRVDD1" x="-25.4" y="-12.7" length="short"/>
<pin name="DRVDD2" x="-25.4" y="-15.24" length="short"/>
<pin name="DRVDD3" x="-25.4" y="-17.78" length="short"/>
<pin name="DRGND1" x="-25.4" y="-25.4" length="short"/>
<pin name="DRGND2" x="-25.4" y="-27.94" length="short"/>
<pin name="DRGND0" x="-25.4" y="-22.86" length="short"/>
<pin name="DRGND3" x="-25.4" y="-30.48" length="short"/>
<pin name="DRGND4" x="-25.4" y="-33.02" length="short"/>
<pin name="INP_A" x="-25.4" y="-45.72" length="short"/>
<pin name="INM_A" x="-25.4" y="-48.26" length="short"/>
<pin name="INP_B" x="-25.4" y="-53.34" length="short"/>
<pin name="INM_B" x="-25.4" y="-55.88" length="short"/>
<pin name="VCM" x="-25.4" y="-60.96" length="short"/>
<pin name="SCLK" x="-25.4" y="-67.31" length="short"/>
<pin name="SDATA" x="-25.4" y="-69.85" length="short"/>
<pin name="SEN" x="-25.4" y="-72.39" length="short"/>
<pin name="CLKOUTP" x="3.81" y="-49.53" length="short" rot="R180"/>
<pin name="CLKP" x="-25.4" y="-38.1" length="short"/>
<pin name="CLKM" x="-25.4" y="-40.64" length="short"/>
<pin name="RST" x="-25.4" y="-64.77" length="short"/>
<pin name="CTRL1" x="3.81" y="-57.15" length="short" rot="R180"/>
<pin name="CTRL2" x="3.81" y="-59.69" length="short" rot="R180"/>
<pin name="CTRL3" x="3.81" y="-62.23" length="short" rot="R180"/>
<pin name="CLKOUTM" x="3.81" y="-52.07" length="short" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="ADS62P45">
<gates>
<gate name="A" symbol="ADS62P45" x="10.16" y="22.86"/>
</gates>
<devices>
<device name="" package="QFN50P900X900X100-65N">
<connects>
<connect gate="A" pin="AGND_0" pad="17"/>
<connect gate="A" pin="AGND_1" pad="18"/>
<connect gate="A" pin="AGND_2" pad="24"/>
<connect gate="A" pin="AGND_3" pad="27"/>
<connect gate="A" pin="AGND_4" pad="28"/>
<connect gate="A" pin="AGND_5" pad="31"/>
<connect gate="A" pin="AGND_6" pad="32"/>
<connect gate="A" pin="AGND_7" pad="21"/>
<connect gate="A" pin="AGND_8" pad="22"/>
<connect gate="A" pin="AVDD_0" pad="16"/>
<connect gate="A" pin="AVDD_1" pad="33"/>
<connect gate="A" pin="AVDD_2" pad="34"/>
<connect gate="A" pin="CLKM" pad="26"/>
<connect gate="A" pin="CLKOUTM" pad="56"/>
<connect gate="A" pin="CLKOUTP" pad="57"/>
<connect gate="A" pin="CLKP" pad="25"/>
<connect gate="A" pin="CTRL1" pad="35"/>
<connect gate="A" pin="CTRL2" pad="36"/>
<connect gate="A" pin="CTRL3" pad="37"/>
<connect gate="A" pin="DA0M" pad="40"/>
<connect gate="A" pin="DA0P" pad="41"/>
<connect gate="A" pin="DA10M" pad="52"/>
<connect gate="A" pin="DA10P" pad="53"/>
<connect gate="A" pin="DA12M" pad="54"/>
<connect gate="A" pin="DA12P" pad="55"/>
<connect gate="A" pin="DA2M" pad="42"/>
<connect gate="A" pin="DA2P" pad="43"/>
<connect gate="A" pin="DA4M" pad="44"/>
<connect gate="A" pin="DA4P" pad="45"/>
<connect gate="A" pin="DA6M" pad="46"/>
<connect gate="A" pin="DA6P" pad="47"/>
<connect gate="A" pin="DA8M" pad="50"/>
<connect gate="A" pin="DA8P" pad="51"/>
<connect gate="A" pin="DB0M" pad="60"/>
<connect gate="A" pin="DB0P" pad="61"/>
<connect gate="A" pin="DB10M" pad="8"/>
<connect gate="A" pin="DB10P" pad="9"/>
<connect gate="A" pin="DB12M" pad="10"/>
<connect gate="A" pin="DB12P" pad="11"/>
<connect gate="A" pin="DB2M" pad="62"/>
<connect gate="A" pin="DB2P" pad="63"/>
<connect gate="A" pin="DB4M" pad="2"/>
<connect gate="A" pin="DB4P" pad="3"/>
<connect gate="A" pin="DB6M" pad="4"/>
<connect gate="A" pin="DB6P" pad="5"/>
<connect gate="A" pin="DB8M" pad="6"/>
<connect gate="A" pin="DB8P" pad="7"/>
<connect gate="A" pin="DRGND0" pad="39"/>
<connect gate="A" pin="DRGND1" pad="49"/>
<connect gate="A" pin="DRGND2" pad="59"/>
<connect gate="A" pin="DRGND3" pad="64"/>
<connect gate="A" pin="DRGND4" pad="65"/>
<connect gate="A" pin="DRVDD0" pad="1"/>
<connect gate="A" pin="DRVDD1" pad="38"/>
<connect gate="A" pin="DRVDD2" pad="48"/>
<connect gate="A" pin="DRVDD3" pad="58"/>
<connect gate="A" pin="INM_A" pad="29"/>
<connect gate="A" pin="INM_B" pad="19"/>
<connect gate="A" pin="INP_A" pad="30"/>
<connect gate="A" pin="INP_B" pad="20"/>
<connect gate="A" pin="RST" pad="12"/>
<connect gate="A" pin="SCLK" pad="13"/>
<connect gate="A" pin="SDATA" pad="14"/>
<connect gate="A" pin="SEN" pad="15"/>
<connect gate="A" pin="VCM" pad="23"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="TI">
<packages>
<package name="SOIC">
<wire x1="-2" y1="2.5" x2="2" y2="2.5" width="0.127" layer="21"/>
<wire x1="2" y1="2.5" x2="2" y2="-2.5" width="0.127" layer="21"/>
<wire x1="-2" y1="-2.5" x2="2" y2="-2.5" width="0.127" layer="21"/>
<wire x1="-2" y1="2.5" x2="-2" y2="-2.5" width="0.127" layer="21"/>
<circle x="-1" y="1.5" radius="0.5" width="0.127" layer="21"/>
<smd name="1" x="-2.54" y="1.905" dx="1.27" dy="0.508" layer="1"/>
<smd name="2" x="-2.54" y="0.635" dx="1.27" dy="0.508" layer="1"/>
<smd name="3" x="-2.54" y="-0.635" dx="1.27" dy="0.508" layer="1"/>
<smd name="4" x="-2.54" y="-1.905" dx="1.27" dy="0.508" layer="1"/>
<smd name="5" x="2.54" y="-1.905" dx="1.27" dy="0.508" layer="1"/>
<smd name="6" x="2.54" y="-0.635" dx="1.27" dy="0.508" layer="1"/>
<smd name="7" x="2.54" y="0.635" dx="1.27" dy="0.508" layer="1"/>
<smd name="8" x="2.54" y="1.905" dx="1.27" dy="0.508" layer="1"/>
</package>
</packages>
<symbols>
<symbol name="LMH6552">
<wire x1="-10.16" y1="12.7" x2="10.16" y2="0" width="0.254" layer="94"/>
<wire x1="10.16" y1="0" x2="-10.16" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-10.16" y1="12.7" x2="-10.16" y2="-12.7" width="0.254" layer="94"/>
<text x="-5.08" y="13.97" size="1.778" layer="95">LHM6552</text>
<text x="-7.62" y="12.7" size="1.778" layer="95" rot="R90">V+</text>
<text x="-8.255" y="-15.24" size="1.778" layer="95" rot="R90">V-</text>
<text x="-13.97" y="8.89" size="1.778" layer="95">EN</text>
<text x="-15.24" y="3.81" size="1.778" layer="95">+IN</text>
<text x="-16.51" y="-1.27" size="1.778" layer="95">VCM</text>
<text x="-15.24" y="-6.35" size="1.778" layer="95">-IN</text>
<text x="-2.54" y="-10.16" size="1.778" layer="95">+OUT</text>
<text x="-2.54" y="10.16" size="1.778" layer="95">-OUT</text>
<pin name="+IN" x="-15.24" y="2.54" visible="off" length="middle"/>
<pin name="-IN" x="-15.24" y="-7.62" visible="off" length="middle"/>
<pin name="VCM" x="-15.24" y="-2.54" visible="off" length="middle"/>
<pin name="V-" x="-7.62" y="-15.875" visible="off" length="middle" rot="R90"/>
<pin name="V+" x="-7.62" y="15.875" visible="off" length="middle" rot="R270"/>
<pin name="+OUT" x="3.175" y="-7.62" visible="off" length="middle" rot="R180"/>
<pin name="-OUT" x="3.81" y="7.62" visible="off" length="middle" function="dot" rot="R180"/>
<pin name="EN" x="-15.24" y="7.62" visible="off" length="middle"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="LMH6552">
<description>1,5 GHz Fully Differential Amplifier</description>
<gates>
<gate name="G$1" symbol="LMH6552" x="-5.08" y="0"/>
</gates>
<devices>
<device name="" package="SOIC">
<connects>
<connect gate="G$1" pin="+IN" pad="8"/>
<connect gate="G$1" pin="+OUT" pad="4"/>
<connect gate="G$1" pin="-IN" pad="1"/>
<connect gate="G$1" pin="-OUT" pad="5"/>
<connect gate="G$1" pin="EN" pad="7"/>
<connect gate="G$1" pin="V+" pad="3"/>
<connect gate="G$1" pin="V-" pad="6"/>
<connect gate="G$1" pin="VCM" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="ADP7105">
<packages>
<package name="RD_8_2">
<wire x1="-1.9812" y1="1.651" x2="-1.9812" y2="2.159" width="0.1524" layer="21"/>
<wire x1="-1.9812" y1="2.159" x2="-3.0988" y2="2.159" width="0.1524" layer="21"/>
<wire x1="-3.0988" y1="2.159" x2="-3.0988" y2="1.651" width="0.1524" layer="21"/>
<wire x1="-3.0988" y1="1.651" x2="-1.9812" y2="1.651" width="0.1524" layer="21"/>
<wire x1="-1.9812" y1="0.381" x2="-1.9812" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.9812" y1="0.889" x2="-3.0988" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-3.0988" y1="0.889" x2="-3.0988" y2="0.381" width="0.1524" layer="21"/>
<wire x1="-3.0988" y1="0.381" x2="-1.9812" y2="0.381" width="0.1524" layer="21"/>
<wire x1="-1.9812" y1="-0.889" x2="-1.9812" y2="-0.381" width="0.1524" layer="21"/>
<wire x1="-1.9812" y1="-0.381" x2="-3.0988" y2="-0.381" width="0.1524" layer="21"/>
<wire x1="-3.0988" y1="-0.381" x2="-3.0988" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-3.0988" y1="-0.889" x2="-1.9812" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-1.9812" y1="-2.159" x2="-1.9812" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="-1.9812" y1="-1.651" x2="-3.0988" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="-3.0988" y1="-1.651" x2="-3.0988" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="-3.0988" y1="-2.159" x2="-1.9812" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="1.9812" y1="-1.651" x2="1.9812" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="1.9812" y1="-2.159" x2="3.0988" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="3.0988" y1="-2.159" x2="3.0988" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="3.0988" y1="-1.651" x2="1.9812" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="1.9812" y1="-0.381" x2="1.9812" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.9812" y1="-0.889" x2="3.0988" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="3.0988" y1="-0.889" x2="3.0988" y2="-0.381" width="0.1524" layer="21"/>
<wire x1="3.0988" y1="-0.381" x2="1.9812" y2="-0.381" width="0.1524" layer="21"/>
<wire x1="1.9812" y1="0.889" x2="1.9812" y2="0.381" width="0.1524" layer="21"/>
<wire x1="1.9812" y1="0.381" x2="3.0988" y2="0.381" width="0.1524" layer="21"/>
<wire x1="3.0988" y1="0.381" x2="3.0988" y2="0.889" width="0.1524" layer="21"/>
<wire x1="3.0988" y1="0.889" x2="1.9812" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.9812" y1="2.159" x2="1.9812" y2="1.651" width="0.1524" layer="21"/>
<wire x1="1.9812" y1="1.651" x2="3.0988" y2="1.651" width="0.1524" layer="21"/>
<wire x1="3.0988" y1="1.651" x2="3.0988" y2="2.159" width="0.1524" layer="21"/>
<wire x1="3.0988" y1="2.159" x2="1.9812" y2="2.159" width="0.1524" layer="21"/>
<wire x1="-1.9812" y1="-2.4892" x2="1.9812" y2="-2.4892" width="0.1524" layer="21"/>
<wire x1="1.9812" y1="-2.4892" x2="1.9812" y2="2.4892" width="0.1524" layer="21"/>
<wire x1="1.9812" y1="2.4892" x2="-0.3048" y2="2.4892" width="0.1524" layer="21"/>
<wire x1="-0.3048" y1="2.4892" x2="-1.9812" y2="2.4892" width="0.1524" layer="21"/>
<wire x1="-1.9812" y1="2.4892" x2="-1.9812" y2="-2.4892" width="0.1524" layer="21"/>
<wire x1="0.3048" y1="2.5146" x2="-0.3048" y2="2.4892" width="0.1524" layer="21" curve="-180"/>
<wire x1="-1.1684" y1="-2.4892" x2="1.1684" y2="-2.4892" width="0.1524" layer="51"/>
<wire x1="1.1684" y1="2.4892" x2="-0.3048" y2="2.4892" width="0.1524" layer="51"/>
<wire x1="-0.3048" y1="2.4892" x2="-1.1684" y2="2.4892" width="0.1524" layer="51"/>
<wire x1="0.3048" y1="2.5146" x2="-0.3048" y2="2.4892" width="0.1524" layer="51" curve="-180"/>
<smd name="1" x="-2.3622" y="1.905" dx="2.1844" dy="0.5588" layer="1"/>
<smd name="2" x="-2.3622" y="0.635" dx="2.1844" dy="0.5588" layer="1"/>
<smd name="3" x="-2.3622" y="-0.635" dx="2.1844" dy="0.5588" layer="1"/>
<smd name="4" x="-2.3622" y="-1.905" dx="2.1844" dy="0.5588" layer="1"/>
<smd name="5" x="2.3622" y="-1.905" dx="2.1844" dy="0.5588" layer="1"/>
<smd name="6" x="2.3622" y="-0.635" dx="2.1844" dy="0.5588" layer="1"/>
<smd name="7" x="2.3622" y="0.635" dx="2.1844" dy="0.5588" layer="1"/>
<smd name="8" x="2.3622" y="1.905" dx="2.1844" dy="0.5588" layer="1"/>
<smd name="9" x="0" y="0" dx="2.24" dy="3.0988" layer="1"/>
<text x="-3.2004" y="2.3368" size="1.27" layer="21" ratio="6" rot="SR0">*</text>
<text x="-3.2004" y="2.3368" size="1.27" layer="51" ratio="6" rot="SR0">*</text>
<text x="-2.8702" y="-0.635" size="1.27" layer="25" ratio="6" rot="SR0">&gt;Name</text>
<text x="-3.2766" y="-0.635" size="1.27" layer="27" ratio="6" rot="SR0">&gt;Value</text>
</package>
</packages>
<symbols>
<symbol name="ADP7105ARDZ">
<wire x1="7.62" y1="5.08" x2="7.62" y2="-25.4" width="0.1524" layer="94"/>
<wire x1="7.62" y1="-25.4" x2="53.34" y2="-25.4" width="0.1524" layer="94"/>
<wire x1="53.34" y1="-25.4" x2="53.34" y2="5.08" width="0.1524" layer="94"/>
<wire x1="53.34" y1="5.08" x2="7.62" y2="5.08" width="0.1524" layer="94"/>
<text x="22.86" y="-2.54" size="2.54" layer="95" ratio="6">ADP7105</text>
<pin name="VIN" x="0" y="0" direction="pas"/>
<pin name="EN/UVLO" x="0" y="-5.08" direction="pas"/>
<pin name="PG" x="0" y="-10.16" direction="pas"/>
<pin name="SENSE/ADJ" x="0" y="-15.24" direction="pas"/>
<pin name="SS" x="0" y="-20.32" direction="pas"/>
<pin name="VOUT" x="60.96" y="0" direction="pas" rot="R180"/>
<pin name="GND_2" x="60.96" y="-7.62" direction="pas" rot="R180"/>
<pin name="GND" x="60.96" y="-12.7" direction="pas" rot="R180"/>
<pin name="EP" x="60.96" y="-20.32" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="ADP7105ARDZ" prefix="U">
<gates>
<gate name="A" symbol="ADP7105ARDZ" x="0" y="0"/>
</gates>
<devices>
<device name="" package="RD_8_2">
<connects>
<connect gate="A" pin="EN/UVLO" pad="5"/>
<connect gate="A" pin="EP" pad="9"/>
<connect gate="A" pin="GND" pad="6"/>
<connect gate="A" pin="GND_2" pad="3"/>
<connect gate="A" pin="PG" pad="7"/>
<connect gate="A" pin="SENSE/ADJ" pad="2"/>
<connect gate="A" pin="SS" pad="4"/>
<connect gate="A" pin="VIN" pad="8"/>
<connect gate="A" pin="VOUT" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="ADP7182">
<packages>
<package name="UJ_5">
<wire x1="-0.254" y1="-1.4478" x2="0.254" y2="-1.4478" width="0.1524" layer="51"/>
<wire x1="0.8128" y1="-0.3302" x2="0.8128" y2="0.3302" width="0.1524" layer="51"/>
<wire x1="0.254" y1="1.4478" x2="-0.254" y2="1.4478" width="0.1524" layer="51"/>
<wire x1="0.1778" y1="1.1938" x2="-0.1778" y2="1.1938" width="0.1524" layer="51" curve="-70.9927"/>
<wire x1="-0.8128" y1="-1.4478" x2="0.8128" y2="-1.4478" width="0.1524" layer="21"/>
<wire x1="0.8128" y1="-1.4478" x2="0.8128" y2="-1.1938" width="0.1524" layer="21"/>
<wire x1="0.8128" y1="1.4478" x2="0.3048" y2="1.4478" width="0.1524" layer="21"/>
<wire x1="0.3048" y1="1.4478" x2="-0.3048" y2="1.4478" width="0.1524" layer="21"/>
<wire x1="-0.3048" y1="1.4478" x2="-0.8128" y2="1.4478" width="0.1524" layer="21"/>
<wire x1="-0.8128" y1="1.4478" x2="-0.8128" y2="1.1938" width="0.1524" layer="21"/>
<wire x1="-0.8128" y1="1.1938" x2="-0.8128" y2="0.6858" width="0.1524" layer="21"/>
<wire x1="-0.8128" y1="0.6858" x2="-0.8128" y2="0.254" width="0.1524" layer="21"/>
<wire x1="-0.8128" y1="1.1938" x2="-1.397" y2="1.1938" width="0.1524" layer="21"/>
<wire x1="-1.397" y1="1.1938" x2="-1.397" y2="0.6858" width="0.1524" layer="21"/>
<wire x1="-1.397" y1="0.6858" x2="-0.8128" y2="0.6858" width="0.1524" layer="21"/>
<wire x1="-0.8128" y1="0.254" x2="-0.8128" y2="-0.254" width="0.1524" layer="21"/>
<wire x1="-0.8128" y1="-0.254" x2="-0.8128" y2="-0.6858" width="0.1524" layer="21"/>
<wire x1="-0.8128" y1="0.254" x2="-1.397" y2="0.254" width="0.1524" layer="21"/>
<wire x1="-1.397" y1="0.254" x2="-1.397" y2="-0.254" width="0.1524" layer="21"/>
<wire x1="-1.397" y1="-0.254" x2="-0.8128" y2="-0.254" width="0.1524" layer="21"/>
<wire x1="-0.8128" y1="-1.4478" x2="-0.8128" y2="-1.1938" width="0.1524" layer="21"/>
<wire x1="-0.8128" y1="-1.1938" x2="-0.8128" y2="-0.6858" width="0.1524" layer="21"/>
<wire x1="-0.8128" y1="-0.6858" x2="-1.397" y2="-0.6858" width="0.1524" layer="21"/>
<wire x1="-1.397" y1="-0.6858" x2="-1.397" y2="-1.1938" width="0.1524" layer="21"/>
<wire x1="-1.397" y1="-1.1938" x2="-0.8128" y2="-1.1938" width="0.1524" layer="21"/>
<wire x1="0.8128" y1="-1.1938" x2="0.8128" y2="-0.6858" width="0.1524" layer="21"/>
<wire x1="0.8128" y1="-0.6858" x2="0.8128" y2="0.6858" width="0.1524" layer="21"/>
<wire x1="0.8128" y1="-1.1938" x2="1.397" y2="-1.1938" width="0.1524" layer="21"/>
<wire x1="1.397" y1="-1.1938" x2="1.397" y2="-0.6858" width="0.1524" layer="21"/>
<wire x1="1.397" y1="-0.6858" x2="0.8128" y2="-0.6858" width="0.1524" layer="21"/>
<wire x1="0.8128" y1="1.4478" x2="0.8128" y2="1.1938" width="0.1524" layer="21"/>
<wire x1="0.8128" y1="1.1938" x2="0.8128" y2="0.6858" width="0.1524" layer="21"/>
<wire x1="0.8128" y1="0.6858" x2="1.397" y2="0.6858" width="0.1524" layer="21"/>
<wire x1="1.397" y1="0.6858" x2="1.397" y2="1.1938" width="0.1524" layer="21"/>
<wire x1="1.397" y1="1.1938" x2="0.8128" y2="1.1938" width="0.1524" layer="21"/>
<wire x1="0.3048" y1="1.4478" x2="-0.3048" y2="1.4478" width="0.1524" layer="21" curve="-180"/>
<smd name="1" x="-1.1684" y="0.95" dx="1.3208" dy="0.5588" layer="1"/>
<smd name="2" x="-1.1684" y="0" dx="1.3208" dy="0.5588" layer="1"/>
<smd name="3" x="-1.1684" y="-0.95" dx="1.3208" dy="0.5588" layer="1"/>
<smd name="4" x="1.1684" y="-0.95" dx="1.3208" dy="0.5588" layer="1"/>
<smd name="5" x="1.1684" y="0.95" dx="1.3208" dy="0.5588" layer="1"/>
<text x="-2.0066" y="1.3716" size="1.27" layer="51" ratio="6" rot="SR0">*</text>
<text x="-2.0066" y="1.3716" size="1.27" layer="21" ratio="6" rot="SR0">*</text>
<text x="-2.8702" y="-0.635" size="1.27" layer="25" ratio="6" rot="SR0">&gt;Name</text>
<text x="-3.2766" y="-0.635" size="1.27" layer="27" ratio="6" rot="SR0">&gt;Value</text>
</package>
</packages>
<symbols>
<symbol name="ADP7182AUJZ-R7">
<wire x1="7.62" y1="5.08" x2="7.62" y2="-15.24" width="0.1524" layer="94"/>
<wire x1="7.62" y1="-15.24" x2="33.02" y2="-15.24" width="0.1524" layer="94"/>
<wire x1="33.02" y1="-15.24" x2="33.02" y2="5.08" width="0.1524" layer="94"/>
<wire x1="33.02" y1="5.08" x2="7.62" y2="5.08" width="0.1524" layer="94"/>
<text x="12.7" y="1.27" size="2.54" layer="95" ratio="6">ADP7182</text>
<pin name="VIN" x="0" y="0" direction="pas"/>
<pin name="VOUT" x="0" y="-5.08" direction="pas"/>
<pin name="\EN" x="0" y="-10.16" direction="pas"/>
<pin name="GND" x="40.64" y="0" direction="pas" rot="R180"/>
<pin name="NC" x="40.64" y="-10.16" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="ADP7182AUJZ-R7" prefix="U">
<gates>
<gate name="A" symbol="ADP7182AUJZ-R7" x="0" y="0"/>
</gates>
<devices>
<device name="" package="UJ_5">
<connects>
<connect gate="A" pin="GND" pad="1"/>
<connect gate="A" pin="NC" pad="4"/>
<connect gate="A" pin="VIN" pad="2"/>
<connect gate="A" pin="VOUT" pad="5"/>
<connect gate="A" pin="\EN" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="AD9573">
<packages>
<package name="RU_16">
<wire x1="-2.2352" y1="2.1336" x2="-2.2352" y2="2.4384" width="0.1524" layer="21"/>
<wire x1="-2.2352" y1="2.4384" x2="-3.2004" y2="2.4384" width="0.1524" layer="21"/>
<wire x1="-3.2004" y1="2.4384" x2="-3.2004" y2="2.1336" width="0.1524" layer="21"/>
<wire x1="-3.2004" y1="2.1336" x2="-2.2352" y2="2.1336" width="0.1524" layer="21"/>
<wire x1="-2.2352" y1="1.4732" x2="-2.2352" y2="1.778" width="0.1524" layer="21"/>
<wire x1="-2.2352" y1="1.778" x2="-3.2004" y2="1.778" width="0.1524" layer="21"/>
<wire x1="-3.2004" y1="1.778" x2="-3.2004" y2="1.4732" width="0.1524" layer="21"/>
<wire x1="-3.2004" y1="1.4732" x2="-2.2352" y2="1.4732" width="0.1524" layer="21"/>
<wire x1="-2.2352" y1="0.8128" x2="-2.2352" y2="1.1176" width="0.1524" layer="21"/>
<wire x1="-2.2352" y1="1.1176" x2="-3.2004" y2="1.1176" width="0.1524" layer="21"/>
<wire x1="-3.2004" y1="1.1176" x2="-3.2004" y2="0.8128" width="0.1524" layer="21"/>
<wire x1="-3.2004" y1="0.8128" x2="-2.2352" y2="0.8128" width="0.1524" layer="21"/>
<wire x1="-2.2352" y1="0.1778" x2="-2.2352" y2="0.4826" width="0.1524" layer="21"/>
<wire x1="-2.2352" y1="0.4826" x2="-3.2004" y2="0.4826" width="0.1524" layer="21"/>
<wire x1="-3.2004" y1="0.4826" x2="-3.2004" y2="0.1778" width="0.1524" layer="21"/>
<wire x1="-3.2004" y1="0.1778" x2="-2.2352" y2="0.1778" width="0.1524" layer="21"/>
<wire x1="-2.2352" y1="-0.4826" x2="-2.2352" y2="-0.1778" width="0.1524" layer="21"/>
<wire x1="-2.2352" y1="-0.1778" x2="-3.2004" y2="-0.1778" width="0.1524" layer="21"/>
<wire x1="-3.2004" y1="-0.1778" x2="-3.2004" y2="-0.4826" width="0.1524" layer="21"/>
<wire x1="-3.2004" y1="-0.4826" x2="-2.2352" y2="-0.4826" width="0.1524" layer="21"/>
<wire x1="-2.2352" y1="-1.1176" x2="-2.2352" y2="-0.8128" width="0.1524" layer="21"/>
<wire x1="-2.2352" y1="-0.8128" x2="-3.2004" y2="-0.8128" width="0.1524" layer="21"/>
<wire x1="-3.2004" y1="-0.8128" x2="-3.2004" y2="-1.1176" width="0.1524" layer="21"/>
<wire x1="-3.2004" y1="-1.1176" x2="-2.2352" y2="-1.1176" width="0.1524" layer="21"/>
<wire x1="-2.2352" y1="-1.778" x2="-2.2352" y2="-1.4732" width="0.1524" layer="21"/>
<wire x1="-2.2352" y1="-1.4732" x2="-3.2004" y2="-1.4732" width="0.1524" layer="21"/>
<wire x1="-3.2004" y1="-1.4732" x2="-3.2004" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="-3.2004" y1="-1.778" x2="-2.2352" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="-2.2352" y1="-2.4384" x2="-2.2352" y2="-2.1336" width="0.1524" layer="21"/>
<wire x1="-2.2352" y1="-2.1336" x2="-3.2004" y2="-2.1336" width="0.1524" layer="21"/>
<wire x1="-3.2004" y1="-2.1336" x2="-3.2004" y2="-2.4384" width="0.1524" layer="21"/>
<wire x1="-3.2004" y1="-2.4384" x2="-2.2352" y2="-2.4384" width="0.1524" layer="21"/>
<wire x1="2.2352" y1="-2.1336" x2="2.2352" y2="-2.4384" width="0.1524" layer="21"/>
<wire x1="2.2352" y1="-2.4384" x2="3.2004" y2="-2.4384" width="0.1524" layer="21"/>
<wire x1="3.2004" y1="-2.4384" x2="3.2004" y2="-2.1336" width="0.1524" layer="21"/>
<wire x1="3.2004" y1="-2.1336" x2="2.2352" y2="-2.1336" width="0.1524" layer="21"/>
<wire x1="2.2352" y1="-1.4732" x2="2.2352" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="2.2352" y1="-1.778" x2="3.2004" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="3.2004" y1="-1.778" x2="3.2004" y2="-1.4732" width="0.1524" layer="21"/>
<wire x1="3.2004" y1="-1.4732" x2="2.2352" y2="-1.4732" width="0.1524" layer="21"/>
<wire x1="2.2352" y1="-0.8128" x2="2.2352" y2="-1.1176" width="0.1524" layer="21"/>
<wire x1="2.2352" y1="-1.1176" x2="3.2004" y2="-1.1176" width="0.1524" layer="21"/>
<wire x1="3.2004" y1="-1.1176" x2="3.2004" y2="-0.8128" width="0.1524" layer="21"/>
<wire x1="3.2004" y1="-0.8128" x2="2.2352" y2="-0.8128" width="0.1524" layer="21"/>
<wire x1="2.2352" y1="-0.1778" x2="2.2352" y2="-0.4826" width="0.1524" layer="21"/>
<wire x1="2.2352" y1="-0.4826" x2="3.2004" y2="-0.4826" width="0.1524" layer="21"/>
<wire x1="3.2004" y1="-0.4826" x2="3.2004" y2="-0.1778" width="0.1524" layer="21"/>
<wire x1="3.2004" y1="-0.1778" x2="2.2352" y2="-0.1778" width="0.1524" layer="21"/>
<wire x1="2.2352" y1="0.4826" x2="2.2352" y2="0.1778" width="0.1524" layer="21"/>
<wire x1="2.2352" y1="0.1778" x2="3.2004" y2="0.1778" width="0.1524" layer="21"/>
<wire x1="3.2004" y1="0.1778" x2="3.2004" y2="0.4826" width="0.1524" layer="21"/>
<wire x1="3.2004" y1="0.4826" x2="2.2352" y2="0.4826" width="0.1524" layer="21"/>
<wire x1="2.2352" y1="1.1176" x2="2.2352" y2="0.8128" width="0.1524" layer="21"/>
<wire x1="2.2352" y1="0.8128" x2="3.2004" y2="0.8128" width="0.1524" layer="21"/>
<wire x1="3.2004" y1="0.8128" x2="3.2004" y2="1.1176" width="0.1524" layer="21"/>
<wire x1="3.2004" y1="1.1176" x2="2.2352" y2="1.1176" width="0.1524" layer="21"/>
<wire x1="2.2352" y1="1.778" x2="2.2352" y2="1.4732" width="0.1524" layer="21"/>
<wire x1="2.2352" y1="1.4732" x2="3.2004" y2="1.4732" width="0.1524" layer="21"/>
<wire x1="3.2004" y1="1.4732" x2="3.2004" y2="1.778" width="0.1524" layer="21"/>
<wire x1="3.2004" y1="1.778" x2="2.2352" y2="1.778" width="0.1524" layer="21"/>
<wire x1="2.2352" y1="2.4384" x2="2.2352" y2="2.1336" width="0.1524" layer="21"/>
<wire x1="2.2352" y1="2.1336" x2="3.2004" y2="2.1336" width="0.1524" layer="21"/>
<wire x1="3.2004" y1="2.1336" x2="3.2004" y2="2.4384" width="0.1524" layer="21"/>
<wire x1="3.2004" y1="2.4384" x2="2.2352" y2="2.4384" width="0.1524" layer="21"/>
<wire x1="-2.2352" y1="-2.54" x2="2.2352" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="2.2352" y1="-2.54" x2="2.2352" y2="2.54" width="0.1524" layer="21"/>
<wire x1="2.2352" y1="2.54" x2="-0.3048" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-0.3048" y1="2.54" x2="-2.2352" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-2.2352" y1="2.54" x2="-2.2352" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="0.3048" y1="2.5654" x2="-0.3048" y2="2.54" width="0.1524" layer="21" curve="-180"/>
<wire x1="-1.778" y1="-2.54" x2="1.778" y2="-2.54" width="0.1524" layer="51"/>
<wire x1="1.778" y1="2.54" x2="-0.3048" y2="2.54" width="0.1524" layer="51"/>
<wire x1="-0.3048" y1="2.54" x2="-1.778" y2="2.54" width="0.1524" layer="51"/>
<wire x1="0.3048" y1="2.5654" x2="-0.3048" y2="2.54" width="0.1524" layer="51" curve="-180"/>
<smd name="1" x="-2.8194" y="2.275" dx="1.4732" dy="0.3556" layer="1"/>
<smd name="2" x="-2.8194" y="1.625" dx="1.4732" dy="0.3556" layer="1"/>
<smd name="3" x="-2.8194" y="0.975" dx="1.4732" dy="0.3556" layer="1"/>
<smd name="4" x="-2.8194" y="0.325" dx="1.4732" dy="0.3556" layer="1"/>
<smd name="5" x="-2.8194" y="-0.325" dx="1.4732" dy="0.3556" layer="1"/>
<smd name="6" x="-2.8194" y="-0.975" dx="1.4732" dy="0.3556" layer="1"/>
<smd name="7" x="-2.8194" y="-1.625" dx="1.4732" dy="0.3556" layer="1"/>
<smd name="8" x="-2.8194" y="-2.275" dx="1.4732" dy="0.3556" layer="1"/>
<smd name="9" x="2.8194" y="-2.275" dx="1.4732" dy="0.3556" layer="1"/>
<smd name="10" x="2.8194" y="-1.625" dx="1.4732" dy="0.3556" layer="1"/>
<smd name="11" x="2.8194" y="-0.975" dx="1.4732" dy="0.3556" layer="1"/>
<smd name="12" x="2.8194" y="-0.325" dx="1.4732" dy="0.3556" layer="1"/>
<smd name="13" x="2.8194" y="0.325" dx="1.4732" dy="0.3556" layer="1"/>
<smd name="14" x="2.8194" y="0.975" dx="1.4732" dy="0.3556" layer="1"/>
<smd name="15" x="2.8194" y="1.625" dx="1.4732" dy="0.3556" layer="1"/>
<smd name="16" x="2.8194" y="2.275" dx="1.4732" dy="0.3556" layer="1"/>
<text x="-3.6576" y="2.5146" size="1.27" layer="21" ratio="6" rot="SR0">*</text>
<text x="-3.6576" y="2.5146" size="1.27" layer="51" ratio="6" rot="SR0">*</text>
<text x="-2.8702" y="-0.635" size="1.27" layer="25" ratio="6" rot="SR0">&gt;Name</text>
<text x="-3.2766" y="-0.635" size="1.27" layer="27" ratio="6" rot="SR0">&gt;Value</text>
<polygon width="0" layer="51">
<vertex x="4.064" y="-1.4345"/>
<vertex x="4.064" y="-1.8155"/>
<vertex x="3.81" y="-1.8155"/>
<vertex x="3.81" y="-1.4345"/>
</polygon>
</package>
</packages>
<symbols>
<symbol name="AD9573ARUZ">
<wire x1="7.62" y1="5.08" x2="7.62" y2="-33.02" width="0.1524" layer="94"/>
<wire x1="7.62" y1="-33.02" x2="58.42" y2="-33.02" width="0.1524" layer="94"/>
<wire x1="58.42" y1="-33.02" x2="58.42" y2="5.08" width="0.1524" layer="94"/>
<wire x1="58.42" y1="5.08" x2="7.62" y2="5.08" width="0.1524" layer="94"/>
<pin name="VDDA_2" x="0" y="0" direction="pas"/>
<pin name="VDDA" x="0" y="-2.54" direction="pas"/>
<pin name="VDDX" x="0" y="-7.62" direction="pas"/>
<pin name="VDD33" x="0" y="-10.16" direction="pas"/>
<pin name="VDD" x="0" y="-15.24" direction="pas"/>
<pin name="33M" x="0" y="-20.32" direction="pas"/>
<pin name="XO1" x="0" y="-25.4" direction="pas"/>
<pin name="XO2" x="0" y="-27.94" direction="pas"/>
<pin name="*100M" x="66.04" y="-27.94" direction="pas" rot="R180"/>
<pin name="100M" x="66.04" y="-25.4" direction="pas" rot="R180"/>
<pin name="*OE" x="66.04" y="-20.32" direction="pas" rot="R180"/>
<pin name="GNDA_2" x="66.04" y="-15.24" direction="pas" rot="R180"/>
<pin name="GNDA" x="66.04" y="-12.7" direction="pas" rot="R180"/>
<pin name="GNDX" x="66.04" y="-7.62" direction="pas" rot="R180"/>
<pin name="GND" x="66.04" y="-2.54" direction="pas" rot="R180"/>
<pin name="GND33" x="66.04" y="0" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="AD9573ARUZ" prefix="U">
<gates>
<gate name="A" symbol="AD9573ARUZ" x="0" y="0"/>
</gates>
<devices>
<device name="" package="RU_16">
<connects>
<connect gate="A" pin="*100M" pad="13"/>
<connect gate="A" pin="*OE" pad="16"/>
<connect gate="A" pin="100M" pad="14"/>
<connect gate="A" pin="33M" pad="10"/>
<connect gate="A" pin="GND" pad="15"/>
<connect gate="A" pin="GND33" pad="9"/>
<connect gate="A" pin="GNDA" pad="7"/>
<connect gate="A" pin="GNDA_2" pad="1"/>
<connect gate="A" pin="GNDX" pad="6"/>
<connect gate="A" pin="VDD" pad="12"/>
<connect gate="A" pin="VDD33" pad="11"/>
<connect gate="A" pin="VDDA" pad="8"/>
<connect gate="A" pin="VDDA_2" pad="2"/>
<connect gate="A" pin="VDDX" pad="3"/>
<connect gate="A" pin="XO1" pad="4"/>
<connect gate="A" pin="XO2" pad="5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply2">
<packages>
</packages>
<symbols>
<symbol name="DGND">
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="-1.27" y2="0" width="0.254" layer="94"/>
<text x="-2.667" y="-3.175" size="1.778" layer="96">&gt;VALUE</text>
<pin name="DGND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="DGND" prefix="SUPPLY">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="DGND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="LT6231">
<packages>
<package name="S8">
<wire x1="-1.9939" y1="2.5019" x2="-1.9939" y2="-2.5019" width="0.127" layer="21"/>
<wire x1="-1.9939" y1="-2.5019" x2="-1.3589" y2="-2.5019" width="0.127" layer="21"/>
<wire x1="-1.3589" y1="-2.5019" x2="1.9939" y2="-2.5019" width="0.127" layer="21"/>
<wire x1="-1.9939" y1="2.5019" x2="-1.3589" y2="2.5019" width="0.127" layer="21"/>
<wire x1="-1.3589" y1="2.5019" x2="1.9939" y2="2.5019" width="0.127" layer="21"/>
<wire x1="1.9939" y1="2.5019" x2="1.9939" y2="-2.5019" width="0.127" layer="21"/>
<wire x1="-1.3589" y1="2.5019" x2="-1.3589" y2="-2.5019" width="0.127" layer="21"/>
<smd name="1" x="-2.5718" y="1.905" dx="1.143" dy="0.762" layer="1"/>
<smd name="2" x="-2.5718" y="0.635" dx="1.143" dy="0.762" layer="1"/>
<smd name="3" x="-2.5718" y="-0.635" dx="1.143" dy="0.762" layer="1"/>
<smd name="4" x="-2.5718" y="-1.905" dx="1.143" dy="0.762" layer="1"/>
<smd name="5" x="2.5082" y="-1.905" dx="1.143" dy="0.762" layer="1"/>
<smd name="6" x="2.5082" y="-0.635" dx="1.143" dy="0.762" layer="1"/>
<smd name="7" x="2.5082" y="0.635" dx="1.143" dy="0.762" layer="1"/>
<smd name="8" x="2.5082" y="1.905" dx="1.143" dy="0.762" layer="1"/>
</package>
</packages>
<symbols>
<symbol name="OPV_P">
<wire x1="-2.54" y1="-5.08" x2="5.08" y2="0" width="0.254" layer="94"/>
<wire x1="5.08" y1="0" x2="-2.54" y2="5.08" width="0.254" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="-2.54" y2="-5.08" width="0.254" layer="94"/>
<text x="-1.905" y="2.54" size="1.27" layer="94">+</text>
<text x="-1.905" y="-2.54" size="1.27" layer="94">-</text>
<pin name="P" x="-5.08" y="2.54" visible="off" length="short"/>
<pin name="M" x="-5.08" y="-2.54" visible="off" length="short"/>
<pin name="OUT" x="7.62" y="0" visible="off" length="short" rot="R180"/>
</symbol>
<symbol name="SUPPLY">
<wire x1="-0.635" y1="-4.445" x2="0" y2="-5.08" width="0.254" layer="94"/>
<wire x1="0" y1="-5.08" x2="0.635" y2="-4.445" width="0.254" layer="94"/>
<wire x1="0" y1="5.08" x2="0.635" y2="4.445" width="0.254" layer="94"/>
<wire x1="0" y1="5.08" x2="-0.635" y2="4.445" width="0.254" layer="94"/>
<pin name="AVDD_M" x="0" y="-2.54" visible="off" length="short" rot="R270"/>
<pin name="AVDD" x="0" y="2.54" visible="off" length="short" rot="R90"/>
</symbol>
<symbol name="OPV_M">
<wire x1="-2.54" y1="5.08" x2="-2.54" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-5.08" x2="5.08" y2="0" width="0.254" layer="94"/>
<wire x1="5.08" y1="0" x2="-2.54" y2="5.08" width="0.254" layer="94"/>
<text x="-1.905" y="2.54" size="1.778" layer="94">-</text>
<text x="-1.905" y="-2.54" size="1.778" layer="94">+</text>
<pin name="M" x="-5.08" y="2.54" visible="off" length="short"/>
<pin name="P" x="-5.08" y="-2.54" visible="off" length="short"/>
<pin name="OUT" x="7.62" y="0" visible="off" length="short" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="LT6231">
<gates>
<gate name="A" symbol="OPV_P" x="-7.62" y="0"/>
<gate name="C" symbol="SUPPLY" x="-27.94" y="0"/>
<gate name="B" symbol="OPV_M" x="10.16" y="0"/>
</gates>
<devices>
<device name="" package="S8">
<connects>
<connect gate="A" pin="M" pad="2"/>
<connect gate="A" pin="OUT" pad="1"/>
<connect gate="A" pin="P" pad="3"/>
<connect gate="B" pin="M" pad="6"/>
<connect gate="B" pin="OUT" pad="7"/>
<connect gate="B" pin="P" pad="5"/>
<connect gate="C" pin="AVDD" pad="8"/>
<connect gate="C" pin="AVDD_M" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="ADI-ADG706">
<packages>
<package name="SOP65P638X110-28N">
<wire x1="-4.953" y1="-1.5748" x2="-3.937" y2="-1.5748" width="0.1524" layer="21"/>
<wire x1="3.937" y1="-0.8636" x2="4.953" y2="-0.8636" width="0.1524" layer="21"/>
<wire x1="2.2606" y1="4.7244" x2="2.2606" y2="4.9022" width="0.1524" layer="21"/>
<wire x1="-2.2606" y1="-4.7244" x2="-2.2606" y2="-4.9022" width="0.1524" layer="21"/>
<wire x1="-2.2606" y1="-4.9022" x2="2.2606" y2="-4.9022" width="0.1524" layer="21"/>
<wire x1="2.2606" y1="-4.9022" x2="2.2606" y2="-4.7244" width="0.1524" layer="21"/>
<wire x1="2.2606" y1="4.9022" x2="0.3048" y2="4.9022" width="0.1524" layer="21"/>
<wire x1="0.3048" y1="4.9022" x2="-0.3048" y2="4.9022" width="0.1524" layer="21"/>
<wire x1="-0.3048" y1="4.9022" x2="-2.2606" y2="4.9022" width="0.1524" layer="21"/>
<wire x1="-2.2606" y1="4.9022" x2="-2.2606" y2="4.7244" width="0.1524" layer="21"/>
<wire x1="0.3048" y1="4.9022" x2="-0.3048" y2="4.9022" width="0.1524" layer="21" curve="-180"/>
<wire x1="-2.2606" y1="4.064" x2="-2.2606" y2="4.3688" width="0" layer="51"/>
<wire x1="-2.2606" y1="4.3688" x2="-3.2512" y2="4.3688" width="0" layer="51"/>
<wire x1="-3.2512" y1="4.3688" x2="-3.2512" y2="4.064" width="0" layer="51"/>
<wire x1="-3.2512" y1="4.064" x2="-2.2606" y2="4.064" width="0" layer="51"/>
<wire x1="-2.2606" y1="3.429" x2="-2.2606" y2="3.7338" width="0" layer="51"/>
<wire x1="-2.2606" y1="3.7338" x2="-3.2512" y2="3.7338" width="0" layer="51"/>
<wire x1="-3.2512" y1="3.7338" x2="-3.2512" y2="3.429" width="0" layer="51"/>
<wire x1="-3.2512" y1="3.429" x2="-2.2606" y2="3.429" width="0" layer="51"/>
<wire x1="-2.2606" y1="2.7686" x2="-2.2606" y2="3.0734" width="0" layer="51"/>
<wire x1="-2.2606" y1="3.0734" x2="-3.2512" y2="3.0734" width="0" layer="51"/>
<wire x1="-3.2512" y1="3.0734" x2="-3.2512" y2="2.7686" width="0" layer="51"/>
<wire x1="-3.2512" y1="2.7686" x2="-2.2606" y2="2.7686" width="0" layer="51"/>
<wire x1="-2.2606" y1="2.1336" x2="-2.2606" y2="2.4384" width="0" layer="51"/>
<wire x1="-2.2606" y1="2.4384" x2="-3.2512" y2="2.4384" width="0" layer="51"/>
<wire x1="-3.2512" y1="2.4384" x2="-3.2512" y2="2.1336" width="0" layer="51"/>
<wire x1="-3.2512" y1="2.1336" x2="-2.2606" y2="2.1336" width="0" layer="51"/>
<wire x1="-2.2606" y1="1.4732" x2="-2.2606" y2="1.778" width="0" layer="51"/>
<wire x1="-2.2606" y1="1.778" x2="-3.2512" y2="1.778" width="0" layer="51"/>
<wire x1="-3.2512" y1="1.778" x2="-3.2512" y2="1.4732" width="0" layer="51"/>
<wire x1="-3.2512" y1="1.4732" x2="-2.2606" y2="1.4732" width="0" layer="51"/>
<wire x1="-2.2606" y1="0.8128" x2="-2.2606" y2="1.1176" width="0" layer="51"/>
<wire x1="-2.2606" y1="1.1176" x2="-3.2512" y2="1.1176" width="0" layer="51"/>
<wire x1="-3.2512" y1="1.1176" x2="-3.2512" y2="0.8128" width="0" layer="51"/>
<wire x1="-3.2512" y1="0.8128" x2="-2.2606" y2="0.8128" width="0" layer="51"/>
<wire x1="-2.2606" y1="0.1778" x2="-2.2606" y2="0.4826" width="0" layer="51"/>
<wire x1="-2.2606" y1="0.4826" x2="-3.2512" y2="0.4826" width="0" layer="51"/>
<wire x1="-3.2512" y1="0.4826" x2="-3.2512" y2="0.1778" width="0" layer="51"/>
<wire x1="-3.2512" y1="0.1778" x2="-2.2606" y2="0.1778" width="0" layer="51"/>
<wire x1="-2.2606" y1="-0.4826" x2="-2.2606" y2="-0.1778" width="0" layer="51"/>
<wire x1="-2.2606" y1="-0.1778" x2="-3.2512" y2="-0.1778" width="0" layer="51"/>
<wire x1="-3.2512" y1="-0.1778" x2="-3.2512" y2="-0.4826" width="0" layer="51"/>
<wire x1="-3.2512" y1="-0.4826" x2="-2.2606" y2="-0.4826" width="0" layer="51"/>
<wire x1="-2.2606" y1="-1.1176" x2="-2.2606" y2="-0.8128" width="0" layer="51"/>
<wire x1="-2.2606" y1="-0.8128" x2="-3.2512" y2="-0.8128" width="0" layer="51"/>
<wire x1="-3.2512" y1="-0.8128" x2="-3.2512" y2="-1.1176" width="0" layer="51"/>
<wire x1="-3.2512" y1="-1.1176" x2="-2.2606" y2="-1.1176" width="0" layer="51"/>
<wire x1="-2.2606" y1="-1.778" x2="-2.2606" y2="-1.4732" width="0" layer="51"/>
<wire x1="-2.2606" y1="-1.4732" x2="-3.2512" y2="-1.4732" width="0" layer="51"/>
<wire x1="-3.2512" y1="-1.4732" x2="-3.2512" y2="-1.778" width="0" layer="51"/>
<wire x1="-3.2512" y1="-1.778" x2="-2.2606" y2="-1.778" width="0" layer="51"/>
<wire x1="-2.2606" y1="-2.4384" x2="-2.2606" y2="-2.1336" width="0" layer="51"/>
<wire x1="-2.2606" y1="-2.1336" x2="-3.2512" y2="-2.1336" width="0" layer="51"/>
<wire x1="-3.2512" y1="-2.1336" x2="-3.2512" y2="-2.4384" width="0" layer="51"/>
<wire x1="-3.2512" y1="-2.4384" x2="-2.2606" y2="-2.4384" width="0" layer="51"/>
<wire x1="-2.2606" y1="-3.0734" x2="-2.2606" y2="-2.7686" width="0" layer="51"/>
<wire x1="-2.2606" y1="-2.7686" x2="-3.2512" y2="-2.7686" width="0" layer="51"/>
<wire x1="-3.2512" y1="-2.7686" x2="-3.2512" y2="-3.0734" width="0" layer="51"/>
<wire x1="-3.2512" y1="-3.0734" x2="-2.2606" y2="-3.0734" width="0" layer="51"/>
<wire x1="-2.2606" y1="-3.7338" x2="-2.2606" y2="-3.429" width="0" layer="51"/>
<wire x1="-2.2606" y1="-3.429" x2="-3.2512" y2="-3.429" width="0" layer="51"/>
<wire x1="-3.2512" y1="-3.429" x2="-3.2512" y2="-3.7338" width="0" layer="51"/>
<wire x1="-3.2512" y1="-3.7338" x2="-2.2606" y2="-3.7338" width="0" layer="51"/>
<wire x1="-2.2606" y1="-4.3688" x2="-2.2606" y2="-4.064" width="0" layer="51"/>
<wire x1="-2.2606" y1="-4.064" x2="-3.2512" y2="-4.064" width="0" layer="51"/>
<wire x1="-3.2512" y1="-4.064" x2="-3.2512" y2="-4.3688" width="0" layer="51"/>
<wire x1="-3.2512" y1="-4.3688" x2="-2.2606" y2="-4.3688" width="0" layer="51"/>
<wire x1="2.2606" y1="-4.064" x2="2.2606" y2="-4.3688" width="0" layer="51"/>
<wire x1="2.2606" y1="-4.3688" x2="3.2512" y2="-4.3688" width="0" layer="51"/>
<wire x1="3.2512" y1="-4.3688" x2="3.2512" y2="-4.064" width="0" layer="51"/>
<wire x1="3.2512" y1="-4.064" x2="2.2606" y2="-4.064" width="0" layer="51"/>
<wire x1="2.2606" y1="-3.429" x2="2.2606" y2="-3.7338" width="0" layer="51"/>
<wire x1="2.2606" y1="-3.7338" x2="3.2512" y2="-3.7338" width="0" layer="51"/>
<wire x1="3.2512" y1="-3.7338" x2="3.2512" y2="-3.429" width="0" layer="51"/>
<wire x1="3.2512" y1="-3.429" x2="2.2606" y2="-3.429" width="0" layer="51"/>
<wire x1="2.2606" y1="-2.7686" x2="2.2606" y2="-3.0734" width="0" layer="51"/>
<wire x1="2.2606" y1="-3.0734" x2="3.2512" y2="-3.0734" width="0" layer="51"/>
<wire x1="3.2512" y1="-3.0734" x2="3.2512" y2="-2.7686" width="0" layer="51"/>
<wire x1="3.2512" y1="-2.7686" x2="2.2606" y2="-2.7686" width="0" layer="51"/>
<wire x1="2.2606" y1="-2.1336" x2="2.2606" y2="-2.4384" width="0" layer="51"/>
<wire x1="2.2606" y1="-2.4384" x2="3.2512" y2="-2.4384" width="0" layer="51"/>
<wire x1="3.2512" y1="-2.4384" x2="3.2512" y2="-2.1336" width="0" layer="51"/>
<wire x1="3.2512" y1="-2.1336" x2="2.2606" y2="-2.1336" width="0" layer="51"/>
<wire x1="2.2606" y1="-1.4732" x2="2.2606" y2="-1.778" width="0" layer="51"/>
<wire x1="2.2606" y1="-1.778" x2="3.2512" y2="-1.778" width="0" layer="51"/>
<wire x1="3.2512" y1="-1.778" x2="3.2512" y2="-1.4732" width="0" layer="51"/>
<wire x1="3.2512" y1="-1.4732" x2="2.2606" y2="-1.4732" width="0" layer="51"/>
<wire x1="2.2606" y1="-0.8128" x2="2.2606" y2="-1.1176" width="0" layer="51"/>
<wire x1="2.2606" y1="-1.1176" x2="3.2512" y2="-1.1176" width="0" layer="51"/>
<wire x1="3.2512" y1="-1.1176" x2="3.2512" y2="-0.8128" width="0" layer="51"/>
<wire x1="3.2512" y1="-0.8128" x2="2.2606" y2="-0.8128" width="0" layer="51"/>
<wire x1="2.2606" y1="-0.1778" x2="2.2606" y2="-0.4826" width="0" layer="51"/>
<wire x1="2.2606" y1="-0.4826" x2="3.2512" y2="-0.4826" width="0" layer="51"/>
<wire x1="3.2512" y1="-0.4826" x2="3.2512" y2="-0.1778" width="0" layer="51"/>
<wire x1="3.2512" y1="-0.1778" x2="2.2606" y2="-0.1778" width="0" layer="51"/>
<wire x1="2.2606" y1="0.4826" x2="2.2606" y2="0.1778" width="0" layer="51"/>
<wire x1="2.2606" y1="0.1778" x2="3.2512" y2="0.1778" width="0" layer="51"/>
<wire x1="3.2512" y1="0.1778" x2="3.2512" y2="0.4826" width="0" layer="51"/>
<wire x1="3.2512" y1="0.4826" x2="2.2606" y2="0.4826" width="0" layer="51"/>
<wire x1="2.2606" y1="1.1176" x2="2.2606" y2="0.8128" width="0" layer="51"/>
<wire x1="2.2606" y1="0.8128" x2="3.2512" y2="0.8128" width="0" layer="51"/>
<wire x1="3.2512" y1="0.8128" x2="3.2512" y2="1.1176" width="0" layer="51"/>
<wire x1="3.2512" y1="1.1176" x2="2.2606" y2="1.1176" width="0" layer="51"/>
<wire x1="2.2606" y1="1.778" x2="2.2606" y2="1.4732" width="0" layer="51"/>
<wire x1="2.2606" y1="1.4732" x2="3.2512" y2="1.4732" width="0" layer="51"/>
<wire x1="3.2512" y1="1.4732" x2="3.2512" y2="1.778" width="0" layer="51"/>
<wire x1="3.2512" y1="1.778" x2="2.2606" y2="1.778" width="0" layer="51"/>
<wire x1="2.2606" y1="2.4384" x2="2.2606" y2="2.1336" width="0" layer="51"/>
<wire x1="2.2606" y1="2.1336" x2="3.2512" y2="2.1336" width="0" layer="51"/>
<wire x1="3.2512" y1="2.1336" x2="3.2512" y2="2.4384" width="0" layer="51"/>
<wire x1="3.2512" y1="2.4384" x2="2.2606" y2="2.4384" width="0" layer="51"/>
<wire x1="2.2606" y1="3.0734" x2="2.2606" y2="2.7686" width="0" layer="51"/>
<wire x1="2.2606" y1="2.7686" x2="3.2512" y2="2.7686" width="0" layer="51"/>
<wire x1="3.2512" y1="2.7686" x2="3.2512" y2="3.0734" width="0" layer="51"/>
<wire x1="3.2512" y1="3.0734" x2="2.2606" y2="3.0734" width="0" layer="51"/>
<wire x1="2.2606" y1="3.7338" x2="2.2606" y2="3.429" width="0" layer="51"/>
<wire x1="2.2606" y1="3.429" x2="3.2512" y2="3.429" width="0" layer="51"/>
<wire x1="3.2512" y1="3.429" x2="3.2512" y2="3.7338" width="0" layer="51"/>
<wire x1="3.2512" y1="3.7338" x2="2.2606" y2="3.7338" width="0" layer="51"/>
<wire x1="2.2606" y1="4.3688" x2="2.2606" y2="4.064" width="0" layer="51"/>
<wire x1="2.2606" y1="4.064" x2="3.2512" y2="4.064" width="0" layer="51"/>
<wire x1="3.2512" y1="4.064" x2="3.2512" y2="4.3688" width="0" layer="51"/>
<wire x1="3.2512" y1="4.3688" x2="2.2606" y2="4.3688" width="0" layer="51"/>
<wire x1="-2.2606" y1="-4.9022" x2="2.2606" y2="-4.9022" width="0" layer="51"/>
<wire x1="2.2606" y1="-4.9022" x2="2.2606" y2="4.9022" width="0" layer="51"/>
<wire x1="2.2606" y1="4.9022" x2="0.3048" y2="4.9022" width="0" layer="51"/>
<wire x1="0.3048" y1="4.9022" x2="-0.3048" y2="4.9022" width="0" layer="51"/>
<wire x1="-0.3048" y1="4.9022" x2="-2.2606" y2="4.9022" width="0" layer="51"/>
<wire x1="-2.2606" y1="4.9022" x2="-2.2606" y2="-4.9022" width="0" layer="51"/>
<wire x1="0.3048" y1="4.9022" x2="-0.3048" y2="4.9022" width="0" layer="51" curve="-180"/>
<smd name="1" x="-2.8956" y="4.2164" dx="1.4224" dy="0.3556" layer="1"/>
<smd name="2" x="-2.8956" y="3.5814" dx="1.4224" dy="0.3556" layer="1"/>
<smd name="3" x="-2.8956" y="2.921" dx="1.4224" dy="0.3556" layer="1"/>
<smd name="4" x="-2.8956" y="2.286" dx="1.4224" dy="0.3556" layer="1"/>
<smd name="5" x="-2.8956" y="1.6256" dx="1.4224" dy="0.3556" layer="1"/>
<smd name="6" x="-2.8956" y="0.9652" dx="1.4224" dy="0.3556" layer="1"/>
<smd name="7" x="-2.8956" y="0.3302" dx="1.4224" dy="0.3556" layer="1"/>
<smd name="8" x="-2.8956" y="-0.3302" dx="1.4224" dy="0.3556" layer="1"/>
<smd name="9" x="-2.8956" y="-0.9652" dx="1.4224" dy="0.3556" layer="1"/>
<smd name="10" x="-2.8956" y="-1.6256" dx="1.4224" dy="0.3556" layer="1"/>
<smd name="11" x="-2.8956" y="-2.286" dx="1.4224" dy="0.3556" layer="1"/>
<smd name="12" x="-2.8956" y="-2.921" dx="1.4224" dy="0.3556" layer="1"/>
<smd name="13" x="-2.8956" y="-3.5814" dx="1.4224" dy="0.3556" layer="1"/>
<smd name="14" x="-2.8956" y="-4.2164" dx="1.4224" dy="0.3556" layer="1"/>
<smd name="15" x="2.8956" y="-4.2164" dx="1.4224" dy="0.3556" layer="1"/>
<smd name="16" x="2.8956" y="-3.5814" dx="1.4224" dy="0.3556" layer="1"/>
<smd name="17" x="2.8956" y="-2.921" dx="1.4224" dy="0.3556" layer="1"/>
<smd name="18" x="2.8956" y="-2.286" dx="1.4224" dy="0.3556" layer="1"/>
<smd name="19" x="2.8956" y="-1.6256" dx="1.4224" dy="0.3556" layer="1"/>
<smd name="20" x="2.8956" y="-0.9652" dx="1.4224" dy="0.3556" layer="1"/>
<smd name="21" x="2.8956" y="-0.3302" dx="1.4224" dy="0.3556" layer="1"/>
<smd name="22" x="2.8956" y="0.3302" dx="1.4224" dy="0.3556" layer="1"/>
<smd name="23" x="2.8956" y="0.9652" dx="1.4224" dy="0.3556" layer="1"/>
<smd name="24" x="2.8956" y="1.6256" dx="1.4224" dy="0.3556" layer="1"/>
<smd name="25" x="2.8956" y="2.286" dx="1.4224" dy="0.3556" layer="1"/>
<smd name="26" x="2.8956" y="2.921" dx="1.4224" dy="0.3556" layer="1"/>
<smd name="27" x="2.8956" y="3.5814" dx="1.4224" dy="0.3556" layer="1"/>
<smd name="28" x="2.8956" y="4.2164" dx="1.4224" dy="0.3556" layer="1"/>
<text x="-3.7338" y="4.445" size="1.27" layer="21" ratio="6" rot="SR0">*</text>
<text x="-3.7338" y="4.445" size="1.27" layer="51" ratio="6" rot="SR0">*</text>
<text x="-3.4544" y="5.715" size="2.0828" layer="25" ratio="10" rot="SR0">&gt;NAME</text>
<text x="-3.4544" y="-7.62" size="2.0828" layer="27" ratio="10" rot="SR0">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="ADG706BRUZ">
<wire x1="-12.7" y1="22.86" x2="-12.7" y2="-30.48" width="0.4064" layer="94"/>
<wire x1="-12.7" y1="-30.48" x2="12.7" y2="-30.48" width="0.4064" layer="94"/>
<wire x1="12.7" y1="-30.48" x2="12.7" y2="22.86" width="0.4064" layer="94"/>
<wire x1="12.7" y1="22.86" x2="-12.7" y2="22.86" width="0.4064" layer="94"/>
<text x="-5.0292" y="24.7904" size="2.0828" layer="95" ratio="10" rot="SR0">&gt;NAME</text>
<text x="-6.4008" y="-33.147" size="2.0828" layer="96" ratio="10" rot="SR0">&gt;VALUE</text>
<pin name="VDD" x="-17.78" y="17.78" length="middle" direction="pwr"/>
<pin name="EN" x="-17.78" y="12.7" length="middle" direction="in"/>
<pin name="A0" x="-17.78" y="7.62" length="middle" direction="in"/>
<pin name="A1" x="-17.78" y="5.08" length="middle" direction="in"/>
<pin name="A2" x="-17.78" y="2.54" length="middle" direction="in"/>
<pin name="A3" x="-17.78" y="0" length="middle" direction="in"/>
<pin name="NC_2" x="-17.78" y="-5.08" length="middle" direction="nc"/>
<pin name="NC_3" x="-17.78" y="-7.62" length="middle" direction="nc"/>
<pin name="NC" x="-17.78" y="-10.16" length="middle" direction="nc"/>
<pin name="GND" x="-17.78" y="-15.24" length="middle" direction="pas"/>
<pin name="VSS" x="-17.78" y="-17.78" length="middle" direction="pas"/>
<pin name="D" x="17.78" y="17.78" length="middle" rot="R180"/>
<pin name="S1" x="17.78" y="12.7" length="middle" rot="R180"/>
<pin name="S2" x="17.78" y="10.16" length="middle" rot="R180"/>
<pin name="S3" x="17.78" y="7.62" length="middle" rot="R180"/>
<pin name="S4" x="17.78" y="5.08" length="middle" rot="R180"/>
<pin name="S5" x="17.78" y="2.54" length="middle" rot="R180"/>
<pin name="S6" x="17.78" y="0" length="middle" rot="R180"/>
<pin name="S7" x="17.78" y="-2.54" length="middle" rot="R180"/>
<pin name="S8" x="17.78" y="-5.08" length="middle" rot="R180"/>
<pin name="S9" x="17.78" y="-7.62" length="middle" rot="R180"/>
<pin name="S10" x="17.78" y="-10.16" length="middle" rot="R180"/>
<pin name="S11" x="17.78" y="-12.7" length="middle" rot="R180"/>
<pin name="S12" x="17.78" y="-15.24" length="middle" rot="R180"/>
<pin name="S13" x="17.78" y="-17.78" length="middle" rot="R180"/>
<pin name="S14" x="17.78" y="-20.32" length="middle" rot="R180"/>
<pin name="S15" x="17.78" y="-22.86" length="middle" rot="R180"/>
<pin name="S16" x="17.78" y="-25.4" length="middle" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="ADG706BRUZ" prefix="U">
<description>Multiplexers</description>
<gates>
<gate name="A" symbol="ADG706BRUZ" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOP65P638X110-28N">
<connects>
<connect gate="A" pin="A0" pad="17"/>
<connect gate="A" pin="A1" pad="16"/>
<connect gate="A" pin="A2" pad="15"/>
<connect gate="A" pin="A3" pad="14"/>
<connect gate="A" pin="D" pad="28"/>
<connect gate="A" pin="EN" pad="18"/>
<connect gate="A" pin="GND" pad="12"/>
<connect gate="A" pin="NC" pad="13"/>
<connect gate="A" pin="NC_2" pad="2"/>
<connect gate="A" pin="NC_3" pad="3"/>
<connect gate="A" pin="S1" pad="19"/>
<connect gate="A" pin="S10" pad="10"/>
<connect gate="A" pin="S11" pad="9"/>
<connect gate="A" pin="S12" pad="8"/>
<connect gate="A" pin="S13" pad="7"/>
<connect gate="A" pin="S14" pad="6"/>
<connect gate="A" pin="S15" pad="5"/>
<connect gate="A" pin="S16" pad="4"/>
<connect gate="A" pin="S2" pad="20"/>
<connect gate="A" pin="S3" pad="21"/>
<connect gate="A" pin="S4" pad="22"/>
<connect gate="A" pin="S5" pad="23"/>
<connect gate="A" pin="S6" pad="24"/>
<connect gate="A" pin="S7" pad="25"/>
<connect gate="A" pin="S8" pad="26"/>
<connect gate="A" pin="S9" pad="11"/>
<connect gate="A" pin="VDD" pad="1"/>
<connect gate="A" pin="VSS" pad="27"/>
</connects>
<technologies>
<technology name="">
<attribute name="MPN" value="ADG706BRUZ" constant="no"/>
<attribute name="OC_FARNELL" value="9604332" constant="no"/>
<attribute name="OC_NEWARK" value="19M0682" constant="no"/>
<attribute name="PACKAGE" value="TSSOP-28" constant="no"/>
<attribute name="SUPPLIER" value="Analog Devices" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="AD8664">
<packages>
<package name="SOIC127P600X175-14N">
<wire x1="-2.0066" y1="3.556" x2="-2.0066" y2="4.064" width="0.1524" layer="51"/>
<wire x1="-2.0066" y1="4.064" x2="-3.0988" y2="4.064" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="4.064" x2="-3.0988" y2="3.556" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="3.556" x2="-2.0066" y2="3.556" width="0.1524" layer="51"/>
<wire x1="-2.0066" y1="2.286" x2="-2.0066" y2="2.794" width="0.1524" layer="51"/>
<wire x1="-2.0066" y1="2.794" x2="-3.0988" y2="2.794" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="2.794" x2="-3.0988" y2="2.286" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="2.286" x2="-2.0066" y2="2.286" width="0.1524" layer="51"/>
<wire x1="-2.0066" y1="1.016" x2="-2.0066" y2="1.524" width="0.1524" layer="51"/>
<wire x1="-2.0066" y1="1.524" x2="-3.0988" y2="1.524" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="1.524" x2="-3.0988" y2="1.016" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="1.016" x2="-2.0066" y2="1.016" width="0.1524" layer="51"/>
<wire x1="-2.0066" y1="-0.254" x2="-2.0066" y2="0.254" width="0.1524" layer="51"/>
<wire x1="-2.0066" y1="0.254" x2="-3.0988" y2="0.254" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="0.254" x2="-3.0988" y2="-0.254" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="-0.254" x2="-2.0066" y2="-0.254" width="0.1524" layer="51"/>
<wire x1="-2.0066" y1="-1.524" x2="-2.0066" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="-2.0066" y1="-1.016" x2="-3.0988" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="-1.016" x2="-3.0988" y2="-1.524" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="-1.524" x2="-2.0066" y2="-1.524" width="0.1524" layer="51"/>
<wire x1="-2.0066" y1="-2.794" x2="-2.0066" y2="-2.286" width="0.1524" layer="51"/>
<wire x1="-2.0066" y1="-2.286" x2="-3.0988" y2="-2.286" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="-2.286" x2="-3.0988" y2="-2.794" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="-2.794" x2="-2.0066" y2="-2.794" width="0.1524" layer="51"/>
<wire x1="-2.0066" y1="-4.064" x2="-2.0066" y2="-3.556" width="0.1524" layer="51"/>
<wire x1="-2.0066" y1="-3.556" x2="-3.0988" y2="-3.556" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="-3.556" x2="-3.0988" y2="-4.064" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="-4.064" x2="-2.0066" y2="-4.064" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="-3.556" x2="2.0066" y2="-4.064" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="-4.064" x2="3.0988" y2="-4.064" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="-4.064" x2="3.0988" y2="-3.556" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="-3.556" x2="2.0066" y2="-3.556" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="-2.286" x2="2.0066" y2="-2.794" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="-2.794" x2="3.0988" y2="-2.794" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="-2.794" x2="3.0988" y2="-2.286" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="-2.286" x2="2.0066" y2="-2.286" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="-1.016" x2="2.0066" y2="-1.524" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="-1.524" x2="3.0988" y2="-1.524" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="-1.524" x2="3.0988" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="-1.016" x2="2.0066" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="0.254" x2="2.0066" y2="-0.254" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="-0.254" x2="3.0988" y2="-0.254" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="-0.254" x2="3.0988" y2="0.254" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="0.254" x2="2.0066" y2="0.254" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="1.524" x2="2.0066" y2="1.016" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="1.016" x2="3.0988" y2="1.016" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="1.016" x2="3.0988" y2="1.524" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="1.524" x2="2.0066" y2="1.524" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="2.794" x2="2.0066" y2="2.286" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="2.286" x2="3.0988" y2="2.286" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="2.286" x2="3.0988" y2="2.794" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="2.794" x2="2.0066" y2="2.794" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="4.064" x2="2.0066" y2="3.556" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="3.556" x2="3.0988" y2="3.556" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="3.556" x2="3.0988" y2="4.064" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="4.064" x2="2.0066" y2="4.064" width="0.1524" layer="51"/>
<wire x1="-2.0066" y1="-4.3688" x2="2.0066" y2="-4.3688" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="-4.3688" x2="2.0066" y2="4.3688" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="4.3688" x2="0.3048" y2="4.3688" width="0.1524" layer="51"/>
<wire x1="0.3048" y1="4.3688" x2="-0.3048" y2="4.3688" width="0.1524" layer="51"/>
<wire x1="-0.3048" y1="4.3688" x2="-2.0066" y2="4.3688" width="0.1524" layer="51"/>
<wire x1="-2.0066" y1="4.3688" x2="-2.0066" y2="-4.3688" width="0.1524" layer="51"/>
<wire x1="0.3048" y1="4.3688" x2="-0.3048" y2="4.3688" width="0" layer="51" curve="-180"/>
<wire x1="-1.2954" y1="-4.3688" x2="1.2954" y2="-4.3688" width="0.1524" layer="21"/>
<wire x1="1.2954" y1="4.3688" x2="0.3048" y2="4.3688" width="0.1524" layer="21"/>
<wire x1="0.3048" y1="4.3688" x2="-0.3048" y2="4.3688" width="0.1524" layer="21"/>
<wire x1="-0.3048" y1="4.3688" x2="-1.2954" y2="4.3688" width="0.1524" layer="21"/>
<wire x1="0.3048" y1="4.3688" x2="-0.3048" y2="4.3688" width="0" layer="21" curve="-180"/>
<wire x1="3.9624" y1="-1.0668" x2="3.9624" y2="-1.4732" width="0.1524" layer="49"/>
<wire x1="3.9624" y1="-1.4732" x2="3.7084" y2="-1.4732" width="0.1524" layer="49"/>
<wire x1="3.7084" y1="-1.4732" x2="3.7084" y2="-1.0668" width="0.1524" layer="49"/>
<smd name="1" x="-2.4638" y="3.81" dx="1.9812" dy="0.5588" layer="1"/>
<smd name="2" x="-2.4638" y="2.54" dx="1.9812" dy="0.5588" layer="1"/>
<smd name="3" x="-2.4638" y="1.27" dx="1.9812" dy="0.5588" layer="1"/>
<smd name="4" x="-2.4638" y="0" dx="1.9812" dy="0.5588" layer="1"/>
<smd name="5" x="-2.4638" y="-1.27" dx="1.9812" dy="0.5588" layer="1"/>
<smd name="6" x="-2.4638" y="-2.54" dx="1.9812" dy="0.5588" layer="1"/>
<smd name="7" x="-2.4638" y="-3.81" dx="1.9812" dy="0.5588" layer="1"/>
<smd name="8" x="2.4638" y="-3.81" dx="1.9812" dy="0.5588" layer="1"/>
<smd name="9" x="2.4638" y="-2.54" dx="1.9812" dy="0.5588" layer="1"/>
<smd name="10" x="2.4638" y="-1.27" dx="1.9812" dy="0.5588" layer="1"/>
<smd name="11" x="2.4638" y="0" dx="1.9812" dy="0.5588" layer="1"/>
<smd name="12" x="2.4638" y="1.27" dx="1.9812" dy="0.5588" layer="1"/>
<smd name="13" x="2.4638" y="2.54" dx="1.9812" dy="0.5588" layer="1"/>
<smd name="14" x="2.4638" y="3.81" dx="1.9812" dy="0.5588" layer="1"/>
<text x="-3.302" y="4.2418" size="1.27" layer="51" ratio="6" rot="SR0">*</text>
<text x="-3.302" y="4.2418" size="1.27" layer="49" ratio="6" rot="SR0">*</text>
<text x="-3.4544" y="5.715" size="2.0828" layer="25" ratio="10" rot="SR0">&gt;NAME</text>
<text x="-3.4544" y="-7.62" size="2.0828" layer="27" ratio="10" rot="SR0">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="AD8664ARZ">
<wire x1="-12.7" y1="15.24" x2="-12.7" y2="-22.86" width="0.4064" layer="94"/>
<wire x1="-12.7" y1="-22.86" x2="12.7" y2="-22.86" width="0.4064" layer="94"/>
<wire x1="12.7" y1="-22.86" x2="12.7" y2="15.24" width="0.4064" layer="94"/>
<wire x1="12.7" y1="15.24" x2="-12.7" y2="15.24" width="0.4064" layer="94"/>
<text x="-5.1054" y="17.3736" size="2.0828" layer="95" ratio="10" rot="SR0">&gt;NAME</text>
<text x="-5.969" y="-25.781" size="2.0828" layer="96" ratio="10" rot="SR0">&gt;VALUE</text>
<pin name="V+" x="-17.78" y="10.16" length="middle" direction="pwr"/>
<pin name="-IN_A" x="-17.78" y="5.08" length="middle" direction="in"/>
<pin name="+IN_A" x="-17.78" y="2.54" length="middle" direction="in"/>
<pin name="-IN_B" x="-17.78" y="0" length="middle" direction="in"/>
<pin name="+IN_B" x="-17.78" y="-2.54" length="middle" direction="in"/>
<pin name="+IN_C" x="-17.78" y="-5.08" length="middle" direction="in"/>
<pin name="-IN_C" x="-17.78" y="-7.62" length="middle" direction="in"/>
<pin name="+IN_D" x="-17.78" y="-10.16" length="middle" direction="in"/>
<pin name="-IN_D" x="-17.78" y="-12.7" length="middle" direction="in"/>
<pin name="V-" x="-17.78" y="-17.78" length="middle" direction="pwr"/>
<pin name="OUT_A" x="17.78" y="10.16" length="middle" direction="out" rot="R180"/>
<pin name="OUT_B" x="17.78" y="7.62" length="middle" direction="out" rot="R180"/>
<pin name="OUT_C" x="17.78" y="5.08" length="middle" direction="out" rot="R180"/>
<pin name="OUT_D" x="17.78" y="2.54" length="middle" direction="out" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="AD8664ARZ" prefix="U">
<description>Operational Amplifiers</description>
<gates>
<gate name="A" symbol="AD8664ARZ" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOIC127P600X175-14N">
<connects>
<connect gate="A" pin="+IN_A" pad="3"/>
<connect gate="A" pin="+IN_B" pad="5"/>
<connect gate="A" pin="+IN_C" pad="10"/>
<connect gate="A" pin="+IN_D" pad="12"/>
<connect gate="A" pin="-IN_A" pad="2"/>
<connect gate="A" pin="-IN_B" pad="6"/>
<connect gate="A" pin="-IN_C" pad="9"/>
<connect gate="A" pin="-IN_D" pad="13"/>
<connect gate="A" pin="OUT_A" pad="1"/>
<connect gate="A" pin="OUT_B" pad="7"/>
<connect gate="A" pin="OUT_C" pad="8"/>
<connect gate="A" pin="OUT_D" pad="14"/>
<connect gate="A" pin="V+" pad="4"/>
<connect gate="A" pin="V-" pad="11"/>
</connects>
<technologies>
<technology name="">
<attribute name="MPN" value="AD8664ARZ" constant="no"/>
<attribute name="OC_FARNELL" value="1498687" constant="no"/>
<attribute name="OC_NEWARK" value="15P1030" constant="no"/>
<attribute name="PACKAGE" value="SOIC-14" constant="no"/>
<attribute name="SUPPLIER" value="Analog Devices" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="MMCX">
<packages>
<package name="MMCX">
<wire x1="-2.54" y1="2.54" x2="-2.54" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-2.54" x2="2.54" y2="-2.54" width="0.127" layer="21"/>
<wire x1="2.54" y1="-2.54" x2="2.54" y2="2.54" width="0.127" layer="21"/>
<wire x1="2.54" y1="2.54" x2="-2.54" y2="2.54" width="0.127" layer="21"/>
<circle x="0" y="0" radius="1.905" width="0.127" layer="21"/>
<pad name="P$1" x="0" y="0" drill="0.8" shape="octagon"/>
<pad name="P$2" x="1.27" y="-1.27" drill="0.8" shape="octagon"/>
<pad name="P$3" x="-1.27" y="-1.27" drill="0.8" shape="octagon"/>
<pad name="P$4" x="-1.27" y="1.27" drill="0.8" shape="octagon"/>
<pad name="P$5" x="1.27" y="1.27" drill="0.8" shape="octagon"/>
<text x="-2.54" y="2.8575" size="1.27" layer="25">&gt;NAME</text>
</package>
</packages>
<symbols>
<symbol name="MMCX_SYM">
<circle x="2.54" y="2.54" radius="2.54" width="0.254" layer="94"/>
<text x="-2.54" y="5.08" size="1.27" layer="95">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.27" layer="96">&gt;VALUE</text>
<pin name="PIN" x="-2.54" y="2.54" visible="pin" length="middle"/>
<pin name="SHIELD" x="-2.54" y="0" visible="pin" length="middle"/>
<pin name="P$1" x="-2.54" y="0" visible="off" length="point"/>
<pin name="P$2" x="-2.54" y="0" visible="off" length="point"/>
<pin name="P$3" x="-2.54" y="0" visible="off" length="point"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="MMCX" prefix="X">
<description>MULTICOMP  37-06-TGG  HF-/Koaxialsteckverbinder, MMCX-Koaxial, Gerade Einbaubuchse mit Verschraubung, Gelötet, 50 Ohm 

&lt;a href="http://de.farnell.com/multicomp/37-06-tgg/kupplung-pcb-mmcx/dp/1169673" &gt;Farnell Bauteilbeschreibung&lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="MMCX_SYM" x="-12.7" y="-7.62"/>
</gates>
<devices>
<device name="" package="MMCX">
<connects>
<connect gate="G$1" pin="P$1" pad="P$2"/>
<connect gate="G$1" pin="P$2" pad="P$3"/>
<connect gate="G$1" pin="P$3" pad="P$4"/>
<connect gate="G$1" pin="PIN" pad="P$1"/>
<connect gate="G$1" pin="SHIELD" pad="P$5"/>
</connects>
<technologies>
<technology name="THT"/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="crystal">
<packages>
<package name="CSM-7X-DU">
<description>&lt;b&gt;SMD CRYSTAL&lt;/b&gt;&lt;p&gt;
Source: www.ecsxtal.com .. Crystal 3.6864MHz CSM_7X_DU.PDF</description>
<wire x1="-5.575" y1="2.3" x2="-2.725" y2="2.3" width="0.2032" layer="51"/>
<wire x1="-2.725" y1="2.3" x2="2.75" y2="2.3" width="0.2032" layer="51"/>
<wire x1="2.75" y1="2.3" x2="5.55" y2="2.3" width="0.2032" layer="51"/>
<wire x1="5.55" y1="2.3" x2="5.55" y2="-2.3" width="0.2032" layer="51"/>
<wire x1="5.55" y1="-2.3" x2="-2.75" y2="-2.3" width="0.2032" layer="51"/>
<wire x1="-2.75" y1="-2.3" x2="-5.575" y2="-2.3" width="0.2032" layer="51"/>
<wire x1="-5.575" y1="-2.3" x2="-5.575" y2="2.3" width="0.2032" layer="51"/>
<wire x1="-2.75" y1="-2.3" x2="-2.725" y2="2.3" width="0.2032" layer="51" curve="-180"/>
<wire x1="2.75" y1="2.3" x2="2.725" y2="-2.3" width="0.2032" layer="51" curve="-180"/>
<smd name="1" x="-4.75" y="0" dx="5.5" dy="2" layer="1"/>
<smd name="2" x="4.75" y="0" dx="5.5" dy="2" layer="1"/>
<text x="-7.6288" y="2.5334" size="1.27" layer="25">&gt;NAME</text>
<text x="-7.6288" y="-4.3114" size="1.27" layer="27">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="Q">
<wire x1="1.016" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.016" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.381" y1="1.524" x2="-0.381" y2="-1.524" width="0.254" layer="94"/>
<wire x1="-0.381" y1="-1.524" x2="0.381" y2="-1.524" width="0.254" layer="94"/>
<wire x1="0.381" y1="-1.524" x2="0.381" y2="1.524" width="0.254" layer="94"/>
<wire x1="0.381" y1="1.524" x2="-0.381" y2="1.524" width="0.254" layer="94"/>
<wire x1="1.016" y1="1.778" x2="1.016" y2="-1.778" width="0.254" layer="94"/>
<wire x1="-1.016" y1="1.778" x2="-1.016" y2="-1.778" width="0.254" layer="94"/>
<text x="2.54" y="1.016" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<text x="-2.159" y="-1.143" size="0.8636" layer="93">1</text>
<text x="1.524" y="-1.143" size="0.8636" layer="93">2</text>
<pin name="2" x="2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="CSM-7X-DU" prefix="Q">
<description>&lt;b&gt;SMD CRYSTAL&lt;/b&gt;&lt;p&gt;
Source: www.ecsxtal.com .. Crystal 3.6864MHz CSM_7X_DU.PDF</description>
<gates>
<gate name="P" symbol="Q" x="0" y="0"/>
</gates>
<devices>
<device name="" package="CSM-7X-DU">
<connects>
<connect gate="P" pin="1" pad="1"/>
<connect gate="P" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="frames">
<packages>
</packages>
<symbols>
<symbol name="A4L-LOC">
<wire x1="256.54" y1="3.81" x2="256.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="256.54" y1="8.89" x2="256.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="256.54" y1="13.97" x2="256.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="256.54" y1="19.05" x2="256.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="161.29" y1="3.81" x2="161.29" y2="24.13" width="0.1016" layer="94"/>
<wire x1="161.29" y1="24.13" x2="215.265" y2="24.13" width="0.1016" layer="94"/>
<wire x1="215.265" y1="24.13" x2="256.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="246.38" y1="3.81" x2="246.38" y2="8.89" width="0.1016" layer="94"/>
<wire x1="246.38" y1="8.89" x2="256.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="246.38" y1="8.89" x2="215.265" y2="8.89" width="0.1016" layer="94"/>
<wire x1="215.265" y1="8.89" x2="215.265" y2="3.81" width="0.1016" layer="94"/>
<wire x1="215.265" y1="8.89" x2="215.265" y2="13.97" width="0.1016" layer="94"/>
<wire x1="215.265" y1="13.97" x2="256.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="215.265" y1="13.97" x2="215.265" y2="19.05" width="0.1016" layer="94"/>
<wire x1="215.265" y1="19.05" x2="256.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="215.265" y1="19.05" x2="215.265" y2="24.13" width="0.1016" layer="94"/>
<text x="217.17" y="15.24" size="2.54" layer="94" font="vector">&gt;DRAWING_NAME</text>
<text x="217.17" y="10.16" size="2.286" layer="94" font="vector">&gt;LAST_DATE_TIME</text>
<text x="230.505" y="5.08" size="2.54" layer="94" font="vector">&gt;SHEET</text>
<text x="216.916" y="4.953" size="2.54" layer="94" font="vector">Sheet:</text>
<frame x1="0" y1="0" x2="260.35" y2="179.07" columns="6" rows="4" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="A4L-LOC" prefix="FRAME" uservalue="yes">
<description>&lt;b&gt;FRAME&lt;/b&gt;&lt;p&gt;
DIN A4, landscape with location and doc. field</description>
<gates>
<gate name="G$1" symbol="A4L-LOC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="R11" library="rcl" deviceset="R-EU_" device="R0402" value="100"/>
<part name="C66" library="rcl" deviceset="C-EU" device="C0402" value="0.1u"/>
<part name="C69" library="rcl" deviceset="C-EU" device="C0402" value="0.1u"/>
<part name="C74" library="rcl" deviceset="C-EU" device="C0402" value="0.1u"/>
<part name="C79" library="rcl" deviceset="C-EU" device="C0402" value="0.1u"/>
<part name="C75" library="rcl" deviceset="C-EU" device="C0402" value="0.1u"/>
<part name="SEN" library="jumper" deviceset="JP2E" device=""/>
<part name="R146" library="rcl" deviceset="R-EU_" device="R0402" value="5k"/>
<part name="R9" library="rcl" deviceset="R-EU_" device="R0402" value="100"/>
<part name="C9" library="rcl" deviceset="C-EU" device="C0402" value="22p"/>
<part name="R27" library="rcl" deviceset="R-EU_" device="R0402" value="274"/>
<part name="R25" library="rcl" deviceset="R-EU_" device="R0402" value="274"/>
<part name="R23" library="rcl" deviceset="R-EU_" device="R0402" value="274"/>
<part name="R22" library="rcl" deviceset="R-EU_" device="R0402" value="274"/>
<part name="R40" library="rcl" deviceset="R-EU_" device="R0402" value="6.8k"/>
<part name="C31" library="rcl" deviceset="C-EU" device="C0402" value="0.1u"/>
<part name="C32" library="rcl" deviceset="C-EU" device="C0402" value="0.1u"/>
<part name="ZED1" library="FMC-LPC" deviceset="FMC-LPC" device=""/>
<part name="R57" library="rcl" deviceset="R-EU_" device="R0402" value="1k"/>
<part name="R38" library="rcl" deviceset="R-EU_" device="R0402" value="1k"/>
<part name="C65" library="resistor" deviceset="C-EU" device="C0603K" value="10u"/>
<part name="C1" library="resistor" deviceset="C-EU" device="C0603K" value="10u"/>
<part name="R41" library="rcl" deviceset="R-EU_" device="R0402" value="200k"/>
<part name="R39" library="rcl" deviceset="R-EU_" device="R0402" value="200k"/>
<part name="R21" library="rcl" deviceset="R-EU_" device="R0402" value="274"/>
<part name="R10" library="rcl" deviceset="R-EU_" device="R0402" value="274"/>
<part name="R17" library="rcl" deviceset="R-EU_" device="R0402" value="274"/>
<part name="R16" library="rcl" deviceset="R-EU_" device="R0402" value="274"/>
<part name="C27" library="rcl" deviceset="C-EU" device="C0402" value="0.1u"/>
<part name="C28" library="rcl" deviceset="C-EU" device="C0402" value="0.1u"/>
<part name="R56" library="rcl" deviceset="R-EU_" device="R0402" value="1k"/>
<part name="R36" library="rcl" deviceset="R-EU_" device="R0402" value="1k"/>
<part name="R19" library="rcl" deviceset="R-EU_" device="R0402" value="27k"/>
<part name="R37" library="rcl" deviceset="R-EU_" device="R0402" value="27k"/>
<part name="$$U$6" library="ADS62P45" deviceset="ADS62P45" device=""/>
<part name="R145" library="rcl" deviceset="R-EU_" device="R0402" value="10k"/>
<part name="R147" library="rcl" deviceset="R-EU_" device="R0402" value="3k"/>
<part name="CTRL3" library="jumper" deviceset="JP2E" device=""/>
<part name="R134" library="rcl" deviceset="R-EU_" device="R0402" value="10k"/>
<part name="CTRL2" library="jumper" deviceset="JP2E" device=""/>
<part name="R135" library="rcl" deviceset="R-EU_" device="R0402" value="10k"/>
<part name="CTRL1" library="jumper" deviceset="JP2E" device=""/>
<part name="R144" library="rcl" deviceset="R-EU_" device="R0402" value="10k"/>
<part name="C21" library="rcl" deviceset="C-EU" device="C0402" value="0.1u"/>
<part name="C22" library="resistor" deviceset="C-EU" device="C0603K" value="10u"/>
<part name="U$2" library="TI" deviceset="LMH6552" device=""/>
<part name="C20" library="rcl" deviceset="C-EU" device="C0402" value="0.1u"/>
<part name="C23" library="resistor" deviceset="C-EU" device="C0603K" value="10u"/>
<part name="C24" library="rcl" deviceset="C-EU" device="C0402" value="0.01u"/>
<part name="C18" library="rcl" deviceset="C-EU" device="C0402" value="0.1u"/>
<part name="C15" library="rcl" deviceset="C-EU" device="C0402" value="0.1u"/>
<part name="C16" library="resistor" deviceset="C-EU" device="C0603K" value="10u"/>
<part name="U$1" library="TI" deviceset="LMH6552" device=""/>
<part name="C7" library="rcl" deviceset="C-EU" device="C0402" value="0.1u"/>
<part name="C8" library="resistor" deviceset="C-EU" device="C0603K" value="10u"/>
<part name="C17" library="rcl" deviceset="C-EU" device="C0402" value="0.01u"/>
<part name="C13" library="rcl" deviceset="C-EU" device="C0402" value="0.1u"/>
<part name="L4" library="rc-master-smd" deviceset="L_" device="PM1008" value="620n"/>
<part name="L1" library="rc-master-smd" deviceset="L_" device="PM1008" value="620n"/>
<part name="R14" library="rcl" deviceset="R-EU_" device="R0402" value="100"/>
<part name="R13" library="rcl" deviceset="R-EU_" device="R0402" value="100"/>
<part name="C2" library="rcl" deviceset="C-EU" device="C0402" value="22p"/>
<part name="L3" library="rc-master-smd" deviceset="L_" device="PM1008" value="620n"/>
<part name="L2" library="rc-master-smd" deviceset="L_" device="PM1008" value="620n"/>
<part name="U2" library="ADP7105" deviceset="ADP7105ARDZ" device="" value="ADP7105ARDZ-3.3"/>
<part name="C5" library="rcl" deviceset="C-EU" device="C0402" value="1u 10V"/>
<part name="C12" library="rcl" deviceset="C-EU" device="C0402" value="1u 10V"/>
<part name="R7" library="rcl" deviceset="R-EU_" device="R0402" value="100k"/>
<part name="C11" library="rcl" deviceset="C-EU" device="C0402" value="15n"/>
<part name="U13" library="ADP7105" deviceset="ADP7105ARDZ" device="" value="ADP7105ARDZ-3.3"/>
<part name="C68" library="rcl" deviceset="C-EU" device="C0402" value="1u 10V"/>
<part name="C70" library="rcl" deviceset="C-EU" device="C0402" value="1u 10V"/>
<part name="R140" library="rcl" deviceset="R-EU_" device="R0402" value="100k"/>
<part name="C61" library="rcl" deviceset="C-EU" device="C0402" value="15n"/>
<part name="U1" library="ADP7182" deviceset="ADP7182AUJZ-R7" device=""/>
<part name="C3" library="rcl" deviceset="C-EU" device="C0402" value="10u 10V"/>
<part name="C10" library="rcl" deviceset="C-EU" device="C0402" value="10u 10V"/>
<part name="U12" library="AD9573" deviceset="AD9573ARUZ" device=""/>
<part name="C62" library="rcl" deviceset="C-EU" device="C0402" value="1u 10V"/>
<part name="C53" library="rcl" deviceset="C-EU" device="C0402" value="0.1u"/>
<part name="C57" library="rcl" deviceset="C-EU" device="C0402" value="0.1u"/>
<part name="C52" library="rcl" deviceset="C-EU" device="C0402" value="0.1u"/>
<part name="C58" library="rcl" deviceset="C-EU" device="C0402" value="0.1u"/>
<part name="C56" library="rcl" deviceset="C-EU" device="C0402" value="0.1u"/>
<part name="R137" library="rcl" deviceset="R-EU_" device="R0402" value="200"/>
<part name="R138" library="rcl" deviceset="R-EU_" device="R0402" value="200"/>
<part name="C60" library="rcl" deviceset="C-EU" device="C0402" value="0.1u"/>
<part name="C59" library="rcl" deviceset="C-EU" device="C0402" value="0.1u"/>
<part name="C55" library="rcl" deviceset="C-EU" device="C0402" value="1n"/>
<part name="C54" library="rcl" deviceset="C-EU" device="C0402" value="33p"/>
<part name="C51" library="rcl" deviceset="C-EU" device="C0402" value="33p"/>
<part name="R2" library="rcl" deviceset="R-EU_" device="R0402" value="5"/>
<part name="R3" library="rcl" deviceset="R-EU_" device="R0402" value="5"/>
<part name="R1" library="rcl" deviceset="R-EU_" device="R0402" value="5"/>
<part name="R12" library="rcl" deviceset="R-EU_" device="R0402" value="5"/>
<part name="R18" library="rcl" deviceset="R-EU_" device="R0402" value="68.1"/>
<part name="R20" library="rcl" deviceset="R-EU_" device="R0402" value="68.1"/>
<part name="R26" library="rcl" deviceset="R-EU_" device="R0402" value="68.1"/>
<part name="R24" library="rcl" deviceset="R-EU_" device="R0402" value="68.1"/>
<part name="C6" library="rcl" deviceset="C-EU" device="C0402" value="0.1u"/>
<part name="C67" library="rcl" deviceset="C-EU" device="C0402" value="0.1u"/>
<part name="C4" library="rcl" deviceset="C-EU" device="C0402" value="0.1u"/>
<part name="C30" library="resistor" deviceset="C-EU" device="C0603K" value="10u"/>
<part name="C33" library="resistor" deviceset="C-EU" device="C0603K" value="10u"/>
<part name="C26" library="rcl" deviceset="C-EU" device="C0402" value="10u"/>
<part name="C29" library="rcl" deviceset="C-EU" device="C0402" value="10u"/>
<part name="R136" library="rcl" deviceset="R-EU_" device="R0402" value="10k"/>
<part name="R143" library="rcl" deviceset="R-EU_" device="R0402" value="10k"/>
<part name="R156" library="rcl" deviceset="R-EU_" device="R0402" value="10k"/>
<part name="R142" library="rcl" deviceset="R-EU_" device="R0402" value="0R"/>
<part name="AGND1" library="supply1" deviceset="AGND" device=""/>
<part name="SUPPLY2" library="supply2" deviceset="DGND" device=""/>
<part name="SUPPLY3" library="supply2" deviceset="DGND" device=""/>
<part name="SUPPLY5" library="supply2" deviceset="DGND" device=""/>
<part name="SUPPLY6" library="supply2" deviceset="DGND" device=""/>
<part name="SUPPLY7" library="supply2" deviceset="DGND" device=""/>
<part name="SUPPLY14" library="supply2" deviceset="DGND" device=""/>
<part name="SUPPLY27" library="supply2" deviceset="DGND" device=""/>
<part name="SUPPLY28" library="supply2" deviceset="DGND" device=""/>
<part name="SUPPLY29" library="supply2" deviceset="DGND" device=""/>
<part name="AGND2" library="supply1" deviceset="AGND" device=""/>
<part name="AGND3" library="supply1" deviceset="AGND" device=""/>
<part name="AGND4" library="supply1" deviceset="AGND" device=""/>
<part name="SUPPLY30" library="supply2" deviceset="DGND" device=""/>
<part name="SUPPLY31" library="supply2" deviceset="DGND" device=""/>
<part name="SUPPLY32" library="supply2" deviceset="DGND" device=""/>
<part name="SUPPLY33" library="supply2" deviceset="DGND" device=""/>
<part name="SUPPLY34" library="supply2" deviceset="DGND" device=""/>
<part name="SUPPLY35" library="supply2" deviceset="DGND" device=""/>
<part name="SUPPLY36" library="supply2" deviceset="DGND" device=""/>
<part name="SUPPLY37" library="supply2" deviceset="DGND" device=""/>
<part name="SUPPLY38" library="supply2" deviceset="DGND" device=""/>
<part name="SUPPLY39" library="supply2" deviceset="DGND" device=""/>
<part name="SUPPLY40" library="supply2" deviceset="DGND" device=""/>
<part name="SUPPLY41" library="supply2" deviceset="DGND" device=""/>
<part name="AGND5" library="supply1" deviceset="AGND" device=""/>
<part name="AGND6" library="supply1" deviceset="AGND" device=""/>
<part name="AGND7" library="supply1" deviceset="AGND" device=""/>
<part name="AGND8" library="supply1" deviceset="AGND" device=""/>
<part name="AGND9" library="supply1" deviceset="AGND" device=""/>
<part name="AGND10" library="supply1" deviceset="AGND" device=""/>
<part name="AGND11" library="supply1" deviceset="AGND" device=""/>
<part name="AGND12" library="supply1" deviceset="AGND" device=""/>
<part name="AGND13" library="supply1" deviceset="AGND" device=""/>
<part name="AGND14" library="supply1" deviceset="AGND" device=""/>
<part name="AGND15" library="supply1" deviceset="AGND" device=""/>
<part name="AGND16" library="supply1" deviceset="AGND" device=""/>
<part name="AGND17" library="supply1" deviceset="AGND" device=""/>
<part name="AGND18" library="supply1" deviceset="AGND" device=""/>
<part name="AGND19" library="supply1" deviceset="AGND" device=""/>
<part name="AGND20" library="supply1" deviceset="AGND" device=""/>
<part name="AGND22" library="supply1" deviceset="AGND" device=""/>
<part name="AGND23" library="supply1" deviceset="AGND" device=""/>
<part name="AGND24" library="supply1" deviceset="AGND" device=""/>
<part name="AGND25" library="supply1" deviceset="AGND" device=""/>
<part name="AGND26" library="supply1" deviceset="AGND" device=""/>
<part name="C76" library="rcl" deviceset="C-EU" device="C0402" value="0.1u"/>
<part name="SUPPLY1" library="supply2" deviceset="DGND" device=""/>
<part name="U$3" library="LT6231" deviceset="LT6231" device=""/>
<part name="$$U$4" library="LT6231" deviceset="LT6231" device=""/>
<part name="C14" library="resistor" deviceset="C-EU" device="C0603K" value="10u"/>
<part name="AGND37" library="supply1" deviceset="AGND" device=""/>
<part name="C19" library="resistor" deviceset="C-EU" device="C0603K" value="10u"/>
<part name="AGND38" library="supply1" deviceset="AGND" device=""/>
<part name="U3" library="ADI-ADG706" deviceset="ADG706BRUZ" device=""/>
<part name="U5" library="ADI-ADG706" deviceset="ADG706BRUZ" device=""/>
<part name="U7" library="ADI-ADG706" deviceset="ADG706BRUZ" device=""/>
<part name="R28" library="rcl" deviceset="R-EU_" device="R0402" value="715k"/>
<part name="R29" library="rcl" deviceset="R-EU_" device="R0402" value="274k"/>
<part name="R30" library="rcl" deviceset="R-EU_" device="R0402" value="147k"/>
<part name="R31" library="rcl" deviceset="R-EU_" device="R0402" value="82k5"/>
<part name="R32" library="rcl" deviceset="R-EU_" device="R0402" value="51k1"/>
<part name="R33" library="rcl" deviceset="R-EU_" device="R0402" value="31k6"/>
<part name="R34" library="rcl" deviceset="R-EU_" device="R0402" value="19k6"/>
<part name="R35" library="rcl" deviceset="R-EU_" device="R0402" value="12k7"/>
<part name="R45" library="rcl" deviceset="R-EU_" device="R0402" value="7k87"/>
<part name="R46" library="rcl" deviceset="R-EU_" device="R0402" value="5k11"/>
<part name="R47" library="rcl" deviceset="R-EU_" device="R0402" value="3k32"/>
<part name="R48" library="rcl" deviceset="R-EU_" device="R0402" value="2k15"/>
<part name="R49" library="rcl" deviceset="R-EU_" device="R0402" value="1k4"/>
<part name="R51" library="rcl" deviceset="R-EU_" device="R0402" value="909"/>
<part name="R52" library="rcl" deviceset="R-EU_" device="R0402" value="590"/>
<part name="R54" library="rcl" deviceset="R-EU_" device="R0402" value="383"/>
<part name="U8" library="AD8664" deviceset="AD8664ARZ" device=""/>
<part name="R59" library="rcl" deviceset="R-EU_" device="R0402" value="470"/>
<part name="R58" library="rcl" deviceset="R-EU_" device="R0402" value="470"/>
<part name="R131" library="rcl" deviceset="R-EU_" device="R0402" value="470"/>
<part name="R60" library="rcl" deviceset="R-EU_" device="R0402" value="470"/>
<part name="R130" library="rcl" deviceset="R-EU_" device="R0402" value="470"/>
<part name="R42" library="rcl" deviceset="R-EU_" device="R0402" value="470"/>
<part name="R43" library="rcl" deviceset="R-EU_" device="R0402" value="470"/>
<part name="R44" library="rcl" deviceset="R-EU_" device="R0402" value="470"/>
<part name="R55" library="rcl" deviceset="R-EU_" device="R0402" value="470"/>
<part name="R125" library="rcl" deviceset="R-EU_" device="R0402" value="470"/>
<part name="R53" library="rcl" deviceset="R-EU_" device="R0402" value="470"/>
<part name="R126" library="rcl" deviceset="R-EU_" device="R0402" value="470"/>
<part name="R50" library="rcl" deviceset="R-EU_" device="R0402" value="470"/>
<part name="R127" library="rcl" deviceset="R-EU_" device="R0402" value="470"/>
<part name="R128" library="rcl" deviceset="R-EU_" device="R0402" value="470"/>
<part name="R129" library="rcl" deviceset="R-EU_" device="R0402" value="470"/>
<part name="AGND42" library="supply1" deviceset="AGND" device=""/>
<part name="R15" library="rcl" deviceset="R-EU_" device="R0402" value="0R"/>
<part name="R8" library="rcl" deviceset="R-EU_" device="R0402" value="0R"/>
<part name="R139" library="rcl" deviceset="R-EU_" device="R0402" value="0R"/>
<part name="R132" library="rcl" deviceset="R-EU_" device="R0402" value="0R"/>
<part name="R6" library="rcl" deviceset="R-EU_" device="R0402" value="0R"/>
<part name="R4" library="rcl" deviceset="R-EU_" device="R0402" value="0R"/>
<part name="S2" library="MMCX" deviceset="MMCX" device="" technology="THT"/>
<part name="S1" library="MMCX" deviceset="MMCX" device="" technology="THT"/>
<part name="S4" library="MMCX" deviceset="MMCX" device="" technology="THT"/>
<part name="S3" library="MMCX" deviceset="MMCX" device="" technology="THT"/>
<part name="R72" library="rcl" deviceset="R-EU_" device="R0402" value="0"/>
<part name="R73" library="rcl" deviceset="R-EU_" device="R0402" value="0"/>
<part name="R82" library="rcl" deviceset="R-EU_" device="R0402" value="0"/>
<part name="R80" library="rcl" deviceset="R-EU_" device="R0402" value="0"/>
<part name="R71" library="rcl" deviceset="R-EU_" device="R0402" value="0"/>
<part name="R64" library="rcl" deviceset="R-EU_" device="R0402" value="0"/>
<part name="R83" library="rcl" deviceset="R-EU_" device="R0402" value="0"/>
<part name="R81" library="rcl" deviceset="R-EU_" device="R0402" value="0"/>
<part name="AGND21" library="supply1" deviceset="AGND" device=""/>
<part name="U9" library="AD8664" deviceset="AD8664ARZ" device=""/>
<part name="S6" library="MMCX" deviceset="MMCX" device="" technology="THT"/>
<part name="S5" library="MMCX" deviceset="MMCX" device="" technology="THT"/>
<part name="S8" library="MMCX" deviceset="MMCX" device="" technology="THT"/>
<part name="S7" library="MMCX" deviceset="MMCX" device="" technology="THT"/>
<part name="R74" library="rcl" deviceset="R-EU_" device="R0402" value="0"/>
<part name="R75" library="rcl" deviceset="R-EU_" device="R0402" value="0"/>
<part name="R86" library="rcl" deviceset="R-EU_" device="R0402" value="0"/>
<part name="R84" library="rcl" deviceset="R-EU_" device="R0402" value="0"/>
<part name="R104" library="rcl" deviceset="R-EU_" device="R0402" value="0"/>
<part name="R65" library="rcl" deviceset="R-EU_" device="R0402" value="0"/>
<part name="R87" library="rcl" deviceset="R-EU_" device="R0402" value="0"/>
<part name="R85" library="rcl" deviceset="R-EU_" device="R0402" value="0"/>
<part name="AGND43" library="supply1" deviceset="AGND" device=""/>
<part name="U10" library="AD8664" deviceset="AD8664ARZ" device=""/>
<part name="S10" library="MMCX" deviceset="MMCX" device="" technology="THT"/>
<part name="S9" library="MMCX" deviceset="MMCX" device="" technology="THT"/>
<part name="S12" library="MMCX" deviceset="MMCX" device="" technology="THT"/>
<part name="S11" library="MMCX" deviceset="MMCX" device="" technology="THT"/>
<part name="R76" library="rcl" deviceset="R-EU_" device="R0402" value="0"/>
<part name="R77" library="rcl" deviceset="R-EU_" device="R0402" value="0"/>
<part name="R90" library="rcl" deviceset="R-EU_" device="R0402" value="0"/>
<part name="R88" library="rcl" deviceset="R-EU_" device="R0402" value="0"/>
<part name="R109" library="rcl" deviceset="R-EU_" device="R0402" value="0"/>
<part name="R67" library="rcl" deviceset="R-EU_" device="R0402" value="0"/>
<part name="R91" library="rcl" deviceset="R-EU_" device="R0402" value="0"/>
<part name="R89" library="rcl" deviceset="R-EU_" device="R0402" value="0"/>
<part name="AGND44" library="supply1" deviceset="AGND" device=""/>
<part name="U11" library="AD8664" deviceset="AD8664ARZ" device=""/>
<part name="S14" library="MMCX" deviceset="MMCX" device="" technology="THT"/>
<part name="S13" library="MMCX" deviceset="MMCX" device="" technology="THT"/>
<part name="S16" library="MMCX" deviceset="MMCX" device="" technology="THT"/>
<part name="S15" library="MMCX" deviceset="MMCX" device="" technology="THT"/>
<part name="R78" library="rcl" deviceset="R-EU_" device="R0402" value="0"/>
<part name="R79" library="rcl" deviceset="R-EU_" device="R0402" value="0"/>
<part name="R94" library="rcl" deviceset="R-EU_" device="R0402" value="0"/>
<part name="R92" library="rcl" deviceset="R-EU_" device="R0402" value="0"/>
<part name="R107" library="rcl" deviceset="R-EU_" device="R0402" value="0"/>
<part name="R69" library="rcl" deviceset="R-EU_" device="R0402" value="0"/>
<part name="R95" library="rcl" deviceset="R-EU_" device="R0402" value="0"/>
<part name="R93" library="rcl" deviceset="R-EU_" device="R0402" value="0"/>
<part name="AGND45" library="supply1" deviceset="AGND" device=""/>
<part name="A-(S)" library="MMCX" deviceset="MMCX" device="" technology="THT"/>
<part name="A+(S)" library="MMCX" deviceset="MMCX" device="" technology="THT"/>
<part name="AGND41" library="supply1" deviceset="AGND" device=""/>
<part name="AGND46" library="supply1" deviceset="AGND" device=""/>
<part name="B+(E)" library="MMCX" deviceset="MMCX" device="" technology="THT"/>
<part name="AGND49" library="supply1" deviceset="AGND" device=""/>
<part name="B-(E)" library="MMCX" deviceset="MMCX" device="" technology="THT"/>
<part name="AGND50" library="supply1" deviceset="AGND" device=""/>
<part name="U6" library="ADI-ADG706" deviceset="ADG706BRUZ" device=""/>
<part name="U4" library="ADI-ADG706" deviceset="ADG706BRUZ" device=""/>
<part name="E4" library="MMCX" deviceset="MMCX" device="" technology="THT"/>
<part name="E8" library="MMCX" deviceset="MMCX" device="" technology="THT"/>
<part name="E3" library="MMCX" deviceset="MMCX" device="" technology="THT"/>
<part name="E7" library="MMCX" deviceset="MMCX" device="" technology="THT"/>
<part name="E2" library="MMCX" deviceset="MMCX" device="" technology="THT"/>
<part name="E6" library="MMCX" deviceset="MMCX" device="" technology="THT"/>
<part name="E1" library="MMCX" deviceset="MMCX" device="" technology="THT"/>
<part name="E5" library="MMCX" deviceset="MMCX" device="" technology="THT"/>
<part name="AGND51" library="supply1" deviceset="AGND" device=""/>
<part name="E16" library="MMCX" deviceset="MMCX" device="" technology="THT"/>
<part name="E12" library="MMCX" deviceset="MMCX" device="" technology="THT"/>
<part name="E15" library="MMCX" deviceset="MMCX" device="" technology="THT"/>
<part name="E11" library="MMCX" deviceset="MMCX" device="" technology="THT"/>
<part name="AGND52" library="supply1" deviceset="AGND" device=""/>
<part name="E14" library="MMCX" deviceset="MMCX" device="" technology="THT"/>
<part name="E10" library="MMCX" deviceset="MMCX" device="" technology="THT"/>
<part name="E13" library="MMCX" deviceset="MMCX" device="" technology="THT"/>
<part name="E9" library="MMCX" deviceset="MMCX" device="" technology="THT"/>
<part name="AGND53" library="supply1" deviceset="AGND" device=""/>
<part name="SIG1" library="MMCX" deviceset="MMCX" device="" technology="THT"/>
<part name="SIG2" library="MMCX" deviceset="MMCX" device="" technology="THT"/>
<part name="AGND54" library="supply1" deviceset="AGND" device=""/>
<part name="AGND55" library="supply1" deviceset="AGND" device=""/>
<part name="AGND47" library="supply1" deviceset="AGND" device=""/>
<part name="R63" library="rcl" deviceset="R-EU_" device="R0402" value="0"/>
<part name="R122" library="rcl" deviceset="R-EU_" device="R0402" value="0"/>
<part name="R123" library="rcl" deviceset="R-EU_" device="R0402" value="0"/>
<part name="R121" library="rcl" deviceset="R-EU_" device="R0402" value="0"/>
<part name="R124" library="rcl" deviceset="R-EU_" device="R0402" value="0"/>
<part name="R120" library="rcl" deviceset="R-EU_" device="R0402" value="0"/>
<part name="R62" library="rcl" deviceset="R-EU_" device="R0402" value="0"/>
<part name="R119" library="rcl" deviceset="R-EU_" device="R0402" value="0"/>
<part name="R68" library="rcl" deviceset="R-EU_" device="R0402" value="0"/>
<part name="R116" library="rcl" deviceset="R-EU_" device="R0402" value="0"/>
<part name="R70" library="rcl" deviceset="R-EU_" device="R0402" value="0"/>
<part name="R115" library="rcl" deviceset="R-EU_" device="R0402" value="0"/>
<part name="R114" library="rcl" deviceset="R-EU_" device="R0402" value="0"/>
<part name="R117" library="rcl" deviceset="R-EU_" device="R0402" value="0"/>
<part name="R113" library="rcl" deviceset="R-EU_" device="R0402" value="0"/>
<part name="R118" library="rcl" deviceset="R-EU_" device="R0402" value="0"/>
<part name="R97" library="rcl" deviceset="R-EU_" device="R0402" value="0"/>
<part name="R99" library="rcl" deviceset="R-EU_" device="R0402" value="0"/>
<part name="R96" library="rcl" deviceset="R-EU_" device="R0402" value="0"/>
<part name="R98" library="rcl" deviceset="R-EU_" device="R0402" value="0"/>
<part name="R112" library="rcl" deviceset="R-EU_" device="R0402" value="0"/>
<part name="R111" library="rcl" deviceset="R-EU_" device="R0402" value="0"/>
<part name="R61" library="rcl" deviceset="R-EU_" device="R0402" value="0"/>
<part name="R110" library="rcl" deviceset="R-EU_" device="R0402" value="0"/>
<part name="R103" library="rcl" deviceset="R-EU_" device="R0402" value="0"/>
<part name="R101" library="rcl" deviceset="R-EU_" device="R0402" value="0"/>
<part name="R102" library="rcl" deviceset="R-EU_" device="R0402" value="0"/>
<part name="R100" library="rcl" deviceset="R-EU_" device="R0402" value="0"/>
<part name="R106" library="rcl" deviceset="R-EU_" device="R0402" value="0"/>
<part name="R108" library="rcl" deviceset="R-EU_" device="R0402" value="0"/>
<part name="R105" library="rcl" deviceset="R-EU_" device="R0402" value="0"/>
<part name="R66" library="rcl" deviceset="R-EU_" device="R0402" value="0"/>
<part name="C50" library="resistor" deviceset="C-EU" device="C0603K" value="10u"/>
<part name="C49" library="resistor" deviceset="C-EU" device="C0603K" value="10u"/>
<part name="AGND39" library="supply1" deviceset="AGND" device=""/>
<part name="AGND40" library="supply1" deviceset="AGND" device=""/>
<part name="C48" library="resistor" deviceset="C-EU" device="C0603K" value="10u"/>
<part name="AGND48" library="supply1" deviceset="AGND" device=""/>
<part name="C47" library="resistor" deviceset="C-EU" device="C0603K" value="10u"/>
<part name="AGND56" library="supply1" deviceset="AGND" device=""/>
<part name="C46" library="resistor" deviceset="C-EU" device="C0603K" value="10u"/>
<part name="AGND57" library="supply1" deviceset="AGND" device=""/>
<part name="C45" library="resistor" deviceset="C-EU" device="C0603K" value="10u"/>
<part name="AGND58" library="supply1" deviceset="AGND" device=""/>
<part name="C44" library="resistor" deviceset="C-EU" device="C0603K" value="10u"/>
<part name="AGND59" library="supply1" deviceset="AGND" device=""/>
<part name="C43" library="resistor" deviceset="C-EU" device="C0603K" value="10u"/>
<part name="AGND60" library="supply1" deviceset="AGND" device=""/>
<part name="AGND61" library="supply1" deviceset="AGND" device=""/>
<part name="C34" library="rcl" deviceset="C-EU" device="C0402" value="0.1u"/>
<part name="AGND62" library="supply1" deviceset="AGND" device=""/>
<part name="C35" library="rcl" deviceset="C-EU" device="C0402" value="0.1u"/>
<part name="AGND63" library="supply1" deviceset="AGND" device=""/>
<part name="C39" library="rcl" deviceset="C-EU" device="C0402" value="0.1u"/>
<part name="AGND64" library="supply1" deviceset="AGND" device=""/>
<part name="C40" library="rcl" deviceset="C-EU" device="C0402" value="0.1u"/>
<part name="AGND65" library="supply1" deviceset="AGND" device=""/>
<part name="C25" library="rcl" deviceset="C-EU" device="C0402" value="0.1u"/>
<part name="AGND66" library="supply1" deviceset="AGND" device=""/>
<part name="C36" library="rcl" deviceset="C-EU" device="C0402" value="0.1u"/>
<part name="AGND67" library="supply1" deviceset="AGND" device=""/>
<part name="C42" library="rcl" deviceset="C-EU" device="C0402" value="0.1u"/>
<part name="AGND68" library="supply1" deviceset="AGND" device=""/>
<part name="C41" library="rcl" deviceset="C-EU" device="C0402" value="0.1u"/>
<part name="AGND69" library="supply1" deviceset="AGND" device=""/>
<part name="C38" library="rcl" deviceset="C-EU" device="C0402" value="0.1u"/>
<part name="AGND70" library="supply1" deviceset="AGND" device=""/>
<part name="C37" library="rcl" deviceset="C-EU" device="C0402" value="0.1u"/>
<part name="Q1" library="crystal" deviceset="CSM-7X-DU" device="" value="25MHz"/>
<part name="R157" library="rcl" deviceset="R-EU_" device="R0402" value="470"/>
<part name="R158" library="rcl" deviceset="R-EU_" device="R0402" value="470"/>
<part name="AGND71" library="supply1" deviceset="AGND" device=""/>
<part name="AGND72" library="supply1" deviceset="AGND" device=""/>
<part name="JP2" library="jumper" deviceset="JP2E" device=""/>
<part name="SUPPLY4" library="supply2" deviceset="DGND" device=""/>
<part name="FRAME1" library="frames" deviceset="A4L-LOC" device=""/>
<part name="FRAME3" library="frames" deviceset="A4L-LOC" device=""/>
<part name="U14" library="ADP7105" deviceset="ADP7105ARDZ" device="" value="ADP7105ARDZ-3.3"/>
<part name="C63" library="rcl" deviceset="C-EU" device="C0402" value="1u 10V"/>
<part name="C64" library="rcl" deviceset="C-EU" device="C0402" value="1u 10V"/>
<part name="R5" library="rcl" deviceset="R-EU_" device="R0402" value="100k"/>
<part name="C71" library="rcl" deviceset="C-EU" device="C0402" value="15n"/>
<part name="U15" library="ADP7182" deviceset="ADP7182AUJZ-R7" device=""/>
<part name="C72" library="rcl" deviceset="C-EU" device="C0402" value="10u 10V"/>
<part name="C73" library="rcl" deviceset="C-EU" device="C0402" value="10u 10V"/>
<part name="C77" library="rcl" deviceset="C-EU" device="C0402" value="0.1u"/>
<part name="C78" library="rcl" deviceset="C-EU" device="C0402" value="0.1u"/>
<part name="SUPPLY8" library="supply2" deviceset="DGND" device=""/>
<part name="SUPPLY9" library="supply2" deviceset="DGND" device=""/>
<part name="SUPPLY10" library="supply2" deviceset="DGND" device=""/>
<part name="SUPPLY11" library="supply2" deviceset="DGND" device=""/>
<part name="SUPPLY12" library="supply2" deviceset="DGND" device=""/>
<part name="AGND27" library="supply1" deviceset="AGND" device=""/>
<part name="AGND28" library="supply1" deviceset="AGND" device=""/>
<part name="AGND29" library="supply1" deviceset="AGND" device=""/>
<part name="AGND30" library="supply1" deviceset="AGND" device=""/>
<part name="R133" library="rcl" deviceset="R-EU_" device="R0402" value="0R"/>
<part name="R141" library="rcl" deviceset="R-EU_" device="R0402" value="0R"/>
<part name="R148" library="rcl" deviceset="R-EU_" device="R0402" value="0R"/>
<part name="R149" library="rcl" deviceset="R-EU_" device="R0402" value="0R"/>
<part name="FRAME4" library="frames" deviceset="A4L-LOC" device=""/>
<part name="FRAME5" library="frames" deviceset="A4L-LOC" device=""/>
<part name="FRAME6" library="frames" deviceset="A4L-LOC" device=""/>
<part name="FRAME7" library="frames" deviceset="A4L-LOC" device=""/>
<part name="FRAME2" library="frames" deviceset="A4L-LOC" device=""/>
<part name="FRAME8" library="frames" deviceset="A4L-LOC" device=""/>
<part name="FRAME9" library="frames" deviceset="A4L-LOC" device=""/>
<part name="FRAME10" library="frames" deviceset="A4L-LOC" device=""/>
</parts>
<sheets>
<sheet>
<plain>
<text x="-546.1" y="-699.77" size="1.778" layer="97" rot="R90">2s complement LVDS</text>
<text x="-548.64" y="-661.67" size="1.778" layer="97" rot="R90">straight binary LVDS</text>
<text x="-229.87" y="-692.15" size="1.27" layer="97">parallel interface conditon: RST=high</text>
<text x="-396.24" y="-672.465" size="1.778" layer="97">OPV SUPPLY</text>
<text x="-187.325" y="-480.695" size="1.778" layer="97">Verstärkung V=60</text>
<text x="-229.87" y="-694.69" size="1.27" layer="97">SCLK=low =&gt; 0dB gain and Internal reference</text>
<text x="-229.87" y="-697.23" size="1.27" layer="97">SEN=low =&gt; 2s complement format and parallel LVDS output</text>
<text x="-439.42" y="-675.005" size="1.778" layer="97">OPV SUPPLY</text>
<text x="-353.06" y="-596.265" size="1.778" layer="97">Betriebsspannungen:</text>
<text x="-353.06" y="-601.345" size="1.778" layer="97">Vcc = 12 V</text>
<text x="-353.06" y="-612.775" size="1.778" layer="97">AVDD = +  3,3 V</text>
<text x="-353.06" y="-616.585" size="1.778" layer="97">AVDD_M = -3 V</text>
<text x="-533.4" y="-670.56" size="1.778" layer="97">SEN</text>
<text x="-201.93" y="-669.29" size="1.778" layer="97">CTRL1</text>
<text x="-201.93" y="-671.83" size="1.778" layer="97">CTRL2</text>
<text x="-201.93" y="-674.37" size="1.778" layer="97">CTRL3</text>
<text x="-512.445" y="-680.085" size="1.778" layer="97">CTRL1</text>
<text x="-492.125" y="-680.085" size="1.778" layer="97">CTRL2</text>
<text x="-470.535" y="-680.085" size="1.778" layer="97">CTRL3</text>
<text x="-201.93" y="-664.21" size="1.778" layer="97">CLKOUTM</text>
<text x="-201.93" y="-636.27" size="1.778" layer="95">DB4P</text>
<text x="-121.285" y="-601.345" size="1.778" layer="95">DA0M</text>
<text x="-121.285" y="-598.805" size="1.778" layer="95">DA0P</text>
<text x="-106.045" y="-606.425" size="1.778" layer="95">DA2M</text>
<text x="-106.045" y="-603.885" size="1.778" layer="95">DA2P</text>
<text x="-172.085" y="-603.885" size="1.778" layer="95">DA4M</text>
<text x="-172.085" y="-601.345" size="1.778" layer="95">DA4P</text>
<text x="-121.285" y="-608.965" size="1.778" layer="95">DA6M</text>
<text x="-121.285" y="-606.425" size="1.778" layer="95">DA6P</text>
<text x="-172.085" y="-611.505" size="1.778" layer="95">DA8M</text>
<text x="-172.085" y="-608.965" size="1.778" layer="95">DA8P</text>
<text x="-106.045" y="-614.045" size="1.778" layer="95">DA10M</text>
<text x="-106.045" y="-611.505" size="1.778" layer="95">DA10P</text>
<text x="-50.165" y="-611.505" size="1.778" layer="95">DA12M</text>
<text x="-50.165" y="-608.965" size="1.778" layer="95">DA12P</text>
<text x="-172.085" y="-619.125" size="1.778" layer="95">DB0M</text>
<text x="-172.085" y="-616.585" size="1.778" layer="95">DB0P</text>
<text x="-121.285" y="-616.585" size="1.778" layer="95">DB2M</text>
<text x="-121.285" y="-614.045" size="1.778" layer="95">DB2P</text>
<text x="-106.045" y="-621.665" size="1.778" layer="95">DB4M</text>
<text x="-106.045" y="-619.125" size="1.778" layer="95">DB4P</text>
<text x="-50.165" y="-621.665" size="1.778" layer="95">DB6M</text>
<text x="-50.165" y="-619.125" size="1.778" layer="95">DB6P</text>
<text x="-172.085" y="-626.745" size="1.778" layer="95">DB8M </text>
<text x="-172.085" y="-624.205" size="1.778" layer="95">DB8P</text>
<text x="-121.285" y="-624.205" size="1.778" layer="95">DB10M</text>
<text x="-121.285" y="-621.665" size="1.778" layer="95">DB10P</text>
<text x="-106.045" y="-629.285" size="1.778" layer="95">DB12M</text>
<text x="-106.045" y="-626.745" size="1.778" layer="95">DB12P</text>
<text x="-174.625" y="-596.265" size="1.778" layer="95">CLKOUTM</text>
<text x="-174.625" y="-593.725" size="1.778" layer="95">CLKOUTP</text>
<text x="102.87" y="-215.9" size="1.778" layer="95">VCM</text>
<text x="-172.085" y="-424.18" size="1.778" layer="95">VCM</text>
<text x="-93.345" y="-414.02" size="1.778" layer="95">SIG_A_M</text>
<text x="-93.345" y="-433.705" size="1.778" layer="95">SIG_A_P</text>
<text x="177.165" y="-207.01" size="1.778" layer="95">SIG_B_M</text>
<text x="177.165" y="-222.885" size="1.778" layer="95">SIG_B_P</text>
<text x="-551.18" y="-591.82" size="1.778" layer="95">V_42_P</text>
<text x="-552.45" y="-622.3" size="1.778" layer="95">V_42_M</text>
<text x="-542.925" y="-593.09" size="1.27" layer="95">V_42_P</text>
<text x="-351.79" y="-570.865" size="1.27" layer="95">AVDD</text>
<text x="-471.17" y="-581.025" size="1.27" layer="95">AVDD</text>
<text x="-471.17" y="-586.105" size="1.27" layer="95">AVDD</text>
<text x="-447.675" y="-563.88" size="2.54" layer="95">Analoge Betriebsspannung + 3,3 V</text>
<text x="5.715" y="-680.72" size="1.27" layer="95">V_42_P</text>
<text x="146.685" y="-681.355" size="1.27" layer="95">DRVDD</text>
<text x="48.26" y="-691.515" size="1.27" layer="95">DRVDD</text>
<text x="48.26" y="-696.595" size="1.27" layer="95">DRVDD</text>
<text x="69.215" y="-674.37" size="2.54" layer="95">Digitale Betriebsspannung + 3,3 V</text>
<text x="3.175" y="-574.04" size="1.27" layer="95">V_42_M</text>
<text x="46.355" y="-579.12" size="1.27" layer="95">AVDD_M</text>
<text x="64.135" y="-568.325" size="2.54" layer="95">Negative Betriebsspannung - 3 V</text>
<text x="-353.06" y="-605.155" size="1.778" layer="97">V_42_P = + 4,2 V</text>
<text x="-353.06" y="-608.965" size="1.778" layer="97">V_42_M = - 4,2 V</text>
<text x="-353.06" y="-620.395" size="1.778" layer="97">DRVDD = + 3.3 V</text>
<text x="-266.7" y="-577.85" size="1.778" layer="95" rot="R90">AVDD</text>
<text x="-276.86" y="-618.49" size="1.778" layer="95" rot="R90">DRVDD</text>
<text x="-542.29" y="-647.7" size="1.778" layer="95" rot="R90">DRVDD</text>
<text x="-113.03" y="-386.08" size="1.778" layer="95" rot="R90">AVDD</text>
<text x="-109.855" y="-452.755" size="1.778" layer="95" rot="R90">AVDD_M</text>
<text x="158.115" y="-175.895" size="1.778" layer="95" rot="R90">AVDD</text>
<text x="161.925" y="-247.015" size="1.778" layer="95" rot="R90">AVDD_M</text>
<text x="-514.35" y="-657.86" size="1.778" layer="95" rot="R90">DRVDD</text>
<text x="-441.96" y="-674.37" size="1.778" layer="95" rot="R90">AVDD</text>
<text x="-442.595" y="-704.85" size="1.778" layer="95" rot="R90">AVDD_M</text>
<text x="-398.78" y="-672.465" size="1.778" layer="95" rot="R90">AVDD</text>
<text x="-399.415" y="-706.755" size="1.778" layer="95" rot="R90">AVDD_M</text>
<text x="-433.07" y="-615.95" size="2.54" layer="95">Diffentielle Takterzeugung 100 MHz</text>
<text x="-501.65" y="-622.3" size="1.778" layer="95">DRVDD</text>
<text x="-334.01" y="-647.7" size="1.778" layer="95">CLKINP</text>
<text x="-334.01" y="-662.305" size="1.778" layer="95">CLKINM</text>
<text x="-252.095" y="-650.24" size="1.778" layer="95">CLKINP</text>
<text x="-252.095" y="-652.78" size="1.778" layer="95">CLKINM</text>
<text x="-474.98" y="-635" size="1.778" layer="95">Anm.: 1nF muss an VDDA</text>
<text x="-565.15" y="-650.24" size="1.778" layer="95" rot="R90">DRVDD</text>
<text x="240.665" y="-207.01" size="1.778" layer="95">INM_B</text>
<text x="240.665" y="-218.44" size="1.778" layer="95">INP_B</text>
<text x="-46.99" y="-416.56" size="1.778" layer="95">INM_A</text>
<text x="-46.99" y="-427.99" size="1.778" layer="95">INP_A</text>
<text x="-172.72" y="-574.04" size="2.54" layer="95">Pinbelegung vom FPGA ist nicht variabel. P und N Belegung muss beachtet werden.</text>
<text x="-182.88" y="-448.31" size="1.778" layer="100" rot="R90">Werte gemäß Excel-Tabelle wählen</text>
<text x="-434.34" y="-558.8" size="1.778" layer="95">ADP7105ARDZ-3.3</text>
<text x="107.315" y="-619.76" size="1.27" layer="95">V_42_P</text>
<text x="240.665" y="-620.395" size="1.27" layer="95">DRVDD</text>
<text x="144.78" y="-630.555" size="1.27" layer="95">DRVDD</text>
<text x="144.78" y="-635.635" size="1.27" layer="95">DRVDD</text>
<text x="165.735" y="-613.41" size="2.54" layer="95">Digitale Betriebsspannung + 3,3 V</text>
<text x="127.635" y="-574.04" size="1.27" layer="95">V_42_M</text>
<text x="168.275" y="-579.12" size="1.27" layer="95">AVDD_M</text>
<text x="186.055" y="-568.325" size="2.54" layer="95">Negative Betriebsspannung - 3 V</text>
</plain>
<instances>
<instance part="R11" gate="G$1" x="-83.82" y="-417.83" rot="R180"/>
<instance part="C66" gate="G$1" x="-247.65" y="-584.2" rot="MR0"/>
<instance part="C69" gate="G$1" x="-255.27" y="-584.2" rot="MR0"/>
<instance part="C74" gate="G$1" x="-247.65" y="-623.57" rot="MR0"/>
<instance part="C79" gate="G$1" x="-255.27" y="-623.57" rot="MR0"/>
<instance part="C75" gate="G$1" x="-264.16" y="-623.57" rot="MR0"/>
<instance part="SEN" gate="1" x="-546.1" y="-668.02" rot="R90"/>
<instance part="R146" gate="G$1" x="-542.29" y="-657.86" rot="R90"/>
<instance part="R9" gate="G$1" x="-83.82" y="-429.26" rot="R180"/>
<instance part="C9" gate="G$1" x="-64.77" y="-422.275"/>
<instance part="R27" gate="G$1" x="120.65" y="-165.1" rot="R180"/>
<instance part="R25" gate="G$1" x="135.89" y="-165.1" rot="R180"/>
<instance part="R23" gate="G$1" x="121.92" y="-251.46" rot="R180"/>
<instance part="R22" gate="G$1" x="135.89" y="-251.46" rot="R180"/>
<instance part="R40" gate="G$1" x="88.265" y="-216.535" rot="R270"/>
<instance part="C31" gate="G$1" x="-393.7" y="-678.815"/>
<instance part="C32" gate="G$1" x="-393.7" y="-688.975"/>
<instance part="ZED1" gate="-A" x="-75.565" y="-636.905" rot="MR0"/>
<instance part="ZED1" gate="-B" x="-141.605" y="-636.905" rot="MR0"/>
<instance part="R57" gate="G$1" x="80.01" y="-162.56" rot="R180"/>
<instance part="R38" gate="G$1" x="80.01" y="-254" smashed="yes" rot="R180">
<attribute name="NAME" x="83.82" y="-255.4986" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="83.82" y="-250.698" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="C65" gate="G$1" x="-266.7" y="-584.2" rot="MR0"/>
<instance part="C1" gate="G$1" x="-276.86" y="-623.57" rot="MR0"/>
<instance part="R41" gate="G$1" x="97.155" y="-210.185" rot="R180"/>
<instance part="R39" gate="G$1" x="97.155" y="-222.885" rot="R180"/>
<instance part="R21" gate="G$1" x="-148.59" y="-378.46" rot="R180"/>
<instance part="R10" gate="G$1" x="-133.35" y="-378.46" rot="R180"/>
<instance part="R17" gate="G$1" x="-149.86" y="-461.01" rot="R180"/>
<instance part="R16" gate="G$1" x="-134.62" y="-461.01" rot="R180"/>
<instance part="C27" gate="G$1" x="-436.88" y="-678.815"/>
<instance part="C28" gate="G$1" x="-436.88" y="-688.975"/>
<instance part="R56" gate="G$1" x="-198.12" y="-375.92" rot="R180"/>
<instance part="R36" gate="G$1" x="-195.58" y="-463.55" smashed="yes" rot="R180">
<attribute name="NAME" x="-191.77" y="-465.0486" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="-191.77" y="-460.248" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R19" gate="G$1" x="-179.07" y="-407.67" rot="R180"/>
<instance part="R37" gate="G$1" x="-179.07" y="-450.85" rot="R180"/>
<instance part="$$U$6" gate="A" x="-208.28" y="-612.14"/>
<instance part="R145" gate="G$1" x="-563.88" y="-656.59" rot="R90"/>
<instance part="R147" gate="G$1" x="-532.13" y="-657.86" rot="R90"/>
<instance part="CTRL3" gate="1" x="-518.795" y="-680.085" rot="R90"/>
<instance part="R134" gate="G$1" x="-513.715" y="-668.655" rot="R90"/>
<instance part="CTRL2" gate="1" x="-498.475" y="-680.085" rot="R90"/>
<instance part="R135" gate="G$1" x="-493.395" y="-668.655" rot="R90"/>
<instance part="CTRL1" gate="1" x="-478.155" y="-680.085" rot="R90"/>
<instance part="R144" gate="G$1" x="-473.075" y="-668.655" rot="R90"/>
<instance part="C21" gate="G$1" x="153.67" y="-185.42"/>
<instance part="C22" gate="G$1" x="163.83" y="-185.42"/>
<instance part="U$2" gate="G$1" x="156.21" y="-213.36"/>
<instance part="C20" gate="G$1" x="139.7" y="-238.76"/>
<instance part="C23" gate="G$1" x="149.86" y="-238.76"/>
<instance part="C24" gate="G$1" x="170.18" y="-212.725"/>
<instance part="C18" gate="G$1" x="121.285" y="-220.345"/>
<instance part="C15" gate="G$1" x="-118.11" y="-392.43"/>
<instance part="C16" gate="G$1" x="-107.95" y="-392.43"/>
<instance part="U$1" gate="G$1" x="-115.57" y="-422.91"/>
<instance part="C7" gate="G$1" x="-130.81" y="-447.04"/>
<instance part="C8" gate="G$1" x="-120.65" y="-447.04"/>
<instance part="C17" gate="G$1" x="-100.33" y="-421.64"/>
<instance part="C13" gate="G$1" x="-149.225" y="-429.26"/>
<instance part="L4" gate="G$1" x="-72.39" y="-417.83"/>
<instance part="L1" gate="G$1" x="-72.39" y="-429.26"/>
<instance part="R14" gate="G$1" x="193.675" y="-208.28" rot="R180"/>
<instance part="R13" gate="G$1" x="193.675" y="-219.71" rot="R180"/>
<instance part="C2" gate="G$1" x="212.725" y="-212.725"/>
<instance part="L3" gate="G$1" x="205.105" y="-208.28"/>
<instance part="L2" gate="G$1" x="205.105" y="-219.71"/>
<instance part="U2" gate="A" x="-451.485" y="-570.865"/>
<instance part="C5" gate="G$1" x="-492.125" y="-580.39"/>
<instance part="C12" gate="G$1" x="-372.745" y="-576.58"/>
<instance part="R7" gate="G$1" x="-460.375" y="-581.025" rot="R180"/>
<instance part="C11" gate="G$1" x="-457.835" y="-596.9"/>
<instance part="U13" gate="A" x="67.31" y="-681.355"/>
<instance part="C68" gate="G$1" x="29.845" y="-687.705"/>
<instance part="C70" gate="G$1" x="142.875" y="-686.435"/>
<instance part="R140" gate="G$1" x="59.055" y="-691.515" rot="R180"/>
<instance part="C61" gate="G$1" x="64.135" y="-707.39"/>
<instance part="U1" gate="A" x="69.85" y="-574.675"/>
<instance part="C3" gate="G$1" x="22.86" y="-582.295"/>
<instance part="C10" gate="G$1" x="63.5" y="-585.47"/>
<instance part="U12" gate="A" x="-439.42" y="-622.3"/>
<instance part="C62" gate="G$1" x="-491.49" y="-628.65" rot="R180"/>
<instance part="C53" gate="G$1" x="-483.87" y="-626.11" rot="MR0"/>
<instance part="C57" gate="G$1" x="-476.885" y="-626.11" rot="MR0"/>
<instance part="C52" gate="G$1" x="-469.9" y="-626.11" rot="MR0"/>
<instance part="C58" gate="G$1" x="-462.28" y="-626.11" rot="MR0"/>
<instance part="C56" gate="G$1" x="-454.66" y="-626.11" rot="MR0"/>
<instance part="R137" gate="G$1" x="-347.98" y="-654.05" rot="R270"/>
<instance part="R138" gate="G$1" x="-339.725" y="-654.05" rot="R270"/>
<instance part="C60" gate="G$1" x="-355.6" y="-647.7" rot="R90"/>
<instance part="C59" gate="G$1" x="-355.6" y="-660.4" rot="R90"/>
<instance part="C55" gate="G$1" x="-447.04" y="-626.11" rot="MR0"/>
<instance part="C54" gate="G$1" x="-462.28" y="-645.16" rot="MR270"/>
<instance part="C51" gate="G$1" x="-462.28" y="-652.78" rot="MR270"/>
<instance part="R2" gate="G$1" x="229.235" y="-208.28" rot="R180"/>
<instance part="R3" gate="G$1" x="229.235" y="-219.71" rot="R180"/>
<instance part="R1" gate="G$1" x="-55.88" y="-417.83" rot="R180"/>
<instance part="R12" gate="G$1" x="-55.88" y="-429.26" rot="R180"/>
<instance part="R18" gate="G$1" x="-162.56" y="-453.39" rot="R90"/>
<instance part="R20" gate="G$1" x="-161.29" y="-386.08" rot="R270"/>
<instance part="R26" gate="G$1" x="113.03" y="-172.72" rot="R270"/>
<instance part="R24" gate="G$1" x="113.03" y="-241.935" rot="R270"/>
<instance part="C6" gate="G$1" x="-478.79" y="-580.39"/>
<instance part="C67" gate="G$1" x="41.275" y="-687.705"/>
<instance part="C4" gate="G$1" x="35.56" y="-582.295"/>
<instance part="C30" gate="G$1" x="-383.54" y="-678.815"/>
<instance part="C33" gate="G$1" x="-383.54" y="-688.975"/>
<instance part="C26" gate="G$1" x="-424.18" y="-678.815"/>
<instance part="C29" gate="G$1" x="-424.18" y="-688.975"/>
<instance part="R136" gate="G$1" x="-513.715" y="-693.42" rot="R90"/>
<instance part="R143" gate="G$1" x="-493.395" y="-694.055" rot="R90"/>
<instance part="R156" gate="G$1" x="-473.075" y="-694.055" rot="R90"/>
<instance part="R142" gate="G$1" x="-560.07" y="-707.39"/>
<instance part="AGND1" gate="VR1" x="-372.745" y="-586.74"/>
<instance part="SUPPLY2" gate="G$1" x="29.845" y="-701.675"/>
<instance part="SUPPLY3" gate="G$1" x="41.275" y="-701.675"/>
<instance part="SUPPLY5" gate="G$1" x="64.135" y="-716.915"/>
<instance part="SUPPLY6" gate="G$1" x="131.445" y="-715.645"/>
<instance part="SUPPLY7" gate="G$1" x="142.875" y="-696.595"/>
<instance part="SUPPLY14" gate="G$1" x="-568.325" y="-711.835"/>
<instance part="SUPPLY27" gate="G$1" x="-491.49" y="-637.54"/>
<instance part="SUPPLY28" gate="G$1" x="-473.71" y="-648.97" rot="R270"/>
<instance part="SUPPLY29" gate="G$1" x="-365.125" y="-637.54" rot="R90"/>
<instance part="AGND2" gate="VR1" x="-266.7" y="-593.09"/>
<instance part="AGND3" gate="VR1" x="-251.46" y="-593.09"/>
<instance part="AGND4" gate="VR1" x="-243.84" y="-605.79" rot="R270"/>
<instance part="SUPPLY30" gate="G$1" x="-276.86" y="-631.825"/>
<instance part="SUPPLY31" gate="G$1" x="-255.27" y="-632.46"/>
<instance part="SUPPLY32" gate="G$1" x="-242.57" y="-640.715"/>
<instance part="SUPPLY33" gate="G$1" x="-251.46" y="-681.99"/>
<instance part="SUPPLY34" gate="G$1" x="-182.245" y="-595.63"/>
<instance part="SUPPLY35" gate="G$1" x="-112.395" y="-683.895"/>
<instance part="SUPPLY36" gate="G$1" x="-102.235" y="-683.895"/>
<instance part="SUPPLY37" gate="G$1" x="-50.165" y="-683.895"/>
<instance part="SUPPLY38" gate="G$1" x="-542.29" y="-683.26"/>
<instance part="SUPPLY39" gate="G$1" x="-513.715" y="-708.025"/>
<instance part="SUPPLY40" gate="G$1" x="-493.395" y="-708.66"/>
<instance part="SUPPLY41" gate="G$1" x="-473.075" y="-709.295"/>
<instance part="AGND5" gate="VR1" x="-413.385" y="-685.165" rot="R90"/>
<instance part="AGND6" gate="VR1" x="-372.745" y="-685.165" rot="R90"/>
<instance part="AGND7" gate="VR1" x="113.03" y="-185.42"/>
<instance part="AGND8" gate="VR1" x="113.03" y="-230.505" rot="R180"/>
<instance part="AGND9" gate="VR1" x="121.285" y="-236.22"/>
<instance part="AGND10" gate="VR1" x="144.78" y="-247.015"/>
<instance part="AGND11" gate="VR1" x="158.75" y="-195.58"/>
<instance part="AGND12" gate="VR1" x="-161.29" y="-396.24"/>
<instance part="AGND13" gate="VR1" x="-162.56" y="-443.23" rot="R180"/>
<instance part="AGND14" gate="VR1" x="-149.225" y="-439.42"/>
<instance part="AGND15" gate="VR1" x="-125.73" y="-455.93"/>
<instance part="AGND16" gate="VR1" x="-113.03" y="-401.32"/>
<instance part="AGND17" gate="VR1" x="63.5" y="-598.17"/>
<instance part="AGND18" gate="VR1" x="-551.18" y="-711.835"/>
<instance part="AGND19" gate="VR1" x="-385.445" y="-605.155"/>
<instance part="AGND20" gate="VR1" x="-457.835" y="-605.79"/>
<instance part="AGND22" gate="VR1" x="-478.79" y="-593.725"/>
<instance part="AGND23" gate="VR1" x="-492.125" y="-593.725"/>
<instance part="AGND24" gate="VR1" x="22.86" y="-591.82"/>
<instance part="AGND25" gate="VR1" x="35.56" y="-592.455"/>
<instance part="AGND26" gate="VR1" x="120.65" y="-578.485"/>
<instance part="C76" gate="G$1" x="-240.03" y="-623.57" rot="MR0"/>
<instance part="SUPPLY1" gate="G$1" x="-532.13" y="-647.7" rot="R180"/>
<instance part="U$3" gate="A" x="-179.07" y="-378.46"/>
<instance part="U$3" gate="C" x="-447.04" y="-684.53"/>
<instance part="U$3" gate="B" x="-179.07" y="-461.01"/>
<instance part="$$U$4" gate="A" x="95.25" y="-165.1"/>
<instance part="$$U$4" gate="C" x="-404.495" y="-686.435"/>
<instance part="$$U$4" gate="B" x="95.885" y="-251.46"/>
<instance part="C14" gate="G$1" x="-164.465" y="-429.26"/>
<instance part="AGND37" gate="VR1" x="-164.465" y="-439.42"/>
<instance part="C19" gate="G$1" x="109.22" y="-219.71"/>
<instance part="AGND38" gate="VR1" x="109.22" y="-236.22"/>
<instance part="U3" gate="A" x="-245.11" y="-425.45"/>
<instance part="U5" gate="A" x="176.53" y="-383.54" rot="MR0"/>
<instance part="U7" gate="A" x="176.53" y="-458.47" rot="MR0"/>
<instance part="R28" gate="G$1" x="-215.9" y="-412.75"/>
<instance part="R29" gate="G$1" x="-205.74" y="-415.29"/>
<instance part="R30" gate="G$1" x="-195.58" y="-417.83"/>
<instance part="R31" gate="G$1" x="-215.9" y="-420.37"/>
<instance part="R32" gate="G$1" x="-205.74" y="-422.91"/>
<instance part="R33" gate="G$1" x="-195.58" y="-425.45"/>
<instance part="R34" gate="G$1" x="-215.9" y="-427.99"/>
<instance part="R35" gate="G$1" x="-205.74" y="-430.53"/>
<instance part="R45" gate="G$1" x="-195.58" y="-433.07"/>
<instance part="R46" gate="G$1" x="-215.9" y="-435.61"/>
<instance part="R47" gate="G$1" x="-205.74" y="-438.15"/>
<instance part="R48" gate="G$1" x="-195.58" y="-440.69"/>
<instance part="R49" gate="G$1" x="-215.9" y="-443.23"/>
<instance part="R51" gate="G$1" x="-205.74" y="-445.77"/>
<instance part="R52" gate="G$1" x="-195.58" y="-448.31"/>
<instance part="R54" gate="G$1" x="-215.9" y="-450.85"/>
<instance part="U8" gate="A" x="-116.84" y="-171.45"/>
<instance part="R59" gate="G$1" x="30.48" y="-416.56" rot="R90"/>
<instance part="R58" gate="G$1" x="35.56" y="-424.18" rot="R90"/>
<instance part="R131" gate="G$1" x="40.64" y="-416.56" rot="R90"/>
<instance part="R60" gate="G$1" x="45.72" y="-424.18" rot="R90"/>
<instance part="R130" gate="G$1" x="50.8" y="-416.56" rot="R90"/>
<instance part="R42" gate="G$1" x="55.88" y="-424.18" rot="R90"/>
<instance part="R43" gate="G$1" x="60.96" y="-416.56" rot="R90"/>
<instance part="R44" gate="G$1" x="66.04" y="-424.18" rot="R90"/>
<instance part="R55" gate="G$1" x="71.12" y="-416.56" rot="R90"/>
<instance part="R125" gate="G$1" x="76.2" y="-424.18" rot="R90"/>
<instance part="R53" gate="G$1" x="81.28" y="-416.56" rot="R90"/>
<instance part="R126" gate="G$1" x="86.36" y="-424.18" rot="R90"/>
<instance part="R50" gate="G$1" x="91.44" y="-416.56" rot="R90"/>
<instance part="R127" gate="G$1" x="96.52" y="-424.18" rot="R90"/>
<instance part="R128" gate="G$1" x="101.6" y="-416.56" rot="R90"/>
<instance part="R129" gate="G$1" x="106.68" y="-424.18" rot="R90"/>
<instance part="AGND42" gate="VR1" x="66.04" y="-441.96"/>
<instance part="R15" gate="G$1" x="-363.855" y="-570.865"/>
<instance part="R8" gate="G$1" x="-504.19" y="-570.865"/>
<instance part="R139" gate="G$1" x="20.955" y="-681.355"/>
<instance part="R132" gate="G$1" x="135.89" y="-681.355"/>
<instance part="R6" gate="G$1" x="14.605" y="-574.675"/>
<instance part="R4" gate="G$1" x="56.515" y="-579.755"/>
<instance part="S2" gate="G$1" x="-229.235" y="-166.37" rot="R180"/>
<instance part="S1" gate="G$1" x="-229.235" y="-175.895" rot="R180"/>
<instance part="S4" gate="G$1" x="-229.235" y="-185.42" rot="R180"/>
<instance part="S3" gate="G$1" x="-229.235" y="-194.945" rot="R180"/>
<instance part="R72" gate="G$1" x="-187.96" y="-166.37"/>
<instance part="R73" gate="G$1" x="-187.325" y="-175.895"/>
<instance part="R82" gate="G$1" x="-187.325" y="-185.42"/>
<instance part="R80" gate="G$1" x="-187.325" y="-194.945"/>
<instance part="R71" gate="G$1" x="-215.9" y="-205.74" rot="R90"/>
<instance part="R64" gate="G$1" x="-208.915" y="-205.74" rot="R90"/>
<instance part="R83" gate="G$1" x="-201.93" y="-205.74" rot="R90"/>
<instance part="R81" gate="G$1" x="-194.945" y="-205.74" rot="R90"/>
<instance part="AGND21" gate="VR1" x="-215.9" y="-218.44"/>
<instance part="U9" gate="A" x="-118.11" y="-243.205"/>
<instance part="S6" gate="G$1" x="-230.505" y="-238.125" rot="R180"/>
<instance part="S5" gate="G$1" x="-230.505" y="-247.65" rot="R180"/>
<instance part="S8" gate="G$1" x="-230.505" y="-257.175" rot="R180"/>
<instance part="S7" gate="G$1" x="-230.505" y="-266.7" rot="R180"/>
<instance part="R74" gate="G$1" x="-189.23" y="-238.125"/>
<instance part="R75" gate="G$1" x="-188.595" y="-247.65"/>
<instance part="R86" gate="G$1" x="-188.595" y="-257.175"/>
<instance part="R84" gate="G$1" x="-188.595" y="-266.7"/>
<instance part="R104" gate="G$1" x="-217.17" y="-277.495" rot="R90"/>
<instance part="R65" gate="G$1" x="-210.185" y="-277.495" rot="R90"/>
<instance part="R87" gate="G$1" x="-203.2" y="-277.495" rot="R90"/>
<instance part="R85" gate="G$1" x="-196.215" y="-277.495" rot="R90"/>
<instance part="AGND43" gate="VR1" x="-217.17" y="-290.195"/>
<instance part="U10" gate="A" x="-397.51" y="-175.895"/>
<instance part="S10" gate="G$1" x="-509.905" y="-170.815" rot="R180"/>
<instance part="S9" gate="G$1" x="-509.905" y="-180.34" rot="R180"/>
<instance part="S12" gate="G$1" x="-509.905" y="-189.865" rot="R180"/>
<instance part="S11" gate="G$1" x="-509.905" y="-199.39" rot="R180"/>
<instance part="R76" gate="G$1" x="-468.63" y="-170.815"/>
<instance part="R77" gate="G$1" x="-467.995" y="-180.34"/>
<instance part="R90" gate="G$1" x="-467.995" y="-189.865"/>
<instance part="R88" gate="G$1" x="-467.995" y="-199.39"/>
<instance part="R109" gate="G$1" x="-496.57" y="-210.185" rot="R90"/>
<instance part="R67" gate="G$1" x="-489.585" y="-210.185" rot="R90"/>
<instance part="R91" gate="G$1" x="-482.6" y="-210.185" rot="R90"/>
<instance part="R89" gate="G$1" x="-475.615" y="-210.185" rot="R90"/>
<instance part="AGND44" gate="VR1" x="-496.57" y="-222.885"/>
<instance part="U11" gate="A" x="-397.51" y="-241.3"/>
<instance part="S14" gate="G$1" x="-509.905" y="-236.22" rot="R180"/>
<instance part="S13" gate="G$1" x="-509.905" y="-245.745" rot="R180"/>
<instance part="S16" gate="G$1" x="-509.905" y="-255.27" rot="R180"/>
<instance part="S15" gate="G$1" x="-509.905" y="-264.795" rot="R180"/>
<instance part="R78" gate="G$1" x="-468.63" y="-236.22"/>
<instance part="R79" gate="G$1" x="-467.995" y="-245.745"/>
<instance part="R94" gate="G$1" x="-467.995" y="-255.27"/>
<instance part="R92" gate="G$1" x="-467.995" y="-264.795"/>
<instance part="R107" gate="G$1" x="-496.57" y="-275.59" rot="R90"/>
<instance part="R69" gate="G$1" x="-489.585" y="-275.59" rot="R90"/>
<instance part="R95" gate="G$1" x="-482.6" y="-275.59" rot="R90"/>
<instance part="R93" gate="G$1" x="-475.615" y="-275.59" rot="R90"/>
<instance part="AGND45" gate="VR1" x="-496.57" y="-288.29"/>
<instance part="A-(S)" gate="G$1" x="-67.31" y="-400.685" rot="MR90"/>
<instance part="A+(S)" gate="G$1" x="-62.23" y="-444.5" rot="MR270"/>
<instance part="AGND41" gate="VR1" x="-54.61" y="-444.5"/>
<instance part="AGND46" gate="VR1" x="-67.31" y="-408.305"/>
<instance part="B+(E)" gate="G$1" x="215.265" y="-231.775" rot="MR270"/>
<instance part="AGND49" gate="VR1" x="222.885" y="-231.775"/>
<instance part="B-(E)" gate="G$1" x="210.185" y="-191.135" rot="MR90"/>
<instance part="AGND50" gate="VR1" x="210.185" y="-198.755"/>
<instance part="U6" gate="A" x="-501.015" y="-386.715"/>
<instance part="U4" gate="A" x="-501.015" y="-452.755"/>
<instance part="E4" gate="G$1" x="-431.8" y="-446.405" rot="R180"/>
<instance part="E8" gate="G$1" x="-431.8" y="-455.93" rot="R180"/>
<instance part="E3" gate="G$1" x="-431.8" y="-465.455" rot="R180"/>
<instance part="E7" gate="G$1" x="-431.8" y="-474.98" rot="R180"/>
<instance part="E2" gate="G$1" x="-411.48" y="-446.405" rot="R180"/>
<instance part="E6" gate="G$1" x="-411.48" y="-455.93" rot="R180"/>
<instance part="E1" gate="G$1" x="-411.48" y="-465.455" rot="R180"/>
<instance part="E5" gate="G$1" x="-411.48" y="-474.98" rot="R180"/>
<instance part="AGND51" gate="VR1" x="-398.145" y="-481.965"/>
<instance part="E16" gate="G$1" x="-389.89" y="-446.405" rot="R180"/>
<instance part="E12" gate="G$1" x="-389.89" y="-455.93" rot="R180"/>
<instance part="E15" gate="G$1" x="-389.89" y="-465.455" rot="R180"/>
<instance part="E11" gate="G$1" x="-389.89" y="-474.98" rot="R180"/>
<instance part="AGND52" gate="VR1" x="-376.555" y="-481.965"/>
<instance part="E14" gate="G$1" x="-368.3" y="-446.405" rot="R180"/>
<instance part="E10" gate="G$1" x="-368.3" y="-455.93" rot="R180"/>
<instance part="E13" gate="G$1" x="-368.3" y="-465.455" rot="R180"/>
<instance part="E9" gate="G$1" x="-368.3" y="-474.98" rot="R180"/>
<instance part="AGND53" gate="VR1" x="-354.965" y="-483.235"/>
<instance part="SIG1" gate="G$1" x="-548.64" y="-426.085" rot="MR0"/>
<instance part="SIG2" gate="G$1" x="-551.18" y="-355.6" rot="MR0"/>
<instance part="AGND54" gate="VR1" x="-545.465" y="-360.045"/>
<instance part="AGND55" gate="VR1" x="-541.02" y="-438.15"/>
<instance part="AGND47" gate="VR1" x="-419.735" y="-482.6"/>
<instance part="R63" gate="G$1" x="-422.91" y="-374.015"/>
<instance part="R122" gate="G$1" x="-422.91" y="-376.555"/>
<instance part="R123" gate="G$1" x="-422.91" y="-379.095"/>
<instance part="R121" gate="G$1" x="-422.91" y="-381.635"/>
<instance part="R124" gate="G$1" x="-422.91" y="-384.175"/>
<instance part="R120" gate="G$1" x="-422.91" y="-386.715"/>
<instance part="R62" gate="G$1" x="-422.91" y="-389.255"/>
<instance part="R119" gate="G$1" x="-422.91" y="-391.795"/>
<instance part="R68" gate="G$1" x="-422.91" y="-394.335"/>
<instance part="R116" gate="G$1" x="-422.91" y="-396.875"/>
<instance part="R70" gate="G$1" x="-422.91" y="-399.415"/>
<instance part="R115" gate="G$1" x="-422.91" y="-401.955"/>
<instance part="R114" gate="G$1" x="-422.91" y="-404.495"/>
<instance part="R117" gate="G$1" x="-422.91" y="-407.035"/>
<instance part="R113" gate="G$1" x="-422.91" y="-409.575"/>
<instance part="R118" gate="G$1" x="-422.91" y="-412.115"/>
<instance part="R97" gate="G$1" x="-410.21" y="-374.015"/>
<instance part="R99" gate="G$1" x="-410.21" y="-376.555"/>
<instance part="R96" gate="G$1" x="-410.21" y="-379.095"/>
<instance part="R98" gate="G$1" x="-410.21" y="-381.635"/>
<instance part="R112" gate="G$1" x="-410.21" y="-384.175"/>
<instance part="R111" gate="G$1" x="-410.21" y="-386.715"/>
<instance part="R61" gate="G$1" x="-410.21" y="-389.255"/>
<instance part="R110" gate="G$1" x="-410.21" y="-391.795"/>
<instance part="R103" gate="G$1" x="-410.21" y="-394.335"/>
<instance part="R101" gate="G$1" x="-410.21" y="-396.875"/>
<instance part="R102" gate="G$1" x="-410.21" y="-399.415"/>
<instance part="R100" gate="G$1" x="-410.21" y="-401.955"/>
<instance part="R106" gate="G$1" x="-410.21" y="-404.495"/>
<instance part="R108" gate="G$1" x="-410.21" y="-407.035"/>
<instance part="R105" gate="G$1" x="-410.21" y="-409.575"/>
<instance part="R66" gate="G$1" x="-410.21" y="-412.115"/>
<instance part="C50" gate="G$1" x="-142.24" y="-161.29" rot="R270"/>
<instance part="C49" gate="G$1" x="-142.24" y="-189.23" rot="R270"/>
<instance part="AGND39" gate="VR1" x="-152.4" y="-161.29" rot="R270"/>
<instance part="AGND40" gate="VR1" x="-152.4" y="-189.23" rot="R270"/>
<instance part="C48" gate="G$1" x="-143.51" y="-233.045" rot="R270"/>
<instance part="AGND48" gate="VR1" x="-153.67" y="-233.045" rot="R270"/>
<instance part="C47" gate="G$1" x="-142.875" y="-260.985" rot="R270"/>
<instance part="AGND56" gate="VR1" x="-153.035" y="-260.985" rot="R270"/>
<instance part="C46" gate="G$1" x="-422.91" y="-165.735" rot="R270"/>
<instance part="AGND57" gate="VR1" x="-433.07" y="-165.735" rot="R270"/>
<instance part="C45" gate="G$1" x="-422.275" y="-193.675" rot="R270"/>
<instance part="AGND58" gate="VR1" x="-432.435" y="-193.675" rot="R270"/>
<instance part="C44" gate="G$1" x="-423.545" y="-231.14" rot="R270"/>
<instance part="AGND59" gate="VR1" x="-433.705" y="-231.14" rot="R270"/>
<instance part="C43" gate="G$1" x="-422.91" y="-259.08" rot="R270"/>
<instance part="AGND60" gate="VR1" x="-433.07" y="-259.08" rot="R270"/>
<instance part="AGND61" gate="VR1" x="196.85" y="-416.56"/>
<instance part="C34" gate="G$1" x="196.85" y="-406.4"/>
<instance part="AGND62" gate="VR1" x="196.85" y="-349.25" rot="R180"/>
<instance part="C35" gate="G$1" x="196.85" y="-359.41" rot="R180"/>
<instance part="AGND63" gate="VR1" x="196.85" y="-491.49"/>
<instance part="C39" gate="G$1" x="196.85" y="-481.33"/>
<instance part="AGND64" gate="VR1" x="196.85" y="-425.45" rot="R180"/>
<instance part="C40" gate="G$1" x="196.85" y="-435.61" rot="R180"/>
<instance part="AGND65" gate="VR1" x="-265.43" y="-458.47"/>
<instance part="C25" gate="G$1" x="-265.43" y="-448.31"/>
<instance part="AGND66" gate="VR1" x="-265.43" y="-392.43" rot="R180"/>
<instance part="C36" gate="G$1" x="-265.43" y="-402.59" rot="R180"/>
<instance part="AGND67" gate="VR1" x="-521.335" y="-353.695" rot="R180"/>
<instance part="C42" gate="G$1" x="-521.335" y="-363.855" rot="R180"/>
<instance part="AGND68" gate="VR1" x="-520.7" y="-419.735"/>
<instance part="C41" gate="G$1" x="-520.7" y="-409.575"/>
<instance part="AGND69" gate="VR1" x="-527.05" y="-419.735" rot="R180"/>
<instance part="C38" gate="G$1" x="-527.05" y="-429.895" rot="R180"/>
<instance part="AGND70" gate="VR1" x="-520.7" y="-485.775"/>
<instance part="C37" gate="G$1" x="-520.7" y="-475.615"/>
<instance part="Q1" gate="P" x="-448.31" y="-648.97" rot="R90"/>
<instance part="R157" gate="G$1" x="-213.36" y="-471.17" rot="R90"/>
<instance part="R158" gate="G$1" x="-212.09" y="-383.54" rot="R90"/>
<instance part="AGND71" gate="VR1" x="-212.09" y="-393.7"/>
<instance part="AGND72" gate="VR1" x="-213.36" y="-481.33"/>
<instance part="JP2" gate="1" x="-568.96" y="-566.42" rot="R90"/>
<instance part="SUPPLY4" gate="G$1" x="-547.37" y="-574.04"/>
<instance part="FRAME1" gate="G$1" x="-289.56" y="-731.52"/>
<instance part="FRAME3" gate="G$1" x="-3.81" y="-730.25"/>
<instance part="U14" gate="A" x="163.83" y="-620.395"/>
<instance part="C63" gate="G$1" x="126.365" y="-626.745"/>
<instance part="C64" gate="G$1" x="239.395" y="-625.475"/>
<instance part="R5" gate="G$1" x="155.575" y="-630.555" rot="R180"/>
<instance part="C71" gate="G$1" x="160.655" y="-646.43"/>
<instance part="U15" gate="A" x="191.77" y="-574.675"/>
<instance part="C72" gate="G$1" x="147.32" y="-582.295"/>
<instance part="C73" gate="G$1" x="185.42" y="-585.47"/>
<instance part="C77" gate="G$1" x="137.795" y="-626.745"/>
<instance part="C78" gate="G$1" x="162.56" y="-584.835"/>
<instance part="SUPPLY8" gate="G$1" x="126.365" y="-640.715"/>
<instance part="SUPPLY9" gate="G$1" x="137.795" y="-640.715"/>
<instance part="SUPPLY10" gate="G$1" x="160.655" y="-655.955"/>
<instance part="SUPPLY11" gate="G$1" x="227.965" y="-654.685"/>
<instance part="SUPPLY12" gate="G$1" x="239.395" y="-635.635"/>
<instance part="AGND27" gate="VR1" x="185.42" y="-598.17"/>
<instance part="AGND28" gate="VR1" x="147.32" y="-596.9"/>
<instance part="AGND29" gate="VR1" x="162.56" y="-597.535"/>
<instance part="AGND30" gate="VR1" x="242.57" y="-578.485"/>
<instance part="R133" gate="G$1" x="117.475" y="-620.395"/>
<instance part="R141" gate="G$1" x="232.41" y="-620.395"/>
<instance part="R148" gate="G$1" x="139.065" y="-574.675"/>
<instance part="R149" gate="G$1" x="178.435" y="-579.755"/>
<instance part="FRAME4" gate="G$1" x="-3.81" y="-730.25"/>
<instance part="FRAME5" gate="G$1" x="-581.66" y="-731.52"/>
<instance part="FRAME6" gate="G$1" x="-1.27" y="-314.96"/>
<instance part="FRAME7" gate="G$1" x="-289.56" y="-521.97"/>
<instance part="FRAME2" gate="G$1" x="-2.54" y="-521.97"/>
<instance part="FRAME8" gate="G$1" x="-289.56" y="-314.96"/>
<instance part="FRAME9" gate="G$1" x="-580.39" y="-314.96"/>
<instance part="FRAME10" gate="G$1" x="-579.755" y="-521.335"/>
</instances>
<busses>
</busses>
<nets>
<net name="DGND" class="0">
<segment>
<wire x1="-247.65" y1="-628.65" x2="-247.65" y2="-629.92" width="0.1524" layer="91"/>
<wire x1="-247.65" y1="-629.92" x2="-255.27" y2="-629.92" width="0.1524" layer="91"/>
<wire x1="-255.27" y1="-629.92" x2="-255.27" y2="-628.65" width="0.1524" layer="91"/>
<wire x1="-264.16" y1="-628.65" x2="-264.16" y2="-629.92" width="0.1524" layer="91"/>
<wire x1="-264.16" y1="-629.92" x2="-255.27" y2="-629.92" width="0.1524" layer="91"/>
<wire x1="-240.03" y1="-628.65" x2="-240.03" y2="-629.92" width="0.1524" layer="91"/>
<wire x1="-240.03" y1="-629.92" x2="-247.65" y2="-629.92" width="0.1524" layer="91"/>
<junction x="-255.27" y="-629.92"/>
<junction x="-255.27" y="-629.92"/>
<junction x="-247.65" y="-629.92"/>
<pinref part="C74" gate="G$1" pin="2"/>
<pinref part="C79" gate="G$1" pin="2"/>
<pinref part="C75" gate="G$1" pin="2"/>
<pinref part="SUPPLY31" gate="G$1" pin="DGND"/>
<pinref part="C76" gate="G$1" pin="2"/>
</segment>
<segment>
<wire x1="-276.86" y1="-628.65" x2="-276.86" y2="-629.285" width="0.1524" layer="91"/>
<pinref part="C1" gate="G$1" pin="2"/>
<pinref part="SUPPLY30" gate="G$1" pin="DGND"/>
</segment>
<segment>
<wire x1="-233.68" y1="-635" x2="-234.95" y2="-635" width="0.1524" layer="91"/>
<wire x1="-234.95" y1="-635" x2="-234.95" y2="-637.54" width="0.1524" layer="91"/>
<wire x1="-233.68" y1="-637.54" x2="-234.95" y2="-637.54" width="0.1524" layer="91"/>
<wire x1="-234.95" y1="-637.54" x2="-234.95" y2="-640.08" width="0.1524" layer="91"/>
<wire x1="-233.68" y1="-640.08" x2="-234.95" y2="-640.08" width="0.1524" layer="91"/>
<wire x1="-234.95" y1="-640.08" x2="-234.95" y2="-642.62" width="0.1524" layer="91"/>
<wire x1="-233.68" y1="-642.62" x2="-234.95" y2="-642.62" width="0.1524" layer="91"/>
<wire x1="-234.95" y1="-642.62" x2="-234.95" y2="-645.16" width="0.1524" layer="91"/>
<wire x1="-234.95" y1="-645.16" x2="-233.68" y2="-645.16" width="0.1524" layer="91"/>
<wire x1="-234.95" y1="-637.54" x2="-242.57" y2="-637.54" width="0.1524" layer="91"/>
<wire x1="-242.57" y1="-637.54" x2="-242.57" y2="-638.175" width="0.1524" layer="91"/>
<junction x="-234.95" y="-637.54"/>
<junction x="-234.95" y="-640.08"/>
<junction x="-234.95" y="-642.62"/>
<pinref part="$$U$6" gate="A" pin="DRGND1"/>
<pinref part="$$U$6" gate="A" pin="DRGND2"/>
<pinref part="$$U$6" gate="A" pin="DRGND0"/>
<pinref part="$$U$6" gate="A" pin="DRGND3"/>
<pinref part="$$U$6" gate="A" pin="DRGND4"/>
<pinref part="SUPPLY32" gate="G$1" pin="DGND"/>
</segment>
<segment>
<wire x1="-543.56" y1="-670.56" x2="-542.29" y2="-670.56" width="0.1524" layer="91"/>
<wire x1="-542.29" y1="-670.56" x2="-542.29" y2="-680.72" width="0.1524" layer="91"/>
<pinref part="SEN" gate="1" pin="1"/>
<pinref part="SUPPLY38" gate="G$1" pin="DGND"/>
</segment>
<segment>
<wire x1="-126.365" y1="-680.085" x2="-122.555" y2="-680.085" width="0.1524" layer="91"/>
<wire x1="-122.555" y1="-680.085" x2="-112.395" y2="-680.085" width="0.1524" layer="91"/>
<wire x1="-112.395" y1="-681.355" x2="-112.395" y2="-680.085" width="0.1524" layer="91"/>
<wire x1="-126.365" y1="-672.465" x2="-122.555" y2="-672.465" width="0.1524" layer="91"/>
<wire x1="-122.555" y1="-672.465" x2="-122.555" y2="-680.085" width="0.1524" layer="91"/>
<wire x1="-122.555" y1="-672.465" x2="-122.555" y2="-664.845" width="0.1524" layer="91"/>
<wire x1="-122.555" y1="-664.845" x2="-122.555" y2="-657.225" width="0.1524" layer="91"/>
<wire x1="-122.555" y1="-657.225" x2="-122.555" y2="-649.605" width="0.1524" layer="91"/>
<wire x1="-122.555" y1="-649.605" x2="-122.555" y2="-641.985" width="0.1524" layer="91"/>
<wire x1="-122.555" y1="-641.985" x2="-122.555" y2="-634.365" width="0.1524" layer="91"/>
<wire x1="-122.555" y1="-634.365" x2="-122.555" y2="-626.745" width="0.1524" layer="91"/>
<wire x1="-122.555" y1="-626.745" x2="-122.555" y2="-619.125" width="0.1524" layer="91"/>
<wire x1="-122.555" y1="-619.125" x2="-122.555" y2="-611.505" width="0.1524" layer="91"/>
<wire x1="-122.555" y1="-611.505" x2="-122.555" y2="-603.885" width="0.1524" layer="91"/>
<wire x1="-122.555" y1="-603.885" x2="-122.555" y2="-596.265" width="0.1524" layer="91"/>
<wire x1="-122.555" y1="-596.265" x2="-122.555" y2="-593.725" width="0.1524" layer="91"/>
<wire x1="-126.365" y1="-596.265" x2="-122.555" y2="-596.265" width="0.1524" layer="91"/>
<wire x1="-126.365" y1="-603.885" x2="-122.555" y2="-603.885" width="0.1524" layer="91"/>
<wire x1="-126.365" y1="-664.845" x2="-122.555" y2="-664.845" width="0.1524" layer="91"/>
<wire x1="-126.365" y1="-657.225" x2="-122.555" y2="-657.225" width="0.1524" layer="91"/>
<wire x1="-126.365" y1="-649.605" x2="-122.555" y2="-649.605" width="0.1524" layer="91"/>
<wire x1="-126.365" y1="-641.985" x2="-122.555" y2="-641.985" width="0.1524" layer="91"/>
<wire x1="-126.365" y1="-634.365" x2="-122.555" y2="-634.365" width="0.1524" layer="91"/>
<wire x1="-126.365" y1="-626.745" x2="-122.555" y2="-626.745" width="0.1524" layer="91"/>
<wire x1="-126.365" y1="-619.125" x2="-122.555" y2="-619.125" width="0.1524" layer="91"/>
<wire x1="-126.365" y1="-611.505" x2="-122.555" y2="-611.505" width="0.1524" layer="91"/>
<wire x1="-126.365" y1="-685.165" x2="-122.555" y2="-685.165" width="0.1524" layer="91"/>
<wire x1="-122.555" y1="-685.165" x2="-122.555" y2="-680.085" width="0.1524" layer="91"/>
<wire x1="-126.365" y1="-593.725" x2="-122.555" y2="-593.725" width="0.1524" layer="91"/>
<wire x1="-126.365" y1="-586.105" x2="-122.555" y2="-586.105" width="0.1524" layer="91"/>
<wire x1="-122.555" y1="-593.725" x2="-122.555" y2="-586.105" width="0.1524" layer="91"/>
<junction x="-122.555" y="-680.085"/>
<junction x="-122.555" y="-672.465"/>
<junction x="-122.555" y="-596.265"/>
<junction x="-122.555" y="-603.885"/>
<junction x="-122.555" y="-664.845"/>
<junction x="-122.555" y="-657.225"/>
<junction x="-122.555" y="-649.605"/>
<junction x="-122.555" y="-641.985"/>
<junction x="-122.555" y="-634.365"/>
<junction x="-122.555" y="-626.745"/>
<junction x="-122.555" y="-619.125"/>
<junction x="-122.555" y="-611.505"/>
<junction x="-122.555" y="-593.725"/>
<label x="-117.475" y="-680.085" size="1.778" layer="95"/>
<pinref part="ZED1" gate="-B" pin="G5"/>
<pinref part="ZED1" gate="-B" pin="G8"/>
<pinref part="ZED1" gate="-B" pin="G11"/>
<pinref part="ZED1" gate="-B" pin="G14"/>
<pinref part="ZED1" gate="-B" pin="G17"/>
<pinref part="ZED1" gate="-B" pin="G20"/>
<pinref part="ZED1" gate="-B" pin="G23"/>
<pinref part="ZED1" gate="-B" pin="G26"/>
<pinref part="ZED1" gate="-B" pin="G29"/>
<pinref part="ZED1" gate="-B" pin="G32"/>
<pinref part="ZED1" gate="-B" pin="G35"/>
<pinref part="ZED1" gate="-B" pin="G38"/>
<pinref part="ZED1" gate="-B" pin="G40"/>
<pinref part="ZED1" gate="-B" pin="G4"/>
<pinref part="ZED1" gate="-B" pin="G1"/>
<pinref part="SUPPLY35" gate="G$1" pin="DGND"/>
</segment>
<segment>
<wire x1="-102.235" y1="-681.355" x2="-102.235" y2="-677.545" width="0.1524" layer="91"/>
<wire x1="-90.805" y1="-682.625" x2="-94.615" y2="-682.625" width="0.1524" layer="91"/>
<wire x1="-94.615" y1="-682.625" x2="-94.615" y2="-677.545" width="0.1524" layer="91"/>
<wire x1="-94.615" y1="-677.545" x2="-90.805" y2="-677.545" width="0.1524" layer="91"/>
<wire x1="-94.615" y1="-677.545" x2="-94.615" y2="-672.465" width="0.1524" layer="91"/>
<wire x1="-94.615" y1="-672.465" x2="-94.615" y2="-654.685" width="0.1524" layer="91"/>
<wire x1="-94.615" y1="-654.685" x2="-94.615" y2="-647.065" width="0.1524" layer="91"/>
<wire x1="-94.615" y1="-647.065" x2="-94.615" y2="-639.445" width="0.1524" layer="91"/>
<wire x1="-94.615" y1="-639.445" x2="-94.615" y2="-631.825" width="0.1524" layer="91"/>
<wire x1="-94.615" y1="-631.825" x2="-94.615" y2="-624.205" width="0.1524" layer="91"/>
<wire x1="-94.615" y1="-624.205" x2="-94.615" y2="-616.585" width="0.1524" layer="91"/>
<wire x1="-94.615" y1="-616.585" x2="-94.615" y2="-608.965" width="0.1524" layer="91"/>
<wire x1="-94.615" y1="-608.965" x2="-94.615" y2="-601.345" width="0.1524" layer="91"/>
<wire x1="-94.615" y1="-601.345" x2="-94.615" y2="-598.805" width="0.1524" layer="91"/>
<wire x1="-94.615" y1="-598.805" x2="-94.615" y2="-591.185" width="0.1524" layer="91"/>
<wire x1="-94.615" y1="-591.185" x2="-94.615" y2="-588.645" width="0.1524" layer="91"/>
<wire x1="-94.615" y1="-588.645" x2="-90.805" y2="-588.645" width="0.1524" layer="91"/>
<wire x1="-90.805" y1="-591.185" x2="-94.615" y2="-591.185" width="0.1524" layer="91"/>
<wire x1="-90.805" y1="-598.805" x2="-94.615" y2="-598.805" width="0.1524" layer="91"/>
<wire x1="-90.805" y1="-601.345" x2="-94.615" y2="-601.345" width="0.1524" layer="91"/>
<wire x1="-90.805" y1="-608.965" x2="-94.615" y2="-608.965" width="0.1524" layer="91"/>
<wire x1="-90.805" y1="-616.585" x2="-94.615" y2="-616.585" width="0.1524" layer="91"/>
<wire x1="-90.805" y1="-624.205" x2="-94.615" y2="-624.205" width="0.1524" layer="91"/>
<wire x1="-90.805" y1="-631.825" x2="-94.615" y2="-631.825" width="0.1524" layer="91"/>
<wire x1="-90.805" y1="-639.445" x2="-94.615" y2="-639.445" width="0.1524" layer="91"/>
<wire x1="-90.805" y1="-647.065" x2="-94.615" y2="-647.065" width="0.1524" layer="91"/>
<wire x1="-90.805" y1="-654.685" x2="-94.615" y2="-654.685" width="0.1524" layer="91"/>
<wire x1="-90.805" y1="-672.465" x2="-94.615" y2="-672.465" width="0.1524" layer="91"/>
<wire x1="-102.235" y1="-677.545" x2="-94.615" y2="-677.545" width="0.1524" layer="91"/>
<junction x="-94.615" y="-677.545"/>
<junction x="-94.615" y="-591.185"/>
<junction x="-94.615" y="-598.805"/>
<junction x="-94.615" y="-601.345"/>
<junction x="-94.615" y="-608.965"/>
<junction x="-94.615" y="-616.585"/>
<junction x="-94.615" y="-624.205"/>
<junction x="-94.615" y="-631.825"/>
<junction x="-94.615" y="-639.445"/>
<junction x="-94.615" y="-647.065"/>
<junction x="-94.615" y="-654.685"/>
<junction x="-94.615" y="-672.465"/>
<pinref part="ZED1" gate="-A" pin="D39"/>
<pinref part="ZED1" gate="-A" pin="D37"/>
<pinref part="ZED1" gate="-A" pin="D2"/>
<pinref part="ZED1" gate="-A" pin="D3"/>
<pinref part="ZED1" gate="-A" pin="D6"/>
<pinref part="ZED1" gate="-A" pin="D7"/>
<pinref part="ZED1" gate="-A" pin="D10"/>
<pinref part="ZED1" gate="-A" pin="D13"/>
<pinref part="ZED1" gate="-A" pin="D16"/>
<pinref part="ZED1" gate="-A" pin="D19"/>
<pinref part="ZED1" gate="-A" pin="D22"/>
<pinref part="ZED1" gate="-A" pin="D25"/>
<pinref part="ZED1" gate="-A" pin="D28"/>
<pinref part="ZED1" gate="-A" pin="D35"/>
<pinref part="SUPPLY36" gate="G$1" pin="DGND"/>
</segment>
<segment>
<wire x1="-50.165" y1="-681.355" x2="-50.165" y2="-680.085" width="0.1524" layer="91"/>
<wire x1="-56.515" y1="-586.105" x2="-60.325" y2="-586.105" width="0.1524" layer="91"/>
<wire x1="-56.515" y1="-593.725" x2="-56.515" y2="-586.105" width="0.1524" layer="91"/>
<wire x1="-60.325" y1="-593.725" x2="-56.515" y2="-593.725" width="0.1524" layer="91"/>
<wire x1="-56.515" y1="-596.265" x2="-56.515" y2="-593.725" width="0.1524" layer="91"/>
<wire x1="-60.325" y1="-596.265" x2="-56.515" y2="-596.265" width="0.1524" layer="91"/>
<wire x1="-56.515" y1="-603.885" x2="-56.515" y2="-596.265" width="0.1524" layer="91"/>
<wire x1="-60.325" y1="-603.885" x2="-56.515" y2="-603.885" width="0.1524" layer="91"/>
<wire x1="-56.515" y1="-606.425" x2="-56.515" y2="-603.885" width="0.1524" layer="91"/>
<wire x1="-60.325" y1="-606.425" x2="-56.515" y2="-606.425" width="0.1524" layer="91"/>
<wire x1="-56.515" y1="-614.045" x2="-56.515" y2="-606.425" width="0.1524" layer="91"/>
<wire x1="-60.325" y1="-614.045" x2="-56.515" y2="-614.045" width="0.1524" layer="91"/>
<wire x1="-56.515" y1="-616.585" x2="-56.515" y2="-614.045" width="0.1524" layer="91"/>
<wire x1="-60.325" y1="-616.585" x2="-56.515" y2="-616.585" width="0.1524" layer="91"/>
<wire x1="-56.515" y1="-624.205" x2="-56.515" y2="-616.585" width="0.1524" layer="91"/>
<wire x1="-60.325" y1="-624.205" x2="-56.515" y2="-624.205" width="0.1524" layer="91"/>
<wire x1="-56.515" y1="-626.745" x2="-56.515" y2="-624.205" width="0.1524" layer="91"/>
<wire x1="-60.325" y1="-626.745" x2="-56.515" y2="-626.745" width="0.1524" layer="91"/>
<wire x1="-56.515" y1="-634.365" x2="-56.515" y2="-626.745" width="0.1524" layer="91"/>
<wire x1="-60.325" y1="-634.365" x2="-56.515" y2="-634.365" width="0.1524" layer="91"/>
<wire x1="-56.515" y1="-636.905" x2="-56.515" y2="-634.365" width="0.1524" layer="91"/>
<wire x1="-60.325" y1="-636.905" x2="-56.515" y2="-636.905" width="0.1524" layer="91"/>
<wire x1="-56.515" y1="-644.525" x2="-56.515" y2="-636.905" width="0.1524" layer="91"/>
<wire x1="-60.325" y1="-644.525" x2="-56.515" y2="-644.525" width="0.1524" layer="91"/>
<wire x1="-56.515" y1="-647.065" x2="-56.515" y2="-644.525" width="0.1524" layer="91"/>
<wire x1="-60.325" y1="-647.065" x2="-56.515" y2="-647.065" width="0.1524" layer="91"/>
<wire x1="-56.515" y1="-654.685" x2="-56.515" y2="-647.065" width="0.1524" layer="91"/>
<wire x1="-60.325" y1="-654.685" x2="-56.515" y2="-654.685" width="0.1524" layer="91"/>
<wire x1="-56.515" y1="-657.225" x2="-56.515" y2="-654.685" width="0.1524" layer="91"/>
<wire x1="-60.325" y1="-657.225" x2="-56.515" y2="-657.225" width="0.1524" layer="91"/>
<wire x1="-56.515" y1="-664.845" x2="-56.515" y2="-657.225" width="0.1524" layer="91"/>
<wire x1="-60.325" y1="-664.845" x2="-56.515" y2="-664.845" width="0.1524" layer="91"/>
<wire x1="-56.515" y1="-667.385" x2="-56.515" y2="-664.845" width="0.1524" layer="91"/>
<wire x1="-60.325" y1="-667.385" x2="-56.515" y2="-667.385" width="0.1524" layer="91"/>
<wire x1="-56.515" y1="-669.925" x2="-56.515" y2="-667.385" width="0.1524" layer="91"/>
<wire x1="-60.325" y1="-669.925" x2="-56.515" y2="-669.925" width="0.1524" layer="91"/>
<wire x1="-60.325" y1="-685.165" x2="-56.515" y2="-685.165" width="0.1524" layer="91"/>
<wire x1="-56.515" y1="-685.165" x2="-56.515" y2="-680.085" width="0.1524" layer="91"/>
<wire x1="-60.325" y1="-680.085" x2="-56.515" y2="-680.085" width="0.1524" layer="91"/>
<wire x1="-56.515" y1="-680.085" x2="-56.515" y2="-675.005" width="0.1524" layer="91"/>
<wire x1="-56.515" y1="-675.005" x2="-56.515" y2="-669.925" width="0.1524" layer="91"/>
<wire x1="-60.325" y1="-675.005" x2="-56.515" y2="-675.005" width="0.1524" layer="91"/>
<wire x1="-50.165" y1="-680.085" x2="-56.515" y2="-680.085" width="0.1524" layer="91"/>
<junction x="-56.515" y="-593.725"/>
<junction x="-56.515" y="-596.265"/>
<junction x="-56.515" y="-603.885"/>
<junction x="-56.515" y="-606.425"/>
<junction x="-56.515" y="-614.045"/>
<junction x="-56.515" y="-616.585"/>
<junction x="-56.515" y="-624.205"/>
<junction x="-56.515" y="-626.745"/>
<junction x="-56.515" y="-634.365"/>
<junction x="-56.515" y="-636.905"/>
<junction x="-56.515" y="-644.525"/>
<junction x="-56.515" y="-647.065"/>
<junction x="-56.515" y="-654.685"/>
<junction x="-56.515" y="-657.225"/>
<junction x="-56.515" y="-664.845"/>
<junction x="-56.515" y="-667.385"/>
<junction x="-56.515" y="-669.925"/>
<junction x="-56.515" y="-680.085"/>
<junction x="-56.515" y="-675.005"/>
<pinref part="ZED1" gate="-A" pin="C1"/>
<pinref part="ZED1" gate="-A" pin="C4"/>
<pinref part="ZED1" gate="-A" pin="C5"/>
<pinref part="ZED1" gate="-A" pin="C8"/>
<pinref part="ZED1" gate="-A" pin="C9"/>
<pinref part="ZED1" gate="-A" pin="C12"/>
<pinref part="ZED1" gate="-A" pin="C13"/>
<pinref part="ZED1" gate="-A" pin="C16"/>
<pinref part="ZED1" gate="-A" pin="C17"/>
<pinref part="ZED1" gate="-A" pin="C20"/>
<pinref part="ZED1" gate="-A" pin="C21"/>
<pinref part="ZED1" gate="-A" pin="C24"/>
<pinref part="ZED1" gate="-A" pin="C25"/>
<pinref part="ZED1" gate="-A" pin="C28"/>
<pinref part="ZED1" gate="-A" pin="C29"/>
<pinref part="ZED1" gate="-A" pin="C32"/>
<pinref part="ZED1" gate="-A" pin="C33"/>
<pinref part="ZED1" gate="-A" pin="C34"/>
<pinref part="ZED1" gate="-A" pin="C40"/>
<pinref part="ZED1" gate="-A" pin="C38"/>
<pinref part="ZED1" gate="-A" pin="C36"/>
<pinref part="SUPPLY37" gate="G$1" pin="DGND"/>
</segment>
<segment>
<wire x1="-233.68" y1="-679.45" x2="-251.46" y2="-679.45" width="0.1524" layer="91"/>
<label x="-242.57" y="-679.45" size="1.778" layer="95"/>
<pinref part="$$U$6" gate="A" pin="SCLK"/>
<pinref part="SUPPLY33" gate="G$1" pin="DGND"/>
</segment>
<segment>
<wire x1="-532.13" y1="-650.24" x2="-532.13" y2="-652.78" width="0.1524" layer="91"/>
<pinref part="R147" gate="G$1" pin="2"/>
<pinref part="SUPPLY1" gate="G$1" pin="DGND"/>
</segment>
<segment>
<wire x1="-156.845" y1="-591.185" x2="-160.655" y2="-591.185" width="0.1524" layer="91"/>
<wire x1="-156.845" y1="-598.805" x2="-160.655" y2="-598.805" width="0.1524" layer="91"/>
<wire x1="-160.655" y1="-598.805" x2="-160.655" y2="-591.185" width="0.1524" layer="91"/>
<wire x1="-160.655" y1="-606.425" x2="-160.655" y2="-598.805" width="0.1524" layer="91"/>
<wire x1="-156.845" y1="-606.425" x2="-160.655" y2="-606.425" width="0.1524" layer="91"/>
<wire x1="-160.655" y1="-614.045" x2="-160.655" y2="-606.425" width="0.1524" layer="91"/>
<wire x1="-156.845" y1="-614.045" x2="-160.655" y2="-614.045" width="0.1524" layer="91"/>
<wire x1="-160.655" y1="-621.665" x2="-160.655" y2="-614.045" width="0.1524" layer="91"/>
<wire x1="-156.845" y1="-621.665" x2="-160.655" y2="-621.665" width="0.1524" layer="91"/>
<wire x1="-160.655" y1="-629.285" x2="-160.655" y2="-621.665" width="0.1524" layer="91"/>
<wire x1="-156.845" y1="-629.285" x2="-160.655" y2="-629.285" width="0.1524" layer="91"/>
<wire x1="-160.655" y1="-636.905" x2="-160.655" y2="-629.285" width="0.1524" layer="91"/>
<wire x1="-156.845" y1="-636.905" x2="-160.655" y2="-636.905" width="0.1524" layer="91"/>
<wire x1="-160.655" y1="-644.525" x2="-160.655" y2="-636.905" width="0.1524" layer="91"/>
<wire x1="-156.845" y1="-644.525" x2="-160.655" y2="-644.525" width="0.1524" layer="91"/>
<wire x1="-160.655" y1="-652.145" x2="-160.655" y2="-644.525" width="0.1524" layer="91"/>
<wire x1="-156.845" y1="-652.145" x2="-160.655" y2="-652.145" width="0.1524" layer="91"/>
<wire x1="-160.655" y1="-659.765" x2="-160.655" y2="-652.145" width="0.1524" layer="91"/>
<wire x1="-156.845" y1="-659.765" x2="-160.655" y2="-659.765" width="0.1524" layer="91"/>
<wire x1="-160.655" y1="-667.385" x2="-160.655" y2="-659.765" width="0.1524" layer="91"/>
<wire x1="-156.845" y1="-667.385" x2="-160.655" y2="-667.385" width="0.1524" layer="91"/>
<wire x1="-160.655" y1="-675.005" x2="-160.655" y2="-667.385" width="0.1524" layer="91"/>
<wire x1="-156.845" y1="-675.005" x2="-160.655" y2="-675.005" width="0.1524" layer="91"/>
<wire x1="-160.655" y1="-682.625" x2="-160.655" y2="-675.005" width="0.1524" layer="91"/>
<wire x1="-156.845" y1="-682.625" x2="-160.655" y2="-682.625" width="0.1524" layer="91"/>
<wire x1="-160.655" y1="-591.185" x2="-182.245" y2="-591.185" width="0.1524" layer="91"/>
<wire x1="-182.245" y1="-591.185" x2="-182.245" y2="-593.09" width="0.1524" layer="91"/>
<junction x="-160.655" y="-598.805"/>
<junction x="-160.655" y="-606.425"/>
<junction x="-160.655" y="-614.045"/>
<junction x="-160.655" y="-621.665"/>
<junction x="-160.655" y="-629.285"/>
<junction x="-160.655" y="-636.905"/>
<junction x="-160.655" y="-644.525"/>
<junction x="-160.655" y="-652.145"/>
<junction x="-160.655" y="-659.765"/>
<junction x="-160.655" y="-667.385"/>
<junction x="-160.655" y="-675.005"/>
<junction x="-160.655" y="-591.185"/>
<pinref part="ZED1" gate="-B" pin="H3"/>
<pinref part="ZED1" gate="-B" pin="H6"/>
<pinref part="ZED1" gate="-B" pin="H9"/>
<pinref part="ZED1" gate="-B" pin="H12"/>
<pinref part="ZED1" gate="-B" pin="H15"/>
<pinref part="ZED1" gate="-B" pin="H18"/>
<pinref part="ZED1" gate="-B" pin="H21"/>
<pinref part="ZED1" gate="-B" pin="H24"/>
<pinref part="ZED1" gate="-B" pin="H27"/>
<pinref part="ZED1" gate="-B" pin="H30"/>
<pinref part="ZED1" gate="-B" pin="H33"/>
<pinref part="ZED1" gate="-B" pin="H36"/>
<pinref part="ZED1" gate="-B" pin="H39"/>
<pinref part="SUPPLY34" gate="G$1" pin="DGND"/>
</segment>
<segment>
<wire x1="128.27" y1="-694.055" x2="131.445" y2="-694.055" width="0.1524" layer="91"/>
<wire x1="131.445" y1="-694.055" x2="131.445" y2="-701.675" width="0.1524" layer="91"/>
<wire x1="131.445" y1="-701.675" x2="131.445" y2="-713.105" width="0.1524" layer="91"/>
<wire x1="128.27" y1="-688.975" x2="131.445" y2="-688.975" width="0.1524" layer="91"/>
<wire x1="131.445" y1="-688.975" x2="131.445" y2="-694.055" width="0.1524" layer="91"/>
<wire x1="128.27" y1="-701.675" x2="131.445" y2="-701.675" width="0.1524" layer="91"/>
<junction x="131.445" y="-694.055"/>
<junction x="131.445" y="-701.675"/>
<pinref part="U13" gate="A" pin="GND"/>
<pinref part="U13" gate="A" pin="GND_2"/>
<pinref part="U13" gate="A" pin="EP"/>
<pinref part="SUPPLY6" gate="G$1" pin="DGND"/>
</segment>
<segment>
<wire x1="29.845" y1="-692.785" x2="29.845" y2="-699.135" width="0.1524" layer="91"/>
<pinref part="C68" gate="G$1" pin="2"/>
<pinref part="SUPPLY2" gate="G$1" pin="DGND"/>
</segment>
<segment>
<wire x1="142.875" y1="-691.515" x2="142.875" y2="-694.055" width="0.1524" layer="91"/>
<pinref part="C70" gate="G$1" pin="2"/>
<pinref part="SUPPLY7" gate="G$1" pin="DGND"/>
</segment>
<segment>
<wire x1="64.135" y1="-712.47" x2="64.135" y2="-714.375" width="0.1524" layer="91"/>
<pinref part="C61" gate="G$1" pin="2"/>
<pinref part="SUPPLY5" gate="G$1" pin="DGND"/>
</segment>
<segment>
<wire x1="-373.38" y1="-637.54" x2="-370.84" y2="-637.54" width="0.1524" layer="91"/>
<wire x1="-370.84" y1="-637.54" x2="-367.665" y2="-637.54" width="0.1524" layer="91"/>
<wire x1="-373.38" y1="-635" x2="-370.84" y2="-635" width="0.1524" layer="91"/>
<wire x1="-370.84" y1="-635" x2="-370.84" y2="-637.54" width="0.1524" layer="91"/>
<wire x1="-373.38" y1="-629.92" x2="-370.84" y2="-629.92" width="0.1524" layer="91"/>
<wire x1="-370.84" y1="-629.92" x2="-370.84" y2="-635" width="0.1524" layer="91"/>
<wire x1="-373.38" y1="-624.84" x2="-370.84" y2="-624.84" width="0.1524" layer="91"/>
<wire x1="-370.84" y1="-624.84" x2="-370.84" y2="-629.92" width="0.1524" layer="91"/>
<wire x1="-373.38" y1="-622.3" x2="-370.84" y2="-622.3" width="0.1524" layer="91"/>
<wire x1="-370.84" y1="-622.3" x2="-370.84" y2="-624.84" width="0.1524" layer="91"/>
<wire x1="-370.84" y1="-642.62" x2="-370.84" y2="-637.54" width="0.1524" layer="91"/>
<wire x1="-373.38" y1="-642.62" x2="-370.84" y2="-642.62" width="0.1524" layer="91"/>
<junction x="-370.84" y="-637.54"/>
<junction x="-370.84" y="-635"/>
<junction x="-370.84" y="-629.92"/>
<junction x="-370.84" y="-624.84"/>
<pinref part="U12" gate="A" pin="GNDA_2"/>
<pinref part="U12" gate="A" pin="GNDA"/>
<pinref part="U12" gate="A" pin="GNDX"/>
<pinref part="U12" gate="A" pin="GND"/>
<pinref part="U12" gate="A" pin="GND33"/>
<pinref part="SUPPLY29" gate="G$1" pin="DGND"/>
<pinref part="U12" gate="A" pin="*OE"/>
</segment>
<segment>
<wire x1="-491.49" y1="-635" x2="-491.49" y2="-632.46" width="0.1524" layer="91"/>
<wire x1="-491.49" y1="-632.46" x2="-491.49" y2="-631.19" width="0.1524" layer="91"/>
<wire x1="-483.87" y1="-631.19" x2="-483.87" y2="-632.46" width="0.1524" layer="91"/>
<wire x1="-483.87" y1="-632.46" x2="-491.49" y2="-632.46" width="0.1524" layer="91"/>
<wire x1="-476.885" y1="-631.19" x2="-476.885" y2="-632.46" width="0.1524" layer="91"/>
<wire x1="-476.885" y1="-632.46" x2="-483.87" y2="-632.46" width="0.1524" layer="91"/>
<wire x1="-469.9" y1="-631.19" x2="-469.9" y2="-632.46" width="0.1524" layer="91"/>
<wire x1="-469.9" y1="-632.46" x2="-476.885" y2="-632.46" width="0.1524" layer="91"/>
<wire x1="-462.28" y1="-631.19" x2="-462.28" y2="-632.46" width="0.1524" layer="91"/>
<wire x1="-462.28" y1="-632.46" x2="-469.9" y2="-632.46" width="0.1524" layer="91"/>
<wire x1="-454.66" y1="-631.19" x2="-454.66" y2="-632.46" width="0.1524" layer="91"/>
<wire x1="-454.66" y1="-632.46" x2="-462.28" y2="-632.46" width="0.1524" layer="91"/>
<wire x1="-447.04" y1="-631.19" x2="-447.04" y2="-632.46" width="0.1524" layer="91"/>
<wire x1="-447.04" y1="-632.46" x2="-454.66" y2="-632.46" width="0.1524" layer="91"/>
<junction x="-491.49" y="-632.46"/>
<junction x="-483.87" y="-632.46"/>
<junction x="-476.885" y="-632.46"/>
<junction x="-469.9" y="-632.46"/>
<junction x="-462.28" y="-632.46"/>
<junction x="-454.66" y="-632.46"/>
<pinref part="C62" gate="G$1" pin="1"/>
<pinref part="C53" gate="G$1" pin="2"/>
<pinref part="C57" gate="G$1" pin="2"/>
<pinref part="C52" gate="G$1" pin="2"/>
<pinref part="C58" gate="G$1" pin="2"/>
<pinref part="C56" gate="G$1" pin="2"/>
<pinref part="C55" gate="G$1" pin="2"/>
<pinref part="SUPPLY27" gate="G$1" pin="DGND"/>
</segment>
<segment>
<wire x1="-464.82" y1="-645.16" x2="-467.36" y2="-645.16" width="0.1524" layer="91"/>
<wire x1="-467.36" y1="-645.16" x2="-467.36" y2="-648.97" width="0.1524" layer="91"/>
<wire x1="-467.36" y1="-648.97" x2="-471.17" y2="-648.97" width="0.1524" layer="91"/>
<wire x1="-464.82" y1="-652.78" x2="-467.36" y2="-652.78" width="0.1524" layer="91"/>
<wire x1="-467.36" y1="-652.78" x2="-467.36" y2="-648.97" width="0.1524" layer="91"/>
<junction x="-467.36" y="-648.97"/>
<pinref part="C54" gate="G$1" pin="1"/>
<pinref part="C51" gate="G$1" pin="1"/>
<pinref part="SUPPLY28" gate="G$1" pin="DGND"/>
</segment>
<segment>
<wire x1="41.275" y1="-699.135" x2="41.275" y2="-692.785" width="0.1524" layer="91"/>
<pinref part="C67" gate="G$1" pin="2"/>
<pinref part="SUPPLY3" gate="G$1" pin="DGND"/>
</segment>
<segment>
<wire x1="-513.715" y1="-705.485" x2="-513.715" y2="-698.5" width="0.1524" layer="91"/>
<pinref part="R136" gate="G$1" pin="1"/>
<pinref part="SUPPLY39" gate="G$1" pin="DGND"/>
</segment>
<segment>
<wire x1="-493.395" y1="-706.12" x2="-493.395" y2="-699.135" width="0.1524" layer="91"/>
<pinref part="SUPPLY40" gate="G$1" pin="DGND"/>
<pinref part="R143" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="-473.075" y1="-706.755" x2="-473.075" y2="-699.135" width="0.1524" layer="91"/>
<pinref part="SUPPLY41" gate="G$1" pin="DGND"/>
<pinref part="R156" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="-565.15" y1="-707.39" x2="-568.325" y2="-707.39" width="0.1524" layer="91"/>
<wire x1="-568.325" y1="-709.295" x2="-568.325" y2="-707.39" width="0.1524" layer="91"/>
<pinref part="R142" gate="G$1" pin="1"/>
<pinref part="SUPPLY14" gate="G$1" pin="DGND"/>
</segment>
<segment>
<wire x1="-547.37" y1="-571.5" x2="-547.37" y2="-566.42" width="0.1524" layer="91"/>
<wire x1="-547.37" y1="-566.42" x2="-566.42" y2="-566.42" width="0.1524" layer="91"/>
<pinref part="SUPPLY4" gate="G$1" pin="DGND"/>
<pinref part="JP2" gate="1" pin="2"/>
</segment>
<segment>
<wire x1="224.79" y1="-633.095" x2="227.965" y2="-633.095" width="0.1524" layer="91"/>
<wire x1="227.965" y1="-633.095" x2="227.965" y2="-640.715" width="0.1524" layer="91"/>
<wire x1="227.965" y1="-640.715" x2="227.965" y2="-652.145" width="0.1524" layer="91"/>
<wire x1="224.79" y1="-628.015" x2="227.965" y2="-628.015" width="0.1524" layer="91"/>
<wire x1="227.965" y1="-628.015" x2="227.965" y2="-633.095" width="0.1524" layer="91"/>
<wire x1="224.79" y1="-640.715" x2="227.965" y2="-640.715" width="0.1524" layer="91"/>
<junction x="227.965" y="-633.095"/>
<junction x="227.965" y="-640.715"/>
<pinref part="U14" gate="A" pin="GND"/>
<pinref part="U14" gate="A" pin="GND_2"/>
<pinref part="U14" gate="A" pin="EP"/>
<pinref part="SUPPLY11" gate="G$1" pin="DGND"/>
</segment>
<segment>
<wire x1="126.365" y1="-631.825" x2="126.365" y2="-638.175" width="0.1524" layer="91"/>
<pinref part="C63" gate="G$1" pin="2"/>
<pinref part="SUPPLY8" gate="G$1" pin="DGND"/>
</segment>
<segment>
<wire x1="239.395" y1="-630.555" x2="239.395" y2="-633.095" width="0.1524" layer="91"/>
<pinref part="C64" gate="G$1" pin="2"/>
<pinref part="SUPPLY12" gate="G$1" pin="DGND"/>
</segment>
<segment>
<wire x1="160.655" y1="-651.51" x2="160.655" y2="-653.415" width="0.1524" layer="91"/>
<pinref part="C71" gate="G$1" pin="2"/>
<pinref part="SUPPLY10" gate="G$1" pin="DGND"/>
</segment>
<segment>
<wire x1="137.795" y1="-638.175" x2="137.795" y2="-631.825" width="0.1524" layer="91"/>
<pinref part="C77" gate="G$1" pin="2"/>
<pinref part="SUPPLY9" gate="G$1" pin="DGND"/>
</segment>
</net>
<net name="MOSI" class="0">
<segment>
<wire x1="-233.68" y1="-681.99" x2="-243.84" y2="-681.99" width="0.1524" layer="91"/>
<label x="-242.57" y="-681.99" size="1.778" layer="95"/>
<pinref part="$$U$6" gate="A" pin="SDATA"/>
</segment>
</net>
<net name="SIG_A_P" class="0">
<segment>
<wire x1="-112.395" y1="-430.53" x2="-94.615" y2="-430.53" width="0.1524" layer="91"/>
<wire x1="-94.615" y1="-430.53" x2="-94.615" y2="-461.01" width="0.1524" layer="91"/>
<wire x1="-94.615" y1="-461.01" x2="-129.54" y2="-461.01" width="0.1524" layer="91"/>
<wire x1="-88.9" y1="-429.26" x2="-93.345" y2="-429.26" width="0.1524" layer="91"/>
<wire x1="-93.345" y1="-429.26" x2="-94.615" y2="-430.53" width="0.1524" layer="91"/>
<junction x="-94.615" y="-430.53"/>
<pinref part="R16" gate="G$1" pin="1"/>
<pinref part="U$1" gate="G$1" pin="+OUT"/>
<pinref part="R9" gate="G$1" pin="2"/>
</segment>
</net>
<net name="SEN" class="0">
<segment>
<wire x1="-233.68" y1="-684.53" x2="-243.84" y2="-684.53" width="0.1524" layer="91"/>
<label x="-242.57" y="-684.53" size="1.778" layer="95"/>
<pinref part="$$U$6" gate="A" pin="SEN"/>
</segment>
<segment>
<wire x1="-543.56" y1="-668.02" x2="-530.86" y2="-668.02" width="0.1524" layer="91"/>
<pinref part="SEN" gate="1" pin="2"/>
</segment>
</net>
<net name="N$48" class="0">
<segment>
<wire x1="-542.29" y1="-662.94" x2="-542.29" y2="-665.48" width="0.1524" layer="91"/>
<wire x1="-542.29" y1="-665.48" x2="-543.56" y2="-665.48" width="0.1524" layer="91"/>
<wire x1="-532.13" y1="-662.94" x2="-532.13" y2="-665.48" width="0.1524" layer="91"/>
<wire x1="-532.13" y1="-665.48" x2="-542.29" y2="-665.48" width="0.1524" layer="91"/>
<junction x="-542.29" y="-665.48"/>
<pinref part="R146" gate="G$1" pin="1"/>
<pinref part="SEN" gate="1" pin="3"/>
<pinref part="R147" gate="G$1" pin="1"/>
</segment>
</net>
<net name="RESET" class="0">
<segment>
<wire x1="-233.68" y1="-676.91" x2="-243.84" y2="-676.91" width="0.1524" layer="91"/>
<label x="-242.57" y="-676.91" size="1.778" layer="95"/>
<label x="-565.15" y="-670.56" size="1.778" layer="95" rot="R90"/>
<pinref part="$$U$6" gate="A" pin="RST"/>
</segment>
<segment>
<wire x1="-563.88" y1="-661.67" x2="-563.88" y2="-670.56" width="0.1524" layer="91"/>
<pinref part="R145" gate="G$1" pin="1"/>
</segment>
</net>
<net name="INP_A" class="0">
<segment>
<wire x1="-233.68" y1="-657.86" x2="-253.365" y2="-657.86" width="0.1524" layer="91"/>
<label x="-252.095" y="-657.86" size="1.778" layer="95"/>
<pinref part="$$U$6" gate="A" pin="INP_A"/>
</segment>
<segment>
<wire x1="-50.8" y1="-429.26" x2="-44.45" y2="-429.26" width="0.1524" layer="91"/>
<pinref part="R12" gate="G$1" pin="1"/>
</segment>
</net>
<net name="INM_A" class="0">
<segment>
<wire x1="-233.68" y1="-660.4" x2="-253.365" y2="-660.4" width="0.1524" layer="91"/>
<label x="-252.095" y="-660.4" size="1.778" layer="95"/>
<pinref part="$$U$6" gate="A" pin="INM_A"/>
</segment>
<segment>
<wire x1="-50.8" y1="-417.83" x2="-44.45" y2="-417.83" width="0.1524" layer="91"/>
<pinref part="R1" gate="G$1" pin="1"/>
</segment>
</net>
<net name="INP_B" class="0">
<segment>
<wire x1="-233.68" y1="-665.48" x2="-253.365" y2="-665.48" width="0.1524" layer="91"/>
<label x="-252.095" y="-665.48" size="1.778" layer="95"/>
<pinref part="$$U$6" gate="A" pin="INP_B"/>
</segment>
<segment>
<wire x1="234.315" y1="-219.71" x2="238.76" y2="-219.71" width="0.1524" layer="91"/>
<pinref part="R3" gate="G$1" pin="1"/>
</segment>
</net>
<net name="INM_B" class="0">
<segment>
<wire x1="-233.68" y1="-668.02" x2="-253.365" y2="-668.02" width="0.1524" layer="91"/>
<label x="-252.095" y="-668.02" size="1.778" layer="95"/>
<pinref part="$$U$6" gate="A" pin="INM_B"/>
</segment>
<segment>
<wire x1="234.315" y1="-208.28" x2="238.76" y2="-208.28" width="0.1524" layer="91"/>
<pinref part="R2" gate="G$1" pin="1"/>
</segment>
</net>
<net name="VCM" class="0">
<segment>
<wire x1="-233.68" y1="-673.1" x2="-253.365" y2="-673.1" width="0.1524" layer="91"/>
<label x="-252.73" y="-673.1" size="1.778" layer="95"/>
<pinref part="$$U$6" gate="A" pin="VCM"/>
</segment>
<segment>
<wire x1="121.285" y1="-217.805" x2="121.285" y2="-216.535" width="0.1524" layer="91"/>
<wire x1="121.285" y1="-216.535" x2="140.97" y2="-215.9" width="0.1524" layer="91"/>
<wire x1="102.235" y1="-216.535" x2="109.22" y2="-216.535" width="0.1524" layer="91"/>
<wire x1="109.22" y1="-216.535" x2="121.285" y2="-216.535" width="0.1524" layer="91"/>
<wire x1="109.22" y1="-217.17" x2="109.22" y2="-216.535" width="0.1524" layer="91"/>
<junction x="121.285" y="-216.535"/>
<junction x="109.22" y="-216.535"/>
<pinref part="C18" gate="G$1" pin="1"/>
<pinref part="U$2" gate="G$1" pin="VCM"/>
<pinref part="C19" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="-149.225" y1="-426.72" x2="-149.225" y2="-425.45" width="0.1524" layer="91"/>
<wire x1="-149.225" y1="-425.45" x2="-130.81" y2="-425.45" width="0.1524" layer="91"/>
<wire x1="-173.99" y1="-425.45" x2="-164.465" y2="-425.45" width="0.1524" layer="91"/>
<wire x1="-164.465" y1="-425.45" x2="-149.225" y2="-425.45" width="0.1524" layer="91"/>
<wire x1="-164.465" y1="-426.72" x2="-164.465" y2="-425.45" width="0.1524" layer="91"/>
<junction x="-149.225" y="-425.45"/>
<junction x="-164.465" y="-425.45"/>
<pinref part="C13" gate="G$1" pin="1"/>
<pinref part="U$1" gate="G$1" pin="VCM"/>
<pinref part="C14" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$70" class="0">
<segment>
<wire x1="125.73" y1="-165.1" x2="128.27" y2="-165.1" width="0.1524" layer="91"/>
<wire x1="128.27" y1="-165.1" x2="130.81" y2="-165.1" width="0.1524" layer="91"/>
<wire x1="128.27" y1="-210.82" x2="128.27" y2="-165.1" width="0.1524" layer="91"/>
<wire x1="128.27" y1="-210.82" x2="140.97" y2="-210.82" width="0.1524" layer="91"/>
<junction x="128.27" y="-165.1"/>
<pinref part="R27" gate="G$1" pin="1"/>
<pinref part="R25" gate="G$1" pin="2"/>
<pinref part="U$2" gate="G$1" pin="+IN"/>
</segment>
</net>
<net name="N$101" class="0">
<segment>
<wire x1="88.265" y1="-211.455" x2="88.265" y2="-210.185" width="0.1524" layer="91"/>
<wire x1="92.075" y1="-210.185" x2="88.265" y2="-210.185" width="0.1524" layer="91"/>
<wire x1="90.17" y1="-167.64" x2="88.265" y2="-168.275" width="0.1524" layer="91"/>
<wire x1="88.265" y1="-168.275" x2="88.265" y2="-210.185" width="0.1524" layer="91"/>
<junction x="88.265" y="-210.185"/>
<pinref part="R40" gate="G$1" pin="1"/>
<pinref part="R41" gate="G$1" pin="2"/>
<pinref part="$$U$4" gate="A" pin="M"/>
</segment>
</net>
<net name="N$104" class="0">
<segment>
<wire x1="127" y1="-251.46" x2="129.54" y2="-251.46" width="0.1524" layer="91"/>
<wire x1="129.54" y1="-251.46" x2="130.81" y2="-251.46" width="0.1524" layer="91"/>
<wire x1="129.54" y1="-221.615" x2="129.54" y2="-251.46" width="0.1524" layer="91"/>
<wire x1="140.97" y1="-220.98" x2="129.54" y2="-221.615" width="0.1524" layer="91"/>
<junction x="129.54" y="-251.46"/>
<pinref part="R23" gate="G$1" pin="1"/>
<pinref part="R22" gate="G$1" pin="2"/>
<pinref part="U$2" gate="G$1" pin="-IN"/>
</segment>
</net>
<net name="SIG_2" class="0">
<segment>
<wire x1="-483.235" y1="-368.935" x2="-480.695" y2="-368.935" width="0.1524" layer="91"/>
<wire x1="-480.695" y1="-368.935" x2="-480.695" y2="-353.06" width="0.1524" layer="91"/>
<wire x1="-480.695" y1="-353.06" x2="-548.64" y2="-353.06" width="0.1524" layer="91"/>
<pinref part="U6" gate="A" pin="D"/>
<pinref part="SIG2" gate="G$1" pin="PIN"/>
<label x="-546.1" y="-352.425" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="74.93" y1="-162.56" x2="67.31" y2="-162.56" width="0.1524" layer="91"/>
<pinref part="R57" gate="G$1" pin="2"/>
<label x="67.31" y="-162.56" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$39" class="0">
<segment>
<wire x1="102.87" y1="-165.1" x2="105.41" y2="-165.1" width="0.1524" layer="91"/>
<wire x1="105.41" y1="-165.1" x2="113.03" y2="-165.1" width="0.1524" layer="91"/>
<wire x1="113.03" y1="-165.1" x2="115.57" y2="-165.1" width="0.1524" layer="91"/>
<wire x1="102.235" y1="-210.185" x2="106.045" y2="-210.185" width="0.1524" layer="91"/>
<wire x1="106.045" y1="-210.185" x2="105.41" y2="-165.1" width="0.1524" layer="91"/>
<wire x1="113.03" y1="-167.64" x2="113.03" y2="-165.1" width="0.1524" layer="91"/>
<junction x="105.41" y="-165.1"/>
<junction x="113.03" y="-165.1"/>
<pinref part="R27" gate="G$1" pin="2"/>
<pinref part="R41" gate="G$1" pin="1"/>
<pinref part="R26" gate="G$1" pin="1"/>
<pinref part="$$U$4" gate="A" pin="OUT"/>
</segment>
</net>
<net name="N$80" class="0">
<segment>
<wire x1="85.09" y1="-162.56" x2="90.17" y2="-162.56" width="0.1524" layer="91"/>
<pinref part="R57" gate="G$1" pin="1"/>
<pinref part="$$U$4" gate="A" pin="P"/>
</segment>
</net>
<net name="N$81" class="0">
<segment>
<wire x1="88.265" y1="-221.615" x2="88.265" y2="-222.885" width="0.1524" layer="91"/>
<wire x1="92.075" y1="-222.885" x2="88.265" y2="-222.885" width="0.1524" layer="91"/>
<wire x1="90.805" y1="-248.92" x2="88.265" y2="-248.92" width="0.1524" layer="91"/>
<wire x1="88.265" y1="-248.92" x2="88.265" y2="-222.885" width="0.1524" layer="91"/>
<junction x="88.265" y="-222.885"/>
<pinref part="R40" gate="G$1" pin="2"/>
<pinref part="R39" gate="G$1" pin="2"/>
<pinref part="$$U$4" gate="B" pin="M"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<wire x1="-143.51" y1="-378.46" x2="-140.97" y2="-378.46" width="0.1524" layer="91"/>
<wire x1="-140.97" y1="-378.46" x2="-138.43" y2="-378.46" width="0.1524" layer="91"/>
<wire x1="-130.81" y1="-420.37" x2="-140.97" y2="-420.37" width="0.1524" layer="91"/>
<wire x1="-140.97" y1="-420.37" x2="-140.97" y2="-378.46" width="0.1524" layer="91"/>
<junction x="-140.97" y="-378.46"/>
<pinref part="R21" gate="G$1" pin="1"/>
<pinref part="R10" gate="G$1" pin="2"/>
<pinref part="U$1" gate="G$1" pin="+IN"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<wire x1="-184.15" y1="-407.67" x2="-187.96" y2="-407.67" width="0.1524" layer="91"/>
<wire x1="-184.15" y1="-381" x2="-187.96" y2="-381" width="0.1524" layer="91"/>
<wire x1="-187.96" y1="-381" x2="-187.96" y2="-407.67" width="0.1524" layer="91"/>
<wire x1="-227.33" y1="-407.67" x2="-187.96" y2="-407.67" width="0.1524" layer="91"/>
<junction x="-187.96" y="-407.67"/>
<pinref part="R19" gate="G$1" pin="2"/>
<pinref part="U$3" gate="A" pin="M"/>
<pinref part="U3" gate="A" pin="D"/>
</segment>
</net>
<net name="N$25" class="0">
<segment>
<wire x1="-171.45" y1="-378.46" x2="-170.18" y2="-378.46" width="0.1524" layer="91"/>
<wire x1="-170.18" y1="-378.46" x2="-161.29" y2="-378.46" width="0.1524" layer="91"/>
<wire x1="-161.29" y1="-378.46" x2="-153.67" y2="-378.46" width="0.1524" layer="91"/>
<wire x1="-173.99" y1="-407.67" x2="-170.18" y2="-407.67" width="0.1524" layer="91"/>
<wire x1="-170.18" y1="-407.67" x2="-170.18" y2="-378.46" width="0.1524" layer="91"/>
<wire x1="-161.29" y1="-381" x2="-161.29" y2="-378.46" width="0.1524" layer="91"/>
<junction x="-170.18" y="-378.46"/>
<junction x="-161.29" y="-378.46"/>
<pinref part="R21" gate="G$1" pin="2"/>
<pinref part="R19" gate="G$1" pin="1"/>
<pinref part="R20" gate="G$1" pin="1"/>
<pinref part="U$3" gate="A" pin="OUT"/>
</segment>
</net>
<net name="N$26" class="0">
<segment>
<wire x1="-193.04" y1="-375.92" x2="-184.15" y2="-375.92" width="0.1524" layer="91"/>
<pinref part="R56" gate="G$1" pin="1"/>
<pinref part="U$3" gate="A" pin="P"/>
</segment>
</net>
<net name="+12V" class="0">
<segment>
<wire x1="-60.325" y1="-672.465" x2="-45.085" y2="-672.465" width="0.1524" layer="91"/>
<label x="-51.435" y="-672.465" size="1.778" layer="95"/>
<pinref part="ZED1" gate="-A" pin="C35"/>
</segment>
</net>
<net name="N$21" class="0">
<segment>
<wire x1="-154.94" y1="-461.01" x2="-162.56" y2="-461.01" width="0.1524" layer="91"/>
<wire x1="-162.56" y1="-461.01" x2="-168.91" y2="-461.01" width="0.1524" layer="91"/>
<wire x1="-168.91" y1="-461.01" x2="-171.45" y2="-461.01" width="0.1524" layer="91"/>
<wire x1="-173.99" y1="-450.85" x2="-168.91" y2="-450.85" width="0.1524" layer="91"/>
<wire x1="-168.91" y1="-450.85" x2="-168.91" y2="-461.01" width="0.1524" layer="91"/>
<wire x1="-162.56" y1="-458.47" x2="-162.56" y2="-461.01" width="0.1524" layer="91"/>
<junction x="-168.91" y="-461.01"/>
<junction x="-162.56" y="-461.01"/>
<pinref part="R17" gate="G$1" pin="2"/>
<pinref part="R37" gate="G$1" pin="1"/>
<pinref part="R18" gate="G$1" pin="1"/>
<pinref part="U$3" gate="B" pin="OUT"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<wire x1="-184.15" y1="-463.55" x2="-190.5" y2="-463.55" width="0.1524" layer="91"/>
<pinref part="R36" gate="G$1" pin="1"/>
<pinref part="U$3" gate="B" pin="P"/>
</segment>
</net>
<net name="N$31" class="0">
<segment>
<wire x1="116.84" y1="-251.46" x2="113.03" y2="-251.46" width="0.1524" layer="91"/>
<wire x1="113.03" y1="-251.46" x2="105.791" y2="-251.46" width="0.1524" layer="91"/>
<wire x1="105.791" y1="-251.46" x2="103.505" y2="-251.46" width="0.1524" layer="91"/>
<wire x1="102.235" y1="-222.885" x2="105.791" y2="-222.885" width="0.1524" layer="91"/>
<wire x1="105.791" y1="-222.885" x2="105.791" y2="-251.46" width="0.1524" layer="91"/>
<wire x1="113.03" y1="-247.015" x2="113.03" y2="-251.46" width="0.1524" layer="91"/>
<junction x="105.791" y="-251.46"/>
<junction x="113.03" y="-251.46"/>
<pinref part="R23" gate="G$1" pin="2"/>
<pinref part="R39" gate="G$1" pin="1"/>
<pinref part="R24" gate="G$1" pin="2"/>
<pinref part="$$U$4" gate="B" pin="OUT"/>
</segment>
</net>
<net name="N$35" class="0">
<segment>
<wire x1="85.09" y1="-254" x2="90.805" y2="-254" width="0.1524" layer="91"/>
<pinref part="R38" gate="G$1" pin="1"/>
<pinref part="$$U$4" gate="B" pin="P"/>
</segment>
</net>
<net name="N$45" class="0">
<segment>
<wire x1="-513.715" y1="-673.735" x2="-513.715" y2="-677.545" width="0.1524" layer="91"/>
<wire x1="-513.715" y1="-677.545" x2="-516.255" y2="-677.545" width="0.1524" layer="91"/>
<pinref part="R134" gate="G$1" pin="1"/>
<pinref part="CTRL3" gate="1" pin="3"/>
</segment>
</net>
<net name="N$52" class="0">
<segment>
<wire x1="-493.395" y1="-673.735" x2="-493.395" y2="-677.545" width="0.1524" layer="91"/>
<wire x1="-493.395" y1="-677.545" x2="-495.935" y2="-677.545" width="0.1524" layer="91"/>
<pinref part="R135" gate="G$1" pin="1"/>
<pinref part="CTRL2" gate="1" pin="3"/>
</segment>
</net>
<net name="N$53" class="0">
<segment>
<wire x1="-473.075" y1="-673.735" x2="-473.075" y2="-677.545" width="0.1524" layer="91"/>
<wire x1="-473.075" y1="-677.545" x2="-475.615" y2="-677.545" width="0.1524" layer="91"/>
<pinref part="R144" gate="G$1" pin="1"/>
<pinref part="CTRL1" gate="1" pin="3"/>
</segment>
</net>
<net name="CTRL1" class="0">
<segment>
<wire x1="-204.47" y1="-669.29" x2="-196.85" y2="-669.29" width="0.1524" layer="91"/>
<pinref part="$$U$6" gate="A" pin="CTRL1"/>
</segment>
<segment>
<wire x1="-516.255" y1="-680.085" x2="-504.825" y2="-680.085" width="0.1524" layer="91"/>
<pinref part="CTRL3" gate="1" pin="2"/>
</segment>
</net>
<net name="CTRL2" class="0">
<segment>
<wire x1="-204.47" y1="-671.83" x2="-196.85" y2="-671.83" width="0.1524" layer="91"/>
<pinref part="$$U$6" gate="A" pin="CTRL2"/>
</segment>
<segment>
<wire x1="-495.935" y1="-680.085" x2="-484.505" y2="-680.085" width="0.1524" layer="91"/>
<pinref part="CTRL2" gate="1" pin="2"/>
</segment>
</net>
<net name="CTRL3" class="0">
<segment>
<wire x1="-204.47" y1="-674.37" x2="-196.85" y2="-674.37" width="0.1524" layer="91"/>
<pinref part="$$U$6" gate="A" pin="CTRL3"/>
</segment>
<segment>
<wire x1="-475.615" y1="-680.085" x2="-462.915" y2="-680.085" width="0.1524" layer="91"/>
<pinref part="CTRL1" gate="1" pin="2"/>
</segment>
</net>
<net name="CLKOUTP" class="0">
<segment>
<wire x1="-204.47" y1="-661.67" x2="-196.85" y2="-661.67" width="0.1524" layer="91"/>
<label x="-201.93" y="-661.67" size="1.778" layer="95"/>
<pinref part="$$U$6" gate="A" pin="CLKOUTP"/>
</segment>
<segment>
<wire x1="-156.845" y1="-593.725" x2="-174.625" y2="-593.725" width="0.1524" layer="91"/>
<pinref part="ZED1" gate="-B" pin="H4"/>
</segment>
</net>
<net name="CLKOUTM" class="0">
<segment>
<wire x1="-204.47" y1="-664.21" x2="-196.85" y2="-664.21" width="0.1524" layer="91"/>
<pinref part="$$U$6" gate="A" pin="CLKOUTM"/>
</segment>
<segment>
<wire x1="-156.845" y1="-596.265" x2="-174.625" y2="-596.265" width="0.1524" layer="91"/>
<pinref part="ZED1" gate="-B" pin="H5"/>
</segment>
</net>
<net name="DA0M" class="0">
<segment>
<wire x1="-204.47" y1="-585.47" x2="-196.85" y2="-585.47" width="0.1524" layer="91"/>
<label x="-201.93" y="-585.47" size="1.778" layer="95"/>
<pinref part="$$U$6" gate="A" pin="DA0M"/>
</segment>
<segment>
<wire x1="-126.365" y1="-601.345" x2="-113.665" y2="-601.345" width="0.1524" layer="91"/>
<pinref part="ZED1" gate="-B" pin="G7"/>
</segment>
</net>
<net name="DA0P" class="0">
<segment>
<wire x1="-204.47" y1="-588.01" x2="-196.85" y2="-588.01" width="0.1524" layer="91"/>
<label x="-201.93" y="-588.01" size="1.778" layer="95"/>
<pinref part="$$U$6" gate="A" pin="DA0P"/>
</segment>
<segment>
<wire x1="-126.365" y1="-598.805" x2="-113.665" y2="-598.805" width="0.1524" layer="91"/>
<pinref part="ZED1" gate="-B" pin="G6"/>
</segment>
</net>
<net name="DA2M" class="0">
<segment>
<wire x1="-204.47" y1="-590.55" x2="-196.85" y2="-590.55" width="0.1524" layer="91"/>
<label x="-201.93" y="-590.55" size="1.778" layer="95"/>
<pinref part="$$U$6" gate="A" pin="DA2M"/>
</segment>
<segment>
<wire x1="-90.805" y1="-606.425" x2="-106.045" y2="-606.425" width="0.1524" layer="91"/>
<pinref part="ZED1" gate="-A" pin="D9"/>
</segment>
</net>
<net name="DA2P" class="0">
<segment>
<wire x1="-204.47" y1="-593.09" x2="-196.85" y2="-593.09" width="0.1524" layer="91"/>
<label x="-201.93" y="-593.09" size="1.778" layer="95"/>
<pinref part="$$U$6" gate="A" pin="DA2P"/>
</segment>
<segment>
<wire x1="-90.805" y1="-603.885" x2="-106.045" y2="-603.885" width="0.1524" layer="91"/>
<pinref part="ZED1" gate="-A" pin="D8"/>
</segment>
</net>
<net name="DA4M" class="0">
<segment>
<wire x1="-204.47" y1="-595.63" x2="-196.85" y2="-595.63" width="0.1524" layer="91"/>
<label x="-201.93" y="-595.63" size="1.778" layer="95"/>
<pinref part="$$U$6" gate="A" pin="DA4M"/>
</segment>
<segment>
<wire x1="-156.845" y1="-603.885" x2="-172.085" y2="-603.885" width="0.1524" layer="91"/>
<pinref part="ZED1" gate="-B" pin="H8"/>
</segment>
</net>
<net name="DA4P" class="0">
<segment>
<wire x1="-204.47" y1="-598.17" x2="-196.85" y2="-598.17" width="0.1524" layer="91"/>
<label x="-201.93" y="-598.17" size="1.778" layer="95"/>
<pinref part="$$U$6" gate="A" pin="DA4P"/>
</segment>
<segment>
<wire x1="-156.845" y1="-601.345" x2="-172.085" y2="-601.345" width="0.1524" layer="91"/>
<pinref part="ZED1" gate="-B" pin="H7"/>
</segment>
</net>
<net name="DA6M" class="0">
<segment>
<wire x1="-204.47" y1="-600.71" x2="-196.85" y2="-600.71" width="0.1524" layer="91"/>
<label x="-201.93" y="-600.71" size="1.778" layer="95"/>
<pinref part="$$U$6" gate="A" pin="DA6M"/>
</segment>
<segment>
<wire x1="-126.365" y1="-608.965" x2="-113.665" y2="-608.965" width="0.1524" layer="91"/>
<pinref part="ZED1" gate="-B" pin="G10"/>
</segment>
</net>
<net name="DA6P" class="0">
<segment>
<wire x1="-204.47" y1="-603.25" x2="-196.85" y2="-603.25" width="0.1524" layer="91"/>
<label x="-201.93" y="-603.25" size="1.778" layer="95"/>
<pinref part="$$U$6" gate="A" pin="DA6P"/>
</segment>
<segment>
<wire x1="-126.365" y1="-606.425" x2="-113.665" y2="-606.425" width="0.1524" layer="91"/>
<pinref part="ZED1" gate="-B" pin="G9"/>
</segment>
</net>
<net name="DA8M" class="0">
<segment>
<wire x1="-204.47" y1="-605.79" x2="-196.85" y2="-605.79" width="0.1524" layer="91"/>
<label x="-201.93" y="-605.79" size="1.778" layer="95"/>
<pinref part="$$U$6" gate="A" pin="DA8M"/>
</segment>
<segment>
<wire x1="-156.845" y1="-611.505" x2="-172.085" y2="-611.505" width="0.1524" layer="91"/>
<pinref part="ZED1" gate="-B" pin="H11"/>
</segment>
</net>
<net name="DA8P" class="0">
<segment>
<wire x1="-204.47" y1="-608.33" x2="-196.85" y2="-608.33" width="0.1524" layer="91"/>
<label x="-201.93" y="-608.33" size="1.778" layer="95"/>
<pinref part="$$U$6" gate="A" pin="DA8P"/>
</segment>
<segment>
<wire x1="-156.845" y1="-608.965" x2="-172.085" y2="-608.965" width="0.1524" layer="91"/>
<pinref part="ZED1" gate="-B" pin="H10"/>
</segment>
</net>
<net name="DA10M" class="0">
<segment>
<wire x1="-204.47" y1="-610.87" x2="-196.85" y2="-610.87" width="0.1524" layer="91"/>
<label x="-203.2" y="-610.87" size="1.778" layer="95"/>
<pinref part="$$U$6" gate="A" pin="DA10M"/>
</segment>
<segment>
<wire x1="-90.805" y1="-614.045" x2="-106.045" y2="-614.045" width="0.1524" layer="91"/>
<pinref part="ZED1" gate="-A" pin="D12"/>
</segment>
</net>
<net name="DA10P" class="0">
<segment>
<wire x1="-204.47" y1="-613.41" x2="-196.85" y2="-613.41" width="0.1524" layer="91"/>
<label x="-203.2" y="-613.41" size="1.778" layer="95"/>
<pinref part="$$U$6" gate="A" pin="DA10P"/>
</segment>
<segment>
<wire x1="-90.805" y1="-611.505" x2="-106.045" y2="-611.505" width="0.1524" layer="91"/>
<pinref part="ZED1" gate="-A" pin="D11"/>
</segment>
</net>
<net name="DA12M" class="0">
<segment>
<wire x1="-204.47" y1="-615.95" x2="-196.85" y2="-615.95" width="0.1524" layer="91"/>
<label x="-203.2" y="-615.95" size="1.778" layer="95"/>
<pinref part="$$U$6" gate="A" pin="DA12M"/>
</segment>
<segment>
<wire x1="-60.325" y1="-611.505" x2="-42.545" y2="-611.505" width="0.1524" layer="91"/>
<pinref part="ZED1" gate="-A" pin="C11"/>
</segment>
</net>
<net name="DA12P" class="0">
<segment>
<wire x1="-204.47" y1="-618.49" x2="-196.85" y2="-618.49" width="0.1524" layer="91"/>
<label x="-203.2" y="-618.49" size="1.778" layer="95"/>
<pinref part="$$U$6" gate="A" pin="DA12P"/>
</segment>
<segment>
<wire x1="-60.325" y1="-608.965" x2="-42.545" y2="-608.965" width="0.1524" layer="91"/>
<pinref part="ZED1" gate="-A" pin="C10"/>
</segment>
</net>
<net name="DB0M" class="0">
<segment>
<wire x1="-204.47" y1="-623.57" x2="-196.85" y2="-623.57" width="0.1524" layer="91"/>
<label x="-201.93" y="-623.57" size="1.778" layer="95"/>
<pinref part="$$U$6" gate="A" pin="DB0M"/>
</segment>
<segment>
<wire x1="-156.845" y1="-619.125" x2="-172.085" y2="-619.125" width="0.1524" layer="91"/>
<pinref part="ZED1" gate="-B" pin="H14"/>
</segment>
</net>
<net name="DB2M" class="0">
<segment>
<wire x1="-204.47" y1="-628.65" x2="-196.85" y2="-628.65" width="0.1524" layer="91"/>
<label x="-201.93" y="-628.65" size="1.778" layer="95"/>
<pinref part="$$U$6" gate="A" pin="DB2M"/>
</segment>
<segment>
<wire x1="-126.365" y1="-616.585" x2="-113.665" y2="-616.585" width="0.1524" layer="91"/>
<pinref part="ZED1" gate="-B" pin="G13"/>
</segment>
</net>
<net name="DB2P" class="0">
<segment>
<wire x1="-204.47" y1="-631.19" x2="-196.85" y2="-631.19" width="0.1524" layer="91"/>
<label x="-201.93" y="-631.19" size="1.778" layer="95"/>
<pinref part="$$U$6" gate="A" pin="DB2P"/>
</segment>
<segment>
<wire x1="-126.365" y1="-614.045" x2="-113.665" y2="-614.045" width="0.1524" layer="91"/>
<pinref part="ZED1" gate="-B" pin="G12"/>
</segment>
</net>
<net name="DB4M" class="0">
<segment>
<wire x1="-204.47" y1="-633.73" x2="-196.85" y2="-633.73" width="0.1524" layer="91"/>
<label x="-201.93" y="-633.73" size="1.778" layer="95"/>
<pinref part="$$U$6" gate="A" pin="DB4M"/>
</segment>
<segment>
<wire x1="-90.805" y1="-621.665" x2="-106.045" y2="-621.665" width="0.1524" layer="91"/>
<pinref part="ZED1" gate="-A" pin="D15"/>
</segment>
</net>
<net name="DB4P" class="0">
<segment>
<wire x1="-204.47" y1="-636.27" x2="-196.85" y2="-636.27" width="0.1524" layer="91"/>
<pinref part="$$U$6" gate="A" pin="DB4P"/>
</segment>
<segment>
<wire x1="-90.805" y1="-619.125" x2="-106.045" y2="-619.125" width="0.1524" layer="91"/>
<pinref part="ZED1" gate="-A" pin="D14"/>
</segment>
</net>
<net name="DB6M" class="0">
<segment>
<wire x1="-204.47" y1="-638.81" x2="-196.85" y2="-638.81" width="0.1524" layer="91"/>
<label x="-201.93" y="-638.81" size="1.778" layer="95"/>
<pinref part="$$U$6" gate="A" pin="DB6M"/>
</segment>
<segment>
<wire x1="-60.325" y1="-621.665" x2="-42.545" y2="-621.665" width="0.1524" layer="91"/>
<pinref part="ZED1" gate="-A" pin="C15"/>
</segment>
</net>
<net name="DB6P" class="0">
<segment>
<wire x1="-204.47" y1="-641.35" x2="-196.85" y2="-641.35" width="0.1524" layer="91"/>
<label x="-201.93" y="-641.35" size="1.778" layer="95"/>
<pinref part="$$U$6" gate="A" pin="DB6P"/>
</segment>
<segment>
<wire x1="-60.325" y1="-619.125" x2="-42.545" y2="-619.125" width="0.1524" layer="91"/>
<pinref part="ZED1" gate="-A" pin="C14"/>
</segment>
</net>
<net name="DB8M" class="0">
<segment>
<wire x1="-204.47" y1="-643.89" x2="-196.85" y2="-643.89" width="0.1524" layer="91"/>
<label x="-201.93" y="-643.89" size="1.778" layer="95"/>
<pinref part="$$U$6" gate="A" pin="DB8M"/>
</segment>
<segment>
<wire x1="-156.845" y1="-626.745" x2="-172.085" y2="-626.745" width="0.1524" layer="91"/>
<pinref part="ZED1" gate="-B" pin="H17"/>
</segment>
</net>
<net name="DB8P" class="0">
<segment>
<wire x1="-204.47" y1="-646.43" x2="-196.85" y2="-646.43" width="0.1524" layer="91"/>
<label x="-201.93" y="-646.43" size="1.778" layer="95"/>
<pinref part="$$U$6" gate="A" pin="DB8P"/>
</segment>
<segment>
<wire x1="-156.845" y1="-624.205" x2="-172.085" y2="-624.205" width="0.1524" layer="91"/>
<pinref part="ZED1" gate="-B" pin="H16"/>
</segment>
</net>
<net name="DB10M" class="0">
<segment>
<wire x1="-204.47" y1="-648.97" x2="-196.85" y2="-648.97" width="0.1524" layer="91"/>
<label x="-201.93" y="-648.97" size="1.778" layer="95"/>
<pinref part="$$U$6" gate="A" pin="DB10M"/>
</segment>
<segment>
<wire x1="-126.365" y1="-624.205" x2="-113.665" y2="-624.205" width="0.1524" layer="91"/>
<pinref part="ZED1" gate="-B" pin="G16"/>
</segment>
</net>
<net name="DB10P" class="0">
<segment>
<wire x1="-204.47" y1="-651.51" x2="-196.85" y2="-651.51" width="0.1524" layer="91"/>
<label x="-201.93" y="-651.51" size="1.778" layer="95"/>
<pinref part="$$U$6" gate="A" pin="DB10P"/>
</segment>
<segment>
<wire x1="-126.365" y1="-621.665" x2="-113.665" y2="-621.665" width="0.1524" layer="91"/>
<pinref part="ZED1" gate="-B" pin="G15"/>
</segment>
</net>
<net name="DB12M" class="0">
<segment>
<wire x1="-204.47" y1="-654.05" x2="-196.85" y2="-654.05" width="0.1524" layer="91"/>
<label x="-201.93" y="-654.05" size="1.778" layer="95"/>
<pinref part="$$U$6" gate="A" pin="DB12M"/>
</segment>
<segment>
<wire x1="-90.805" y1="-629.285" x2="-106.045" y2="-629.285" width="0.1524" layer="91"/>
<pinref part="ZED1" gate="-A" pin="D18"/>
</segment>
</net>
<net name="DB12P" class="0">
<segment>
<wire x1="-204.47" y1="-656.59" x2="-196.85" y2="-656.59" width="0.1524" layer="91"/>
<label x="-201.93" y="-656.59" size="1.778" layer="95"/>
<pinref part="$$U$6" gate="A" pin="DB12P"/>
</segment>
<segment>
<wire x1="-90.805" y1="-626.745" x2="-106.045" y2="-626.745" width="0.1524" layer="91"/>
<pinref part="ZED1" gate="-A" pin="D17"/>
</segment>
</net>
<net name="DB0P" class="0">
<segment>
<wire x1="-156.845" y1="-616.585" x2="-172.085" y2="-616.585" width="0.1524" layer="91"/>
<pinref part="ZED1" gate="-B" pin="H13"/>
</segment>
<segment>
<wire x1="-204.47" y1="-626.11" x2="-196.85" y2="-626.11" width="0.1524" layer="91"/>
<label x="-201.93" y="-626.11" size="1.778" layer="95"/>
<pinref part="$$U$6" gate="A" pin="DB0P"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<wire x1="160.02" y1="-205.74" x2="173.99" y2="-205.74" width="0.1524" layer="91"/>
<wire x1="173.99" y1="-205.74" x2="173.99" y2="-165.1" width="0.1524" layer="91"/>
<wire x1="173.99" y1="-165.1" x2="140.97" y2="-165.1" width="0.1524" layer="91"/>
<wire x1="188.595" y1="-208.28" x2="176.53" y2="-208.28" width="0.1524" layer="91"/>
<wire x1="176.53" y1="-208.28" x2="173.99" y2="-205.74" width="0.1524" layer="91"/>
<junction x="173.99" y="-205.74"/>
<pinref part="U$2" gate="G$1" pin="-OUT"/>
<pinref part="R25" gate="G$1" pin="1"/>
<pinref part="R14" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<wire x1="174.625" y1="-251.46" x2="140.97" y2="-251.46" width="0.1524" layer="91"/>
<wire x1="159.385" y1="-220.98" x2="174.625" y2="-221.615" width="0.1524" layer="91"/>
<wire x1="174.625" y1="-221.615" x2="174.625" y2="-251.46" width="0.1524" layer="91"/>
<wire x1="188.595" y1="-219.71" x2="176.53" y2="-219.71" width="0.1524" layer="91"/>
<wire x1="176.53" y1="-219.71" x2="174.625" y2="-221.615" width="0.1524" layer="91"/>
<junction x="174.625" y="-221.615"/>
<pinref part="R22" gate="G$1" pin="1"/>
<pinref part="U$2" gate="G$1" pin="+OUT"/>
<pinref part="R13" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$82" class="0">
<segment>
<wire x1="-144.78" y1="-461.01" x2="-142.24" y2="-461.01" width="0.1524" layer="91"/>
<wire x1="-142.24" y1="-461.01" x2="-139.7" y2="-461.01" width="0.1524" layer="91"/>
<wire x1="-130.81" y1="-430.53" x2="-142.24" y2="-430.53" width="0.1524" layer="91"/>
<wire x1="-142.24" y1="-430.53" x2="-142.24" y2="-461.01" width="0.1524" layer="91"/>
<junction x="-142.24" y="-461.01"/>
<pinref part="U$1" gate="G$1" pin="-IN"/>
<pinref part="R17" gate="G$1" pin="1"/>
<pinref part="R16" gate="G$1" pin="2"/>
</segment>
</net>
<net name="SIG_A_M" class="0">
<segment>
<wire x1="-128.27" y1="-378.46" x2="-95.25" y2="-378.46" width="0.1524" layer="91"/>
<wire x1="-95.25" y1="-378.46" x2="-95.25" y2="-415.29" width="0.1524" layer="91"/>
<wire x1="-95.25" y1="-415.29" x2="-111.76" y2="-415.29" width="0.1524" layer="91"/>
<wire x1="-88.9" y1="-417.83" x2="-92.71" y2="-417.83" width="0.1524" layer="91"/>
<wire x1="-92.71" y1="-417.83" x2="-95.25" y2="-415.29" width="0.1524" layer="91"/>
<junction x="-95.25" y="-415.29"/>
<pinref part="R10" gate="G$1" pin="1"/>
<pinref part="U$1" gate="G$1" pin="-OUT"/>
<pinref part="R11" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<wire x1="-78.74" y1="-417.83" x2="-77.47" y2="-417.83" width="0.1524" layer="91"/>
<pinref part="R11" gate="G$1" pin="1"/>
<pinref part="L4" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<wire x1="-78.74" y1="-429.26" x2="-77.47" y2="-429.26" width="0.1524" layer="91"/>
<pinref part="R9" gate="G$1" pin="1"/>
<pinref part="L1" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<wire x1="198.755" y1="-208.28" x2="200.025" y2="-208.28" width="0.1524" layer="91"/>
<pinref part="R14" gate="G$1" pin="1"/>
<pinref part="L3" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<wire x1="198.755" y1="-219.71" x2="200.025" y2="-219.71" width="0.1524" layer="91"/>
<pinref part="R13" gate="G$1" pin="1"/>
<pinref part="L2" gate="G$1" pin="2"/>
</segment>
</net>
<net name="V_42_P" class="0">
<segment>
<wire x1="-556.26" y1="-591.82" x2="-552.45" y2="-591.82" width="0.1524" layer="91"/>
<wire x1="-552.45" y1="-591.82" x2="-551.18" y2="-591.82" width="0.1524" layer="91"/>
<wire x1="-566.42" y1="-563.88" x2="-552.45" y2="-563.88" width="0.1524" layer="91"/>
<wire x1="-552.45" y1="-563.88" x2="-552.45" y2="-591.82" width="0.1524" layer="91"/>
<junction x="-552.45" y="-591.82"/>
<pinref part="JP2" gate="1" pin="3"/>
</segment>
<segment>
<wire x1="-509.27" y1="-570.865" x2="-514.35" y2="-570.865" width="0.1524" layer="91"/>
<pinref part="R8" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="15.875" y1="-681.355" x2="6.35" y2="-681.355" width="0.1524" layer="91"/>
<pinref part="R139" gate="G$1" pin="1"/>
</segment>
</net>
<net name="V_42_M" class="0">
<segment>
<wire x1="-554.99" y1="-623.57" x2="-551.18" y2="-623.57" width="0.1524" layer="91"/>
<wire x1="-556.26" y1="-621.03" x2="-554.99" y2="-621.03" width="0.1524" layer="91"/>
<wire x1="-554.99" y1="-621.03" x2="-554.99" y2="-623.57" width="0.1524" layer="91"/>
<wire x1="-566.42" y1="-568.96" x2="-554.99" y2="-568.96" width="0.1524" layer="91"/>
<wire x1="-554.99" y1="-568.96" x2="-554.99" y2="-621.03" width="0.1524" layer="91"/>
<junction x="-554.99" y="-621.03"/>
<pinref part="JP2" gate="1" pin="1"/>
</segment>
<segment>
<wire x1="9.525" y1="-574.675" x2="5.715" y2="-574.675" width="0.1524" layer="91"/>
<pinref part="R6" gate="G$1" pin="1"/>
</segment>
</net>
<net name="AVDD" class="0">
<segment>
<wire x1="-358.775" y1="-570.865" x2="-346.71" y2="-570.865" width="0.1524" layer="91"/>
<pinref part="R15" gate="G$1" pin="2"/>
</segment>
<segment>
<wire x1="-465.455" y1="-581.025" x2="-471.17" y2="-581.025" width="0.1524" layer="91"/>
<pinref part="R7" gate="G$1" pin="2"/>
</segment>
<segment>
<wire x1="-451.485" y1="-586.105" x2="-471.17" y2="-586.105" width="0.1524" layer="91"/>
<pinref part="U2" gate="A" pin="SENSE/ADJ"/>
</segment>
<segment>
<wire x1="-247.65" y1="-580.39" x2="-255.27" y2="-580.39" width="0.1524" layer="91"/>
<wire x1="-255.27" y1="-580.39" x2="-255.27" y2="-581.66" width="0.1524" layer="91"/>
<wire x1="-247.65" y1="-581.66" x2="-247.65" y2="-580.39" width="0.1524" layer="91"/>
<wire x1="-266.7" y1="-581.66" x2="-266.7" y2="-580.39" width="0.1524" layer="91"/>
<wire x1="-266.7" y1="-580.39" x2="-255.27" y2="-580.39" width="0.1524" layer="91"/>
<wire x1="-233.68" y1="-585.47" x2="-234.95" y2="-585.47" width="0.1524" layer="91"/>
<wire x1="-234.95" y1="-585.47" x2="-234.95" y2="-588.01" width="0.1524" layer="91"/>
<wire x1="-233.68" y1="-588.01" x2="-234.95" y2="-588.01" width="0.1524" layer="91"/>
<wire x1="-234.95" y1="-588.01" x2="-234.95" y2="-590.55" width="0.1524" layer="91"/>
<wire x1="-234.95" y1="-590.55" x2="-233.68" y2="-590.55" width="0.1524" layer="91"/>
<wire x1="-234.95" y1="-585.47" x2="-234.95" y2="-580.39" width="0.1524" layer="91"/>
<wire x1="-234.95" y1="-580.39" x2="-247.65" y2="-580.39" width="0.1524" layer="91"/>
<wire x1="-266.7" y1="-580.39" x2="-266.7" y2="-571.5" width="0.1524" layer="91"/>
<junction x="-247.65" y="-580.39"/>
<junction x="-255.27" y="-580.39"/>
<junction x="-234.95" y="-588.01"/>
<junction x="-234.95" y="-585.47"/>
<junction x="-266.7" y="-580.39"/>
<pinref part="C69" gate="G$1" pin="1"/>
<pinref part="C66" gate="G$1" pin="1"/>
<pinref part="C65" gate="G$1" pin="1"/>
<pinref part="$$U$6" gate="A" pin="AVDD_0"/>
<pinref part="$$U$6" gate="A" pin="AVDD_1"/>
<pinref part="$$U$6" gate="A" pin="AVDD_2"/>
</segment>
<segment>
<wire x1="-107.95" y1="-388.62" x2="-107.95" y2="-389.89" width="0.1524" layer="91"/>
<wire x1="-107.95" y1="-388.62" x2="-113.03" y2="-388.62" width="0.1524" layer="91"/>
<wire x1="-113.03" y1="-388.62" x2="-118.11" y2="-388.62" width="0.1524" layer="91"/>
<wire x1="-118.11" y1="-388.62" x2="-118.11" y2="-389.89" width="0.1524" layer="91"/>
<wire x1="-118.11" y1="-388.62" x2="-123.19" y2="-388.62" width="0.1524" layer="91"/>
<wire x1="-123.19" y1="-407.035" x2="-123.19" y2="-402.59" width="0.1524" layer="91"/>
<wire x1="-123.19" y1="-402.59" x2="-123.19" y2="-388.62" width="0.1524" layer="91"/>
<wire x1="-130.81" y1="-415.29" x2="-133.985" y2="-415.29" width="0.1524" layer="91"/>
<wire x1="-133.985" y1="-415.29" x2="-133.985" y2="-402.59" width="0.1524" layer="91"/>
<wire x1="-133.985" y1="-402.59" x2="-123.19" y2="-402.59" width="0.1524" layer="91"/>
<wire x1="-100.33" y1="-419.1" x2="-100.33" y2="-388.62" width="0.1524" layer="91"/>
<wire x1="-100.33" y1="-388.62" x2="-107.95" y2="-388.62" width="0.1524" layer="91"/>
<wire x1="-113.03" y1="-388.62" x2="-113.03" y2="-379.73" width="0.1524" layer="91"/>
<junction x="-118.11" y="-388.62"/>
<junction x="-123.19" y="-402.59"/>
<junction x="-107.95" y="-388.62"/>
<junction x="-113.03" y="-388.62"/>
<pinref part="C16" gate="G$1" pin="1"/>
<pinref part="C15" gate="G$1" pin="1"/>
<pinref part="U$1" gate="G$1" pin="V+"/>
<pinref part="U$1" gate="G$1" pin="EN"/>
<pinref part="C17" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="163.83" y1="-180.34" x2="163.83" y2="-182.88" width="0.1524" layer="91"/>
<wire x1="163.83" y1="-180.34" x2="158.75" y2="-180.34" width="0.1524" layer="91"/>
<wire x1="158.75" y1="-180.34" x2="153.67" y2="-180.34" width="0.1524" layer="91"/>
<wire x1="153.67" y1="-180.34" x2="153.67" y2="-182.88" width="0.1524" layer="91"/>
<wire x1="153.67" y1="-180.34" x2="148.59" y2="-180.34" width="0.1524" layer="91"/>
<wire x1="148.59" y1="-197.485" x2="148.59" y2="-193.04" width="0.1524" layer="91"/>
<wire x1="148.59" y1="-193.04" x2="148.59" y2="-180.34" width="0.1524" layer="91"/>
<wire x1="140.97" y1="-205.74" x2="135.89" y2="-205.74" width="0.1524" layer="91"/>
<wire x1="135.89" y1="-205.74" x2="135.89" y2="-193.04" width="0.1524" layer="91"/>
<wire x1="135.89" y1="-193.04" x2="148.59" y2="-193.04" width="0.1524" layer="91"/>
<wire x1="170.18" y1="-210.185" x2="170.18" y2="-180.34" width="0.1524" layer="91"/>
<wire x1="170.18" y1="-180.34" x2="163.83" y2="-180.34" width="0.1524" layer="91"/>
<wire x1="158.75" y1="-180.34" x2="158.75" y2="-170.18" width="0.1524" layer="91"/>
<junction x="153.67" y="-180.34"/>
<junction x="148.59" y="-193.04"/>
<junction x="163.83" y="-180.34"/>
<junction x="158.75" y="-180.34"/>
<pinref part="C22" gate="G$1" pin="1"/>
<pinref part="C21" gate="G$1" pin="1"/>
<pinref part="U$2" gate="G$1" pin="V+"/>
<pinref part="U$2" gate="G$1" pin="EN"/>
<pinref part="C24" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="-441.96" y1="-676.275" x2="-436.88" y2="-676.275" width="0.1524" layer="91"/>
<wire x1="-441.96" y1="-676.275" x2="-441.96" y2="-668.02" width="0.1524" layer="91"/>
<wire x1="-441.96" y1="-676.275" x2="-441.96" y2="-682.625" width="0.1524" layer="91"/>
<wire x1="-441.96" y1="-682.625" x2="-447.04" y2="-682.625" width="0.1524" layer="91"/>
<wire x1="-424.18" y1="-676.275" x2="-436.88" y2="-676.275" width="0.1524" layer="91"/>
<wire x1="-447.04" y1="-682.625" x2="-447.04" y2="-681.99" width="0.1524" layer="91"/>
<junction x="-441.96" y="-676.275"/>
<junction x="-436.88" y="-676.275"/>
<pinref part="C27" gate="G$1" pin="1"/>
<pinref part="C26" gate="G$1" pin="1"/>
<pinref part="U$3" gate="C" pin="AVDD"/>
</segment>
<segment>
<wire x1="-398.78" y1="-674.37" x2="-398.78" y2="-666.115" width="0.1524" layer="91"/>
<wire x1="-393.7" y1="-676.275" x2="-393.7" y2="-674.37" width="0.1524" layer="91"/>
<wire x1="-393.7" y1="-674.37" x2="-398.78" y2="-674.37" width="0.1524" layer="91"/>
<wire x1="-404.495" y1="-684.53" x2="-398.78" y2="-684.53" width="0.1524" layer="91"/>
<wire x1="-398.78" y1="-684.53" x2="-398.78" y2="-674.37" width="0.1524" layer="91"/>
<wire x1="-383.54" y1="-676.275" x2="-383.54" y2="-674.37" width="0.1524" layer="91"/>
<wire x1="-383.54" y1="-674.37" x2="-393.7" y2="-674.37" width="0.1524" layer="91"/>
<wire x1="-404.495" y1="-684.53" x2="-404.495" y2="-683.895" width="0.1524" layer="91"/>
<junction x="-398.78" y="-674.37"/>
<junction x="-393.7" y="-674.37"/>
<pinref part="C31" gate="G$1" pin="1"/>
<pinref part="C30" gate="G$1" pin="1"/>
<pinref part="$$U$4" gate="C" pin="AVDD"/>
</segment>
<segment>
<wire x1="-134.62" y1="-161.29" x2="-139.7" y2="-161.29" width="0.1524" layer="91"/>
<label x="-137.16" y="-161.29" size="1.778" layer="95"/>
<pinref part="U8" gate="A" pin="V+"/>
<pinref part="C50" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="194.31" y1="-440.69" x2="196.85" y2="-440.69" width="0.1524" layer="91"/>
<wire x1="196.85" y1="-440.69" x2="199.39" y2="-440.69" width="0.1524" layer="91"/>
<wire x1="196.85" y1="-438.15" x2="196.85" y2="-440.69" width="0.1524" layer="91"/>
<junction x="196.85" y="-440.69"/>
<label x="199.39" y="-440.69" size="1.778" layer="95"/>
<pinref part="U7" gate="A" pin="VDD"/>
<pinref part="C40" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="194.31" y1="-365.76" x2="196.85" y2="-365.76" width="0.1524" layer="91"/>
<wire x1="196.85" y1="-365.76" x2="199.39" y2="-365.76" width="0.1524" layer="91"/>
<wire x1="196.85" y1="-361.95" x2="196.85" y2="-365.76" width="0.1524" layer="91"/>
<junction x="196.85" y="-365.76"/>
<label x="199.39" y="-365.76" size="1.778" layer="95"/>
<pinref part="U5" gate="A" pin="VDD"/>
<pinref part="C35" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="-135.89" y1="-233.045" x2="-140.97" y2="-233.045" width="0.1524" layer="91"/>
<label x="-138.43" y="-233.045" size="1.778" layer="95"/>
<pinref part="U9" gate="A" pin="V+"/>
<pinref part="C48" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="-415.29" y1="-165.735" x2="-420.37" y2="-165.735" width="0.1524" layer="91"/>
<label x="-417.83" y="-165.735" size="1.778" layer="95"/>
<pinref part="U10" gate="A" pin="V+"/>
<pinref part="C46" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="-421.005" y1="-231.14" x2="-415.29" y2="-231.14" width="0.1524" layer="91"/>
<label x="-417.83" y="-231.14" size="1.778" layer="95"/>
<pinref part="U11" gate="A" pin="V+"/>
<pinref part="C44" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="-262.89" y1="-407.67" x2="-265.43" y2="-407.67" width="0.1524" layer="91"/>
<wire x1="-265.43" y1="-407.67" x2="-279.4" y2="-407.67" width="0.1524" layer="91"/>
<wire x1="-265.43" y1="-405.13" x2="-265.43" y2="-407.67" width="0.1524" layer="91"/>
<junction x="-265.43" y="-407.67"/>
<label x="-279.4" y="-407.67" size="1.778" layer="95"/>
<pinref part="U3" gate="A" pin="VDD"/>
<pinref part="C36" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="-518.795" y1="-368.935" x2="-521.335" y2="-368.935" width="0.1524" layer="91"/>
<wire x1="-521.335" y1="-368.935" x2="-535.305" y2="-368.935" width="0.1524" layer="91"/>
<wire x1="-521.335" y1="-366.395" x2="-521.335" y2="-368.935" width="0.1524" layer="91"/>
<junction x="-521.335" y="-368.935"/>
<label x="-535.305" y="-368.935" size="1.778" layer="95"/>
<pinref part="U6" gate="A" pin="VDD"/>
<pinref part="C42" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="-518.795" y1="-434.975" x2="-527.05" y2="-434.975" width="0.1524" layer="91"/>
<wire x1="-527.05" y1="-434.975" x2="-535.305" y2="-434.975" width="0.1524" layer="91"/>
<wire x1="-527.05" y1="-432.435" x2="-527.05" y2="-434.975" width="0.1524" layer="91"/>
<junction x="-527.05" y="-434.975"/>
<label x="-535.305" y="-434.975" size="1.778" layer="95"/>
<pinref part="U4" gate="A" pin="VDD"/>
<pinref part="C38" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$60" class="0">
<segment>
<wire x1="-457.835" y1="-591.185" x2="-457.835" y2="-594.36" width="0.1524" layer="91"/>
<wire x1="-451.485" y1="-591.185" x2="-457.835" y2="-591.185" width="0.1524" layer="91"/>
<pinref part="C11" gate="G$1" pin="1"/>
<pinref part="U2" gate="A" pin="SS"/>
</segment>
</net>
<net name="N$47" class="0">
<segment>
<wire x1="67.31" y1="-691.515" x2="64.135" y2="-691.515" width="0.1524" layer="91"/>
<pinref part="R140" gate="G$1" pin="1"/>
<pinref part="U13" gate="A" pin="PG"/>
</segment>
</net>
<net name="N$50" class="0">
<segment>
<wire x1="67.31" y1="-701.675" x2="64.135" y2="-701.675" width="0.1524" layer="91"/>
<wire x1="64.135" y1="-701.675" x2="64.135" y2="-704.85" width="0.1524" layer="91"/>
<pinref part="C61" gate="G$1" pin="1"/>
<pinref part="U13" gate="A" pin="SS"/>
</segment>
</net>
<net name="DRVDD" class="0">
<segment>
<wire x1="142.875" y1="-681.355" x2="142.875" y2="-683.895" width="0.1524" layer="91"/>
<wire x1="142.875" y1="-681.355" x2="151.13" y2="-681.355" width="0.1524" layer="91"/>
<wire x1="142.875" y1="-681.355" x2="140.97" y2="-681.355" width="0.1524" layer="91"/>
<junction x="142.875" y="-681.355"/>
<pinref part="C70" gate="G$1" pin="1"/>
<pinref part="R132" gate="G$1" pin="2"/>
</segment>
<segment>
<wire x1="53.975" y1="-691.515" x2="48.26" y2="-691.515" width="0.1524" layer="91"/>
<pinref part="R140" gate="G$1" pin="2"/>
</segment>
<segment>
<wire x1="67.31" y1="-696.595" x2="48.26" y2="-696.595" width="0.1524" layer="91"/>
<pinref part="U13" gate="A" pin="SENSE/ADJ"/>
</segment>
<segment>
<wire x1="-247.65" y1="-621.03" x2="-247.65" y2="-619.76" width="0.1524" layer="91"/>
<wire x1="-247.65" y1="-619.76" x2="-255.27" y2="-619.76" width="0.1524" layer="91"/>
<wire x1="-255.27" y1="-619.76" x2="-255.27" y2="-621.03" width="0.1524" layer="91"/>
<wire x1="-276.86" y1="-621.03" x2="-276.86" y2="-619.76" width="0.1524" layer="91"/>
<wire x1="-276.86" y1="-619.76" x2="-264.16" y2="-619.76" width="0.1524" layer="91"/>
<wire x1="-264.16" y1="-619.76" x2="-255.27" y2="-619.76" width="0.1524" layer="91"/>
<wire x1="-234.95" y1="-619.76" x2="-240.03" y2="-619.76" width="0.1524" layer="91"/>
<wire x1="-240.03" y1="-619.76" x2="-247.65" y2="-619.76" width="0.1524" layer="91"/>
<wire x1="-234.95" y1="-629.92" x2="-233.68" y2="-629.92" width="0.1524" layer="91"/>
<wire x1="-234.95" y1="-627.38" x2="-234.95" y2="-629.92" width="0.1524" layer="91"/>
<wire x1="-233.68" y1="-627.38" x2="-234.95" y2="-627.38" width="0.1524" layer="91"/>
<wire x1="-233.68" y1="-622.3" x2="-234.95" y2="-622.3" width="0.1524" layer="91"/>
<wire x1="-234.95" y1="-622.3" x2="-234.95" y2="-624.84" width="0.1524" layer="91"/>
<wire x1="-234.95" y1="-624.84" x2="-234.95" y2="-627.38" width="0.1524" layer="91"/>
<wire x1="-233.68" y1="-624.84" x2="-234.95" y2="-624.84" width="0.1524" layer="91"/>
<wire x1="-234.95" y1="-619.76" x2="-234.95" y2="-622.3" width="0.1524" layer="91"/>
<wire x1="-264.16" y1="-621.03" x2="-264.16" y2="-619.76" width="0.1524" layer="91"/>
<wire x1="-276.86" y1="-619.76" x2="-276.86" y2="-609.6" width="0.1524" layer="91"/>
<wire x1="-240.03" y1="-621.03" x2="-240.03" y2="-619.76" width="0.1524" layer="91"/>
<junction x="-255.27" y="-619.76"/>
<junction x="-247.65" y="-619.76"/>
<junction x="-234.95" y="-627.38"/>
<junction x="-234.95" y="-624.84"/>
<junction x="-234.95" y="-622.3"/>
<junction x="-264.16" y="-619.76"/>
<junction x="-276.86" y="-619.76"/>
<junction x="-240.03" y="-619.76"/>
<pinref part="C74" gate="G$1" pin="1"/>
<pinref part="C79" gate="G$1" pin="1"/>
<pinref part="C75" gate="G$1" pin="1"/>
<pinref part="C1" gate="G$1" pin="1"/>
<pinref part="$$U$6" gate="A" pin="DRVDD0"/>
<pinref part="$$U$6" gate="A" pin="DRVDD1"/>
<pinref part="$$U$6" gate="A" pin="DRVDD2"/>
<pinref part="$$U$6" gate="A" pin="DRVDD3"/>
<pinref part="C76" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="-473.075" y1="-661.035" x2="-473.075" y2="-663.575" width="0.1524" layer="91"/>
<wire x1="-513.715" y1="-661.035" x2="-513.715" y2="-663.575" width="0.1524" layer="91"/>
<wire x1="-473.075" y1="-661.035" x2="-493.395" y2="-661.035" width="0.1524" layer="91"/>
<wire x1="-493.395" y1="-661.035" x2="-513.715" y2="-661.035" width="0.1524" layer="91"/>
<wire x1="-513.715" y1="-661.035" x2="-513.715" y2="-648.97" width="0.1524" layer="91"/>
<wire x1="-493.395" y1="-663.575" x2="-493.395" y2="-661.035" width="0.1524" layer="91"/>
<junction x="-513.715" y="-661.035"/>
<junction x="-493.395" y="-661.035"/>
<pinref part="R144" gate="G$1" pin="2"/>
<pinref part="R134" gate="G$1" pin="2"/>
<pinref part="R135" gate="G$1" pin="2"/>
</segment>
<segment>
<wire x1="-439.42" y1="-622.3" x2="-443.23" y2="-622.3" width="0.1524" layer="91"/>
<wire x1="-443.23" y1="-622.3" x2="-447.04" y2="-622.3" width="0.1524" layer="91"/>
<wire x1="-447.04" y1="-622.3" x2="-454.66" y2="-622.3" width="0.1524" layer="91"/>
<wire x1="-454.66" y1="-622.3" x2="-462.28" y2="-622.3" width="0.1524" layer="91"/>
<wire x1="-462.28" y1="-622.3" x2="-469.9" y2="-622.3" width="0.1524" layer="91"/>
<wire x1="-469.9" y1="-622.3" x2="-476.885" y2="-622.3" width="0.1524" layer="91"/>
<wire x1="-476.885" y1="-622.3" x2="-483.87" y2="-622.3" width="0.1524" layer="91"/>
<wire x1="-483.87" y1="-622.3" x2="-491.49" y2="-622.3" width="0.1524" layer="91"/>
<wire x1="-491.49" y1="-622.3" x2="-491.49" y2="-623.57" width="0.1524" layer="91"/>
<wire x1="-483.87" y1="-624.205" x2="-483.87" y2="-623.57" width="0.1524" layer="91"/>
<wire x1="-483.87" y1="-623.57" x2="-483.87" y2="-622.3" width="0.1524" layer="91"/>
<wire x1="-476.885" y1="-623.57" x2="-476.885" y2="-622.3" width="0.1524" layer="91"/>
<wire x1="-469.9" y1="-623.57" x2="-469.9" y2="-622.3" width="0.1524" layer="91"/>
<wire x1="-462.28" y1="-623.57" x2="-462.28" y2="-622.3" width="0.1524" layer="91"/>
<wire x1="-454.66" y1="-623.57" x2="-454.66" y2="-622.3" width="0.1524" layer="91"/>
<wire x1="-491.49" y1="-622.3" x2="-501.015" y2="-622.3" width="0.1524" layer="91"/>
<wire x1="-439.42" y1="-624.84" x2="-443.23" y2="-624.84" width="0.1524" layer="91"/>
<wire x1="-443.23" y1="-624.84" x2="-443.23" y2="-622.3" width="0.1524" layer="91"/>
<wire x1="-439.42" y1="-629.92" x2="-443.23" y2="-629.92" width="0.1524" layer="91"/>
<wire x1="-443.23" y1="-629.92" x2="-443.23" y2="-624.84" width="0.1524" layer="91"/>
<wire x1="-439.42" y1="-632.46" x2="-443.23" y2="-632.46" width="0.1524" layer="91"/>
<wire x1="-443.23" y1="-632.46" x2="-443.23" y2="-629.92" width="0.1524" layer="91"/>
<wire x1="-439.42" y1="-637.54" x2="-443.23" y2="-637.54" width="0.1524" layer="91"/>
<wire x1="-443.23" y1="-637.54" x2="-443.23" y2="-632.46" width="0.1524" layer="91"/>
<wire x1="-447.04" y1="-623.57" x2="-447.04" y2="-622.3" width="0.1524" layer="91"/>
<junction x="-483.87" y="-622.3"/>
<junction x="-476.885" y="-622.3"/>
<junction x="-469.9" y="-622.3"/>
<junction x="-462.28" y="-622.3"/>
<junction x="-454.66" y="-622.3"/>
<junction x="-491.49" y="-622.3"/>
<junction x="-443.23" y="-622.3"/>
<junction x="-443.23" y="-624.84"/>
<junction x="-443.23" y="-629.92"/>
<junction x="-443.23" y="-632.46"/>
<junction x="-483.87" y="-623.57"/>
<junction x="-447.04" y="-622.3"/>
<pinref part="U12" gate="A" pin="VDDA_2"/>
<pinref part="C62" gate="G$1" pin="2"/>
<pinref part="C57" gate="G$1" pin="1"/>
<pinref part="C52" gate="G$1" pin="1"/>
<pinref part="C58" gate="G$1" pin="1"/>
<pinref part="C56" gate="G$1" pin="1"/>
<pinref part="U12" gate="A" pin="VDDA"/>
<pinref part="U12" gate="A" pin="VDDX"/>
<pinref part="U12" gate="A" pin="VDD33"/>
<pinref part="U12" gate="A" pin="VDD"/>
<pinref part="C53" gate="G$1" pin="1"/>
<pinref part="C55" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="-563.88" y1="-651.51" x2="-563.88" y2="-641.35" width="0.1524" layer="91"/>
<pinref part="R145" gate="G$1" pin="2"/>
</segment>
<segment>
<wire x1="-542.29" y1="-652.78" x2="-542.29" y2="-641.35" width="0.1524" layer="91"/>
<pinref part="R146" gate="G$1" pin="2"/>
</segment>
</net>
<net name="AVDD_M" class="0">
<segment>
<wire x1="51.435" y1="-579.755" x2="41.275" y2="-579.755" width="0.1524" layer="91"/>
<pinref part="R4" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="-123.19" y1="-438.785" x2="-123.19" y2="-441.325" width="0.1524" layer="91"/>
<wire x1="-120.65" y1="-444.5" x2="-120.65" y2="-441.325" width="0.1524" layer="91"/>
<wire x1="-120.65" y1="-441.325" x2="-123.19" y2="-441.325" width="0.1524" layer="91"/>
<wire x1="-123.19" y1="-441.325" x2="-130.81" y2="-441.325" width="0.1524" layer="91"/>
<wire x1="-130.81" y1="-441.325" x2="-130.81" y2="-444.5" width="0.1524" layer="91"/>
<wire x1="-100.33" y1="-426.72" x2="-100.33" y2="-441.325" width="0.1524" layer="91"/>
<wire x1="-120.65" y1="-441.325" x2="-109.22" y2="-441.325" width="0.1524" layer="91"/>
<wire x1="-109.22" y1="-441.325" x2="-100.33" y2="-441.325" width="0.1524" layer="91"/>
<wire x1="-109.22" y1="-441.325" x2="-109.22" y2="-453.39" width="0.1524" layer="91"/>
<junction x="-120.65" y="-441.325"/>
<junction x="-123.19" y="-441.325"/>
<junction x="-109.22" y="-441.325"/>
<pinref part="U$1" gate="G$1" pin="V-"/>
<pinref part="C8" gate="G$1" pin="1"/>
<pinref part="C7" gate="G$1" pin="1"/>
<pinref part="C17" gate="G$1" pin="2"/>
</segment>
<segment>
<wire x1="148.59" y1="-229.235" x2="147.32" y2="-233.045" width="0.1524" layer="91"/>
<wire x1="149.86" y1="-236.22" x2="149.86" y2="-233.045" width="0.1524" layer="91"/>
<wire x1="149.86" y1="-233.045" x2="147.32" y2="-233.045" width="0.1524" layer="91"/>
<wire x1="147.32" y1="-233.045" x2="139.7" y2="-233.045" width="0.1524" layer="91"/>
<wire x1="139.7" y1="-233.045" x2="139.7" y2="-236.22" width="0.1524" layer="91"/>
<wire x1="170.18" y1="-217.805" x2="170.18" y2="-233.045" width="0.1524" layer="91"/>
<wire x1="149.86" y1="-233.045" x2="162.56" y2="-233.045" width="0.1524" layer="91"/>
<wire x1="162.56" y1="-233.045" x2="170.18" y2="-233.045" width="0.1524" layer="91"/>
<wire x1="162.56" y1="-233.045" x2="162.56" y2="-247.65" width="0.1524" layer="91"/>
<junction x="149.86" y="-233.045"/>
<junction x="147.32" y="-233.045"/>
<junction x="162.56" y="-233.045"/>
<pinref part="U$2" gate="G$1" pin="V-"/>
<pinref part="C23" gate="G$1" pin="1"/>
<pinref part="C20" gate="G$1" pin="1"/>
<pinref part="C24" gate="G$1" pin="2"/>
</segment>
<segment>
<wire x1="-441.96" y1="-705.485" x2="-441.96" y2="-694.055" width="0.1524" layer="91"/>
<wire x1="-441.96" y1="-694.055" x2="-436.88" y2="-694.055" width="0.1524" layer="91"/>
<wire x1="-447.04" y1="-686.435" x2="-441.96" y2="-686.435" width="0.1524" layer="91"/>
<wire x1="-441.96" y1="-686.435" x2="-441.96" y2="-694.055" width="0.1524" layer="91"/>
<wire x1="-424.18" y1="-694.055" x2="-436.88" y2="-694.055" width="0.1524" layer="91"/>
<wire x1="-447.04" y1="-686.435" x2="-447.04" y2="-687.07" width="0.1524" layer="91"/>
<junction x="-441.96" y="-694.055"/>
<junction x="-436.88" y="-694.055"/>
<pinref part="C28" gate="G$1" pin="2"/>
<pinref part="C29" gate="G$1" pin="2"/>
<pinref part="U$3" gate="C" pin="AVDD_M"/>
</segment>
<segment>
<wire x1="-398.78" y1="-708.025" x2="-398.78" y2="-694.055" width="0.1524" layer="91"/>
<wire x1="-398.78" y1="-694.055" x2="-393.7" y2="-694.055" width="0.1524" layer="91"/>
<wire x1="-404.495" y1="-688.34" x2="-398.78" y2="-688.34" width="0.1524" layer="91"/>
<wire x1="-398.78" y1="-688.34" x2="-398.78" y2="-694.055" width="0.1524" layer="91"/>
<wire x1="-383.54" y1="-694.055" x2="-393.7" y2="-694.055" width="0.1524" layer="91"/>
<wire x1="-404.495" y1="-688.34" x2="-404.495" y2="-688.975" width="0.1524" layer="91"/>
<junction x="-398.78" y="-694.055"/>
<junction x="-393.7" y="-694.055"/>
<pinref part="C32" gate="G$1" pin="2"/>
<pinref part="C33" gate="G$1" pin="2"/>
<pinref part="$$U$4" gate="C" pin="AVDD_M"/>
</segment>
<segment>
<wire x1="-139.7" y1="-189.23" x2="-134.62" y2="-189.23" width="0.1524" layer="91"/>
<label x="-137.16" y="-189.23" size="1.778" layer="95"/>
<pinref part="U8" gate="A" pin="V-"/>
<pinref part="C49" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="194.31" y1="-401.32" x2="196.85" y2="-401.32" width="0.1524" layer="91"/>
<wire x1="196.85" y1="-401.32" x2="199.39" y2="-401.32" width="0.1524" layer="91"/>
<wire x1="196.85" y1="-403.86" x2="196.85" y2="-401.32" width="0.1524" layer="91"/>
<junction x="196.85" y="-401.32"/>
<label x="199.39" y="-401.32" size="1.778" layer="95"/>
<pinref part="U5" gate="A" pin="VSS"/>
<pinref part="C34" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="194.31" y1="-476.25" x2="196.85" y2="-476.25" width="0.1524" layer="91"/>
<wire x1="196.85" y1="-476.25" x2="199.39" y2="-476.25" width="0.1524" layer="91"/>
<wire x1="196.85" y1="-478.79" x2="196.85" y2="-476.25" width="0.1524" layer="91"/>
<junction x="196.85" y="-476.25"/>
<label x="199.39" y="-476.25" size="1.778" layer="95"/>
<pinref part="U7" gate="A" pin="VSS"/>
<pinref part="C39" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="-135.89" y1="-260.985" x2="-140.335" y2="-260.985" width="0.1524" layer="91"/>
<label x="-138.43" y="-260.985" size="1.778" layer="95"/>
<pinref part="U9" gate="A" pin="V-"/>
<pinref part="C47" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="-415.29" y1="-193.675" x2="-419.735" y2="-193.675" width="0.1524" layer="91"/>
<label x="-417.83" y="-193.675" size="1.778" layer="95"/>
<pinref part="U10" gate="A" pin="V-"/>
<pinref part="C45" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="-415.29" y1="-259.08" x2="-420.37" y2="-259.08" width="0.1524" layer="91"/>
<label x="-417.83" y="-259.08" size="1.778" layer="95"/>
<pinref part="U11" gate="A" pin="V-"/>
<pinref part="C43" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="-262.89" y1="-443.23" x2="-265.43" y2="-443.23" width="0.1524" layer="91"/>
<wire x1="-265.43" y1="-443.23" x2="-267.97" y2="-443.23" width="0.1524" layer="91"/>
<wire x1="-265.43" y1="-445.77" x2="-265.43" y2="-443.23" width="0.1524" layer="91"/>
<junction x="-265.43" y="-443.23"/>
<label x="-271.78" y="-443.23" size="1.778" layer="95"/>
<pinref part="U3" gate="A" pin="VSS"/>
<pinref part="C25" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="-535.305" y1="-404.495" x2="-520.7" y2="-404.495" width="0.1524" layer="91"/>
<wire x1="-520.7" y1="-404.495" x2="-518.795" y2="-404.495" width="0.1524" layer="91"/>
<wire x1="-520.7" y1="-407.035" x2="-520.7" y2="-404.495" width="0.1524" layer="91"/>
<junction x="-520.7" y="-404.495"/>
<label x="-535.305" y="-404.495" size="1.778" layer="95"/>
<pinref part="U6" gate="A" pin="VSS"/>
<pinref part="C41" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="-535.305" y1="-470.535" x2="-520.7" y2="-470.535" width="0.1524" layer="91"/>
<wire x1="-520.7" y1="-470.535" x2="-518.795" y2="-470.535" width="0.1524" layer="91"/>
<wire x1="-520.7" y1="-473.075" x2="-520.7" y2="-470.535" width="0.1524" layer="91"/>
<junction x="-520.7" y="-470.535"/>
<label x="-535.305" y="-470.535" size="1.778" layer="95"/>
<pinref part="U4" gate="A" pin="VSS"/>
<pinref part="C37" gate="G$1" pin="1"/>
</segment>
</net>
<net name="CLKINP" class="0">
<segment>
<wire x1="-339.725" y1="-647.7" x2="-339.725" y2="-648.97" width="0.1524" layer="91"/>
<wire x1="-339.725" y1="-647.7" x2="-325.12" y2="-647.7" width="0.1524" layer="91"/>
<wire x1="-347.98" y1="-647.7" x2="-339.725" y2="-647.7" width="0.1524" layer="91"/>
<wire x1="-347.98" y1="-647.7" x2="-347.98" y2="-648.97" width="0.1524" layer="91"/>
<wire x1="-347.98" y1="-647.7" x2="-350.52" y2="-647.7" width="0.1524" layer="91"/>
<junction x="-339.725" y="-647.7"/>
<junction x="-347.98" y="-647.7"/>
<pinref part="R138" gate="G$1" pin="1"/>
<pinref part="R137" gate="G$1" pin="1"/>
<pinref part="C60" gate="G$1" pin="2"/>
</segment>
<segment>
<wire x1="-233.68" y1="-650.24" x2="-253.365" y2="-650.24" width="0.1524" layer="91"/>
<pinref part="$$U$6" gate="A" pin="CLKP"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<wire x1="-370.84" y1="-650.24" x2="-360.68" y2="-660.4" width="0.1524" layer="91"/>
<wire x1="-373.38" y1="-650.24" x2="-370.84" y2="-650.24" width="0.1524" layer="91"/>
<wire x1="-358.14" y1="-660.4" x2="-360.68" y2="-660.4" width="0.1524" layer="91"/>
<pinref part="U12" gate="A" pin="*100M"/>
<pinref part="C59" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<wire x1="-358.14" y1="-647.7" x2="-373.38" y2="-647.7" width="0.1524" layer="91"/>
<pinref part="C60" gate="G$1" pin="1"/>
<pinref part="U12" gate="A" pin="100M"/>
</segment>
</net>
<net name="CLKINM" class="0">
<segment>
<wire x1="-347.98" y1="-660.4" x2="-347.98" y2="-659.13" width="0.1524" layer="91"/>
<wire x1="-347.98" y1="-660.4" x2="-339.725" y2="-660.4" width="0.1524" layer="91"/>
<wire x1="-339.725" y1="-660.4" x2="-339.725" y2="-659.13" width="0.1524" layer="91"/>
<wire x1="-339.725" y1="-660.4" x2="-325.12" y2="-660.4" width="0.1524" layer="91"/>
<wire x1="-347.98" y1="-660.4" x2="-350.52" y2="-660.4" width="0.1524" layer="91"/>
<junction x="-339.725" y="-660.4"/>
<junction x="-347.98" y="-660.4"/>
<pinref part="R138" gate="G$1" pin="2"/>
<pinref part="R137" gate="G$1" pin="2"/>
<pinref part="C59" gate="G$1" pin="2"/>
</segment>
<segment>
<wire x1="-233.68" y1="-652.78" x2="-253.365" y2="-652.78" width="0.1524" layer="91"/>
<pinref part="$$U$6" gate="A" pin="CLKM"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<wire x1="-439.42" y1="-647.7" x2="-440.69" y2="-647.7" width="0.1524" layer="91"/>
<wire x1="-440.69" y1="-647.7" x2="-443.23" y2="-645.16" width="0.1524" layer="91"/>
<wire x1="-457.2" y1="-645.16" x2="-448.31" y2="-645.16" width="0.1524" layer="91"/>
<wire x1="-448.31" y1="-645.16" x2="-443.23" y2="-645.16" width="0.1524" layer="91"/>
<wire x1="-448.31" y1="-646.43" x2="-448.31" y2="-645.16" width="0.1524" layer="91"/>
<junction x="-448.31" y="-645.16"/>
<pinref part="U12" gate="A" pin="XO1"/>
<pinref part="C54" gate="G$1" pin="2"/>
<pinref part="Q1" gate="P" pin="2"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<wire x1="-439.42" y1="-650.24" x2="-440.69" y2="-650.24" width="0.1524" layer="91"/>
<wire x1="-440.69" y1="-650.24" x2="-443.23" y2="-652.78" width="0.1524" layer="91"/>
<wire x1="-457.2" y1="-652.78" x2="-448.31" y2="-652.78" width="0.1524" layer="91"/>
<wire x1="-448.31" y1="-652.78" x2="-443.23" y2="-652.78" width="0.1524" layer="91"/>
<wire x1="-448.31" y1="-651.51" x2="-448.31" y2="-652.78" width="0.1524" layer="91"/>
<junction x="-448.31" y="-652.78"/>
<pinref part="U12" gate="A" pin="XO2"/>
<pinref part="C51" gate="G$1" pin="2"/>
<pinref part="Q1" gate="P" pin="1"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<wire x1="210.185" y1="-208.28" x2="212.725" y2="-208.28" width="0.1524" layer="91"/>
<wire x1="212.725" y1="-208.28" x2="224.155" y2="-208.28" width="0.1524" layer="91"/>
<wire x1="212.725" y1="-210.185" x2="212.725" y2="-208.28" width="0.1524" layer="91"/>
<wire x1="212.725" y1="-193.675" x2="212.725" y2="-208.28" width="0.1524" layer="91"/>
<junction x="212.725" y="-208.28"/>
<pinref part="L3" gate="G$1" pin="1"/>
<pinref part="C2" gate="G$1" pin="1"/>
<pinref part="R2" gate="G$1" pin="2"/>
<pinref part="B-(E)" gate="G$1" pin="PIN"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<wire x1="212.725" y1="-219.71" x2="212.725" y2="-217.805" width="0.1524" layer="91"/>
<wire x1="212.725" y1="-219.71" x2="224.155" y2="-219.71" width="0.1524" layer="91"/>
<wire x1="210.185" y1="-219.71" x2="212.725" y2="-219.71" width="0.1524" layer="91"/>
<wire x1="212.725" y1="-229.235" x2="212.725" y2="-219.71" width="0.1524" layer="91"/>
<junction x="212.725" y="-219.71"/>
<pinref part="C2" gate="G$1" pin="2"/>
<pinref part="L2" gate="G$1" pin="1"/>
<pinref part="R3" gate="G$1" pin="2"/>
<pinref part="B+(E)" gate="G$1" pin="PIN"/>
</segment>
</net>
<net name="A/D_A-" class="0">
<segment>
<wire x1="-67.31" y1="-417.83" x2="-64.77" y2="-417.83" width="0.1524" layer="91"/>
<wire x1="-64.77" y1="-417.83" x2="-60.96" y2="-417.83" width="0.1524" layer="91"/>
<wire x1="-64.77" y1="-419.735" x2="-64.77" y2="-417.83" width="0.1524" layer="91"/>
<wire x1="-64.77" y1="-403.225" x2="-64.77" y2="-417.83" width="0.1524" layer="91"/>
<junction x="-64.77" y="-417.83"/>
<pinref part="L4" gate="G$1" pin="1"/>
<pinref part="C9" gate="G$1" pin="1"/>
<pinref part="R1" gate="G$1" pin="2"/>
<pinref part="A-(S)" gate="G$1" pin="PIN"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<wire x1="-64.77" y1="-429.26" x2="-64.77" y2="-427.355" width="0.1524" layer="91"/>
<wire x1="-64.77" y1="-429.26" x2="-60.96" y2="-429.26" width="0.1524" layer="91"/>
<wire x1="-67.31" y1="-429.26" x2="-64.77" y2="-429.26" width="0.1524" layer="91"/>
<wire x1="-64.77" y1="-441.96" x2="-64.77" y2="-429.26" width="0.1524" layer="91"/>
<junction x="-64.77" y="-429.26"/>
<pinref part="C9" gate="G$1" pin="2"/>
<pinref part="L1" gate="G$1" pin="1"/>
<pinref part="R12" gate="G$1" pin="2"/>
<pinref part="A+(S)" gate="G$1" pin="PIN"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<wire x1="-516.255" y1="-682.625" x2="-513.715" y2="-682.625" width="0.1524" layer="91"/>
<wire x1="-513.715" y1="-688.34" x2="-513.715" y2="-682.625" width="0.1524" layer="91"/>
<pinref part="CTRL3" gate="1" pin="1"/>
<pinref part="R136" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$19" class="0">
<segment>
<wire x1="-495.935" y1="-682.625" x2="-493.395" y2="-682.625" width="0.1524" layer="91"/>
<wire x1="-493.395" y1="-688.975" x2="-493.395" y2="-682.625" width="0.1524" layer="91"/>
<pinref part="CTRL2" gate="1" pin="1"/>
<pinref part="R143" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$20" class="0">
<segment>
<wire x1="-475.615" y1="-682.625" x2="-473.075" y2="-682.625" width="0.1524" layer="91"/>
<wire x1="-473.075" y1="-688.975" x2="-473.075" y2="-682.625" width="0.1524" layer="91"/>
<pinref part="CTRL1" gate="1" pin="1"/>
<pinref part="R156" gate="G$1" pin="2"/>
</segment>
</net>
<net name="AGND" class="0">
<segment>
<wire x1="-233.68" y1="-595.63" x2="-234.95" y2="-595.63" width="0.1524" layer="91"/>
<wire x1="-234.95" y1="-595.63" x2="-234.95" y2="-598.17" width="0.1524" layer="91"/>
<wire x1="-234.95" y1="-598.17" x2="-234.95" y2="-600.71" width="0.1524" layer="91"/>
<wire x1="-234.95" y1="-600.71" x2="-234.95" y2="-603.25" width="0.1524" layer="91"/>
<wire x1="-234.95" y1="-603.25" x2="-234.95" y2="-605.79" width="0.1524" layer="91"/>
<wire x1="-234.95" y1="-605.79" x2="-234.95" y2="-608.33" width="0.1524" layer="91"/>
<wire x1="-234.95" y1="-608.33" x2="-234.95" y2="-610.87" width="0.1524" layer="91"/>
<wire x1="-234.95" y1="-610.87" x2="-234.95" y2="-613.41" width="0.1524" layer="91"/>
<wire x1="-234.95" y1="-613.41" x2="-234.95" y2="-615.95" width="0.1524" layer="91"/>
<wire x1="-234.95" y1="-615.95" x2="-233.68" y2="-615.95" width="0.1524" layer="91"/>
<wire x1="-233.68" y1="-613.41" x2="-234.95" y2="-613.41" width="0.1524" layer="91"/>
<wire x1="-233.68" y1="-610.87" x2="-234.95" y2="-610.87" width="0.1524" layer="91"/>
<wire x1="-233.68" y1="-608.33" x2="-234.95" y2="-608.33" width="0.1524" layer="91"/>
<wire x1="-233.68" y1="-605.79" x2="-234.95" y2="-605.79" width="0.1524" layer="91"/>
<wire x1="-233.68" y1="-603.25" x2="-234.95" y2="-603.25" width="0.1524" layer="91"/>
<wire x1="-233.68" y1="-600.71" x2="-234.95" y2="-600.71" width="0.1524" layer="91"/>
<wire x1="-233.68" y1="-598.17" x2="-234.95" y2="-598.17" width="0.1524" layer="91"/>
<wire x1="-241.3" y1="-605.79" x2="-234.95" y2="-605.79" width="0.1524" layer="91"/>
<junction x="-234.95" y="-613.41"/>
<junction x="-234.95" y="-610.87"/>
<junction x="-234.95" y="-608.33"/>
<junction x="-234.95" y="-605.79"/>
<junction x="-234.95" y="-603.25"/>
<junction x="-234.95" y="-600.71"/>
<junction x="-234.95" y="-598.17"/>
<pinref part="$$U$6" gate="A" pin="AGND_0"/>
<pinref part="$$U$6" gate="A" pin="AGND_1"/>
<pinref part="$$U$6" gate="A" pin="AGND_2"/>
<pinref part="$$U$6" gate="A" pin="AGND_3"/>
<pinref part="$$U$6" gate="A" pin="AGND_4"/>
<pinref part="$$U$6" gate="A" pin="AGND_5"/>
<pinref part="$$U$6" gate="A" pin="AGND_6"/>
<pinref part="$$U$6" gate="A" pin="AGND_7"/>
<pinref part="$$U$6" gate="A" pin="AGND_8"/>
<pinref part="AGND4" gate="VR1" pin="AGND"/>
</segment>
<segment>
<wire x1="-266.7" y1="-589.28" x2="-266.7" y2="-590.55" width="0.1524" layer="91"/>
<pinref part="C65" gate="G$1" pin="2"/>
<pinref part="AGND2" gate="VR1" pin="AGND"/>
</segment>
<segment>
<wire x1="-372.745" y1="-584.2" x2="-372.745" y2="-581.66" width="0.1524" layer="91"/>
<pinref part="C12" gate="G$1" pin="2"/>
<pinref part="AGND1" gate="VR1" pin="AGND"/>
</segment>
<segment>
<wire x1="-436.88" y1="-683.895" x2="-436.88" y2="-685.165" width="0.1524" layer="91"/>
<wire x1="-436.88" y1="-685.165" x2="-436.88" y2="-686.435" width="0.1524" layer="91"/>
<wire x1="-415.925" y1="-685.165" x2="-424.18" y2="-685.165" width="0.1524" layer="91"/>
<wire x1="-424.18" y1="-685.165" x2="-436.88" y2="-685.165" width="0.1524" layer="91"/>
<wire x1="-424.18" y1="-683.895" x2="-424.18" y2="-685.165" width="0.1524" layer="91"/>
<wire x1="-424.18" y1="-686.435" x2="-424.18" y2="-685.165" width="0.1524" layer="91"/>
<junction x="-436.88" y="-685.165"/>
<junction x="-424.18" y="-685.165"/>
<pinref part="C28" gate="G$1" pin="1"/>
<pinref part="C27" gate="G$1" pin="2"/>
<pinref part="C26" gate="G$1" pin="2"/>
<pinref part="C29" gate="G$1" pin="1"/>
<pinref part="AGND5" gate="VR1" pin="AGND"/>
</segment>
<segment>
<wire x1="-393.7" y1="-683.895" x2="-393.7" y2="-685.165" width="0.1524" layer="91"/>
<wire x1="-393.7" y1="-685.165" x2="-393.7" y2="-686.435" width="0.1524" layer="91"/>
<wire x1="-375.285" y1="-685.165" x2="-383.54" y2="-685.165" width="0.1524" layer="91"/>
<wire x1="-383.54" y1="-685.165" x2="-393.7" y2="-685.165" width="0.1524" layer="91"/>
<wire x1="-383.54" y1="-683.895" x2="-383.54" y2="-685.165" width="0.1524" layer="91"/>
<wire x1="-383.54" y1="-686.435" x2="-383.54" y2="-685.165" width="0.1524" layer="91"/>
<junction x="-393.7" y="-685.165"/>
<junction x="-383.54" y="-685.165"/>
<pinref part="C32" gate="G$1" pin="1"/>
<pinref part="C31" gate="G$1" pin="2"/>
<pinref part="C30" gate="G$1" pin="2"/>
<pinref part="C33" gate="G$1" pin="1"/>
<pinref part="AGND6" gate="VR1" pin="AGND"/>
</segment>
<segment>
<wire x1="113.03" y1="-177.8" x2="113.03" y2="-182.88" width="0.1524" layer="91"/>
<pinref part="R26" gate="G$1" pin="2"/>
<pinref part="AGND7" gate="VR1" pin="AGND"/>
</segment>
<segment>
<wire x1="113.03" y1="-233.045" x2="113.03" y2="-236.855" width="0.1524" layer="91"/>
<pinref part="R24" gate="G$1" pin="1"/>
<pinref part="AGND8" gate="VR1" pin="AGND"/>
</segment>
<segment>
<wire x1="121.285" y1="-233.68" x2="121.285" y2="-225.425" width="0.1524" layer="91"/>
<pinref part="C18" gate="G$1" pin="2"/>
<pinref part="AGND9" gate="VR1" pin="AGND"/>
</segment>
<segment>
<wire x1="139.7" y1="-243.84" x2="139.7" y2="-244.475" width="0.1524" layer="91"/>
<wire x1="139.7" y1="-244.475" x2="144.78" y2="-244.475" width="0.1524" layer="91"/>
<wire x1="144.78" y1="-244.475" x2="149.86" y2="-244.475" width="0.1524" layer="91"/>
<wire x1="149.86" y1="-244.475" x2="149.86" y2="-243.84" width="0.1524" layer="91"/>
<junction x="144.78" y="-244.475"/>
<pinref part="C20" gate="G$1" pin="2"/>
<pinref part="C23" gate="G$1" pin="2"/>
<pinref part="AGND10" gate="VR1" pin="AGND"/>
</segment>
<segment>
<wire x1="153.67" y1="-190.5" x2="153.67" y2="-193.04" width="0.1524" layer="91"/>
<wire x1="153.67" y1="-193.04" x2="158.75" y2="-193.04" width="0.1524" layer="91"/>
<wire x1="158.75" y1="-193.04" x2="163.83" y2="-193.04" width="0.1524" layer="91"/>
<wire x1="163.83" y1="-193.04" x2="163.83" y2="-190.5" width="0.1524" layer="91"/>
<junction x="158.75" y="-193.04"/>
<pinref part="C21" gate="G$1" pin="2"/>
<pinref part="C22" gate="G$1" pin="2"/>
<pinref part="AGND11" gate="VR1" pin="AGND"/>
</segment>
<segment>
<wire x1="-161.29" y1="-391.16" x2="-161.29" y2="-393.7" width="0.1524" layer="91"/>
<pinref part="R20" gate="G$1" pin="2"/>
<pinref part="AGND12" gate="VR1" pin="AGND"/>
</segment>
<segment>
<wire x1="-162.56" y1="-448.31" x2="-162.56" y2="-445.77" width="0.1524" layer="91"/>
<pinref part="R18" gate="G$1" pin="2"/>
<pinref part="AGND13" gate="VR1" pin="AGND"/>
</segment>
<segment>
<wire x1="-149.225" y1="-436.88" x2="-149.225" y2="-434.34" width="0.1524" layer="91"/>
<pinref part="C13" gate="G$1" pin="2"/>
<pinref part="AGND14" gate="VR1" pin="AGND"/>
</segment>
<segment>
<wire x1="-130.81" y1="-452.12" x2="-130.81" y2="-453.39" width="0.1524" layer="91"/>
<wire x1="-130.81" y1="-453.39" x2="-125.73" y2="-453.39" width="0.1524" layer="91"/>
<wire x1="-125.73" y1="-453.39" x2="-120.65" y2="-453.39" width="0.1524" layer="91"/>
<wire x1="-120.65" y1="-453.39" x2="-120.65" y2="-452.12" width="0.1524" layer="91"/>
<junction x="-125.73" y="-453.39"/>
<pinref part="C7" gate="G$1" pin="2"/>
<pinref part="C8" gate="G$1" pin="2"/>
<pinref part="AGND15" gate="VR1" pin="AGND"/>
</segment>
<segment>
<wire x1="-118.11" y1="-397.51" x2="-118.11" y2="-398.78" width="0.1524" layer="91"/>
<wire x1="-118.11" y1="-398.78" x2="-113.03" y2="-398.78" width="0.1524" layer="91"/>
<wire x1="-113.03" y1="-398.78" x2="-107.95" y2="-398.78" width="0.1524" layer="91"/>
<wire x1="-107.95" y1="-398.78" x2="-107.95" y2="-397.51" width="0.1524" layer="91"/>
<junction x="-113.03" y="-398.78"/>
<pinref part="C15" gate="G$1" pin="2"/>
<pinref part="C16" gate="G$1" pin="2"/>
<pinref part="AGND16" gate="VR1" pin="AGND"/>
</segment>
<segment>
<wire x1="63.5" y1="-590.55" x2="63.5" y2="-595.63" width="0.1524" layer="91"/>
<pinref part="C10" gate="G$1" pin="2"/>
<pinref part="AGND17" gate="VR1" pin="AGND"/>
</segment>
<segment>
<wire x1="-554.99" y1="-707.39" x2="-551.18" y2="-707.39" width="0.1524" layer="91"/>
<wire x1="-551.18" y1="-707.39" x2="-551.18" y2="-709.295" width="0.1524" layer="91"/>
<pinref part="R142" gate="G$1" pin="2"/>
<pinref part="AGND18" gate="VR1" pin="AGND"/>
</segment>
<segment>
<wire x1="-492.125" y1="-585.47" x2="-492.125" y2="-591.185" width="0.1524" layer="91"/>
<pinref part="C5" gate="G$1" pin="2"/>
<pinref part="AGND23" gate="VR1" pin="AGND"/>
</segment>
<segment>
<wire x1="-478.79" y1="-591.185" x2="-478.79" y2="-585.47" width="0.1524" layer="91"/>
<pinref part="C6" gate="G$1" pin="2"/>
<pinref part="AGND22" gate="VR1" pin="AGND"/>
</segment>
<segment>
<wire x1="-457.835" y1="-601.98" x2="-457.835" y2="-603.25" width="0.1524" layer="91"/>
<pinref part="C11" gate="G$1" pin="2"/>
<pinref part="AGND20" gate="VR1" pin="AGND"/>
</segment>
<segment>
<wire x1="-390.525" y1="-583.565" x2="-385.445" y2="-583.565" width="0.1524" layer="91"/>
<wire x1="-385.445" y1="-583.565" x2="-385.445" y2="-591.185" width="0.1524" layer="91"/>
<wire x1="-385.445" y1="-591.185" x2="-385.445" y2="-602.615" width="0.1524" layer="91"/>
<wire x1="-390.525" y1="-578.485" x2="-385.445" y2="-578.485" width="0.1524" layer="91"/>
<wire x1="-385.445" y1="-578.485" x2="-385.445" y2="-583.565" width="0.1524" layer="91"/>
<wire x1="-390.525" y1="-591.185" x2="-385.445" y2="-591.185" width="0.1524" layer="91"/>
<junction x="-385.445" y="-583.565"/>
<junction x="-385.445" y="-591.185"/>
<pinref part="U2" gate="A" pin="GND"/>
<pinref part="U2" gate="A" pin="GND_2"/>
<pinref part="U2" gate="A" pin="EP"/>
<pinref part="AGND19" gate="VR1" pin="AGND"/>
</segment>
<segment>
<wire x1="22.86" y1="-587.375" x2="22.86" y2="-589.28" width="0.1524" layer="91"/>
<pinref part="C3" gate="G$1" pin="2"/>
<pinref part="AGND24" gate="VR1" pin="AGND"/>
</segment>
<segment>
<wire x1="110.49" y1="-574.675" x2="120.65" y2="-574.675" width="0.1524" layer="91"/>
<wire x1="120.65" y1="-574.675" x2="120.65" y2="-575.945" width="0.1524" layer="91"/>
<pinref part="U1" gate="A" pin="GND"/>
<pinref part="AGND26" gate="VR1" pin="AGND"/>
</segment>
<segment>
<wire x1="35.56" y1="-589.915" x2="35.56" y2="-587.375" width="0.1524" layer="91"/>
<pinref part="AGND25" gate="VR1" pin="AGND"/>
<pinref part="C4" gate="G$1" pin="2"/>
</segment>
<segment>
<wire x1="-247.65" y1="-589.28" x2="-247.65" y2="-590.55" width="0.1524" layer="91"/>
<wire x1="-247.65" y1="-590.55" x2="-251.46" y2="-590.55" width="0.1524" layer="91"/>
<wire x1="-251.46" y1="-590.55" x2="-255.27" y2="-590.55" width="0.1524" layer="91"/>
<wire x1="-255.27" y1="-590.55" x2="-255.27" y2="-589.28" width="0.1524" layer="91"/>
<junction x="-251.46" y="-590.55"/>
<pinref part="C69" gate="G$1" pin="2"/>
<pinref part="C66" gate="G$1" pin="2"/>
<pinref part="AGND3" gate="VR1" pin="AGND"/>
</segment>
<segment>
<wire x1="-164.465" y1="-436.88" x2="-164.465" y2="-434.34" width="0.1524" layer="91"/>
<pinref part="AGND37" gate="VR1" pin="AGND"/>
<pinref part="C14" gate="G$1" pin="2"/>
</segment>
<segment>
<wire x1="109.22" y1="-233.68" x2="109.22" y2="-224.79" width="0.1524" layer="91"/>
<pinref part="AGND38" gate="VR1" pin="AGND"/>
<pinref part="C19" gate="G$1" pin="2"/>
</segment>
<segment>
<wire x1="81.28" y1="-421.64" x2="81.28" y2="-431.8" width="0.1524" layer="91"/>
<wire x1="81.28" y1="-431.8" x2="86.36" y2="-431.8" width="0.1524" layer="91"/>
<wire x1="86.36" y1="-431.8" x2="91.44" y2="-431.8" width="0.1524" layer="91"/>
<wire x1="91.44" y1="-431.8" x2="96.52" y2="-431.8" width="0.1524" layer="91"/>
<wire x1="96.52" y1="-431.8" x2="101.6" y2="-431.8" width="0.1524" layer="91"/>
<wire x1="101.6" y1="-431.8" x2="106.68" y2="-431.8" width="0.1524" layer="91"/>
<wire x1="106.68" y1="-431.8" x2="106.68" y2="-429.26" width="0.1524" layer="91"/>
<wire x1="101.6" y1="-421.64" x2="101.6" y2="-431.8" width="0.1524" layer="91"/>
<wire x1="96.52" y1="-429.26" x2="96.52" y2="-431.8" width="0.1524" layer="91"/>
<wire x1="91.44" y1="-421.64" x2="91.44" y2="-431.8" width="0.1524" layer="91"/>
<wire x1="86.36" y1="-429.26" x2="86.36" y2="-431.8" width="0.1524" layer="91"/>
<wire x1="76.2" y1="-429.26" x2="76.2" y2="-431.8" width="0.1524" layer="91"/>
<wire x1="76.2" y1="-431.8" x2="81.28" y2="-431.8" width="0.1524" layer="91"/>
<wire x1="71.12" y1="-421.64" x2="71.12" y2="-431.8" width="0.1524" layer="91"/>
<wire x1="71.12" y1="-431.8" x2="76.2" y2="-431.8" width="0.1524" layer="91"/>
<wire x1="66.04" y1="-429.26" x2="66.04" y2="-431.8" width="0.1524" layer="91"/>
<wire x1="66.04" y1="-431.8" x2="71.12" y2="-431.8" width="0.1524" layer="91"/>
<wire x1="60.96" y1="-421.64" x2="60.96" y2="-431.8" width="0.1524" layer="91"/>
<wire x1="60.96" y1="-431.8" x2="66.04" y2="-431.8" width="0.1524" layer="91"/>
<wire x1="55.88" y1="-429.26" x2="55.88" y2="-431.8" width="0.1524" layer="91"/>
<wire x1="55.88" y1="-431.8" x2="60.96" y2="-431.8" width="0.1524" layer="91"/>
<wire x1="50.8" y1="-421.64" x2="50.8" y2="-431.8" width="0.1524" layer="91"/>
<wire x1="50.8" y1="-431.8" x2="55.88" y2="-431.8" width="0.1524" layer="91"/>
<wire x1="45.72" y1="-429.26" x2="45.72" y2="-431.8" width="0.1524" layer="91"/>
<wire x1="45.72" y1="-431.8" x2="50.8" y2="-431.8" width="0.1524" layer="91"/>
<wire x1="40.64" y1="-421.64" x2="40.64" y2="-431.8" width="0.1524" layer="91"/>
<wire x1="40.64" y1="-431.8" x2="45.72" y2="-431.8" width="0.1524" layer="91"/>
<wire x1="35.56" y1="-429.26" x2="35.56" y2="-431.8" width="0.1524" layer="91"/>
<wire x1="35.56" y1="-431.8" x2="40.64" y2="-431.8" width="0.1524" layer="91"/>
<wire x1="30.48" y1="-421.64" x2="30.48" y2="-431.8" width="0.1524" layer="91"/>
<wire x1="30.48" y1="-431.8" x2="35.56" y2="-431.8" width="0.1524" layer="91"/>
<wire x1="66.04" y1="-439.42" x2="66.04" y2="-431.8" width="0.1524" layer="91"/>
<junction x="101.6" y="-431.8"/>
<junction x="96.52" y="-431.8"/>
<junction x="91.44" y="-431.8"/>
<junction x="86.36" y="-431.8"/>
<junction x="81.28" y="-431.8"/>
<junction x="76.2" y="-431.8"/>
<junction x="71.12" y="-431.8"/>
<junction x="66.04" y="-431.8"/>
<junction x="60.96" y="-431.8"/>
<junction x="55.88" y="-431.8"/>
<junction x="50.8" y="-431.8"/>
<junction x="45.72" y="-431.8"/>
<junction x="40.64" y="-431.8"/>
<junction x="35.56" y="-431.8"/>
<pinref part="R53" gate="G$1" pin="1"/>
<pinref part="R129" gate="G$1" pin="1"/>
<pinref part="R128" gate="G$1" pin="1"/>
<pinref part="R127" gate="G$1" pin="1"/>
<pinref part="R50" gate="G$1" pin="1"/>
<pinref part="R126" gate="G$1" pin="1"/>
<pinref part="R125" gate="G$1" pin="1"/>
<pinref part="R55" gate="G$1" pin="1"/>
<pinref part="R44" gate="G$1" pin="1"/>
<pinref part="R43" gate="G$1" pin="1"/>
<pinref part="R42" gate="G$1" pin="1"/>
<pinref part="R130" gate="G$1" pin="1"/>
<pinref part="R60" gate="G$1" pin="1"/>
<pinref part="R131" gate="G$1" pin="1"/>
<pinref part="R58" gate="G$1" pin="1"/>
<pinref part="R59" gate="G$1" pin="1"/>
<pinref part="AGND42" gate="VR1" pin="AGND"/>
</segment>
<segment>
<wire x1="-215.9" y1="-210.82" x2="-215.9" y2="-213.36" width="0.1524" layer="91"/>
<wire x1="-215.9" y1="-213.36" x2="-208.915" y2="-213.36" width="0.1524" layer="91"/>
<wire x1="-208.915" y1="-213.36" x2="-201.93" y2="-213.36" width="0.1524" layer="91"/>
<wire x1="-201.93" y1="-213.36" x2="-194.945" y2="-213.36" width="0.1524" layer="91"/>
<wire x1="-194.945" y1="-213.36" x2="-194.945" y2="-210.82" width="0.1524" layer="91"/>
<wire x1="-201.93" y1="-210.82" x2="-201.93" y2="-213.36" width="0.1524" layer="91"/>
<wire x1="-208.915" y1="-210.82" x2="-208.915" y2="-213.36" width="0.1524" layer="91"/>
<wire x1="-215.9" y1="-215.9" x2="-215.9" y2="-213.36" width="0.1524" layer="91"/>
<junction x="-201.93" y="-213.36"/>
<junction x="-208.915" y="-213.36"/>
<junction x="-215.9" y="-213.36"/>
<pinref part="R71" gate="G$1" pin="1"/>
<pinref part="R81" gate="G$1" pin="1"/>
<pinref part="R83" gate="G$1" pin="1"/>
<pinref part="R64" gate="G$1" pin="1"/>
<pinref part="AGND21" gate="VR1" pin="AGND"/>
</segment>
<segment>
<wire x1="-217.17" y1="-282.575" x2="-217.17" y2="-285.115" width="0.1524" layer="91"/>
<wire x1="-217.17" y1="-285.115" x2="-210.185" y2="-285.115" width="0.1524" layer="91"/>
<wire x1="-210.185" y1="-285.115" x2="-203.2" y2="-285.115" width="0.1524" layer="91"/>
<wire x1="-203.2" y1="-285.115" x2="-196.215" y2="-285.115" width="0.1524" layer="91"/>
<wire x1="-196.215" y1="-285.115" x2="-196.215" y2="-282.575" width="0.1524" layer="91"/>
<wire x1="-203.2" y1="-282.575" x2="-203.2" y2="-285.115" width="0.1524" layer="91"/>
<wire x1="-210.185" y1="-282.575" x2="-210.185" y2="-285.115" width="0.1524" layer="91"/>
<wire x1="-217.17" y1="-287.655" x2="-217.17" y2="-285.115" width="0.1524" layer="91"/>
<junction x="-203.2" y="-285.115"/>
<junction x="-210.185" y="-285.115"/>
<junction x="-217.17" y="-285.115"/>
<pinref part="R104" gate="G$1" pin="1"/>
<pinref part="R85" gate="G$1" pin="1"/>
<pinref part="R87" gate="G$1" pin="1"/>
<pinref part="R65" gate="G$1" pin="1"/>
<pinref part="AGND43" gate="VR1" pin="AGND"/>
</segment>
<segment>
<wire x1="-496.57" y1="-215.265" x2="-496.57" y2="-217.805" width="0.1524" layer="91"/>
<wire x1="-496.57" y1="-217.805" x2="-489.585" y2="-217.805" width="0.1524" layer="91"/>
<wire x1="-489.585" y1="-217.805" x2="-482.6" y2="-217.805" width="0.1524" layer="91"/>
<wire x1="-482.6" y1="-217.805" x2="-475.615" y2="-217.805" width="0.1524" layer="91"/>
<wire x1="-475.615" y1="-217.805" x2="-475.615" y2="-215.265" width="0.1524" layer="91"/>
<wire x1="-482.6" y1="-215.265" x2="-482.6" y2="-217.805" width="0.1524" layer="91"/>
<wire x1="-489.585" y1="-215.265" x2="-489.585" y2="-217.805" width="0.1524" layer="91"/>
<wire x1="-496.57" y1="-220.345" x2="-496.57" y2="-217.805" width="0.1524" layer="91"/>
<junction x="-482.6" y="-217.805"/>
<junction x="-489.585" y="-217.805"/>
<junction x="-496.57" y="-217.805"/>
<pinref part="R109" gate="G$1" pin="1"/>
<pinref part="R89" gate="G$1" pin="1"/>
<pinref part="R91" gate="G$1" pin="1"/>
<pinref part="R67" gate="G$1" pin="1"/>
<pinref part="AGND44" gate="VR1" pin="AGND"/>
</segment>
<segment>
<wire x1="-496.57" y1="-280.67" x2="-496.57" y2="-283.21" width="0.1524" layer="91"/>
<wire x1="-496.57" y1="-283.21" x2="-489.585" y2="-283.21" width="0.1524" layer="91"/>
<wire x1="-489.585" y1="-283.21" x2="-482.6" y2="-283.21" width="0.1524" layer="91"/>
<wire x1="-482.6" y1="-283.21" x2="-475.615" y2="-283.21" width="0.1524" layer="91"/>
<wire x1="-475.615" y1="-283.21" x2="-475.615" y2="-280.67" width="0.1524" layer="91"/>
<wire x1="-482.6" y1="-280.67" x2="-482.6" y2="-283.21" width="0.1524" layer="91"/>
<wire x1="-489.585" y1="-280.67" x2="-489.585" y2="-283.21" width="0.1524" layer="91"/>
<wire x1="-496.57" y1="-285.75" x2="-496.57" y2="-283.21" width="0.1524" layer="91"/>
<junction x="-482.6" y="-283.21"/>
<junction x="-489.585" y="-283.21"/>
<junction x="-496.57" y="-283.21"/>
<pinref part="R107" gate="G$1" pin="1"/>
<pinref part="R93" gate="G$1" pin="1"/>
<pinref part="R95" gate="G$1" pin="1"/>
<pinref part="R69" gate="G$1" pin="1"/>
<pinref part="AGND45" gate="VR1" pin="AGND"/>
</segment>
<segment>
<wire x1="194.31" y1="-398.78" x2="199.39" y2="-398.78" width="0.1524" layer="91"/>
<label x="199.39" y="-398.78" size="1.778" layer="95"/>
<pinref part="U5" gate="A" pin="GND"/>
</segment>
<segment>
<wire x1="-262.89" y1="-440.69" x2="-267.97" y2="-440.69" width="0.1524" layer="91"/>
<label x="-271.78" y="-440.69" size="1.778" layer="95"/>
<pinref part="U3" gate="A" pin="GND"/>
</segment>
<segment>
<wire x1="199.39" y1="-473.71" x2="194.31" y2="-473.71" width="0.1524" layer="91"/>
<label x="199.39" y="-473.71" size="1.778" layer="95"/>
<pinref part="U7" gate="A" pin="GND"/>
</segment>
<segment>
<wire x1="-67.31" y1="-403.225" x2="-67.31" y2="-405.765" width="0.1524" layer="91"/>
<junction x="-67.31" y="-403.225"/>
<pinref part="A-(S)" gate="G$1" pin="SHIELD"/>
<pinref part="AGND46" gate="VR1" pin="AGND"/>
<pinref part="A-(S)" gate="G$1" pin="P$1"/>
<pinref part="A-(S)" gate="G$1" pin="P$2"/>
<pinref part="A-(S)" gate="G$1" pin="P$3"/>
</segment>
<segment>
<wire x1="-54.61" y1="-441.96" x2="-54.61" y2="-438.785" width="0.1524" layer="91"/>
<wire x1="-54.61" y1="-438.785" x2="-62.23" y2="-438.785" width="0.1524" layer="91"/>
<wire x1="-62.23" y1="-438.785" x2="-62.23" y2="-441.96" width="0.1524" layer="91"/>
<junction x="-62.23" y="-441.96"/>
<pinref part="AGND41" gate="VR1" pin="AGND"/>
<pinref part="A+(S)" gate="G$1" pin="SHIELD"/>
<pinref part="A+(S)" gate="G$1" pin="P$1"/>
<pinref part="A+(S)" gate="G$1" pin="P$2"/>
<pinref part="A+(S)" gate="G$1" pin="P$3"/>
</segment>
<segment>
<wire x1="222.885" y1="-229.235" x2="222.885" y2="-226.06" width="0.1524" layer="91"/>
<wire x1="222.885" y1="-226.06" x2="215.265" y2="-226.06" width="0.1524" layer="91"/>
<wire x1="215.265" y1="-226.06" x2="215.265" y2="-229.235" width="0.1524" layer="91"/>
<junction x="215.265" y="-229.235"/>
<pinref part="AGND49" gate="VR1" pin="AGND"/>
<pinref part="B+(E)" gate="G$1" pin="SHIELD"/>
<pinref part="B+(E)" gate="G$1" pin="P$1"/>
<pinref part="B+(E)" gate="G$1" pin="P$2"/>
<pinref part="B+(E)" gate="G$1" pin="P$3"/>
</segment>
<segment>
<wire x1="210.185" y1="-193.675" x2="210.185" y2="-196.215" width="0.1524" layer="91"/>
<junction x="210.185" y="-193.675"/>
<pinref part="B-(E)" gate="G$1" pin="SHIELD"/>
<pinref part="AGND50" gate="VR1" pin="AGND"/>
<pinref part="B-(E)" gate="G$1" pin="P$1"/>
<pinref part="B-(E)" gate="G$1" pin="P$2"/>
<pinref part="B-(E)" gate="G$1" pin="P$3"/>
</segment>
<segment>
<wire x1="-376.555" y1="-446.405" x2="-387.35" y2="-446.405" width="0.1524" layer="91"/>
<wire x1="-376.555" y1="-479.425" x2="-376.555" y2="-474.98" width="0.1524" layer="91"/>
<wire x1="-376.555" y1="-474.98" x2="-376.555" y2="-465.455" width="0.1524" layer="91"/>
<wire x1="-376.555" y1="-465.455" x2="-376.555" y2="-455.93" width="0.1524" layer="91"/>
<wire x1="-376.555" y1="-455.93" x2="-376.555" y2="-446.405" width="0.1524" layer="91"/>
<wire x1="-387.35" y1="-455.93" x2="-376.555" y2="-455.93" width="0.1524" layer="91"/>
<wire x1="-387.35" y1="-465.455" x2="-376.555" y2="-465.455" width="0.1524" layer="91"/>
<wire x1="-387.35" y1="-474.98" x2="-376.555" y2="-474.98" width="0.1524" layer="91"/>
<junction x="-387.35" y="-446.405"/>
<junction x="-387.35" y="-455.93"/>
<junction x="-376.555" y="-455.93"/>
<junction x="-387.35" y="-465.455"/>
<junction x="-376.555" y="-465.455"/>
<junction x="-387.35" y="-474.98"/>
<junction x="-376.555" y="-474.98"/>
<pinref part="AGND52" gate="VR1" pin="AGND"/>
<pinref part="E16" gate="G$1" pin="SHIELD"/>
<pinref part="E12" gate="G$1" pin="SHIELD"/>
<pinref part="E15" gate="G$1" pin="SHIELD"/>
<pinref part="E11" gate="G$1" pin="SHIELD"/>
<pinref part="E16" gate="G$1" pin="P$1"/>
<pinref part="E16" gate="G$1" pin="P$2"/>
<pinref part="E16" gate="G$1" pin="P$3"/>
<pinref part="E12" gate="G$1" pin="P$1"/>
<pinref part="E12" gate="G$1" pin="P$2"/>
<pinref part="E12" gate="G$1" pin="P$3"/>
<pinref part="E15" gate="G$1" pin="P$1"/>
<pinref part="E15" gate="G$1" pin="P$2"/>
<pinref part="E15" gate="G$1" pin="P$3"/>
<pinref part="E11" gate="G$1" pin="P$1"/>
<pinref part="E11" gate="G$1" pin="P$2"/>
<pinref part="E11" gate="G$1" pin="P$3"/>
</segment>
<segment>
<wire x1="-518.795" y1="-467.995" x2="-535.305" y2="-467.995" width="0.1524" layer="91"/>
<label x="-535.305" y="-467.995" size="1.778" layer="95"/>
<pinref part="U4" gate="A" pin="GND"/>
</segment>
<segment>
<wire x1="-518.795" y1="-401.955" x2="-535.305" y2="-401.955" width="0.1524" layer="91"/>
<label x="-535.305" y="-401.955" size="1.778" layer="95"/>
<pinref part="U6" gate="A" pin="GND"/>
</segment>
<segment>
<wire x1="-545.465" y1="-357.505" x2="-545.465" y2="-355.6" width="0.1524" layer="91"/>
<wire x1="-545.465" y1="-355.6" x2="-548.64" y2="-355.6" width="0.1524" layer="91"/>
<junction x="-548.64" y="-355.6"/>
<pinref part="AGND54" gate="VR1" pin="AGND"/>
<pinref part="SIG2" gate="G$1" pin="SHIELD"/>
<pinref part="SIG2" gate="G$1" pin="P$1"/>
<pinref part="SIG2" gate="G$1" pin="P$2"/>
<pinref part="SIG2" gate="G$1" pin="P$3"/>
</segment>
<segment>
<wire x1="-541.02" y1="-435.61" x2="-541.02" y2="-426.085" width="0.1524" layer="91"/>
<wire x1="-541.02" y1="-426.085" x2="-546.1" y2="-426.085" width="0.1524" layer="91"/>
<junction x="-546.1" y="-426.085"/>
<pinref part="AGND55" gate="VR1" pin="AGND"/>
<pinref part="SIG1" gate="G$1" pin="SHIELD"/>
<pinref part="SIG1" gate="G$1" pin="P$1"/>
<pinref part="SIG1" gate="G$1" pin="P$2"/>
<pinref part="SIG1" gate="G$1" pin="P$3"/>
</segment>
<segment>
<wire x1="-354.965" y1="-446.405" x2="-365.76" y2="-446.405" width="0.1524" layer="91"/>
<wire x1="-354.965" y1="-480.695" x2="-354.965" y2="-474.98" width="0.1524" layer="91"/>
<wire x1="-354.965" y1="-474.98" x2="-354.965" y2="-465.455" width="0.1524" layer="91"/>
<wire x1="-354.965" y1="-465.455" x2="-354.965" y2="-455.93" width="0.1524" layer="91"/>
<wire x1="-354.965" y1="-455.93" x2="-354.965" y2="-446.405" width="0.1524" layer="91"/>
<wire x1="-365.76" y1="-455.93" x2="-354.965" y2="-455.93" width="0.1524" layer="91"/>
<wire x1="-365.76" y1="-465.455" x2="-354.965" y2="-465.455" width="0.1524" layer="91"/>
<wire x1="-365.76" y1="-474.98" x2="-354.965" y2="-474.98" width="0.1524" layer="91"/>
<junction x="-365.76" y="-446.405"/>
<junction x="-365.76" y="-455.93"/>
<junction x="-354.965" y="-455.93"/>
<junction x="-365.76" y="-465.455"/>
<junction x="-354.965" y="-465.455"/>
<junction x="-365.76" y="-474.98"/>
<junction x="-354.965" y="-474.98"/>
<pinref part="E14" gate="G$1" pin="SHIELD"/>
<pinref part="AGND53" gate="VR1" pin="AGND"/>
<pinref part="E10" gate="G$1" pin="SHIELD"/>
<pinref part="E13" gate="G$1" pin="SHIELD"/>
<pinref part="E9" gate="G$1" pin="SHIELD"/>
<pinref part="E14" gate="G$1" pin="P$1"/>
<pinref part="E14" gate="G$1" pin="P$2"/>
<pinref part="E14" gate="G$1" pin="P$3"/>
<pinref part="E10" gate="G$1" pin="P$1"/>
<pinref part="E10" gate="G$1" pin="P$2"/>
<pinref part="E10" gate="G$1" pin="P$3"/>
<pinref part="E13" gate="G$1" pin="P$1"/>
<pinref part="E13" gate="G$1" pin="P$2"/>
<pinref part="E13" gate="G$1" pin="P$3"/>
<pinref part="E9" gate="G$1" pin="P$1"/>
<pinref part="E9" gate="G$1" pin="P$2"/>
<pinref part="E9" gate="G$1" pin="P$3"/>
</segment>
<segment>
<wire x1="-398.145" y1="-446.405" x2="-408.94" y2="-446.405" width="0.1524" layer="91"/>
<wire x1="-398.145" y1="-479.425" x2="-398.145" y2="-474.98" width="0.1524" layer="91"/>
<wire x1="-398.145" y1="-474.98" x2="-398.145" y2="-465.455" width="0.1524" layer="91"/>
<wire x1="-398.145" y1="-465.455" x2="-398.145" y2="-455.93" width="0.1524" layer="91"/>
<wire x1="-398.145" y1="-455.93" x2="-398.145" y2="-446.405" width="0.1524" layer="91"/>
<wire x1="-408.94" y1="-455.93" x2="-398.145" y2="-455.93" width="0.1524" layer="91"/>
<wire x1="-408.94" y1="-465.455" x2="-398.145" y2="-465.455" width="0.1524" layer="91"/>
<wire x1="-408.94" y1="-474.98" x2="-398.145" y2="-474.98" width="0.1524" layer="91"/>
<junction x="-408.94" y="-446.405"/>
<junction x="-408.94" y="-455.93"/>
<junction x="-398.145" y="-455.93"/>
<junction x="-398.145" y="-465.455"/>
<junction x="-408.94" y="-465.455"/>
<junction x="-408.94" y="-474.98"/>
<junction x="-398.145" y="-474.98"/>
<pinref part="E2" gate="G$1" pin="SHIELD"/>
<pinref part="AGND51" gate="VR1" pin="AGND"/>
<pinref part="E6" gate="G$1" pin="SHIELD"/>
<pinref part="E1" gate="G$1" pin="SHIELD"/>
<pinref part="E5" gate="G$1" pin="SHIELD"/>
<pinref part="E2" gate="G$1" pin="P$1"/>
<pinref part="E2" gate="G$1" pin="P$2"/>
<pinref part="E2" gate="G$1" pin="P$3"/>
<pinref part="E6" gate="G$1" pin="P$1"/>
<pinref part="E6" gate="G$1" pin="P$2"/>
<pinref part="E6" gate="G$1" pin="P$3"/>
<pinref part="E1" gate="G$1" pin="P$1"/>
<pinref part="E1" gate="G$1" pin="P$2"/>
<pinref part="E1" gate="G$1" pin="P$3"/>
<pinref part="E5" gate="G$1" pin="P$1"/>
<pinref part="E5" gate="G$1" pin="P$2"/>
<pinref part="E5" gate="G$1" pin="P$3"/>
</segment>
<segment>
<wire x1="-419.735" y1="-480.06" x2="-419.735" y2="-474.98" width="0.1524" layer="91"/>
<wire x1="-429.26" y1="-474.98" x2="-419.735" y2="-474.98" width="0.1524" layer="91"/>
<wire x1="-419.735" y1="-474.98" x2="-419.735" y2="-465.455" width="0.1524" layer="91"/>
<wire x1="-429.26" y1="-465.455" x2="-419.735" y2="-465.455" width="0.1524" layer="91"/>
<wire x1="-419.735" y1="-465.455" x2="-419.735" y2="-455.93" width="0.1524" layer="91"/>
<wire x1="-429.26" y1="-455.93" x2="-419.735" y2="-455.93" width="0.1524" layer="91"/>
<wire x1="-429.26" y1="-446.405" x2="-419.735" y2="-446.405" width="0.1524" layer="91"/>
<wire x1="-419.735" y1="-446.405" x2="-419.735" y2="-455.93" width="0.1524" layer="91"/>
<junction x="-429.26" y="-474.98"/>
<junction x="-419.735" y="-474.98"/>
<junction x="-429.26" y="-465.455"/>
<junction x="-419.735" y="-465.455"/>
<junction x="-429.26" y="-455.93"/>
<junction x="-429.26" y="-446.405"/>
<junction x="-419.735" y="-455.93"/>
<pinref part="AGND47" gate="VR1" pin="AGND"/>
<pinref part="E7" gate="G$1" pin="SHIELD"/>
<pinref part="E3" gate="G$1" pin="SHIELD"/>
<pinref part="E8" gate="G$1" pin="SHIELD"/>
<pinref part="E4" gate="G$1" pin="SHIELD"/>
<pinref part="E4" gate="G$1" pin="P$1"/>
<pinref part="E4" gate="G$1" pin="P$2"/>
<pinref part="E4" gate="G$1" pin="P$3"/>
<pinref part="E8" gate="G$1" pin="P$1"/>
<pinref part="E8" gate="G$1" pin="P$2"/>
<pinref part="E8" gate="G$1" pin="P$3"/>
<pinref part="E3" gate="G$1" pin="P$1"/>
<pinref part="E3" gate="G$1" pin="P$2"/>
<pinref part="E3" gate="G$1" pin="P$3"/>
<pinref part="E7" gate="G$1" pin="P$1"/>
<pinref part="E7" gate="G$1" pin="P$2"/>
<pinref part="E7" gate="G$1" pin="P$3"/>
</segment>
<segment>
<wire x1="-147.32" y1="-161.29" x2="-149.86" y2="-161.29" width="0.1524" layer="91"/>
<pinref part="C50" gate="G$1" pin="2"/>
<pinref part="AGND39" gate="VR1" pin="AGND"/>
</segment>
<segment>
<wire x1="-147.32" y1="-189.23" x2="-149.86" y2="-189.23" width="0.1524" layer="91"/>
<pinref part="C49" gate="G$1" pin="2"/>
<pinref part="AGND40" gate="VR1" pin="AGND"/>
</segment>
<segment>
<wire x1="-148.59" y1="-233.045" x2="-151.13" y2="-233.045" width="0.1524" layer="91"/>
<pinref part="C48" gate="G$1" pin="2"/>
<pinref part="AGND48" gate="VR1" pin="AGND"/>
</segment>
<segment>
<wire x1="-147.955" y1="-260.985" x2="-150.495" y2="-260.985" width="0.1524" layer="91"/>
<pinref part="C47" gate="G$1" pin="2"/>
<pinref part="AGND56" gate="VR1" pin="AGND"/>
</segment>
<segment>
<wire x1="-427.99" y1="-165.735" x2="-430.53" y2="-165.735" width="0.1524" layer="91"/>
<pinref part="C46" gate="G$1" pin="2"/>
<pinref part="AGND57" gate="VR1" pin="AGND"/>
</segment>
<segment>
<wire x1="-427.355" y1="-193.675" x2="-429.895" y2="-193.675" width="0.1524" layer="91"/>
<pinref part="C45" gate="G$1" pin="2"/>
<pinref part="AGND58" gate="VR1" pin="AGND"/>
</segment>
<segment>
<wire x1="-428.625" y1="-231.14" x2="-431.165" y2="-231.14" width="0.1524" layer="91"/>
<pinref part="C44" gate="G$1" pin="2"/>
<pinref part="AGND59" gate="VR1" pin="AGND"/>
</segment>
<segment>
<wire x1="-427.99" y1="-259.08" x2="-430.53" y2="-259.08" width="0.1524" layer="91"/>
<pinref part="C43" gate="G$1" pin="2"/>
<pinref part="AGND60" gate="VR1" pin="AGND"/>
</segment>
<segment>
<wire x1="196.85" y1="-411.48" x2="196.85" y2="-414.02" width="0.1524" layer="91"/>
<pinref part="AGND61" gate="VR1" pin="AGND"/>
<pinref part="C34" gate="G$1" pin="2"/>
</segment>
<segment>
<wire x1="196.85" y1="-354.33" x2="196.85" y2="-351.79" width="0.1524" layer="91"/>
<pinref part="AGND62" gate="VR1" pin="AGND"/>
<pinref part="C35" gate="G$1" pin="2"/>
</segment>
<segment>
<wire x1="196.85" y1="-486.41" x2="196.85" y2="-488.95" width="0.1524" layer="91"/>
<pinref part="AGND63" gate="VR1" pin="AGND"/>
<pinref part="C39" gate="G$1" pin="2"/>
</segment>
<segment>
<wire x1="196.85" y1="-430.53" x2="196.85" y2="-427.99" width="0.1524" layer="91"/>
<pinref part="AGND64" gate="VR1" pin="AGND"/>
<pinref part="C40" gate="G$1" pin="2"/>
</segment>
<segment>
<wire x1="-265.43" y1="-453.39" x2="-265.43" y2="-455.93" width="0.1524" layer="91"/>
<pinref part="AGND65" gate="VR1" pin="AGND"/>
<pinref part="C25" gate="G$1" pin="2"/>
</segment>
<segment>
<wire x1="-265.43" y1="-397.51" x2="-265.43" y2="-394.97" width="0.1524" layer="91"/>
<pinref part="AGND66" gate="VR1" pin="AGND"/>
<pinref part="C36" gate="G$1" pin="2"/>
</segment>
<segment>
<wire x1="-521.335" y1="-358.775" x2="-521.335" y2="-356.235" width="0.1524" layer="91"/>
<pinref part="AGND67" gate="VR1" pin="AGND"/>
<pinref part="C42" gate="G$1" pin="2"/>
</segment>
<segment>
<wire x1="-520.7" y1="-414.655" x2="-520.7" y2="-417.195" width="0.1524" layer="91"/>
<pinref part="AGND68" gate="VR1" pin="AGND"/>
<pinref part="C41" gate="G$1" pin="2"/>
</segment>
<segment>
<wire x1="-527.05" y1="-424.815" x2="-527.05" y2="-422.275" width="0.1524" layer="91"/>
<pinref part="AGND69" gate="VR1" pin="AGND"/>
<pinref part="C38" gate="G$1" pin="2"/>
</segment>
<segment>
<wire x1="-520.7" y1="-480.695" x2="-520.7" y2="-483.235" width="0.1524" layer="91"/>
<pinref part="AGND70" gate="VR1" pin="AGND"/>
<pinref part="C37" gate="G$1" pin="2"/>
</segment>
<segment>
<wire x1="-213.36" y1="-478.79" x2="-213.36" y2="-476.25" width="0.1524" layer="91"/>
<pinref part="AGND72" gate="VR1" pin="AGND"/>
<pinref part="R157" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="-212.09" y1="-391.16" x2="-212.09" y2="-388.62" width="0.1524" layer="91"/>
<pinref part="AGND71" gate="VR1" pin="AGND"/>
<pinref part="R158" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="185.42" y1="-590.55" x2="185.42" y2="-595.63" width="0.1524" layer="91"/>
<pinref part="C73" gate="G$1" pin="2"/>
<pinref part="AGND27" gate="VR1" pin="AGND"/>
</segment>
<segment>
<wire x1="147.32" y1="-587.375" x2="147.32" y2="-594.36" width="0.1524" layer="91"/>
<pinref part="C72" gate="G$1" pin="2"/>
<pinref part="AGND28" gate="VR1" pin="AGND"/>
</segment>
<segment>
<wire x1="232.41" y1="-574.675" x2="242.57" y2="-574.675" width="0.1524" layer="91"/>
<wire x1="242.57" y1="-574.675" x2="242.57" y2="-575.945" width="0.1524" layer="91"/>
<pinref part="U15" gate="A" pin="GND"/>
<pinref part="AGND30" gate="VR1" pin="AGND"/>
</segment>
<segment>
<wire x1="162.56" y1="-594.995" x2="162.56" y2="-589.915" width="0.1524" layer="91"/>
<pinref part="AGND29" gate="VR1" pin="AGND"/>
<pinref part="C78" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$22" class="0">
<segment>
<wire x1="-220.98" y1="-412.75" x2="-227.33" y2="-412.75" width="0.1524" layer="91"/>
<pinref part="R28" gate="G$1" pin="1"/>
<pinref part="U3" gate="A" pin="S1"/>
</segment>
</net>
<net name="N$27" class="0">
<segment>
<wire x1="-210.82" y1="-415.29" x2="-227.33" y2="-415.29" width="0.1524" layer="91"/>
<pinref part="R29" gate="G$1" pin="1"/>
<pinref part="U3" gate="A" pin="S2"/>
</segment>
</net>
<net name="N$29" class="0">
<segment>
<wire x1="-227.33" y1="-417.83" x2="-200.66" y2="-417.83" width="0.1524" layer="91"/>
<pinref part="U3" gate="A" pin="S3"/>
<pinref part="R30" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$30" class="0">
<segment>
<wire x1="-227.33" y1="-420.37" x2="-220.98" y2="-420.37" width="0.1524" layer="91"/>
<pinref part="U3" gate="A" pin="S4"/>
<pinref part="R31" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$40" class="0">
<segment>
<wire x1="-227.33" y1="-422.91" x2="-210.82" y2="-422.91" width="0.1524" layer="91"/>
<pinref part="U3" gate="A" pin="S5"/>
<pinref part="R32" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$42" class="0">
<segment>
<wire x1="-227.33" y1="-425.45" x2="-200.66" y2="-425.45" width="0.1524" layer="91"/>
<pinref part="U3" gate="A" pin="S6"/>
<pinref part="R33" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$54" class="0">
<segment>
<wire x1="-227.33" y1="-427.99" x2="-220.98" y2="-427.99" width="0.1524" layer="91"/>
<pinref part="U3" gate="A" pin="S7"/>
<pinref part="R34" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$55" class="0">
<segment>
<wire x1="-227.33" y1="-430.53" x2="-210.82" y2="-430.53" width="0.1524" layer="91"/>
<pinref part="U3" gate="A" pin="S8"/>
<pinref part="R35" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$57" class="0">
<segment>
<wire x1="-227.33" y1="-433.07" x2="-200.66" y2="-433.07" width="0.1524" layer="91"/>
<pinref part="U3" gate="A" pin="S9"/>
<pinref part="R45" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$58" class="0">
<segment>
<wire x1="-227.33" y1="-435.61" x2="-220.98" y2="-435.61" width="0.1524" layer="91"/>
<pinref part="U3" gate="A" pin="S10"/>
<pinref part="R46" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$61" class="0">
<segment>
<wire x1="-227.33" y1="-438.15" x2="-210.82" y2="-438.15" width="0.1524" layer="91"/>
<pinref part="U3" gate="A" pin="S11"/>
<pinref part="R47" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$62" class="0">
<segment>
<wire x1="-227.33" y1="-440.69" x2="-200.66" y2="-440.69" width="0.1524" layer="91"/>
<pinref part="U3" gate="A" pin="S12"/>
<pinref part="R48" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$63" class="0">
<segment>
<wire x1="-227.33" y1="-443.23" x2="-220.98" y2="-443.23" width="0.1524" layer="91"/>
<pinref part="U3" gate="A" pin="S13"/>
<pinref part="R49" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$64" class="0">
<segment>
<wire x1="-227.33" y1="-445.77" x2="-210.82" y2="-445.77" width="0.1524" layer="91"/>
<pinref part="U3" gate="A" pin="S14"/>
<pinref part="R51" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$65" class="0">
<segment>
<wire x1="-227.33" y1="-448.31" x2="-200.66" y2="-448.31" width="0.1524" layer="91"/>
<pinref part="U3" gate="A" pin="S15"/>
<pinref part="R52" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$66" class="0">
<segment>
<wire x1="-227.33" y1="-450.85" x2="-220.98" y2="-450.85" width="0.1524" layer="91"/>
<pinref part="U3" gate="A" pin="S16"/>
<pinref part="R54" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$67" class="0">
<segment>
<wire x1="-184.15" y1="-450.85" x2="-187.96" y2="-450.85" width="0.1524" layer="91"/>
<wire x1="-184.15" y1="-458.47" x2="-187.96" y2="-458.47" width="0.1524" layer="91"/>
<wire x1="-187.96" y1="-458.47" x2="-187.96" y2="-450.85" width="0.1524" layer="91"/>
<wire x1="-187.96" y1="-450.85" x2="-210.82" y2="-450.85" width="0.1524" layer="91"/>
<wire x1="-187.96" y1="-448.31" x2="-187.96" y2="-450.85" width="0.1524" layer="91"/>
<wire x1="-190.5" y1="-448.31" x2="-187.96" y2="-448.31" width="0.1524" layer="91"/>
<wire x1="-187.96" y1="-445.77" x2="-187.96" y2="-448.31" width="0.1524" layer="91"/>
<wire x1="-200.66" y1="-445.77" x2="-187.96" y2="-445.77" width="0.1524" layer="91"/>
<wire x1="-210.82" y1="-412.75" x2="-187.96" y2="-412.75" width="0.1524" layer="91"/>
<wire x1="-187.96" y1="-412.75" x2="-187.96" y2="-415.29" width="0.1524" layer="91"/>
<wire x1="-200.66" y1="-415.29" x2="-187.96" y2="-415.29" width="0.1524" layer="91"/>
<wire x1="-187.96" y1="-415.29" x2="-187.96" y2="-417.83" width="0.1524" layer="91"/>
<wire x1="-190.5" y1="-417.83" x2="-187.96" y2="-417.83" width="0.1524" layer="91"/>
<wire x1="-187.96" y1="-417.83" x2="-187.96" y2="-420.37" width="0.1524" layer="91"/>
<wire x1="-210.82" y1="-420.37" x2="-187.96" y2="-420.37" width="0.1524" layer="91"/>
<wire x1="-187.96" y1="-420.37" x2="-187.96" y2="-422.91" width="0.1524" layer="91"/>
<wire x1="-200.66" y1="-422.91" x2="-187.96" y2="-422.91" width="0.1524" layer="91"/>
<wire x1="-187.96" y1="-422.91" x2="-187.96" y2="-425.45" width="0.1524" layer="91"/>
<wire x1="-190.5" y1="-425.45" x2="-187.96" y2="-425.45" width="0.1524" layer="91"/>
<wire x1="-187.96" y1="-425.45" x2="-187.96" y2="-427.99" width="0.1524" layer="91"/>
<wire x1="-210.82" y1="-427.99" x2="-187.96" y2="-427.99" width="0.1524" layer="91"/>
<wire x1="-187.96" y1="-427.99" x2="-187.96" y2="-430.53" width="0.1524" layer="91"/>
<wire x1="-200.66" y1="-430.53" x2="-187.96" y2="-430.53" width="0.1524" layer="91"/>
<wire x1="-187.96" y1="-430.53" x2="-187.96" y2="-433.07" width="0.1524" layer="91"/>
<wire x1="-190.5" y1="-433.07" x2="-187.96" y2="-433.07" width="0.1524" layer="91"/>
<wire x1="-187.96" y1="-433.07" x2="-187.96" y2="-435.61" width="0.1524" layer="91"/>
<wire x1="-210.82" y1="-435.61" x2="-187.96" y2="-435.61" width="0.1524" layer="91"/>
<wire x1="-187.96" y1="-435.61" x2="-187.96" y2="-438.15" width="0.1524" layer="91"/>
<wire x1="-200.66" y1="-438.15" x2="-187.96" y2="-438.15" width="0.1524" layer="91"/>
<wire x1="-187.96" y1="-438.15" x2="-187.96" y2="-440.69" width="0.1524" layer="91"/>
<wire x1="-190.5" y1="-440.69" x2="-187.96" y2="-440.69" width="0.1524" layer="91"/>
<wire x1="-187.96" y1="-440.69" x2="-187.96" y2="-443.23" width="0.1524" layer="91"/>
<wire x1="-187.96" y1="-443.23" x2="-187.96" y2="-445.77" width="0.1524" layer="91"/>
<wire x1="-210.82" y1="-443.23" x2="-187.96" y2="-443.23" width="0.1524" layer="91"/>
<junction x="-187.96" y="-448.31"/>
<junction x="-187.96" y="-445.77"/>
<junction x="-187.96" y="-415.29"/>
<junction x="-187.96" y="-417.83"/>
<junction x="-187.96" y="-420.37"/>
<junction x="-187.96" y="-422.91"/>
<junction x="-187.96" y="-425.45"/>
<junction x="-187.96" y="-427.99"/>
<junction x="-187.96" y="-430.53"/>
<junction x="-187.96" y="-433.07"/>
<junction x="-187.96" y="-435.61"/>
<junction x="-187.96" y="-438.15"/>
<junction x="-187.96" y="-440.69"/>
<junction x="-187.96" y="-443.23"/>
<junction x="-187.96" y="-450.85"/>
<junction x="-187.96" y="-450.85"/>
<pinref part="R37" gate="G$1" pin="2"/>
<pinref part="U$3" gate="B" pin="M"/>
<pinref part="R54" gate="G$1" pin="2"/>
<pinref part="R52" gate="G$1" pin="2"/>
<pinref part="R51" gate="G$1" pin="2"/>
<pinref part="R28" gate="G$1" pin="2"/>
<pinref part="R29" gate="G$1" pin="2"/>
<pinref part="R30" gate="G$1" pin="2"/>
<pinref part="R31" gate="G$1" pin="2"/>
<pinref part="R32" gate="G$1" pin="2"/>
<pinref part="R33" gate="G$1" pin="2"/>
<pinref part="R34" gate="G$1" pin="2"/>
<pinref part="R35" gate="G$1" pin="2"/>
<pinref part="R45" gate="G$1" pin="2"/>
<pinref part="R46" gate="G$1" pin="2"/>
<pinref part="R47" gate="G$1" pin="2"/>
<pinref part="R48" gate="G$1" pin="2"/>
<pinref part="R49" gate="G$1" pin="2"/>
</segment>
</net>
<net name="S16_IMP" class="0">
<segment>
<wire x1="30.48" y1="-411.48" x2="30.48" y2="-370.84" width="0.1524" layer="91"/>
<wire x1="158.75" y1="-445.77" x2="151.13" y2="-445.77" width="0.1524" layer="91"/>
<wire x1="151.13" y1="-445.77" x2="151.13" y2="-370.84" width="0.1524" layer="91"/>
<wire x1="151.13" y1="-370.84" x2="30.48" y2="-370.84" width="0.1524" layer="91"/>
<wire x1="158.75" y1="-370.84" x2="151.13" y2="-370.84" width="0.1524" layer="91"/>
<wire x1="30.48" y1="-370.84" x2="12.7" y2="-370.84" width="0.1524" layer="91"/>
<junction x="151.13" y="-370.84"/>
<junction x="30.48" y="-370.84"/>
<pinref part="R59" gate="G$1" pin="2"/>
<pinref part="U7" gate="A" pin="S1"/>
<pinref part="U5" gate="A" pin="S1"/>
<label x="12.7" y="-370.84" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R94" gate="G$1" pin="2"/>
<wire x1="-462.915" y1="-255.27" x2="-455.295" y2="-255.27" width="0.1524" layer="91"/>
<wire x1="-455.295" y1="-255.27" x2="-455.295" y2="-248.92" width="0.1524" layer="91"/>
<pinref part="U11" gate="A" pin="OUT_C"/>
<wire x1="-379.73" y1="-236.22" x2="-372.11" y2="-236.22" width="0.1524" layer="91"/>
<wire x1="-372.11" y1="-236.22" x2="-358.14" y2="-236.22" width="0.1524" layer="91"/>
<junction x="-372.11" y="-236.22"/>
<wire x1="-372.11" y1="-274.32" x2="-372.11" y2="-236.22" width="0.1524" layer="91"/>
<wire x1="-440.055" y1="-274.32" x2="-372.11" y2="-274.32" width="0.1524" layer="91"/>
<pinref part="U11" gate="A" pin="-IN_C"/>
<wire x1="-415.29" y1="-248.92" x2="-440.055" y2="-248.92" width="0.1524" layer="91"/>
<wire x1="-440.055" y1="-248.92" x2="-440.055" y2="-274.32" width="0.1524" layer="91"/>
<junction x="-440.055" y="-248.92"/>
<wire x1="-440.055" y1="-248.92" x2="-455.295" y2="-248.92" width="0.1524" layer="91"/>
<label x="-368.3" y="-236.22" size="1.778" layer="95"/>
</segment>
</net>
<net name="S12_IMP" class="0">
<segment>
<wire x1="158.75" y1="-450.85" x2="146.05" y2="-450.85" width="0.1524" layer="91"/>
<wire x1="146.05" y1="-450.85" x2="146.05" y2="-375.92" width="0.1524" layer="91"/>
<wire x1="40.64" y1="-411.48" x2="40.64" y2="-375.92" width="0.1524" layer="91"/>
<wire x1="40.64" y1="-375.92" x2="146.05" y2="-375.92" width="0.1524" layer="91"/>
<wire x1="146.05" y1="-375.92" x2="158.75" y2="-375.92" width="0.1524" layer="91"/>
<wire x1="40.64" y1="-375.92" x2="12.7" y2="-375.92" width="0.1524" layer="91"/>
<junction x="146.05" y="-375.92"/>
<junction x="40.64" y="-375.92"/>
<pinref part="U7" gate="A" pin="S3"/>
<pinref part="R131" gate="G$1" pin="2"/>
<pinref part="U5" gate="A" pin="S3"/>
<label x="12.7" y="-375.92" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U10" gate="A" pin="OUT_C"/>
<wire x1="-379.73" y1="-170.815" x2="-372.11" y2="-170.815" width="0.1524" layer="91"/>
<pinref part="R90" gate="G$1" pin="2"/>
<wire x1="-462.915" y1="-189.865" x2="-455.295" y2="-189.865" width="0.1524" layer="91"/>
<wire x1="-455.295" y1="-189.865" x2="-455.295" y2="-183.515" width="0.1524" layer="91"/>
<pinref part="U10" gate="A" pin="-IN_C"/>
<wire x1="-415.29" y1="-183.515" x2="-440.055" y2="-183.515" width="0.1524" layer="91"/>
<wire x1="-440.055" y1="-183.515" x2="-455.295" y2="-183.515" width="0.1524" layer="91"/>
<junction x="-440.055" y="-183.515"/>
<wire x1="-440.055" y1="-183.515" x2="-440.055" y2="-208.915" width="0.1524" layer="91"/>
<wire x1="-440.055" y1="-208.915" x2="-372.11" y2="-208.915" width="0.1524" layer="91"/>
<wire x1="-372.11" y1="-208.915" x2="-372.11" y2="-170.815" width="0.1524" layer="91"/>
<junction x="-372.11" y="-170.815"/>
<wire x1="-372.11" y1="-170.815" x2="-358.14" y2="-170.815" width="0.1524" layer="91"/>
<label x="-368.3" y="-170.18" size="1.778" layer="95"/>
</segment>
</net>
<net name="S11_IMP" class="0">
<segment>
<wire x1="45.72" y1="-419.1" x2="45.72" y2="-378.46" width="0.1524" layer="91"/>
<wire x1="158.75" y1="-453.39" x2="143.51" y2="-453.39" width="0.1524" layer="91"/>
<wire x1="143.51" y1="-453.39" x2="143.51" y2="-378.46" width="0.1524" layer="91"/>
<wire x1="143.51" y1="-378.46" x2="45.72" y2="-378.46" width="0.1524" layer="91"/>
<wire x1="158.75" y1="-378.46" x2="143.51" y2="-378.46" width="0.1524" layer="91"/>
<wire x1="45.72" y1="-378.46" x2="12.7" y2="-378.46" width="0.1524" layer="91"/>
<junction x="143.51" y="-378.46"/>
<junction x="45.72" y="-378.46"/>
<pinref part="R60" gate="G$1" pin="2"/>
<pinref part="U7" gate="A" pin="S4"/>
<pinref part="U5" gate="A" pin="S4"/>
<label x="12.7" y="-378.46" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U10" gate="A" pin="OUT_D"/>
<wire x1="-369.57" y1="-173.355" x2="-379.73" y2="-173.355" width="0.1524" layer="91"/>
<pinref part="R88" gate="G$1" pin="2"/>
<wire x1="-462.915" y1="-199.39" x2="-450.215" y2="-199.39" width="0.1524" layer="91"/>
<wire x1="-450.215" y1="-199.39" x2="-450.215" y2="-188.595" width="0.1524" layer="91"/>
<pinref part="U10" gate="A" pin="-IN_D"/>
<wire x1="-415.29" y1="-188.595" x2="-437.515" y2="-188.595" width="0.1524" layer="91"/>
<wire x1="-437.515" y1="-188.595" x2="-450.215" y2="-188.595" width="0.1524" layer="91"/>
<junction x="-437.515" y="-188.595"/>
<wire x1="-437.515" y1="-188.595" x2="-437.515" y2="-211.455" width="0.1524" layer="91"/>
<wire x1="-437.515" y1="-211.455" x2="-369.57" y2="-211.455" width="0.1524" layer="91"/>
<wire x1="-369.57" y1="-211.455" x2="-369.57" y2="-173.355" width="0.1524" layer="91"/>
<junction x="-369.57" y="-173.355"/>
<wire x1="-369.57" y1="-173.355" x2="-358.14" y2="-173.355" width="0.1524" layer="91"/>
<label x="-368.3" y="-172.72" size="1.778" layer="95"/>
</segment>
</net>
<net name="MUX1" class="0">
<segment>
<wire x1="-200.66" y1="-463.55" x2="-213.36" y2="-463.55" width="0.1524" layer="91"/>
<wire x1="-213.36" y1="-463.55" x2="-224.79" y2="-463.55" width="0.1524" layer="91"/>
<wire x1="-213.36" y1="-466.09" x2="-213.36" y2="-463.55" width="0.1524" layer="91"/>
<junction x="-213.36" y="-463.55"/>
<pinref part="R36" gate="G$1" pin="2"/>
<pinref part="R157" gate="G$1" pin="2"/>
<label x="-223.52" y="-463.55" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U5" gate="A" pin="D"/>
<wire x1="151.13" y1="-365.76" x2="158.75" y2="-365.76" width="0.1524" layer="91"/>
<wire x1="151.13" y1="-353.06" x2="151.13" y2="-365.76" width="0.1524" layer="91"/>
<wire x1="213.36" y1="-353.06" x2="151.13" y2="-353.06" width="0.1524" layer="91"/>
<label x="207.01" y="-353.06" size="1.778" layer="95"/>
</segment>
</net>
<net name="S8_IMP" class="0">
<segment>
<wire x1="50.8" y1="-411.48" x2="50.8" y2="-381" width="0.1524" layer="91"/>
<wire x1="158.75" y1="-455.93" x2="140.97" y2="-455.93" width="0.1524" layer="91"/>
<wire x1="140.97" y1="-455.93" x2="140.97" y2="-381" width="0.1524" layer="91"/>
<wire x1="140.97" y1="-381" x2="50.8" y2="-381" width="0.1524" layer="91"/>
<wire x1="158.75" y1="-381" x2="140.97" y2="-381" width="0.1524" layer="91"/>
<wire x1="50.8" y1="-381" x2="12.7" y2="-381" width="0.1524" layer="91"/>
<junction x="140.97" y="-381"/>
<junction x="50.8" y="-381"/>
<pinref part="R130" gate="G$1" pin="2"/>
<pinref part="U7" gate="A" pin="S5"/>
<pinref part="U5" gate="A" pin="S5"/>
<label x="12.7" y="-381" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U9" gate="A" pin="OUT_C"/>
<wire x1="-100.33" y1="-238.125" x2="-92.71" y2="-238.125" width="0.1524" layer="91"/>
<pinref part="R86" gate="G$1" pin="2"/>
<wire x1="-183.515" y1="-257.175" x2="-175.895" y2="-257.175" width="0.1524" layer="91"/>
<wire x1="-175.895" y1="-257.175" x2="-175.895" y2="-250.825" width="0.1524" layer="91"/>
<pinref part="U9" gate="A" pin="-IN_C"/>
<wire x1="-135.89" y1="-250.825" x2="-160.655" y2="-250.825" width="0.1524" layer="91"/>
<wire x1="-160.655" y1="-250.825" x2="-175.895" y2="-250.825" width="0.1524" layer="91"/>
<junction x="-160.655" y="-250.825"/>
<wire x1="-160.655" y1="-250.825" x2="-160.655" y2="-276.225" width="0.1524" layer="91"/>
<wire x1="-160.655" y1="-276.225" x2="-92.71" y2="-276.225" width="0.1524" layer="91"/>
<wire x1="-92.71" y1="-276.225" x2="-92.71" y2="-238.125" width="0.1524" layer="91"/>
<junction x="-92.71" y="-238.125"/>
<wire x1="-92.71" y1="-238.125" x2="-78.74" y2="-238.125" width="0.1524" layer="91"/>
<label x="-87.63" y="-237.49" size="1.778" layer="95"/>
</segment>
</net>
<net name="S7_IMP" class="0">
<segment>
<wire x1="55.88" y1="-419.1" x2="55.88" y2="-383.54" width="0.1524" layer="91"/>
<wire x1="138.43" y1="-383.54" x2="55.88" y2="-383.54" width="0.1524" layer="91"/>
<wire x1="158.75" y1="-458.47" x2="138.43" y2="-458.47" width="0.1524" layer="91"/>
<wire x1="138.43" y1="-458.47" x2="138.43" y2="-383.54" width="0.1524" layer="91"/>
<wire x1="158.75" y1="-383.54" x2="138.43" y2="-383.54" width="0.1524" layer="91"/>
<wire x1="55.88" y1="-383.54" x2="12.7" y2="-383.54" width="0.1524" layer="91"/>
<junction x="138.43" y="-383.54"/>
<junction x="55.88" y="-383.54"/>
<pinref part="R42" gate="G$1" pin="2"/>
<pinref part="U7" gate="A" pin="S6"/>
<pinref part="U5" gate="A" pin="S6"/>
<label x="12.7" y="-383.54" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U9" gate="A" pin="OUT_D"/>
<wire x1="-90.17" y1="-240.665" x2="-100.33" y2="-240.665" width="0.1524" layer="91"/>
<pinref part="R84" gate="G$1" pin="2"/>
<wire x1="-183.515" y1="-266.7" x2="-170.815" y2="-266.7" width="0.1524" layer="91"/>
<wire x1="-170.815" y1="-266.7" x2="-170.815" y2="-255.905" width="0.1524" layer="91"/>
<pinref part="U9" gate="A" pin="-IN_D"/>
<wire x1="-135.89" y1="-255.905" x2="-158.115" y2="-255.905" width="0.1524" layer="91"/>
<wire x1="-158.115" y1="-255.905" x2="-170.815" y2="-255.905" width="0.1524" layer="91"/>
<junction x="-158.115" y="-255.905"/>
<wire x1="-158.115" y1="-255.905" x2="-158.115" y2="-278.765" width="0.1524" layer="91"/>
<wire x1="-158.115" y1="-278.765" x2="-90.17" y2="-278.765" width="0.1524" layer="91"/>
<wire x1="-90.17" y1="-278.765" x2="-90.17" y2="-240.665" width="0.1524" layer="91"/>
<junction x="-90.17" y="-240.665"/>
<wire x1="-90.17" y1="-240.665" x2="-78.74" y2="-240.665" width="0.1524" layer="91"/>
<label x="-87.63" y="-240.03" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$56" class="0">
<segment>
<wire x1="-457.2" y1="-575.945" x2="-457.2" y2="-570.865" width="0.1524" layer="91"/>
<wire x1="-451.485" y1="-570.865" x2="-457.2" y2="-570.865" width="0.1524" layer="91"/>
<wire x1="-499.11" y1="-570.865" x2="-492.125" y2="-570.865" width="0.1524" layer="91"/>
<wire x1="-492.125" y1="-570.865" x2="-492.125" y2="-577.85" width="0.1524" layer="91"/>
<wire x1="-457.2" y1="-570.865" x2="-478.79" y2="-570.865" width="0.1524" layer="91"/>
<wire x1="-478.79" y1="-570.865" x2="-492.125" y2="-570.865" width="0.1524" layer="91"/>
<wire x1="-478.79" y1="-577.85" x2="-478.79" y2="-570.865" width="0.1524" layer="91"/>
<wire x1="-451.485" y1="-575.945" x2="-457.2" y2="-575.945" width="0.1524" layer="91"/>
<junction x="-457.2" y="-570.865"/>
<junction x="-492.125" y="-570.865"/>
<junction x="-478.79" y="-570.865"/>
<pinref part="U2" gate="A" pin="VIN"/>
<pinref part="R8" gate="G$1" pin="2"/>
<pinref part="C5" gate="G$1" pin="1"/>
<pinref part="C6" gate="G$1" pin="1"/>
<pinref part="U2" gate="A" pin="EN/UVLO"/>
</segment>
</net>
<net name="N$33" class="0">
<segment>
<wire x1="130.81" y1="-681.355" x2="128.27" y2="-681.355" width="0.1524" layer="91"/>
<pinref part="R132" gate="G$1" pin="1"/>
<pinref part="U13" gate="A" pin="VOUT"/>
</segment>
</net>
<net name="N$107" class="0">
<segment>
<wire x1="67.31" y1="-681.355" x2="64.135" y2="-681.355" width="0.1524" layer="91"/>
<wire x1="64.135" y1="-686.435" x2="64.135" y2="-681.355" width="0.1524" layer="91"/>
<wire x1="64.135" y1="-681.355" x2="41.275" y2="-681.355" width="0.1524" layer="91"/>
<wire x1="41.275" y1="-685.165" x2="41.275" y2="-681.355" width="0.1524" layer="91"/>
<wire x1="41.275" y1="-681.355" x2="29.845" y2="-681.355" width="0.1524" layer="91"/>
<wire x1="29.845" y1="-685.165" x2="29.845" y2="-681.355" width="0.1524" layer="91"/>
<wire x1="29.845" y1="-681.355" x2="26.035" y2="-681.355" width="0.1524" layer="91"/>
<wire x1="67.31" y1="-686.435" x2="64.135" y2="-686.435" width="0.1524" layer="91"/>
<junction x="64.135" y="-681.355"/>
<junction x="41.275" y="-681.355"/>
<junction x="29.845" y="-681.355"/>
<pinref part="U13" gate="A" pin="VIN"/>
<pinref part="C67" gate="G$1" pin="1"/>
<pinref part="C68" gate="G$1" pin="1"/>
<pinref part="R139" gate="G$1" pin="2"/>
<pinref part="U13" gate="A" pin="EN/UVLO"/>
</segment>
</net>
<net name="N$106" class="0">
<segment>
<wire x1="69.85" y1="-579.755" x2="63.5" y2="-579.755" width="0.1524" layer="91"/>
<wire x1="63.5" y1="-579.755" x2="61.595" y2="-579.755" width="0.1524" layer="91"/>
<wire x1="63.5" y1="-582.93" x2="63.5" y2="-579.755" width="0.1524" layer="91"/>
<junction x="63.5" y="-579.755"/>
<pinref part="U1" gate="A" pin="VOUT"/>
<pinref part="R4" gate="G$1" pin="2"/>
<pinref part="C10" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$110" class="0">
<segment>
<wire x1="69.85" y1="-574.675" x2="67.945" y2="-574.675" width="0.1524" layer="91"/>
<wire x1="69.85" y1="-584.835" x2="67.945" y2="-584.835" width="0.1524" layer="91"/>
<wire x1="67.945" y1="-584.835" x2="67.945" y2="-574.675" width="0.1524" layer="91"/>
<wire x1="19.685" y1="-574.675" x2="22.86" y2="-574.675" width="0.1524" layer="91"/>
<wire x1="22.86" y1="-574.675" x2="22.86" y2="-579.755" width="0.1524" layer="91"/>
<wire x1="22.86" y1="-574.675" x2="35.56" y2="-574.675" width="0.1524" layer="91"/>
<wire x1="35.56" y1="-574.675" x2="35.56" y2="-579.755" width="0.1524" layer="91"/>
<wire x1="67.945" y1="-574.675" x2="35.56" y2="-574.675" width="0.1524" layer="91"/>
<junction x="22.86" y="-574.675"/>
<junction x="67.945" y="-574.675"/>
<junction x="35.56" y="-574.675"/>
<pinref part="U1" gate="A" pin="VIN"/>
<pinref part="U1" gate="A" pin="\EN"/>
<pinref part="R6" gate="G$1" pin="2"/>
<pinref part="C3" gate="G$1" pin="1"/>
<pinref part="C4" gate="G$1" pin="1"/>
</segment>
</net>
<net name="S15_IMP" class="0">
<segment>
<wire x1="35.56" y1="-419.1" x2="35.56" y2="-373.38" width="0.1524" layer="91"/>
<wire x1="158.75" y1="-448.31" x2="148.59" y2="-448.31" width="0.1524" layer="91"/>
<wire x1="148.59" y1="-448.31" x2="148.59" y2="-373.38" width="0.1524" layer="91"/>
<wire x1="148.59" y1="-373.38" x2="35.56" y2="-373.38" width="0.1524" layer="91"/>
<wire x1="158.75" y1="-373.38" x2="148.59" y2="-373.38" width="0.1524" layer="91"/>
<wire x1="35.56" y1="-373.38" x2="12.7" y2="-373.38" width="0.1524" layer="91"/>
<junction x="148.59" y="-373.38"/>
<junction x="35.56" y="-373.38"/>
<pinref part="R58" gate="G$1" pin="2"/>
<pinref part="U7" gate="A" pin="S2"/>
<pinref part="U5" gate="A" pin="S2"/>
<label x="12.7" y="-373.38" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R92" gate="G$1" pin="2"/>
<wire x1="-462.915" y1="-264.795" x2="-450.215" y2="-264.795" width="0.1524" layer="91"/>
<wire x1="-450.215" y1="-264.795" x2="-450.215" y2="-254" width="0.1524" layer="91"/>
<pinref part="U11" gate="A" pin="OUT_D"/>
<wire x1="-369.57" y1="-238.76" x2="-379.73" y2="-238.76" width="0.1524" layer="91"/>
<wire x1="-369.57" y1="-238.76" x2="-358.14" y2="-238.76" width="0.1524" layer="91"/>
<junction x="-369.57" y="-238.76"/>
<wire x1="-369.57" y1="-276.86" x2="-369.57" y2="-238.76" width="0.1524" layer="91"/>
<wire x1="-437.515" y1="-276.86" x2="-369.57" y2="-276.86" width="0.1524" layer="91"/>
<pinref part="U11" gate="A" pin="-IN_D"/>
<wire x1="-415.29" y1="-254" x2="-437.515" y2="-254" width="0.1524" layer="91"/>
<wire x1="-437.515" y1="-254" x2="-437.515" y2="-276.86" width="0.1524" layer="91"/>
<junction x="-437.515" y="-254"/>
<wire x1="-437.515" y1="-254" x2="-450.215" y2="-254" width="0.1524" layer="91"/>
<label x="-368.3" y="-238.76" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$74" class="0">
<segment>
<wire x1="-215.9" y1="-166.37" x2="-226.695" y2="-166.37" width="0.1524" layer="91"/>
<wire x1="-215.9" y1="-200.66" x2="-215.9" y2="-166.37" width="0.1524" layer="91"/>
<wire x1="-193.04" y1="-166.37" x2="-215.9" y2="-166.37" width="0.1524" layer="91"/>
<junction x="-226.695" y="-166.37"/>
<junction x="-215.9" y="-166.37"/>
<pinref part="S2" gate="G$1" pin="SHIELD"/>
<pinref part="R71" gate="G$1" pin="2"/>
<pinref part="R72" gate="G$1" pin="1"/>
<pinref part="S2" gate="G$1" pin="P$1"/>
<pinref part="S2" gate="G$1" pin="P$2"/>
<pinref part="S2" gate="G$1" pin="P$3"/>
</segment>
</net>
<net name="N$75" class="0">
<segment>
<wire x1="-226.695" y1="-175.895" x2="-208.915" y2="-175.895" width="0.1524" layer="91"/>
<wire x1="-208.915" y1="-175.895" x2="-192.405" y2="-175.895" width="0.1524" layer="91"/>
<wire x1="-208.915" y1="-200.66" x2="-208.915" y2="-175.895" width="0.1524" layer="91"/>
<junction x="-226.695" y="-175.895"/>
<junction x="-208.915" y="-175.895"/>
<pinref part="S1" gate="G$1" pin="SHIELD"/>
<pinref part="R73" gate="G$1" pin="1"/>
<pinref part="R64" gate="G$1" pin="2"/>
<pinref part="S1" gate="G$1" pin="P$1"/>
<pinref part="S1" gate="G$1" pin="P$2"/>
<pinref part="S1" gate="G$1" pin="P$3"/>
</segment>
</net>
<net name="N$111" class="0">
<segment>
<wire x1="-226.695" y1="-185.42" x2="-201.93" y2="-185.42" width="0.1524" layer="91"/>
<wire x1="-201.93" y1="-185.42" x2="-192.405" y2="-185.42" width="0.1524" layer="91"/>
<wire x1="-201.93" y1="-200.66" x2="-201.93" y2="-185.42" width="0.1524" layer="91"/>
<junction x="-226.695" y="-185.42"/>
<junction x="-201.93" y="-185.42"/>
<pinref part="S4" gate="G$1" pin="SHIELD"/>
<pinref part="R82" gate="G$1" pin="1"/>
<pinref part="R83" gate="G$1" pin="2"/>
<pinref part="S4" gate="G$1" pin="P$1"/>
<pinref part="S4" gate="G$1" pin="P$2"/>
<pinref part="S4" gate="G$1" pin="P$3"/>
</segment>
</net>
<net name="N$112" class="0">
<segment>
<wire x1="-226.695" y1="-194.945" x2="-194.945" y2="-194.945" width="0.1524" layer="91"/>
<wire x1="-194.945" y1="-194.945" x2="-192.405" y2="-194.945" width="0.1524" layer="91"/>
<wire x1="-194.945" y1="-200.66" x2="-194.945" y2="-194.945" width="0.1524" layer="91"/>
<junction x="-226.695" y="-194.945"/>
<junction x="-194.945" y="-194.945"/>
<pinref part="S3" gate="G$1" pin="SHIELD"/>
<pinref part="R80" gate="G$1" pin="1"/>
<pinref part="R81" gate="G$1" pin="2"/>
<pinref part="S3" gate="G$1" pin="P$1"/>
<pinref part="S3" gate="G$1" pin="P$2"/>
<pinref part="S3" gate="G$1" pin="P$3"/>
</segment>
</net>
<net name="S7" class="0">
<segment>
<wire x1="-168.275" y1="-253.365" x2="-135.89" y2="-253.365" width="0.1524" layer="91"/>
<wire x1="-168.275" y1="-253.365" x2="-168.275" y2="-269.24" width="0.1524" layer="91"/>
<wire x1="-168.275" y1="-269.24" x2="-227.965" y2="-269.24" width="0.1524" layer="91"/>
<pinref part="U9" gate="A" pin="+IN_D"/>
<pinref part="S7" gate="G$1" pin="PIN"/>
<label x="-223.52" y="-269.24" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R98" gate="G$1" pin="2"/>
<wire x1="-405.13" y1="-381.635" x2="-399.415" y2="-381.635" width="0.1524" layer="91"/>
<label x="-403.86" y="-381.635" size="1.778" layer="95"/>
</segment>
</net>
<net name="S8" class="0">
<segment>
<wire x1="-173.355" y1="-259.715" x2="-227.965" y2="-259.715" width="0.1524" layer="91"/>
<wire x1="-173.355" y1="-248.285" x2="-173.355" y2="-259.715" width="0.1524" layer="91"/>
<wire x1="-135.89" y1="-248.285" x2="-173.355" y2="-248.285" width="0.1524" layer="91"/>
<pinref part="S8" gate="G$1" pin="PIN"/>
<pinref part="U9" gate="A" pin="+IN_C"/>
<label x="-223.52" y="-259.08" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="-405.13" y1="-376.555" x2="-399.415" y2="-376.555" width="0.1524" layer="91"/>
<pinref part="R99" gate="G$1" pin="2"/>
<label x="-403.86" y="-376.555" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$100" class="0">
<segment>
<wire x1="-217.17" y1="-238.125" x2="-227.965" y2="-238.125" width="0.1524" layer="91"/>
<wire x1="-217.17" y1="-272.415" x2="-217.17" y2="-238.125" width="0.1524" layer="91"/>
<wire x1="-194.31" y1="-238.125" x2="-217.17" y2="-238.125" width="0.1524" layer="91"/>
<junction x="-227.965" y="-238.125"/>
<junction x="-217.17" y="-238.125"/>
<pinref part="S6" gate="G$1" pin="SHIELD"/>
<pinref part="R104" gate="G$1" pin="2"/>
<pinref part="R74" gate="G$1" pin="1"/>
<pinref part="S6" gate="G$1" pin="P$1"/>
<pinref part="S6" gate="G$1" pin="P$2"/>
<pinref part="S6" gate="G$1" pin="P$3"/>
</segment>
</net>
<net name="N$102" class="0">
<segment>
<wire x1="-227.965" y1="-247.65" x2="-210.185" y2="-247.65" width="0.1524" layer="91"/>
<wire x1="-210.185" y1="-247.65" x2="-193.675" y2="-247.65" width="0.1524" layer="91"/>
<wire x1="-210.185" y1="-272.415" x2="-210.185" y2="-247.65" width="0.1524" layer="91"/>
<junction x="-227.965" y="-247.65"/>
<junction x="-210.185" y="-247.65"/>
<pinref part="S5" gate="G$1" pin="SHIELD"/>
<pinref part="R75" gate="G$1" pin="1"/>
<pinref part="R65" gate="G$1" pin="2"/>
<pinref part="S5" gate="G$1" pin="P$1"/>
<pinref part="S5" gate="G$1" pin="P$2"/>
<pinref part="S5" gate="G$1" pin="P$3"/>
</segment>
</net>
<net name="N$103" class="0">
<segment>
<wire x1="-227.965" y1="-257.175" x2="-203.2" y2="-257.175" width="0.1524" layer="91"/>
<wire x1="-203.2" y1="-257.175" x2="-193.675" y2="-257.175" width="0.1524" layer="91"/>
<wire x1="-203.2" y1="-272.415" x2="-203.2" y2="-257.175" width="0.1524" layer="91"/>
<junction x="-227.965" y="-257.175"/>
<junction x="-203.2" y="-257.175"/>
<pinref part="S8" gate="G$1" pin="SHIELD"/>
<pinref part="R86" gate="G$1" pin="1"/>
<pinref part="R87" gate="G$1" pin="2"/>
<pinref part="S8" gate="G$1" pin="P$1"/>
<pinref part="S8" gate="G$1" pin="P$2"/>
<pinref part="S8" gate="G$1" pin="P$3"/>
</segment>
</net>
<net name="N$105" class="0">
<segment>
<wire x1="-227.965" y1="-266.7" x2="-196.215" y2="-266.7" width="0.1524" layer="91"/>
<wire x1="-196.215" y1="-266.7" x2="-193.675" y2="-266.7" width="0.1524" layer="91"/>
<wire x1="-196.215" y1="-272.415" x2="-196.215" y2="-266.7" width="0.1524" layer="91"/>
<junction x="-227.965" y="-266.7"/>
<junction x="-196.215" y="-266.7"/>
<pinref part="S7" gate="G$1" pin="SHIELD"/>
<pinref part="R84" gate="G$1" pin="1"/>
<pinref part="R85" gate="G$1" pin="2"/>
<pinref part="S7" gate="G$1" pin="P$1"/>
<pinref part="S7" gate="G$1" pin="P$2"/>
<pinref part="S7" gate="G$1" pin="P$3"/>
</segment>
</net>
<net name="S9" class="0">
<segment>
<wire x1="-457.835" y1="-178.435" x2="-415.29" y2="-178.435" width="0.1524" layer="91"/>
<wire x1="-457.835" y1="-182.88" x2="-457.835" y2="-178.435" width="0.1524" layer="91"/>
<wire x1="-507.365" y1="-182.88" x2="-457.835" y2="-182.88" width="0.1524" layer="91"/>
<pinref part="U10" gate="A" pin="+IN_B"/>
<pinref part="S9" gate="G$1" pin="PIN"/>
<label x="-502.92" y="-182.88" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R66" gate="G$1" pin="2"/>
<wire x1="-405.13" y1="-412.115" x2="-399.415" y2="-412.115" width="0.1524" layer="91"/>
<label x="-403.86" y="-412.115" size="1.778" layer="95"/>
</segment>
</net>
<net name="S10" class="0">
<segment>
<wire x1="-414.655" y1="-173.355" x2="-415.29" y2="-173.355" width="0.1524" layer="91"/>
<wire x1="-415.29" y1="-173.355" x2="-507.365" y2="-173.355" width="0.1524" layer="91"/>
<junction x="-415.29" y="-173.355"/>
<pinref part="U10" gate="A" pin="+IN_A"/>
<pinref part="S10" gate="G$1" pin="PIN"/>
<label x="-502.92" y="-172.72" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R108" gate="G$1" pin="2"/>
<wire x1="-405.13" y1="-407.035" x2="-399.415" y2="-407.035" width="0.1524" layer="91"/>
<label x="-403.86" y="-407.035" size="1.778" layer="95"/>
</segment>
</net>
<net name="S11" class="0">
<segment>
<wire x1="-447.675" y1="-186.055" x2="-415.29" y2="-186.055" width="0.1524" layer="91"/>
<wire x1="-447.675" y1="-186.055" x2="-447.675" y2="-201.93" width="0.1524" layer="91"/>
<wire x1="-447.675" y1="-201.93" x2="-507.365" y2="-201.93" width="0.1524" layer="91"/>
<pinref part="S11" gate="G$1" pin="PIN"/>
<pinref part="U10" gate="A" pin="+IN_D"/>
<label x="-502.92" y="-201.93" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R100" gate="G$1" pin="2"/>
<wire x1="-405.13" y1="-401.955" x2="-399.415" y2="-401.955" width="0.1524" layer="91"/>
<label x="-403.86" y="-401.955" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$117" class="0">
<segment>
<wire x1="-473.71" y1="-170.815" x2="-496.57" y2="-170.815" width="0.1524" layer="91"/>
<wire x1="-496.57" y1="-170.815" x2="-507.365" y2="-170.815" width="0.1524" layer="91"/>
<wire x1="-496.57" y1="-205.105" x2="-496.57" y2="-170.815" width="0.1524" layer="91"/>
<junction x="-507.365" y="-170.815"/>
<junction x="-496.57" y="-170.815"/>
<pinref part="S10" gate="G$1" pin="SHIELD"/>
<pinref part="R76" gate="G$1" pin="1"/>
<pinref part="R109" gate="G$1" pin="2"/>
<pinref part="S10" gate="G$1" pin="P$1"/>
<pinref part="S10" gate="G$1" pin="P$2"/>
<pinref part="S10" gate="G$1" pin="P$3"/>
</segment>
</net>
<net name="S12" class="0">
<segment>
<wire x1="-415.29" y1="-180.975" x2="-452.755" y2="-180.975" width="0.1524" layer="91"/>
<wire x1="-452.755" y1="-180.975" x2="-452.755" y2="-192.405" width="0.1524" layer="91"/>
<wire x1="-452.755" y1="-192.405" x2="-507.365" y2="-192.405" width="0.1524" layer="91"/>
<pinref part="S12" gate="G$1" pin="PIN"/>
<pinref part="U10" gate="A" pin="+IN_C"/>
<label x="-502.92" y="-191.77" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R101" gate="G$1" pin="2"/>
<wire x1="-405.13" y1="-396.875" x2="-399.415" y2="-396.875" width="0.1524" layer="91"/>
<label x="-403.86" y="-396.875" size="1.778" layer="95"/>
</segment>
</net>
<net name="S13" class="0">
<segment>
<wire x1="-507.365" y1="-248.285" x2="-457.835" y2="-248.285" width="0.1524" layer="91"/>
<wire x1="-457.835" y1="-248.285" x2="-457.835" y2="-243.84" width="0.1524" layer="91"/>
<wire x1="-457.835" y1="-243.84" x2="-415.29" y2="-243.84" width="0.1524" layer="91"/>
<pinref part="U11" gate="A" pin="+IN_B"/>
<pinref part="S13" gate="G$1" pin="PIN"/>
<label x="-502.92" y="-247.65" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R105" gate="G$1" pin="2"/>
<wire x1="-399.415" y1="-409.575" x2="-405.13" y2="-409.575" width="0.1524" layer="91"/>
<label x="-403.86" y="-409.575" size="1.778" layer="95"/>
</segment>
</net>
<net name="S14" class="0">
<segment>
<wire x1="-414.655" y1="-238.76" x2="-415.29" y2="-238.76" width="0.1524" layer="91"/>
<wire x1="-415.29" y1="-238.76" x2="-507.365" y2="-238.76" width="0.1524" layer="91"/>
<junction x="-415.29" y="-238.76"/>
<pinref part="S14" gate="G$1" pin="PIN"/>
<pinref part="U11" gate="A" pin="+IN_A"/>
<label x="-502.92" y="-238.76" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R106" gate="G$1" pin="2"/>
<wire x1="-399.415" y1="-404.495" x2="-405.13" y2="-404.495" width="0.1524" layer="91"/>
<label x="-403.86" y="-404.495" size="1.778" layer="95"/>
</segment>
</net>
<net name="S16" class="0">
<segment>
<wire x1="-452.755" y1="-257.81" x2="-507.365" y2="-257.81" width="0.1524" layer="91"/>
<wire x1="-415.29" y1="-246.38" x2="-452.755" y2="-246.38" width="0.1524" layer="91"/>
<wire x1="-452.755" y1="-246.38" x2="-452.755" y2="-257.81" width="0.1524" layer="91"/>
<pinref part="S16" gate="G$1" pin="PIN"/>
<pinref part="U11" gate="A" pin="+IN_C"/>
<label x="-502.92" y="-257.81" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R103" gate="G$1" pin="2"/>
<wire x1="-399.415" y1="-394.335" x2="-405.13" y2="-394.335" width="0.1524" layer="91"/>
<label x="-403.86" y="-394.335" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$132" class="0">
<segment>
<wire x1="-507.365" y1="-245.745" x2="-489.585" y2="-245.745" width="0.1524" layer="91"/>
<wire x1="-489.585" y1="-245.745" x2="-473.075" y2="-245.745" width="0.1524" layer="91"/>
<wire x1="-489.585" y1="-270.51" x2="-489.585" y2="-245.745" width="0.1524" layer="91"/>
<junction x="-507.365" y="-245.745"/>
<junction x="-489.585" y="-245.745"/>
<pinref part="S13" gate="G$1" pin="SHIELD"/>
<pinref part="R79" gate="G$1" pin="1"/>
<pinref part="R69" gate="G$1" pin="2"/>
<pinref part="S13" gate="G$1" pin="P$1"/>
<pinref part="S13" gate="G$1" pin="P$2"/>
<pinref part="S13" gate="G$1" pin="P$3"/>
</segment>
</net>
<net name="N$133" class="0">
<segment>
<wire x1="-507.365" y1="-255.27" x2="-482.6" y2="-255.27" width="0.1524" layer="91"/>
<wire x1="-482.6" y1="-255.27" x2="-473.075" y2="-255.27" width="0.1524" layer="91"/>
<wire x1="-482.6" y1="-270.51" x2="-482.6" y2="-255.27" width="0.1524" layer="91"/>
<junction x="-507.365" y="-255.27"/>
<junction x="-482.6" y="-255.27"/>
<pinref part="S16" gate="G$1" pin="SHIELD"/>
<pinref part="R94" gate="G$1" pin="1"/>
<pinref part="R95" gate="G$1" pin="2"/>
<pinref part="S16" gate="G$1" pin="P$1"/>
<pinref part="S16" gate="G$1" pin="P$2"/>
<pinref part="S16" gate="G$1" pin="P$3"/>
</segment>
</net>
<net name="N$134" class="0">
<segment>
<wire x1="-507.365" y1="-264.795" x2="-475.615" y2="-264.795" width="0.1524" layer="91"/>
<wire x1="-475.615" y1="-264.795" x2="-473.075" y2="-264.795" width="0.1524" layer="91"/>
<wire x1="-475.615" y1="-270.51" x2="-475.615" y2="-264.795" width="0.1524" layer="91"/>
<junction x="-507.365" y="-264.795"/>
<junction x="-475.615" y="-264.795"/>
<pinref part="S15" gate="G$1" pin="SHIELD"/>
<pinref part="R92" gate="G$1" pin="1"/>
<pinref part="R93" gate="G$1" pin="2"/>
<pinref part="S15" gate="G$1" pin="P$1"/>
<pinref part="S15" gate="G$1" pin="P$2"/>
<pinref part="S15" gate="G$1" pin="P$3"/>
</segment>
</net>
<net name="MUX_V_EN" class="0">
<segment>
<wire x1="-262.89" y1="-412.75" x2="-279.4" y2="-412.75" width="0.1524" layer="91"/>
<label x="-279.4" y="-412.75" size="1.778" layer="95"/>
<pinref part="U3" gate="A" pin="EN"/>
</segment>
<segment>
<wire x1="-126.365" y1="-662.305" x2="-113.665" y2="-662.305" width="0.1524" layer="91"/>
<label x="-121.285" y="-662.305" size="1.778" layer="95"/>
<pinref part="ZED1" gate="-B" pin="G31"/>
</segment>
</net>
<net name="MUX_V_A3" class="0">
<segment>
<wire x1="-262.89" y1="-425.45" x2="-279.4" y2="-425.45" width="0.1524" layer="91"/>
<label x="-279.4" y="-425.45" size="1.778" layer="95"/>
<pinref part="U3" gate="A" pin="A3"/>
</segment>
<segment>
<wire x1="-126.365" y1="-654.685" x2="-113.665" y2="-654.685" width="0.1524" layer="91"/>
<label x="-121.285" y="-654.685" size="1.778" layer="95"/>
<pinref part="ZED1" gate="-B" pin="G28"/>
</segment>
</net>
<net name="MUX_V_A2" class="0">
<segment>
<wire x1="-262.89" y1="-422.91" x2="-279.4" y2="-422.91" width="0.1524" layer="91"/>
<label x="-279.4" y="-422.91" size="1.778" layer="95"/>
<pinref part="U3" gate="A" pin="A2"/>
</segment>
<segment>
<wire x1="-156.845" y1="-654.685" x2="-172.085" y2="-654.685" width="0.1524" layer="91"/>
<wire x1="-172.085" y1="-654.685" x2="-172.085" y2="-655.32" width="0.1524" layer="91"/>
<label x="-172.085" y="-654.685" size="1.778" layer="95"/>
<pinref part="ZED1" gate="-B" pin="H28"/>
</segment>
</net>
<net name="MUX_V_A1" class="0">
<segment>
<wire x1="-262.89" y1="-420.37" x2="-279.4" y2="-420.37" width="0.1524" layer="91"/>
<label x="-279.4" y="-420.37" size="1.778" layer="95"/>
<pinref part="U3" gate="A" pin="A1"/>
</segment>
<segment>
<wire x1="-156.845" y1="-657.225" x2="-172.085" y2="-657.225" width="0.1524" layer="91"/>
<wire x1="-172.085" y1="-657.225" x2="-172.085" y2="-657.86" width="0.1524" layer="91"/>
<label x="-172.085" y="-657.225" size="1.778" layer="95"/>
<pinref part="ZED1" gate="-B" pin="H29"/>
</segment>
</net>
<net name="MUX_V_A0" class="0">
<segment>
<wire x1="-279.4" y1="-417.83" x2="-262.89" y2="-417.83" width="0.1524" layer="91"/>
<label x="-279.4" y="-417.83" size="1.778" layer="95"/>
<pinref part="U3" gate="A" pin="A0"/>
</segment>
<segment>
<wire x1="-126.365" y1="-659.765" x2="-113.665" y2="-659.765" width="0.1524" layer="91"/>
<label x="-121.285" y="-659.765" size="1.778" layer="95"/>
<pinref part="ZED1" gate="-B" pin="G30"/>
</segment>
</net>
<net name="MUX_S1_A3" class="0">
<segment>
<wire x1="194.31" y1="-383.54" x2="199.39" y2="-383.54" width="0.1524" layer="91"/>
<label x="199.39" y="-383.54" size="1.778" layer="95"/>
<pinref part="U5" gate="A" pin="A3"/>
</segment>
<segment>
<wire x1="-156.845" y1="-662.305" x2="-172.085" y2="-662.305" width="0.1524" layer="91"/>
<label x="-172.085" y="-662.305" size="1.778" layer="95"/>
<pinref part="ZED1" gate="-B" pin="H31"/>
</segment>
</net>
<net name="MUX_S1_EN" class="0">
<segment>
<wire x1="194.31" y1="-370.84" x2="199.39" y2="-370.84" width="0.1524" layer="91"/>
<label x="199.39" y="-370.84" size="1.778" layer="95"/>
<pinref part="U5" gate="A" pin="EN"/>
</segment>
<segment>
<wire x1="-156.845" y1="-680.085" x2="-171.45" y2="-680.085" width="0.1524" layer="91"/>
<label x="-171.45" y="-680.085" size="1.778" layer="95"/>
<pinref part="ZED1" gate="-B" pin="H38"/>
</segment>
</net>
<net name="MUX_S1_A0" class="0">
<segment>
<wire x1="194.31" y1="-375.92" x2="199.39" y2="-375.92" width="0.1524" layer="91"/>
<label x="199.39" y="-375.92" size="1.778" layer="95"/>
<pinref part="U5" gate="A" pin="A0"/>
</segment>
<segment>
<wire x1="-156.845" y1="-677.545" x2="-171.45" y2="-677.545" width="0.1524" layer="91"/>
<label x="-171.45" y="-677.545" size="1.778" layer="95"/>
<pinref part="ZED1" gate="-B" pin="H37"/>
</segment>
</net>
<net name="MUX_S1_A1" class="0">
<segment>
<wire x1="194.31" y1="-378.46" x2="199.39" y2="-378.46" width="0.1524" layer="91"/>
<label x="199.39" y="-378.46" size="1.778" layer="95"/>
<pinref part="U5" gate="A" pin="A1"/>
</segment>
<segment>
<wire x1="-113.665" y1="-677.545" x2="-126.365" y2="-677.545" width="0.1524" layer="91"/>
<label x="-121.285" y="-677.545" size="1.778" layer="95"/>
<pinref part="ZED1" gate="-B" pin="G37"/>
</segment>
</net>
<net name="MUX_S1_A2" class="0">
<segment>
<wire x1="194.31" y1="-381" x2="199.39" y2="-381" width="0.1524" layer="91"/>
<label x="199.39" y="-381" size="1.778" layer="95"/>
<pinref part="U5" gate="A" pin="A2"/>
</segment>
<segment>
<wire x1="-126.365" y1="-675.005" x2="-113.665" y2="-675.005" width="0.1524" layer="91"/>
<label x="-121.285" y="-675.005" size="1.778" layer="95"/>
<pinref part="ZED1" gate="-B" pin="G36"/>
</segment>
</net>
<net name="MUX_S2_A3" class="0">
<segment>
<wire x1="199.39" y1="-458.47" x2="194.31" y2="-458.47" width="0.1524" layer="91"/>
<label x="199.39" y="-458.47" size="1.778" layer="95"/>
<pinref part="U7" gate="A" pin="A3"/>
</segment>
<segment>
<wire x1="-156.845" y1="-664.845" x2="-172.085" y2="-664.845" width="0.1524" layer="91"/>
<label x="-172.085" y="-664.845" size="1.778" layer="95"/>
<pinref part="ZED1" gate="-B" pin="H32"/>
</segment>
</net>
<net name="MUX_S2_EN" class="0">
<segment>
<wire x1="194.31" y1="-445.77" x2="199.39" y2="-445.77" width="0.1524" layer="91"/>
<label x="199.39" y="-445.77" size="1.778" layer="95"/>
<pinref part="U7" gate="A" pin="EN"/>
</segment>
<segment>
<wire x1="-156.845" y1="-672.465" x2="-171.45" y2="-672.465" width="0.1524" layer="91"/>
<label x="-171.45" y="-672.465" size="1.778" layer="95"/>
<pinref part="ZED1" gate="-B" pin="H35"/>
</segment>
</net>
<net name="MUX_S2_A0" class="0">
<segment>
<wire x1="194.31" y1="-450.85" x2="199.39" y2="-450.85" width="0.1524" layer="91"/>
<label x="199.39" y="-450.85" size="1.778" layer="95"/>
<pinref part="U7" gate="A" pin="A0"/>
</segment>
<segment>
<wire x1="-156.845" y1="-669.925" x2="-172.085" y2="-669.925" width="0.1524" layer="91"/>
<label x="-171.45" y="-669.925" size="1.778" layer="95"/>
<pinref part="ZED1" gate="-B" pin="H34"/>
</segment>
</net>
<net name="MUX_S2_A1" class="0">
<segment>
<wire x1="199.39" y1="-453.39" x2="194.31" y2="-453.39" width="0.1524" layer="91"/>
<label x="199.39" y="-453.39" size="1.778" layer="95"/>
<pinref part="U7" gate="A" pin="A1"/>
</segment>
<segment>
<wire x1="-126.365" y1="-669.925" x2="-113.665" y2="-669.925" width="0.1524" layer="91"/>
<label x="-121.285" y="-669.925" size="1.778" layer="95"/>
<pinref part="ZED1" gate="-B" pin="G34"/>
</segment>
</net>
<net name="MUX_S2_A2" class="0">
<segment>
<wire x1="194.31" y1="-455.93" x2="199.39" y2="-455.93" width="0.1524" layer="91"/>
<label x="199.39" y="-455.93" size="1.778" layer="95"/>
<pinref part="U7" gate="A" pin="A2"/>
</segment>
<segment>
<wire x1="-126.365" y1="-667.385" x2="-113.665" y2="-667.385" width="0.1524" layer="91"/>
<label x="-121.285" y="-667.385" size="1.778" layer="95"/>
<pinref part="ZED1" gate="-B" pin="G33"/>
</segment>
</net>
<net name="MUX_E1_A0" class="0">
<segment>
<wire x1="-518.795" y1="-379.095" x2="-535.305" y2="-379.095" width="0.1524" layer="91"/>
<label x="-535.305" y="-379.095" size="1.778" layer="95"/>
<pinref part="U6" gate="A" pin="A0"/>
</segment>
<segment>
<wire x1="-156.845" y1="-641.985" x2="-172.085" y2="-641.985" width="0.1524" layer="91"/>
<label x="-172.085" y="-641.985" size="1.778" layer="95"/>
<pinref part="ZED1" gate="-B" pin="H23"/>
</segment>
</net>
<net name="MUX_E2_A3" class="0">
<segment>
<wire x1="-518.795" y1="-452.755" x2="-535.305" y2="-452.755" width="0.1524" layer="91"/>
<label x="-535.305" y="-452.755" size="1.778" layer="95"/>
<pinref part="U4" gate="A" pin="A3"/>
</segment>
<segment>
<wire x1="-126.365" y1="-631.825" x2="-113.665" y2="-631.825" width="0.1524" layer="91"/>
<label x="-121.285" y="-631.825" size="1.778" layer="95"/>
<pinref part="ZED1" gate="-B" pin="G19"/>
</segment>
</net>
<net name="MUX_E2_A2" class="0">
<segment>
<wire x1="-518.795" y1="-450.215" x2="-535.305" y2="-450.215" width="0.1524" layer="91"/>
<label x="-535.305" y="-450.215" size="1.778" layer="95"/>
<pinref part="U4" gate="A" pin="A2"/>
</segment>
<segment>
<wire x1="-156.845" y1="-647.065" x2="-172.085" y2="-647.065" width="0.1524" layer="91"/>
<label x="-172.085" y="-647.065" size="1.778" layer="95"/>
<pinref part="ZED1" gate="-B" pin="H25"/>
</segment>
</net>
<net name="MUX_E2_A1" class="0">
<segment>
<wire x1="-518.795" y1="-447.675" x2="-535.305" y2="-447.675" width="0.1524" layer="91"/>
<label x="-535.305" y="-447.675" size="1.778" layer="95"/>
<pinref part="U4" gate="A" pin="A1"/>
</segment>
<segment>
<wire x1="-126.365" y1="-647.065" x2="-113.665" y2="-647.065" width="0.1524" layer="91"/>
<label x="-121.285" y="-647.065" size="1.778" layer="95"/>
<pinref part="ZED1" gate="-B" pin="G25"/>
</segment>
</net>
<net name="MUX_E2_A0" class="0">
<segment>
<wire x1="-518.795" y1="-445.135" x2="-535.305" y2="-445.135" width="0.1524" layer="91"/>
<label x="-535.305" y="-445.135" size="1.778" layer="95"/>
<pinref part="U4" gate="A" pin="A0"/>
</segment>
<segment>
<wire x1="-156.845" y1="-649.605" x2="-172.085" y2="-649.605" width="0.1524" layer="91"/>
<label x="-172.085" y="-649.605" size="1.778" layer="95"/>
<pinref part="ZED1" gate="-B" pin="H26"/>
</segment>
</net>
<net name="MUX_E2_EN" class="0">
<segment>
<wire x1="-518.795" y1="-440.055" x2="-535.305" y2="-440.055" width="0.1524" layer="91"/>
<label x="-535.305" y="-440.055" size="1.778" layer="95"/>
<pinref part="U4" gate="A" pin="EN"/>
</segment>
<segment>
<wire x1="-126.365" y1="-652.145" x2="-113.665" y2="-652.145" width="0.1524" layer="91"/>
<label x="-121.285" y="-652.145" size="1.778" layer="95"/>
<pinref part="ZED1" gate="-B" pin="G27"/>
</segment>
</net>
<net name="MUX_E1_EN" class="0">
<segment>
<wire x1="-518.795" y1="-374.015" x2="-535.305" y2="-374.015" width="0.1524" layer="91"/>
<label x="-535.305" y="-374.015" size="1.778" layer="95"/>
<pinref part="U6" gate="A" pin="EN"/>
</segment>
<segment>
<wire x1="-126.365" y1="-644.525" x2="-113.665" y2="-644.525" width="0.1524" layer="91"/>
<label x="-121.285" y="-644.525" size="1.778" layer="95"/>
<pinref part="ZED1" gate="-B" pin="G24"/>
</segment>
</net>
<net name="MUX_E1_A1" class="0">
<segment>
<wire x1="-518.795" y1="-381.635" x2="-535.305" y2="-381.635" width="0.1524" layer="91"/>
<label x="-535.305" y="-381.635" size="1.778" layer="95"/>
<pinref part="U6" gate="A" pin="A1"/>
</segment>
<segment>
<wire x1="-126.365" y1="-639.445" x2="-113.665" y2="-639.445" width="0.1524" layer="91"/>
<label x="-121.285" y="-639.445" size="1.778" layer="95"/>
<pinref part="ZED1" gate="-B" pin="G22"/>
</segment>
</net>
<net name="MUX_E1_A2" class="0">
<segment>
<wire x1="-518.795" y1="-384.175" x2="-535.305" y2="-384.175" width="0.1524" layer="91"/>
<label x="-535.305" y="-384.175" size="1.778" layer="95"/>
<pinref part="U6" gate="A" pin="A2"/>
</segment>
<segment>
<wire x1="-156.845" y1="-639.445" x2="-172.085" y2="-639.445" width="0.1524" layer="91"/>
<label x="-172.085" y="-639.445" size="1.778" layer="95"/>
<pinref part="ZED1" gate="-B" pin="H22"/>
</segment>
</net>
<net name="MUX_E1_A3" class="0">
<segment>
<wire x1="-518.795" y1="-386.715" x2="-535.305" y2="-386.715" width="0.1524" layer="91"/>
<label x="-535.305" y="-386.715" size="1.778" layer="95"/>
<pinref part="U6" gate="A" pin="A3"/>
</segment>
<segment>
<wire x1="-126.365" y1="-636.905" x2="-113.665" y2="-636.905" width="0.1524" layer="91"/>
<label x="-121.285" y="-636.905" size="1.778" layer="95"/>
<pinref part="ZED1" gate="-B" pin="G21"/>
</segment>
</net>
<net name="E8" class="0">
<segment>
<wire x1="-483.235" y1="-442.595" x2="-442.595" y2="-442.595" width="0.1524" layer="91"/>
<wire x1="-442.595" y1="-376.555" x2="-442.595" y2="-442.595" width="0.1524" layer="91"/>
<wire x1="-427.99" y1="-376.555" x2="-442.595" y2="-376.555" width="0.1524" layer="91"/>
<wire x1="-442.595" y1="-376.555" x2="-483.235" y2="-376.555" width="0.1524" layer="91"/>
<junction x="-442.595" y="-376.555"/>
<pinref part="U4" gate="A" pin="S2"/>
<pinref part="R122" gate="G$1" pin="1"/>
<pinref part="U6" gate="A" pin="S2"/>
<label x="-433.07" y="-376.555" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="E8" gate="G$1" pin="PIN"/>
<wire x1="-429.26" y1="-458.47" x2="-423.545" y2="-458.47" width="0.1524" layer="91"/>
<label x="-426.72" y="-457.835" size="1.778" layer="95"/>
</segment>
</net>
<net name="E7" class="0">
<segment>
<wire x1="-483.235" y1="-447.675" x2="-447.675" y2="-447.675" width="0.1524" layer="91"/>
<wire x1="-483.235" y1="-381.635" x2="-447.675" y2="-381.635" width="0.1524" layer="91"/>
<wire x1="-447.675" y1="-381.635" x2="-447.675" y2="-447.675" width="0.1524" layer="91"/>
<wire x1="-427.99" y1="-381.635" x2="-447.675" y2="-381.635" width="0.1524" layer="91"/>
<junction x="-447.675" y="-381.635"/>
<pinref part="U4" gate="A" pin="S4"/>
<pinref part="U6" gate="A" pin="S4"/>
<pinref part="R121" gate="G$1" pin="1"/>
<label x="-433.07" y="-381.635" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="-423.545" y1="-477.52" x2="-429.26" y2="-477.52" width="0.1524" layer="91"/>
<pinref part="E7" gate="G$1" pin="PIN"/>
<label x="-426.72" y="-476.885" size="1.778" layer="95"/>
</segment>
</net>
<net name="E3" class="0">
<segment>
<wire x1="-483.235" y1="-445.135" x2="-445.135" y2="-445.135" width="0.1524" layer="91"/>
<wire x1="-483.235" y1="-379.095" x2="-445.135" y2="-379.095" width="0.1524" layer="91"/>
<wire x1="-445.135" y1="-379.095" x2="-445.135" y2="-445.135" width="0.1524" layer="91"/>
<wire x1="-445.135" y1="-379.095" x2="-427.99" y2="-379.095" width="0.1524" layer="91"/>
<junction x="-445.135" y="-379.095"/>
<pinref part="U4" gate="A" pin="S3"/>
<pinref part="U6" gate="A" pin="S3"/>
<pinref part="R123" gate="G$1" pin="1"/>
<label x="-433.07" y="-379.095" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="E3" gate="G$1" pin="PIN"/>
<wire x1="-423.545" y1="-467.995" x2="-429.26" y2="-467.995" width="0.1524" layer="91"/>
<label x="-426.72" y="-467.995" size="1.778" layer="95"/>
</segment>
</net>
<net name="E6" class="0">
<segment>
<wire x1="-483.235" y1="-452.755" x2="-452.755" y2="-452.755" width="0.1524" layer="91"/>
<wire x1="-452.755" y1="-452.755" x2="-452.755" y2="-386.715" width="0.1524" layer="91"/>
<wire x1="-452.755" y1="-386.715" x2="-483.235" y2="-386.715" width="0.1524" layer="91"/>
<wire x1="-427.99" y1="-386.715" x2="-452.755" y2="-386.715" width="0.1524" layer="91"/>
<junction x="-452.755" y="-386.715"/>
<pinref part="U4" gate="A" pin="S6"/>
<pinref part="U6" gate="A" pin="S6"/>
<pinref part="R120" gate="G$1" pin="1"/>
<label x="-433.07" y="-386.715" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="E6" gate="G$1" pin="PIN"/>
<wire x1="-408.94" y1="-458.47" x2="-403.225" y2="-458.47" width="0.1524" layer="91"/>
<label x="-406.4" y="-458.47" size="1.778" layer="95"/>
</segment>
</net>
<net name="E2" class="0">
<segment>
<wire x1="-450.215" y1="-450.215" x2="-483.235" y2="-450.215" width="0.1524" layer="91"/>
<wire x1="-483.235" y1="-384.175" x2="-450.215" y2="-384.175" width="0.1524" layer="91"/>
<wire x1="-450.215" y1="-384.175" x2="-450.215" y2="-450.215" width="0.1524" layer="91"/>
<wire x1="-450.215" y1="-384.175" x2="-427.99" y2="-384.175" width="0.1524" layer="91"/>
<junction x="-450.215" y="-384.175"/>
<pinref part="U4" gate="A" pin="S5"/>
<pinref part="U6" gate="A" pin="S5"/>
<pinref part="R124" gate="G$1" pin="1"/>
<label x="-433.07" y="-384.175" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="-403.225" y1="-448.945" x2="-408.94" y2="-448.945" width="0.1524" layer="91"/>
<pinref part="E2" gate="G$1" pin="PIN"/>
<label x="-406.4" y="-448.31" size="1.778" layer="95"/>
</segment>
</net>
<net name="E5" class="0">
<segment>
<wire x1="-457.835" y1="-457.835" x2="-483.235" y2="-457.835" width="0.1524" layer="91"/>
<wire x1="-457.835" y1="-457.835" x2="-457.835" y2="-391.795" width="0.1524" layer="91"/>
<wire x1="-457.835" y1="-391.795" x2="-483.235" y2="-391.795" width="0.1524" layer="91"/>
<wire x1="-427.99" y1="-391.795" x2="-457.835" y2="-391.795" width="0.1524" layer="91"/>
<junction x="-457.835" y="-391.795"/>
<pinref part="U4" gate="A" pin="S8"/>
<pinref part="U6" gate="A" pin="S8"/>
<pinref part="R119" gate="G$1" pin="1"/>
<label x="-433.07" y="-391.795" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="E5" gate="G$1" pin="PIN"/>
<wire x1="-401.955" y1="-477.52" x2="-408.94" y2="-477.52" width="0.1524" layer="91"/>
<label x="-406.4" y="-477.52" size="1.778" layer="95"/>
</segment>
</net>
<net name="E1" class="0">
<segment>
<wire x1="-483.235" y1="-455.295" x2="-455.295" y2="-455.295" width="0.1524" layer="91"/>
<wire x1="-483.235" y1="-389.255" x2="-455.295" y2="-389.255" width="0.1524" layer="91"/>
<wire x1="-455.295" y1="-389.255" x2="-455.295" y2="-455.295" width="0.1524" layer="91"/>
<wire x1="-455.295" y1="-389.255" x2="-427.99" y2="-389.255" width="0.1524" layer="91"/>
<junction x="-455.295" y="-389.255"/>
<pinref part="U4" gate="A" pin="S7"/>
<pinref part="U6" gate="A" pin="S7"/>
<pinref part="R62" gate="G$1" pin="1"/>
<label x="-433.07" y="-389.255" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="E1" gate="G$1" pin="PIN"/>
<wire x1="-403.225" y1="-467.995" x2="-408.94" y2="-467.995" width="0.1524" layer="91"/>
<label x="-406.4" y="-467.36" size="1.778" layer="95"/>
</segment>
</net>
<net name="E12" class="0">
<segment>
<wire x1="-462.915" y1="-462.915" x2="-483.235" y2="-462.915" width="0.1524" layer="91"/>
<wire x1="-462.915" y1="-462.915" x2="-462.915" y2="-396.875" width="0.1524" layer="91"/>
<wire x1="-462.915" y1="-396.875" x2="-483.235" y2="-396.875" width="0.1524" layer="91"/>
<wire x1="-427.99" y1="-396.875" x2="-462.915" y2="-396.875" width="0.1524" layer="91"/>
<junction x="-462.915" y="-396.875"/>
<pinref part="U4" gate="A" pin="S10"/>
<pinref part="U6" gate="A" pin="S10"/>
<pinref part="R116" gate="G$1" pin="1"/>
<label x="-433.07" y="-396.875" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="E12" gate="G$1" pin="PIN"/>
<wire x1="-387.35" y1="-458.47" x2="-379.095" y2="-458.47" width="0.1524" layer="91"/>
<label x="-383.54" y="-458.47" size="1.778" layer="95"/>
</segment>
</net>
<net name="E16" class="0">
<segment>
<wire x1="-483.235" y1="-460.375" x2="-460.375" y2="-460.375" width="0.1524" layer="91"/>
<wire x1="-483.235" y1="-394.335" x2="-460.375" y2="-394.335" width="0.1524" layer="91"/>
<wire x1="-460.375" y1="-394.335" x2="-460.375" y2="-460.375" width="0.1524" layer="91"/>
<wire x1="-460.375" y1="-394.335" x2="-427.99" y2="-394.335" width="0.1524" layer="91"/>
<junction x="-460.375" y="-394.335"/>
<pinref part="U4" gate="A" pin="S9"/>
<pinref part="U6" gate="A" pin="S9"/>
<pinref part="R68" gate="G$1" pin="1"/>
<label x="-433.07" y="-394.335" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="E16" gate="G$1" pin="PIN"/>
<wire x1="-379.095" y1="-448.945" x2="-387.35" y2="-448.945" width="0.1524" layer="91"/>
<label x="-383.54" y="-448.31" size="1.778" layer="95"/>
</segment>
</net>
<net name="E11" class="0">
<segment>
<wire x1="-483.235" y1="-467.995" x2="-467.995" y2="-467.995" width="0.1524" layer="91"/>
<wire x1="-467.995" y1="-467.995" x2="-467.995" y2="-401.955" width="0.1524" layer="91"/>
<wire x1="-467.995" y1="-401.955" x2="-483.235" y2="-401.955" width="0.1524" layer="91"/>
<wire x1="-467.995" y1="-401.955" x2="-427.99" y2="-401.955" width="0.1524" layer="91"/>
<junction x="-467.995" y="-401.955"/>
<pinref part="U4" gate="A" pin="S12"/>
<pinref part="U6" gate="A" pin="S12"/>
<pinref part="R115" gate="G$1" pin="1"/>
<label x="-433.07" y="-401.955" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="E11" gate="G$1" pin="PIN"/>
<wire x1="-379.095" y1="-477.52" x2="-387.35" y2="-477.52" width="0.1524" layer="91"/>
<label x="-383.54" y="-477.52" size="1.778" layer="95"/>
</segment>
</net>
<net name="E15" class="0">
<segment>
<wire x1="-483.235" y1="-465.455" x2="-465.455" y2="-465.455" width="0.1524" layer="91"/>
<wire x1="-483.235" y1="-399.415" x2="-465.455" y2="-399.415" width="0.1524" layer="91"/>
<wire x1="-465.455" y1="-399.415" x2="-465.455" y2="-465.455" width="0.1524" layer="91"/>
<wire x1="-427.99" y1="-399.415" x2="-465.455" y2="-399.415" width="0.1524" layer="91"/>
<junction x="-465.455" y="-399.415"/>
<pinref part="U4" gate="A" pin="S11"/>
<pinref part="U6" gate="A" pin="S11"/>
<pinref part="R70" gate="G$1" pin="1"/>
<label x="-433.07" y="-399.415" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="E15" gate="G$1" pin="PIN"/>
<wire x1="-379.095" y1="-467.995" x2="-387.35" y2="-467.995" width="0.1524" layer="91"/>
<label x="-383.54" y="-467.36" size="1.778" layer="95"/>
</segment>
</net>
<net name="E10" class="0">
<segment>
<wire x1="-483.235" y1="-473.075" x2="-473.075" y2="-473.075" width="0.1524" layer="91"/>
<wire x1="-473.075" y1="-473.075" x2="-473.075" y2="-407.035" width="0.1524" layer="91"/>
<wire x1="-473.075" y1="-407.035" x2="-483.235" y2="-407.035" width="0.1524" layer="91"/>
<wire x1="-473.075" y1="-407.035" x2="-427.99" y2="-407.035" width="0.1524" layer="91"/>
<junction x="-473.075" y="-407.035"/>
<pinref part="U4" gate="A" pin="S14"/>
<pinref part="U6" gate="A" pin="S14"/>
<pinref part="R117" gate="G$1" pin="1"/>
<label x="-433.07" y="-407.035" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="E10" gate="G$1" pin="PIN"/>
<wire x1="-365.76" y1="-458.47" x2="-357.505" y2="-458.47" width="0.1524" layer="91"/>
<label x="-361.95" y="-457.835" size="1.778" layer="95"/>
</segment>
</net>
<net name="E14" class="0">
<segment>
<wire x1="-470.535" y1="-470.535" x2="-483.235" y2="-470.535" width="0.1524" layer="91"/>
<wire x1="-483.235" y1="-404.495" x2="-470.535" y2="-404.495" width="0.1524" layer="91"/>
<wire x1="-470.535" y1="-404.495" x2="-470.535" y2="-470.535" width="0.1524" layer="91"/>
<wire x1="-427.99" y1="-404.495" x2="-470.535" y2="-404.495" width="0.1524" layer="91"/>
<junction x="-470.535" y="-404.495"/>
<pinref part="U4" gate="A" pin="S13"/>
<pinref part="U6" gate="A" pin="S13"/>
<pinref part="R114" gate="G$1" pin="1"/>
<label x="-433.07" y="-404.495" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="E14" gate="G$1" pin="PIN"/>
<wire x1="-357.505" y1="-448.945" x2="-365.76" y2="-448.945" width="0.1524" layer="91"/>
<label x="-361.95" y="-448.945" size="1.778" layer="95"/>
</segment>
</net>
<net name="E9" class="0">
<segment>
<wire x1="-483.235" y1="-478.155" x2="-478.155" y2="-478.155" width="0.1524" layer="91"/>
<wire x1="-478.155" y1="-478.155" x2="-478.155" y2="-412.115" width="0.1524" layer="91"/>
<wire x1="-478.155" y1="-412.115" x2="-483.235" y2="-412.115" width="0.1524" layer="91"/>
<wire x1="-478.155" y1="-412.115" x2="-427.99" y2="-412.115" width="0.1524" layer="91"/>
<junction x="-478.155" y="-412.115"/>
<pinref part="U4" gate="A" pin="S16"/>
<pinref part="U6" gate="A" pin="S16"/>
<pinref part="R118" gate="G$1" pin="1"/>
<label x="-433.07" y="-412.115" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="E9" gate="G$1" pin="PIN"/>
<wire x1="-357.505" y1="-477.52" x2="-365.76" y2="-477.52" width="0.1524" layer="91"/>
<label x="-361.95" y="-476.885" size="1.778" layer="95"/>
</segment>
</net>
<net name="E13" class="0">
<segment>
<wire x1="-475.615" y1="-475.615" x2="-483.235" y2="-475.615" width="0.1524" layer="91"/>
<wire x1="-475.615" y1="-475.615" x2="-475.615" y2="-409.575" width="0.1524" layer="91"/>
<wire x1="-475.615" y1="-409.575" x2="-483.235" y2="-409.575" width="0.1524" layer="91"/>
<wire x1="-427.99" y1="-409.575" x2="-475.615" y2="-409.575" width="0.1524" layer="91"/>
<junction x="-475.615" y="-409.575"/>
<pinref part="U4" gate="A" pin="S15"/>
<pinref part="U6" gate="A" pin="S15"/>
<pinref part="R113" gate="G$1" pin="1"/>
<label x="-433.07" y="-409.575" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="E13" gate="G$1" pin="PIN"/>
<wire x1="-357.505" y1="-467.995" x2="-365.76" y2="-467.995" width="0.1524" layer="91"/>
<label x="-361.95" y="-467.995" size="1.778" layer="95"/>
</segment>
</net>
<net name="SIG_1" class="0">
<segment>
<wire x1="-483.235" y1="-434.975" x2="-480.695" y2="-434.975" width="0.1524" layer="91"/>
<wire x1="-480.695" y1="-434.975" x2="-480.695" y2="-423.545" width="0.1524" layer="91"/>
<wire x1="-480.695" y1="-423.545" x2="-546.1" y2="-423.545" width="0.1524" layer="91"/>
<pinref part="U4" gate="A" pin="D"/>
<pinref part="SIG1" gate="G$1" pin="PIN"/>
<label x="-541.655" y="-423.545" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="74.93" y1="-254" x2="64.77" y2="-254" width="0.1524" layer="91"/>
<pinref part="R38" gate="G$1" pin="2"/>
<label x="67.31" y="-254" size="1.778" layer="95"/>
</segment>
</net>
<net name="S1" class="0">
<segment>
<wire x1="-226.695" y1="-178.435" x2="-177.165" y2="-178.435" width="0.1524" layer="91"/>
<wire x1="-177.165" y1="-178.435" x2="-177.165" y2="-173.99" width="0.1524" layer="91"/>
<wire x1="-177.165" y1="-173.99" x2="-134.62" y2="-173.99" width="0.1524" layer="91"/>
<pinref part="S1" gate="G$1" pin="PIN"/>
<pinref part="U8" gate="A" pin="+IN_B"/>
<label x="-223.52" y="-177.8" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="-399.415" y1="-389.255" x2="-405.13" y2="-389.255" width="0.1524" layer="91"/>
<pinref part="R61" gate="G$1" pin="2"/>
<label x="-403.86" y="-389.255" size="1.778" layer="95"/>
</segment>
</net>
<net name="S2" class="0">
<segment>
<wire x1="-134.62" y1="-168.91" x2="-226.695" y2="-168.91" width="0.1524" layer="91"/>
<pinref part="S2" gate="G$1" pin="PIN"/>
<pinref part="U8" gate="A" pin="+IN_A"/>
<label x="-223.52" y="-168.91" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="-399.415" y1="-384.175" x2="-405.13" y2="-384.175" width="0.1524" layer="91"/>
<pinref part="R112" gate="G$1" pin="2"/>
<label x="-403.86" y="-384.175" size="1.778" layer="95"/>
</segment>
</net>
<net name="S4" class="0">
<segment>
<wire x1="-172.085" y1="-187.96" x2="-226.695" y2="-187.96" width="0.1524" layer="91"/>
<wire x1="-172.085" y1="-176.53" x2="-172.085" y2="-187.96" width="0.1524" layer="91"/>
<wire x1="-134.62" y1="-176.53" x2="-172.085" y2="-176.53" width="0.1524" layer="91"/>
<pinref part="S4" gate="G$1" pin="PIN"/>
<pinref part="U8" gate="A" pin="+IN_C"/>
<label x="-223.52" y="-187.96" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="-399.415" y1="-374.015" x2="-405.13" y2="-374.015" width="0.1524" layer="91"/>
<pinref part="R97" gate="G$1" pin="2"/>
<label x="-403.86" y="-374.015" size="1.778" layer="95"/>
</segment>
</net>
<net name="S5" class="0">
<segment>
<wire x1="-227.965" y1="-250.19" x2="-178.435" y2="-250.19" width="0.1524" layer="91"/>
<wire x1="-178.435" y1="-250.19" x2="-178.435" y2="-245.745" width="0.1524" layer="91"/>
<wire x1="-178.435" y1="-245.745" x2="-135.89" y2="-245.745" width="0.1524" layer="91"/>
<pinref part="S5" gate="G$1" pin="PIN"/>
<pinref part="U9" gate="A" pin="+IN_B"/>
<label x="-223.52" y="-250.19" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R110" gate="G$1" pin="2"/>
<wire x1="-405.13" y1="-391.795" x2="-399.415" y2="-391.795" width="0.1524" layer="91"/>
<label x="-403.86" y="-391.795" size="1.778" layer="95"/>
</segment>
</net>
<net name="S3" class="0">
<segment>
<wire x1="-167.005" y1="-197.485" x2="-226.695" y2="-197.485" width="0.1524" layer="91"/>
<wire x1="-167.005" y1="-181.61" x2="-167.005" y2="-197.485" width="0.1524" layer="91"/>
<wire x1="-167.005" y1="-181.61" x2="-134.62" y2="-181.61" width="0.1524" layer="91"/>
<pinref part="S3" gate="G$1" pin="PIN"/>
<pinref part="U8" gate="A" pin="+IN_D"/>
<label x="-223.52" y="-196.85" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="-399.415" y1="-379.095" x2="-405.13" y2="-379.095" width="0.1524" layer="91"/>
<pinref part="R96" gate="G$1" pin="2"/>
<label x="-403.86" y="-379.095" size="1.778" layer="95"/>
</segment>
</net>
<net name="S6" class="0">
<segment>
<wire x1="-135.255" y1="-240.665" x2="-135.89" y2="-240.665" width="0.1524" layer="91"/>
<wire x1="-135.89" y1="-240.665" x2="-227.965" y2="-240.665" width="0.1524" layer="91"/>
<junction x="-135.89" y="-240.665"/>
<pinref part="S6" gate="G$1" pin="PIN"/>
<pinref part="U9" gate="A" pin="+IN_A"/>
<label x="-223.52" y="-240.03" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="-405.13" y1="-386.715" x2="-399.415" y2="-386.715" width="0.1524" layer="91"/>
<pinref part="R111" gate="G$1" pin="2"/>
<label x="-403.86" y="-386.715" size="1.778" layer="95"/>
</segment>
</net>
<net name="E4" class="0">
<segment>
<wire x1="-483.235" y1="-440.055" x2="-440.055" y2="-440.055" width="0.1524" layer="91"/>
<wire x1="-483.235" y1="-374.015" x2="-440.055" y2="-374.015" width="0.1524" layer="91"/>
<wire x1="-440.055" y1="-374.015" x2="-440.055" y2="-440.055" width="0.1524" layer="91"/>
<wire x1="-440.055" y1="-374.015" x2="-427.99" y2="-374.015" width="0.1524" layer="91"/>
<junction x="-440.055" y="-374.015"/>
<pinref part="U4" gate="A" pin="S1"/>
<pinref part="U6" gate="A" pin="S1"/>
<pinref part="R63" gate="G$1" pin="1"/>
<label x="-433.07" y="-374.015" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="E4" gate="G$1" pin="PIN"/>
<wire x1="-423.545" y1="-448.945" x2="-429.26" y2="-448.945" width="0.1524" layer="91"/>
<label x="-426.72" y="-448.945" size="1.778" layer="95"/>
</segment>
</net>
<net name="S15" class="0">
<segment>
<wire x1="-447.675" y1="-267.335" x2="-507.365" y2="-267.335" width="0.1524" layer="91"/>
<wire x1="-447.675" y1="-251.46" x2="-447.675" y2="-267.335" width="0.1524" layer="91"/>
<wire x1="-447.675" y1="-251.46" x2="-415.29" y2="-251.46" width="0.1524" layer="91"/>
<pinref part="U11" gate="A" pin="+IN_D"/>
<pinref part="S15" gate="G$1" pin="PIN"/>
<label x="-502.92" y="-266.7" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R102" gate="G$1" pin="2"/>
<wire x1="-399.415" y1="-399.415" x2="-405.13" y2="-399.415" width="0.1524" layer="91"/>
<label x="-403.86" y="-399.415" size="1.778" layer="95"/>
</segment>
</net>
<net name="S4_IMP" class="0">
<segment>
<wire x1="60.96" y1="-411.48" x2="60.96" y2="-386.08" width="0.1524" layer="91"/>
<wire x1="60.96" y1="-386.08" x2="135.89" y2="-386.08" width="0.1524" layer="91"/>
<wire x1="158.75" y1="-461.01" x2="135.89" y2="-461.01" width="0.1524" layer="91"/>
<wire x1="135.89" y1="-461.01" x2="135.89" y2="-386.08" width="0.1524" layer="91"/>
<wire x1="135.89" y1="-386.08" x2="158.75" y2="-386.08" width="0.1524" layer="91"/>
<junction x="135.89" y="-386.08"/>
<junction x="60.96" y="-386.08"/>
<pinref part="R43" gate="G$1" pin="2"/>
<pinref part="U7" gate="A" pin="S7"/>
<pinref part="U5" gate="A" pin="S7"/>
<wire x1="60.96" y1="-386.08" x2="12.7" y2="-386.08" width="0.1524" layer="91"/>
<label x="12.7" y="-386.08" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U8" gate="A" pin="OUT_C"/>
<wire x1="-99.06" y1="-166.37" x2="-91.44" y2="-166.37" width="0.1524" layer="91"/>
<wire x1="-91.44" y1="-166.37" x2="-77.47" y2="-166.37" width="0.1524" layer="91"/>
<junction x="-91.44" y="-166.37"/>
<pinref part="R82" gate="G$1" pin="2"/>
<wire x1="-182.245" y1="-185.42" x2="-174.625" y2="-185.42" width="0.1524" layer="91"/>
<wire x1="-174.625" y1="-185.42" x2="-174.625" y2="-179.07" width="0.1524" layer="91"/>
<pinref part="U8" gate="A" pin="-IN_C"/>
<wire x1="-134.62" y1="-179.07" x2="-159.385" y2="-179.07" width="0.1524" layer="91"/>
<wire x1="-159.385" y1="-179.07" x2="-174.625" y2="-179.07" width="0.1524" layer="91"/>
<junction x="-159.385" y="-179.07"/>
<wire x1="-159.385" y1="-179.07" x2="-159.385" y2="-204.47" width="0.1524" layer="91"/>
<wire x1="-159.385" y1="-204.47" x2="-91.44" y2="-204.47" width="0.1524" layer="91"/>
<wire x1="-91.44" y1="-204.47" x2="-91.44" y2="-166.37" width="0.1524" layer="91"/>
<label x="-87.63" y="-166.37" size="1.778" layer="95"/>
</segment>
</net>
<net name="S3_IMP" class="0">
<segment>
<wire x1="66.04" y1="-419.1" x2="66.04" y2="-388.62" width="0.1524" layer="91"/>
<wire x1="133.35" y1="-388.62" x2="66.04" y2="-388.62" width="0.1524" layer="91"/>
<wire x1="158.75" y1="-463.55" x2="133.35" y2="-463.55" width="0.1524" layer="91"/>
<wire x1="133.35" y1="-463.55" x2="133.35" y2="-388.62" width="0.1524" layer="91"/>
<wire x1="158.75" y1="-388.62" x2="133.35" y2="-388.62" width="0.1524" layer="91"/>
<junction x="66.04" y="-388.62"/>
<junction x="133.35" y="-388.62"/>
<pinref part="R44" gate="G$1" pin="2"/>
<pinref part="U7" gate="A" pin="S8"/>
<pinref part="U5" gate="A" pin="S8"/>
<wire x1="66.04" y1="-388.62" x2="12.7" y2="-388.62" width="0.1524" layer="91"/>
<label x="12.7" y="-388.62" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U8" gate="A" pin="OUT_D"/>
<wire x1="-88.9" y1="-168.91" x2="-99.06" y2="-168.91" width="0.1524" layer="91"/>
<wire x1="-88.9" y1="-168.91" x2="-77.47" y2="-168.91" width="0.1524" layer="91"/>
<junction x="-88.9" y="-168.91"/>
<pinref part="R80" gate="G$1" pin="2"/>
<wire x1="-182.245" y1="-194.945" x2="-169.545" y2="-194.945" width="0.1524" layer="91"/>
<wire x1="-169.545" y1="-194.945" x2="-169.545" y2="-184.15" width="0.1524" layer="91"/>
<pinref part="U8" gate="A" pin="-IN_D"/>
<wire x1="-134.62" y1="-184.15" x2="-156.845" y2="-184.15" width="0.1524" layer="91"/>
<wire x1="-156.845" y1="-184.15" x2="-169.545" y2="-184.15" width="0.1524" layer="91"/>
<junction x="-156.845" y="-184.15"/>
<wire x1="-156.845" y1="-184.15" x2="-156.845" y2="-207.01" width="0.1524" layer="91"/>
<wire x1="-156.845" y1="-207.01" x2="-88.9" y2="-207.01" width="0.1524" layer="91"/>
<wire x1="-88.9" y1="-207.01" x2="-88.9" y2="-168.91" width="0.1524" layer="91"/>
<label x="-87.63" y="-168.91" size="1.778" layer="95"/>
</segment>
</net>
<net name="S13_IMP" class="0">
<segment>
<wire x1="71.12" y1="-411.48" x2="71.12" y2="-391.16" width="0.1524" layer="91"/>
<wire x1="158.75" y1="-466.09" x2="130.81" y2="-466.09" width="0.1524" layer="91"/>
<wire x1="130.81" y1="-466.09" x2="130.81" y2="-391.16" width="0.1524" layer="91"/>
<wire x1="130.81" y1="-391.16" x2="71.12" y2="-391.16" width="0.1524" layer="91"/>
<wire x1="158.75" y1="-391.16" x2="130.81" y2="-391.16" width="0.1524" layer="91"/>
<wire x1="71.12" y1="-391.16" x2="12.7" y2="-391.16" width="0.1524" layer="91"/>
<junction x="130.81" y="-391.16"/>
<junction x="71.12" y="-391.16"/>
<pinref part="R55" gate="G$1" pin="2"/>
<pinref part="U7" gate="A" pin="S9"/>
<pinref part="U5" gate="A" pin="S9"/>
<label x="12.7" y="-391.16" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U11" gate="A" pin="OUT_B"/>
<wire x1="-374.65" y1="-233.68" x2="-379.73" y2="-233.68" width="0.1524" layer="91"/>
<wire x1="-358.14" y1="-233.68" x2="-374.65" y2="-233.68" width="0.1524" layer="91"/>
<junction x="-374.65" y="-233.68"/>
<wire x1="-374.65" y1="-271.78" x2="-374.65" y2="-233.68" width="0.1524" layer="91"/>
<wire x1="-442.595" y1="-271.78" x2="-374.65" y2="-271.78" width="0.1524" layer="91"/>
<pinref part="U11" gate="A" pin="-IN_B"/>
<wire x1="-415.29" y1="-241.3" x2="-442.595" y2="-241.3" width="0.1524" layer="91"/>
<wire x1="-442.595" y1="-241.3" x2="-442.595" y2="-271.78" width="0.1524" layer="91"/>
<junction x="-442.595" y="-241.3"/>
<pinref part="R79" gate="G$1" pin="2"/>
<wire x1="-462.915" y1="-245.745" x2="-460.375" y2="-245.745" width="0.1524" layer="91"/>
<wire x1="-460.375" y1="-241.3" x2="-460.375" y2="-245.745" width="0.1524" layer="91"/>
<wire x1="-460.375" y1="-241.3" x2="-442.595" y2="-241.3" width="0.1524" layer="91"/>
<label x="-368.3" y="-233.68" size="1.778" layer="95"/>
</segment>
</net>
<net name="S9_IMP" class="0">
<segment>
<wire x1="81.28" y1="-411.48" x2="81.28" y2="-396.24" width="0.1524" layer="91"/>
<wire x1="81.28" y1="-396.24" x2="125.73" y2="-396.24" width="0.1524" layer="91"/>
<wire x1="158.75" y1="-471.17" x2="125.73" y2="-471.17" width="0.1524" layer="91"/>
<wire x1="125.73" y1="-471.17" x2="125.73" y2="-396.24" width="0.1524" layer="91"/>
<wire x1="125.73" y1="-396.24" x2="158.75" y2="-396.24" width="0.1524" layer="91"/>
<wire x1="81.28" y1="-396.24" x2="12.7" y2="-396.24" width="0.1524" layer="91"/>
<junction x="125.73" y="-396.24"/>
<junction x="81.28" y="-396.24"/>
<pinref part="R53" gate="G$1" pin="2"/>
<pinref part="U7" gate="A" pin="S11"/>
<pinref part="U5" gate="A" pin="S11"/>
<label x="12.7" y="-396.24" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U10" gate="A" pin="OUT_B"/>
<wire x1="-374.65" y1="-168.275" x2="-379.73" y2="-168.275" width="0.1524" layer="91"/>
<pinref part="U10" gate="A" pin="-IN_B"/>
<wire x1="-415.29" y1="-175.895" x2="-442.595" y2="-175.895" width="0.1524" layer="91"/>
<pinref part="R77" gate="G$1" pin="2"/>
<wire x1="-462.915" y1="-180.34" x2="-460.375" y2="-180.34" width="0.1524" layer="91"/>
<wire x1="-460.375" y1="-175.895" x2="-460.375" y2="-180.34" width="0.1524" layer="91"/>
<wire x1="-460.375" y1="-175.895" x2="-442.595" y2="-175.895" width="0.1524" layer="91"/>
<junction x="-442.595" y="-175.895"/>
<wire x1="-442.595" y1="-175.895" x2="-442.595" y2="-206.375" width="0.1524" layer="91"/>
<wire x1="-442.595" y1="-206.375" x2="-374.65" y2="-206.375" width="0.1524" layer="91"/>
<wire x1="-374.65" y1="-206.375" x2="-374.65" y2="-168.275" width="0.1524" layer="91"/>
<junction x="-374.65" y="-168.275"/>
<wire x1="-358.14" y1="-168.275" x2="-374.65" y2="-168.275" width="0.1524" layer="91"/>
<label x="-368.3" y="-167.64" size="1.778" layer="95"/>
</segment>
</net>
<net name="S14_IMP" class="0">
<segment>
<wire x1="76.2" y1="-419.1" x2="76.2" y2="-393.7" width="0.1524" layer="91"/>
<wire x1="128.27" y1="-393.7" x2="76.2" y2="-393.7" width="0.1524" layer="91"/>
<wire x1="158.75" y1="-468.63" x2="128.27" y2="-468.63" width="0.1524" layer="91"/>
<wire x1="128.27" y1="-468.63" x2="128.27" y2="-393.7" width="0.1524" layer="91"/>
<wire x1="158.75" y1="-393.7" x2="128.27" y2="-393.7" width="0.1524" layer="91"/>
<wire x1="76.2" y1="-393.7" x2="12.7" y2="-393.7" width="0.1524" layer="91"/>
<junction x="128.27" y="-393.7"/>
<junction x="76.2" y="-393.7"/>
<pinref part="R125" gate="G$1" pin="2"/>
<pinref part="U7" gate="A" pin="S10"/>
<pinref part="U5" gate="A" pin="S10"/>
<label x="12.7" y="-393.7" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U11" gate="A" pin="OUT_A"/>
<wire x1="-379.73" y1="-231.14" x2="-377.19" y2="-231.14" width="0.1524" layer="91"/>
<wire x1="-377.19" y1="-231.14" x2="-358.14" y2="-231.14" width="0.1524" layer="91"/>
<junction x="-377.19" y="-231.14"/>
<wire x1="-377.19" y1="-269.24" x2="-377.19" y2="-231.14" width="0.1524" layer="91"/>
<wire x1="-445.135" y1="-269.24" x2="-377.19" y2="-269.24" width="0.1524" layer="91"/>
<pinref part="U11" gate="A" pin="-IN_A"/>
<wire x1="-415.29" y1="-236.22" x2="-445.135" y2="-236.22" width="0.1524" layer="91"/>
<wire x1="-445.135" y1="-236.22" x2="-445.135" y2="-269.24" width="0.1524" layer="91"/>
<junction x="-445.135" y="-236.22"/>
<wire x1="-445.135" y1="-236.22" x2="-463.55" y2="-236.22" width="0.1524" layer="91"/>
<pinref part="R78" gate="G$1" pin="2"/>
<label x="-368.3" y="-231.14" size="1.778" layer="95"/>
</segment>
</net>
<net name="S10_IMP" class="0">
<segment>
<wire x1="86.36" y1="-419.1" x2="86.36" y2="-398.78" width="0.1524" layer="91"/>
<wire x1="123.19" y1="-398.78" x2="86.36" y2="-398.78" width="0.1524" layer="91"/>
<wire x1="158.75" y1="-473.71" x2="123.19" y2="-473.71" width="0.1524" layer="91"/>
<wire x1="123.19" y1="-473.71" x2="123.19" y2="-398.78" width="0.1524" layer="91"/>
<wire x1="158.75" y1="-398.78" x2="123.19" y2="-398.78" width="0.1524" layer="91"/>
<wire x1="86.36" y1="-398.78" x2="12.7" y2="-398.78" width="0.1524" layer="91"/>
<junction x="123.19" y="-398.78"/>
<junction x="86.36" y="-398.78"/>
<pinref part="R126" gate="G$1" pin="2"/>
<pinref part="U7" gate="A" pin="S12"/>
<pinref part="U5" gate="A" pin="S12"/>
<label x="12.7" y="-398.78" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U10" gate="A" pin="OUT_A"/>
<wire x1="-379.73" y1="-165.735" x2="-377.19" y2="-165.735" width="0.1524" layer="91"/>
<pinref part="R76" gate="G$1" pin="2"/>
<pinref part="U10" gate="A" pin="-IN_A"/>
<wire x1="-415.29" y1="-170.815" x2="-445.135" y2="-170.815" width="0.1524" layer="91"/>
<wire x1="-445.135" y1="-170.815" x2="-463.55" y2="-170.815" width="0.1524" layer="91"/>
<junction x="-445.135" y="-170.815"/>
<wire x1="-445.135" y1="-170.815" x2="-445.135" y2="-203.835" width="0.1524" layer="91"/>
<wire x1="-445.135" y1="-203.835" x2="-377.19" y2="-203.835" width="0.1524" layer="91"/>
<wire x1="-377.19" y1="-203.835" x2="-377.19" y2="-165.735" width="0.1524" layer="91"/>
<junction x="-377.19" y="-165.735"/>
<wire x1="-377.19" y1="-165.735" x2="-358.14" y2="-165.735" width="0.1524" layer="91"/>
<label x="-368.3" y="-165.1" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="-377.19" y1="-164.465" x2="-358.14" y2="-164.465" width="0.1524" layer="91"/>
</segment>
</net>
<net name="S5_IMP" class="0">
<segment>
<wire x1="91.44" y1="-411.48" x2="91.44" y2="-401.32" width="0.1524" layer="91"/>
<wire x1="158.75" y1="-476.25" x2="120.65" y2="-476.25" width="0.1524" layer="91"/>
<wire x1="120.65" y1="-476.25" x2="120.65" y2="-401.32" width="0.1524" layer="91"/>
<wire x1="120.65" y1="-401.32" x2="91.44" y2="-401.32" width="0.1524" layer="91"/>
<wire x1="158.75" y1="-401.32" x2="120.65" y2="-401.32" width="0.1524" layer="91"/>
<wire x1="91.44" y1="-401.32" x2="12.7" y2="-401.32" width="0.1524" layer="91"/>
<junction x="120.65" y="-401.32"/>
<junction x="91.44" y="-401.32"/>
<pinref part="R50" gate="G$1" pin="2"/>
<pinref part="U7" gate="A" pin="S13"/>
<pinref part="U5" gate="A" pin="S13"/>
<label x="12.7" y="-401.32" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U9" gate="A" pin="OUT_B"/>
<wire x1="-95.25" y1="-235.585" x2="-100.33" y2="-235.585" width="0.1524" layer="91"/>
<pinref part="U9" gate="A" pin="-IN_B"/>
<wire x1="-135.89" y1="-243.205" x2="-163.195" y2="-243.205" width="0.1524" layer="91"/>
<pinref part="R75" gate="G$1" pin="2"/>
<wire x1="-183.515" y1="-247.65" x2="-180.975" y2="-247.65" width="0.1524" layer="91"/>
<wire x1="-180.975" y1="-243.205" x2="-180.975" y2="-247.65" width="0.1524" layer="91"/>
<wire x1="-180.975" y1="-243.205" x2="-163.195" y2="-243.205" width="0.1524" layer="91"/>
<junction x="-163.195" y="-243.205"/>
<wire x1="-163.195" y1="-243.205" x2="-163.195" y2="-273.685" width="0.1524" layer="91"/>
<wire x1="-163.195" y1="-273.685" x2="-95.25" y2="-273.685" width="0.1524" layer="91"/>
<wire x1="-95.25" y1="-273.685" x2="-95.25" y2="-235.585" width="0.1524" layer="91"/>
<junction x="-95.25" y="-235.585"/>
<wire x1="-78.74" y1="-235.585" x2="-95.25" y2="-235.585" width="0.1524" layer="91"/>
<label x="-87.63" y="-234.95" size="1.778" layer="95"/>
</segment>
</net>
<net name="S6_IMP" class="0">
<segment>
<wire x1="96.52" y1="-419.1" x2="96.52" y2="-403.86" width="0.1524" layer="91"/>
<wire x1="96.52" y1="-403.86" x2="118.11" y2="-403.86" width="0.1524" layer="91"/>
<wire x1="158.75" y1="-478.79" x2="118.11" y2="-478.79" width="0.1524" layer="91"/>
<wire x1="118.11" y1="-478.79" x2="118.11" y2="-403.86" width="0.1524" layer="91"/>
<wire x1="118.11" y1="-403.86" x2="158.75" y2="-403.86" width="0.1524" layer="91"/>
<wire x1="96.52" y1="-403.86" x2="12.7" y2="-403.86" width="0.1524" layer="91"/>
<junction x="118.11" y="-403.86"/>
<junction x="96.52" y="-403.86"/>
<pinref part="R127" gate="G$1" pin="2"/>
<pinref part="U7" gate="A" pin="S14"/>
<pinref part="U5" gate="A" pin="S14"/>
<label x="12.7" y="-403.86" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U9" gate="A" pin="OUT_A"/>
<wire x1="-100.33" y1="-233.045" x2="-97.79" y2="-233.045" width="0.1524" layer="91"/>
<pinref part="R74" gate="G$1" pin="2"/>
<pinref part="U9" gate="A" pin="-IN_A"/>
<wire x1="-135.89" y1="-238.125" x2="-165.735" y2="-238.125" width="0.1524" layer="91"/>
<wire x1="-165.735" y1="-238.125" x2="-184.15" y2="-238.125" width="0.1524" layer="91"/>
<junction x="-165.735" y="-238.125"/>
<wire x1="-165.735" y1="-238.125" x2="-165.735" y2="-271.145" width="0.1524" layer="91"/>
<wire x1="-165.735" y1="-271.145" x2="-97.79" y2="-271.145" width="0.1524" layer="91"/>
<wire x1="-97.79" y1="-271.145" x2="-97.79" y2="-233.045" width="0.1524" layer="91"/>
<junction x="-97.79" y="-233.045"/>
<wire x1="-97.79" y1="-233.045" x2="-78.74" y2="-233.045" width="0.1524" layer="91"/>
<label x="-87.63" y="-232.41" size="1.778" layer="95"/>
</segment>
</net>
<net name="S1_IMP" class="0">
<segment>
<wire x1="101.6" y1="-411.48" x2="101.6" y2="-406.4" width="0.1524" layer="91"/>
<wire x1="115.57" y1="-406.4" x2="101.6" y2="-406.4" width="0.1524" layer="91"/>
<wire x1="158.75" y1="-481.33" x2="115.57" y2="-481.33" width="0.1524" layer="91"/>
<wire x1="115.57" y1="-481.33" x2="115.57" y2="-406.4" width="0.1524" layer="91"/>
<wire x1="158.75" y1="-406.4" x2="115.57" y2="-406.4" width="0.1524" layer="91"/>
<junction x="115.57" y="-406.4"/>
<junction x="101.6" y="-406.4"/>
<pinref part="R128" gate="G$1" pin="2"/>
<pinref part="U7" gate="A" pin="S15"/>
<pinref part="U5" gate="A" pin="S15"/>
<wire x1="101.6" y1="-406.4" x2="12.7" y2="-406.4" width="0.1524" layer="91"/>
<label x="12.7" y="-406.4" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U8" gate="A" pin="OUT_B"/>
<wire x1="-93.98" y1="-163.83" x2="-99.06" y2="-163.83" width="0.1524" layer="91"/>
<junction x="-93.98" y="-163.83"/>
<wire x1="-77.47" y1="-163.83" x2="-93.98" y2="-163.83" width="0.1524" layer="91"/>
<pinref part="U8" gate="A" pin="-IN_B"/>
<wire x1="-134.62" y1="-171.45" x2="-161.925" y2="-171.45" width="0.1524" layer="91"/>
<pinref part="R73" gate="G$1" pin="2"/>
<wire x1="-182.245" y1="-175.895" x2="-179.705" y2="-175.895" width="0.1524" layer="91"/>
<wire x1="-179.705" y1="-171.45" x2="-179.705" y2="-175.895" width="0.1524" layer="91"/>
<wire x1="-179.705" y1="-171.45" x2="-161.925" y2="-171.45" width="0.1524" layer="91"/>
<junction x="-161.925" y="-171.45"/>
<wire x1="-161.925" y1="-171.45" x2="-161.925" y2="-201.93" width="0.1524" layer="91"/>
<wire x1="-161.925" y1="-201.93" x2="-93.98" y2="-201.93" width="0.1524" layer="91"/>
<wire x1="-93.98" y1="-201.93" x2="-93.98" y2="-163.83" width="0.1524" layer="91"/>
<label x="-87.63" y="-163.83" size="1.778" layer="95"/>
</segment>
</net>
<net name="S2_IMP" class="0">
<segment>
<wire x1="106.68" y1="-419.1" x2="106.68" y2="-408.94" width="0.1524" layer="91"/>
<wire x1="106.68" y1="-408.94" x2="113.03" y2="-408.94" width="0.1524" layer="91"/>
<wire x1="158.75" y1="-483.87" x2="113.03" y2="-483.87" width="0.1524" layer="91"/>
<wire x1="113.03" y1="-483.87" x2="113.03" y2="-408.94" width="0.1524" layer="91"/>
<wire x1="113.03" y1="-408.94" x2="158.75" y2="-408.94" width="0.1524" layer="91"/>
<junction x="113.03" y="-408.94"/>
<junction x="106.68" y="-408.94"/>
<pinref part="R129" gate="G$1" pin="2"/>
<pinref part="U7" gate="A" pin="S16"/>
<pinref part="U5" gate="A" pin="S16"/>
<wire x1="106.68" y1="-408.94" x2="12.7" y2="-408.94" width="0.1524" layer="91"/>
<label x="12.7" y="-408.94" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U8" gate="A" pin="OUT_A"/>
<wire x1="-99.06" y1="-161.29" x2="-96.52" y2="-161.29" width="0.1524" layer="91"/>
<junction x="-96.52" y="-161.29"/>
<wire x1="-77.47" y1="-161.29" x2="-96.52" y2="-161.29" width="0.1524" layer="91"/>
<pinref part="R72" gate="G$1" pin="2"/>
<pinref part="U8" gate="A" pin="-IN_A"/>
<wire x1="-134.62" y1="-166.37" x2="-164.465" y2="-166.37" width="0.1524" layer="91"/>
<wire x1="-164.465" y1="-166.37" x2="-182.88" y2="-166.37" width="0.1524" layer="91"/>
<junction x="-164.465" y="-166.37"/>
<wire x1="-164.465" y1="-166.37" x2="-164.465" y2="-199.39" width="0.1524" layer="91"/>
<wire x1="-164.465" y1="-199.39" x2="-96.52" y2="-199.39" width="0.1524" layer="91"/>
<wire x1="-96.52" y1="-199.39" x2="-96.52" y2="-161.29" width="0.1524" layer="91"/>
<label x="-87.63" y="-161.29" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$71" class="0">
<segment>
<wire x1="-372.745" y1="-574.04" x2="-372.745" y2="-570.865" width="0.1524" layer="91"/>
<wire x1="-372.745" y1="-570.865" x2="-390.525" y2="-570.865" width="0.1524" layer="91"/>
<wire x1="-368.935" y1="-570.865" x2="-372.745" y2="-570.865" width="0.1524" layer="91"/>
<junction x="-372.745" y="-570.865"/>
<pinref part="C12" gate="G$1" pin="1"/>
<pinref part="U2" gate="A" pin="VOUT"/>
<pinref part="R15" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$73" class="0">
<segment>
<wire x1="-496.57" y1="-236.22" x2="-507.365" y2="-236.22" width="0.1524" layer="91"/>
<wire x1="-496.57" y1="-270.51" x2="-496.57" y2="-236.22" width="0.1524" layer="91"/>
<wire x1="-473.71" y1="-236.22" x2="-496.57" y2="-236.22" width="0.1524" layer="91"/>
<junction x="-507.365" y="-236.22"/>
<junction x="-496.57" y="-236.22"/>
<pinref part="S14" gate="G$1" pin="SHIELD"/>
<pinref part="R107" gate="G$1" pin="2"/>
<pinref part="R78" gate="G$1" pin="1"/>
<pinref part="S14" gate="G$1" pin="P$1"/>
<pinref part="S14" gate="G$1" pin="P$2"/>
<pinref part="S14" gate="G$1" pin="P$3"/>
</segment>
</net>
<net name="N$79" class="0">
<segment>
<wire x1="-489.585" y1="-180.34" x2="-473.075" y2="-180.34" width="0.1524" layer="91"/>
<wire x1="-489.585" y1="-205.105" x2="-489.585" y2="-180.34" width="0.1524" layer="91"/>
<wire x1="-507.365" y1="-180.34" x2="-489.585" y2="-180.34" width="0.1524" layer="91"/>
<junction x="-489.585" y="-180.34"/>
<junction x="-507.365" y="-180.34"/>
<pinref part="R77" gate="G$1" pin="1"/>
<pinref part="R67" gate="G$1" pin="2"/>
<pinref part="S9" gate="G$1" pin="SHIELD"/>
<pinref part="S9" gate="G$1" pin="P$1"/>
<pinref part="S9" gate="G$1" pin="P$2"/>
<pinref part="S9" gate="G$1" pin="P$3"/>
</segment>
</net>
<net name="N$83" class="0">
<segment>
<wire x1="-475.615" y1="-199.39" x2="-473.075" y2="-199.39" width="0.1524" layer="91"/>
<wire x1="-475.615" y1="-205.105" x2="-475.615" y2="-199.39" width="0.1524" layer="91"/>
<wire x1="-507.365" y1="-199.39" x2="-475.615" y2="-199.39" width="0.1524" layer="91"/>
<junction x="-475.615" y="-199.39"/>
<junction x="-507.365" y="-199.39"/>
<pinref part="R88" gate="G$1" pin="1"/>
<pinref part="R89" gate="G$1" pin="2"/>
<pinref part="S11" gate="G$1" pin="SHIELD"/>
<pinref part="S11" gate="G$1" pin="P$1"/>
<pinref part="S11" gate="G$1" pin="P$2"/>
<pinref part="S11" gate="G$1" pin="P$3"/>
</segment>
</net>
<net name="N$86" class="0">
<segment>
<wire x1="-482.6" y1="-189.865" x2="-473.075" y2="-189.865" width="0.1524" layer="91"/>
<wire x1="-482.6" y1="-205.105" x2="-482.6" y2="-189.865" width="0.1524" layer="91"/>
<wire x1="-507.365" y1="-189.865" x2="-482.6" y2="-189.865" width="0.1524" layer="91"/>
<junction x="-482.6" y="-189.865"/>
<junction x="-507.365" y="-189.865"/>
<pinref part="R90" gate="G$1" pin="1"/>
<pinref part="R91" gate="G$1" pin="2"/>
<pinref part="S12" gate="G$1" pin="SHIELD"/>
<pinref part="S12" gate="G$1" pin="P$1"/>
<pinref part="S12" gate="G$1" pin="P$2"/>
<pinref part="S12" gate="G$1" pin="P$3"/>
</segment>
</net>
<net name="N$88" class="0">
<segment>
<wire x1="-415.29" y1="-412.115" x2="-417.83" y2="-412.115" width="0.1524" layer="91"/>
<pinref part="R66" gate="G$1" pin="1"/>
<pinref part="R118" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$89" class="0">
<segment>
<wire x1="-415.29" y1="-409.575" x2="-417.83" y2="-409.575" width="0.1524" layer="91"/>
<pinref part="R105" gate="G$1" pin="1"/>
<pinref part="R113" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$90" class="0">
<segment>
<wire x1="-417.83" y1="-407.035" x2="-415.29" y2="-407.035" width="0.1524" layer="91"/>
<pinref part="R117" gate="G$1" pin="2"/>
<pinref part="R108" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$91" class="0">
<segment>
<wire x1="-415.29" y1="-404.495" x2="-417.83" y2="-404.495" width="0.1524" layer="91"/>
<pinref part="R106" gate="G$1" pin="1"/>
<pinref part="R114" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$92" class="0">
<segment>
<wire x1="-415.29" y1="-401.955" x2="-417.83" y2="-401.955" width="0.1524" layer="91"/>
<pinref part="R100" gate="G$1" pin="1"/>
<pinref part="R115" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$96" class="0">
<segment>
<wire x1="-415.29" y1="-399.415" x2="-417.83" y2="-399.415" width="0.1524" layer="91"/>
<pinref part="R102" gate="G$1" pin="1"/>
<pinref part="R70" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$97" class="0">
<segment>
<wire x1="-415.29" y1="-396.875" x2="-417.83" y2="-396.875" width="0.1524" layer="91"/>
<pinref part="R101" gate="G$1" pin="1"/>
<pinref part="R116" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$98" class="0">
<segment>
<wire x1="-415.29" y1="-394.335" x2="-417.83" y2="-394.335" width="0.1524" layer="91"/>
<pinref part="R103" gate="G$1" pin="1"/>
<pinref part="R68" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$99" class="0">
<segment>
<wire x1="-415.29" y1="-391.795" x2="-417.83" y2="-391.795" width="0.1524" layer="91"/>
<pinref part="R110" gate="G$1" pin="1"/>
<pinref part="R119" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$108" class="0">
<segment>
<wire x1="-415.29" y1="-389.255" x2="-417.83" y2="-389.255" width="0.1524" layer="91"/>
<pinref part="R61" gate="G$1" pin="1"/>
<pinref part="R62" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$116" class="0">
<segment>
<wire x1="-415.29" y1="-386.715" x2="-417.83" y2="-386.715" width="0.1524" layer="91"/>
<pinref part="R111" gate="G$1" pin="1"/>
<pinref part="R120" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$118" class="0">
<segment>
<wire x1="-415.29" y1="-384.175" x2="-417.83" y2="-384.175" width="0.1524" layer="91"/>
<pinref part="R112" gate="G$1" pin="1"/>
<pinref part="R124" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$120" class="0">
<segment>
<wire x1="-417.83" y1="-381.635" x2="-415.29" y2="-381.635" width="0.1524" layer="91"/>
<pinref part="R121" gate="G$1" pin="2"/>
<pinref part="R98" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$121" class="0">
<segment>
<wire x1="-415.29" y1="-379.095" x2="-417.83" y2="-379.095" width="0.1524" layer="91"/>
<pinref part="R96" gate="G$1" pin="1"/>
<pinref part="R123" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$124" class="0">
<segment>
<wire x1="-415.29" y1="-376.555" x2="-417.83" y2="-376.555" width="0.1524" layer="91"/>
<pinref part="R99" gate="G$1" pin="1"/>
<pinref part="R122" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$129" class="0">
<segment>
<wire x1="-415.29" y1="-374.015" x2="-417.83" y2="-374.015" width="0.1524" layer="91"/>
<pinref part="R97" gate="G$1" pin="1"/>
<pinref part="R63" gate="G$1" pin="2"/>
</segment>
</net>
<net name="MUX2" class="0">
<segment>
<wire x1="156.21" y1="-440.69" x2="158.75" y2="-440.69" width="0.1524" layer="91"/>
<wire x1="217.805" y1="-419.1" x2="156.21" y2="-419.1" width="0.1524" layer="91"/>
<wire x1="156.21" y1="-419.1" x2="156.21" y2="-440.69" width="0.1524" layer="91"/>
<pinref part="U7" gate="A" pin="D"/>
<label x="212.09" y="-417.83" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R56" gate="G$1" pin="2"/>
<wire x1="-203.2" y1="-375.92" x2="-212.09" y2="-375.92" width="0.1524" layer="91"/>
<pinref part="R158" gate="G$1" pin="2"/>
<wire x1="-212.09" y1="-378.46" x2="-212.09" y2="-375.92" width="0.1524" layer="91"/>
<junction x="-212.09" y="-375.92"/>
<wire x1="-212.09" y1="-375.92" x2="-225.425" y2="-375.92" width="0.1524" layer="91"/>
<label x="-223.52" y="-374.65" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<wire x1="-451.485" y1="-581.025" x2="-455.295" y2="-581.025" width="0.1524" layer="91"/>
<pinref part="U2" gate="A" pin="PG"/>
<pinref part="R7" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$32" class="0">
<segment>
<wire x1="-60.325" y1="-677.545" x2="-52.705" y2="-677.545" width="0.1524" layer="91"/>
<pinref part="ZED1" gate="-A" pin="C37"/>
</segment>
</net>
<net name="V_42_P1" class="0">
<segment>
<wire x1="112.395" y1="-620.395" x2="107.95" y2="-620.395" width="0.1524" layer="91"/>
<pinref part="R133" gate="G$1" pin="1"/>
</segment>
</net>
<net name="V_42_M1" class="0">
<segment>
<wire x1="133.985" y1="-574.675" x2="127.635" y2="-574.675" width="0.1524" layer="91"/>
<pinref part="R148" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$34" class="0">
<segment>
<wire x1="163.83" y1="-630.555" x2="160.655" y2="-630.555" width="0.1524" layer="91"/>
<pinref part="R5" gate="G$1" pin="1"/>
<pinref part="U14" gate="A" pin="PG"/>
</segment>
</net>
<net name="N$36" class="0">
<segment>
<wire x1="163.83" y1="-640.715" x2="160.655" y2="-640.715" width="0.1524" layer="91"/>
<wire x1="160.655" y1="-640.715" x2="160.655" y2="-643.89" width="0.1524" layer="91"/>
<pinref part="C71" gate="G$1" pin="1"/>
<pinref part="U14" gate="A" pin="SS"/>
</segment>
</net>
<net name="DRVDD1" class="0">
<segment>
<wire x1="239.395" y1="-620.395" x2="239.395" y2="-622.935" width="0.1524" layer="91"/>
<wire x1="239.395" y1="-620.395" x2="247.65" y2="-620.395" width="0.1524" layer="91"/>
<wire x1="239.395" y1="-620.395" x2="237.49" y2="-620.395" width="0.1524" layer="91"/>
<junction x="239.395" y="-620.395"/>
<pinref part="C64" gate="G$1" pin="1"/>
<pinref part="R141" gate="G$1" pin="2"/>
</segment>
<segment>
<wire x1="150.495" y1="-630.555" x2="144.78" y2="-630.555" width="0.1524" layer="91"/>
<pinref part="R5" gate="G$1" pin="2"/>
</segment>
<segment>
<wire x1="163.83" y1="-635.635" x2="144.78" y2="-635.635" width="0.1524" layer="91"/>
<pinref part="U14" gate="A" pin="SENSE/ADJ"/>
</segment>
</net>
<net name="AVDD_M1" class="0">
<segment>
<wire x1="173.355" y1="-579.755" x2="168.275" y2="-579.755" width="0.1524" layer="91"/>
<pinref part="R149" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$37" class="0">
<segment>
<wire x1="227.33" y1="-620.395" x2="224.79" y2="-620.395" width="0.1524" layer="91"/>
<pinref part="R141" gate="G$1" pin="1"/>
<pinref part="U14" gate="A" pin="VOUT"/>
</segment>
</net>
<net name="N$38" class="0">
<segment>
<wire x1="163.83" y1="-620.395" x2="160.655" y2="-620.395" width="0.1524" layer="91"/>
<wire x1="160.655" y1="-625.475" x2="160.655" y2="-620.395" width="0.1524" layer="91"/>
<wire x1="160.655" y1="-620.395" x2="137.795" y2="-620.395" width="0.1524" layer="91"/>
<wire x1="137.795" y1="-624.205" x2="137.795" y2="-620.395" width="0.1524" layer="91"/>
<wire x1="137.795" y1="-620.395" x2="126.365" y2="-620.395" width="0.1524" layer="91"/>
<wire x1="126.365" y1="-624.205" x2="126.365" y2="-620.395" width="0.1524" layer="91"/>
<wire x1="126.365" y1="-620.395" x2="122.555" y2="-620.395" width="0.1524" layer="91"/>
<wire x1="163.83" y1="-625.475" x2="160.655" y2="-625.475" width="0.1524" layer="91"/>
<junction x="160.655" y="-620.395"/>
<junction x="137.795" y="-620.395"/>
<junction x="126.365" y="-620.395"/>
<pinref part="U14" gate="A" pin="VIN"/>
<pinref part="C77" gate="G$1" pin="1"/>
<pinref part="C63" gate="G$1" pin="1"/>
<pinref part="R133" gate="G$1" pin="2"/>
<pinref part="U14" gate="A" pin="EN/UVLO"/>
</segment>
</net>
<net name="N$41" class="0">
<segment>
<wire x1="191.77" y1="-579.755" x2="185.42" y2="-579.755" width="0.1524" layer="91"/>
<wire x1="185.42" y1="-579.755" x2="183.515" y2="-579.755" width="0.1524" layer="91"/>
<wire x1="185.42" y1="-582.93" x2="185.42" y2="-579.755" width="0.1524" layer="91"/>
<junction x="185.42" y="-579.755"/>
<pinref part="U15" gate="A" pin="VOUT"/>
<pinref part="R149" gate="G$1" pin="2"/>
<pinref part="C73" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$43" class="0">
<segment>
<wire x1="191.77" y1="-574.675" x2="189.865" y2="-574.675" width="0.1524" layer="91"/>
<wire x1="191.77" y1="-584.835" x2="189.865" y2="-584.835" width="0.1524" layer="91"/>
<wire x1="189.865" y1="-584.835" x2="189.865" y2="-574.675" width="0.1524" layer="91"/>
<wire x1="144.145" y1="-574.675" x2="147.32" y2="-574.675" width="0.1524" layer="91"/>
<wire x1="147.32" y1="-574.675" x2="147.32" y2="-579.755" width="0.1524" layer="91"/>
<wire x1="147.32" y1="-574.675" x2="162.56" y2="-574.675" width="0.1524" layer="91"/>
<wire x1="162.56" y1="-574.675" x2="162.56" y2="-582.295" width="0.1524" layer="91"/>
<wire x1="189.865" y1="-574.675" x2="162.56" y2="-574.675" width="0.1524" layer="91"/>
<junction x="147.32" y="-574.675"/>
<junction x="189.865" y="-574.675"/>
<junction x="162.56" y="-574.675"/>
<pinref part="U15" gate="A" pin="VIN"/>
<pinref part="U15" gate="A" pin="\EN"/>
<pinref part="R148" gate="G$1" pin="2"/>
<pinref part="C72" gate="G$1" pin="1"/>
<pinref part="C78" gate="G$1" pin="1"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>
